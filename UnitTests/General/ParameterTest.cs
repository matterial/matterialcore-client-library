﻿using MatterialCore;
using MatterialCore.Enums;
using MatterialCore.Params;
using MatterialCore.Params.Doc;
using MatterialCore.Params.Person;

using NUnit.Framework;

using System.Collections.Generic;
using System.Diagnostics;



namespace MatterialCore.Tests.Unit
{
    internal class ParameterTest : TestBase
    {
        #region Setup

        [OneTimeSetUp] //once per class
        public void Setup()
        {
        }

        [OneTimeTearDown] //once per class
        public void TearDown()
        {
        }

        [SetUp] //once per test
        public void SetupTest()
        {
        }

        [TearDown] //once per test
        public void TearDownTest()
        {
        }

        #endregion Setup

        //private void CheckUrlParams(IDebugUrlParams p, List<string> checkList, out string errMsg, bool loadAll = false)
        //{
        //    p.DebugUrlParameter = loadAll;
        //    string s = p.UrlParameter().ToString();
        //    CheckUrlParams(s, checkList, out errMsg);
        //}
        private void CheckUrlParams(string s, List<string> checkList, out string errMsg, bool debugBreak = false)
        {
            //Debug of URL parameter mode;

            List<string> paramList = new();

            if (!string.IsNullOrEmpty(s))
            {
                paramList = new List<string>(s.Split('&'));
            }
            if (Debugger.IsAttached)
            {
                string paramStr = "";
                foreach (var item in paramList)
                {
                    paramStr += "checkList.Add(\"" + item + "\");\r\n";
                }
                if (debugBreak)
                    Debugger.Break();
            }

            errMsg = "";
            foreach (var item in checkList)
            {
                if (!paramList.Contains(item))
                {
                    errMsg = "Missing url param: " + item;
                    return;
                }
            }
            foreach (var item in paramList)
            {
                if (!checkList.Contains(item))
                {
                    errMsg = "The parameter is not set:  " + item;
                    return;
                }
            }
            if (checkList.Count != paramList.Count)
            {
                errMsg = "The number of items in paramList and check list are not equal.";
                return;
            }
        }

        [Test]
        public void AttachmentLoadParams()
        {
            //AttachmentLoadParamsTest: testing properties and url params
            List<string> checkList = new();
            string errMsg;

            //default url params
            AttachmentLoadParams p = new();
            checkList.Add("size=s");
            CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);

            //All 
            p = new AttachmentLoadParams();
            checkList = new List<string>();
            checkList.Add("loadPdf=false");
            checkList.Add("forceAttachment=false");
            checkList.Add("size=s");
            p.DebugUrlParameter = true; CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);

            //Original 
            p = new AttachmentLoadParams();
            checkList = new List<string>();
            p.Format = AttachmentFormats.Original;
            p.Size = LoadSize.Small;
            checkList.Add("size=s");
            p.ForceAttachment = true;
            checkList.Add("forceAttachment=true");
            CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);
            //Large size
            p = new AttachmentLoadParams();
            checkList = new List<string>();
            p.Size = LoadSize.Large;
            checkList.Add("size=l");
            CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);

            Assert.That(errMsg, Is.Empty, errMsg);
            p = new AttachmentLoadParams();
            checkList = new List<string>();
            p.Size = LoadSize.Thumbnail;
            checkList.Add("size=t");
            CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);
        }

        [Test]
        public void AttachmentsLoadParams()
        {
            //Testing if properties generate the correct url-params
            List<string> checkList = new();
            string errMsg;
            AttachmentsLoadParams p = new(1);

            //Default (no properties set)
            checkList.Add("offset=0");
            checkList.Add("limit=" + Matterial.DefaultSearchResultLimit);
            checkList.Add("documentLanguageVersionId=1");
            CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);

            //All params
            p = new AttachmentsLoadParams(1);
            checkList = new List<string>();
            checkList.Add("languageKey=en");
            checkList.Add("offset=0");
            checkList.Add("limit=20");
            checkList.Add("attachmentOrderBy=name");
            checkList.Add("orderDir=asc");
            checkList.Add("documentLanguageVersionId=1");
            p.DebugUrlParameter = true; CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);

            //More properties set
            p = new AttachmentsLoadParams(1);
            checkList = new List<string>();
            p.Offset = 1;
            checkList.Add("offset=1");
            p.Limit = 11;
            checkList.Add("limit=11");
            p.LanguageKey = LangKey.es;
            checkList.Add("languageKey=es");
            p.DocumentLanguageVersionId = 1;
            checkList.Add("documentLanguageVersionId=1");
            p.OrderBy = AttachmentsOrderBy.CreateTime;
            checkList.Add("attachmentOrderBy=" + p.OrderBy.KeyAndValue().Value);
            p.OrderDir = OrderDirection.desc;
            checkList.Add("orderDir=" + p.OrderDir.KeyAndValue().Value);

            CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);
        }

        [Test]
        public void ChangeLogMapParams()
        {
            //testing properties and url params
            List<string> checkList = new();
            string errMsg;

            //default url params
            ChangeLogMapParams p = new();
            checkList = new List<string>();
            checkList.Add("offset=0");
            checkList.Add("limit=" + Matterial.DefaultSearchResultLimit);
            CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);

            //All params
            p = new ChangeLogMapParams();
            checkList = new List<string>();
            checkList.Add("offset=0");
            checkList.Add("limit=20");
            checkList.Add("documentId=0");
            checkList.Add("documentLanguageVersionId=0");
            checkList.Add("contactId=0");
            checkList.Add("count=false");
            p.DebugUrlParameter = true; CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);

            p = new ChangeLogMapParams(55);
            checkList = new List<string>();
            checkList.Add("documentId=55");
            p.DocumentLanguageVersionId = 99;
            checkList.Add("documentLanguageVersionId=99");
            p.ContactId = 77;
            checkList.Add("contactId=77");
            p.Count = true;
            checkList.Add("count=true");
            p.Offset = 1;
            checkList.Add("offset=1");
            p.Limit = 77;
            checkList.Add("limit=77");
            CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);
        }

        [Test]
        public void ChangeLogParams()
        {
            // testing properties and url params
            List<string> checkList = new();
            string errMsg;

            //default url params
            ChangeLogParams p = new();
            checkList = new List<string>();
            checkList.Add("offset=0");
            checkList.Add("limit=" + Matterial.DefaultSearchResultLimit);
            CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);

            //All params
            p = new ChangeLogParams(55);
            checkList = new List<string>();
            checkList.Add("offset=0");
            checkList.Add("limit=" + Matterial.DefaultSearchResultLimit);
            checkList.Add("documentId=55");
            checkList.Add("documentLanguageVersionId=0");
            checkList.Add("contactId=0");
            checkList.Add("count=false");
            checkList.Add("orderDir=asc");
            checkList.Add("orderBy=date");
            p.DebugUrlParameter = true; CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);

            p = new ChangeLogParams(55);
            checkList = new List<string>();
            checkList.Add("documentId=55");
            p.DocumentLanguageVersionId = 99;
            checkList.Add("documentLanguageVersionId=99");
            p.ContactId = 77;
            checkList.Add("contactId=77");
            p.Count = true;
            checkList.Add("count=true");
            p.OrderBy = DocChangeLogOrderBy.Id;
            checkList.Add("orderBy=id");
            p.OrderDir = OrderDirection.desc;
            checkList.Add("orderDir=desc");
            p.Offset = 1;
            checkList.Add("offset=1");
            p.Limit = 77;
            checkList.Add("limit=77");
            CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);
        }

        [Test]
        public void DocAutocompleteParams()
        {
            List<string> checkList = new();
            string errMsg;
            DocAutocompleteParams p = new();

            //Default test (no properties set)
            p = new MatterialCore.Params.Doc.DocAutocompleteParams();
            checkList = new List<string>();
            CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);

            //check all properties expkicit
            p = new MatterialCore.Params.Doc.DocAutocompleteParams();
            checkList = new List<string>();
            checkList.Add("languageKey=en");
            checkList.Add("showBios=false");
            checkList.Add("showInvalid=false");
            checkList.Add("hideSnap=false");
            checkList.Add("showTemplates=false");
            checkList.Add("showArchived=false");
            checkList.Add("hideLanguageVersionReviewed=false");
            checkList.Add("hideLanguageVersionReady=false");
            checkList.Add("hideLanguageVersionCurrentlyInProcessing=false");
            checkList.Add("hideLanguageVersionReviewRequested=false");
            checkList.Add("hideLandscape=false");
            checkList.Add("categoryIdsAnd=0");
            checkList.Add("categoryIdsOr=0");
            checkList.Add("roleRightRoleId=0");
            checkList.Add("roleRightType=0");
            checkList.Add("read=");
            checkList.Add("markedHelpfulByContactId=0");
            checkList.Add("responsible=false");
            checkList.Add("responsibleContactId=0");
            checkList.Add("authorship=false");
            checkList.Add("authorshipAccountId=0");
            checkList.Add("additionalPropertyId=0");
            checkList.Add("additionalPropertyType=-1");
            checkList.Add("lastChangedSince=0");
            checkList.Add("lastChangedSinceDays=0");
            checkList.Add("following=false");
            checkList.Add("followerAccountId=0");
            checkList.Add("showRemoved=false");
            checkList.Add("allLanguages=false");
            p.DebugUrlParameter = true; CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);

            //Follower test
            p = new MatterialCore.Params.Doc.DocAutocompleteParams();
            checkList = new List<string>();
            p.FollowerAccountId = 10;
            checkList.Add("followerAccountId=10");
            p.ResponsibleContactId = 44;
            checkList.Add("responsibleContactId=44");
            p.AuthorshipAccountId = 22;
            checkList.Add("authorshipAccountId=22");
            CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);

            //Last changed test
            p = new MatterialCore.Params.Doc.DocAutocompleteParams();
            checkList = new List<string>();
            p.LastChangedSinceDays = 10;
            checkList.Add("lastChangedSinceDays=10");
            //p.LastChangedSince = Matterial.GetUnixTimestamp();
            //checkList.Add("lastChangedSince=" + Matterial.GetUnixTimestamp());
            CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);

            p = new MatterialCore.Params.Doc.DocAutocompleteParams();
            checkList = new List<string>();
            p.LanguageKey = LangKey.es;
            checkList.Add("languageKey=es");
            p.CategoryAndIds = new List<long> { 1, 2, 3 };
            checkList.Add("categoryIdsAnd=1");
            checkList.Add("categoryIdsAnd=2");
            checkList.Add("categoryIdsAnd=3");
            p.CategoryOrIds = new List<long> { 5, 6, 7 };
            checkList.Add("categoryIdsOr=5");
            checkList.Add("categoryIdsOr=6");
            checkList.Add("categoryIdsOr=7");
            p.RoleRightRoleId = 4;
            checkList.Add("roleRightRoleId=4");
            p.RoleRightType = RoleRightFilterTypes.EDIT;
            checkList.Add("roleRightType=" + ((int)RoleRightFilterTypes.EDIT).ToString());
            p.ReadStatus = ReadStatuses.Unread;
            checkList.Add("read=false");
            p.Invalid = ParamShowValue.Exclusive;
            checkList.Add("showInvalidOnly=true");
            p.Bios = ParamShowValue.Exclusive;
            checkList.Add("showBiosOnly=true");
            p.Templates = ParamShowValue.Exclusive;
            checkList.Add("showTemplatesOnly=true");
            p.Archived = ParamShowValue.Include;
            checkList.Add("showArchived=true");
            p.Removed = ParamShowValue.Include;
            checkList.Add("showRemoved=true");
            p.Landscape = ParamHideValue.Exclusive;
            checkList.Add("showLandscapeOnly=true");
            //p.FollowerAccountId = 10;
            //checkList.Add("followerAccountId=10");
            p.Following = true;
            checkList.Add("following=true");
            p.MarkedHelpfulByContactId = 12;
            checkList.Add("markedHelpfulByContactId=12");
            //p.LastChangedSinceDays = 10;
            //checkList.Add("lastChangedSinceDays=10");
            p.LastChangedSince = Matterial.UnixTimestamp();
            checkList.Add("lastChangedSince=" + Matterial.UnixTimestamp());
            //p.ResponsibleContactId = 44;
            //checkList.Add("responsibleContactId=44");
            p.Responsible = true;
            checkList.Add("responsible=true");
            //p.AuthorshipAccountId = 22;
            //checkList.Add("authorshipAccountId=22");
            p.Authorship = true;
            checkList.Add("authorship=true");
            p.Reviewed = ParamHideValue.Exclusive;
            checkList.Add("showLanguageVersionReviewedOnly=true");
            p.Published = ParamHideValue.Exclusive;
            checkList.Add("showLanguageVersionReadyOnly=true");
            p.InProcessing = ParamHideValue.Exclude;
            checkList.Add("hideLanguageVersionCurrentlyInProcessing=true");
            p.ReviewRequested = ParamHideValue.Exclusive;
            checkList.Add("showLanguageVersionReviewRequestedOnly=true");
            p.HasCategoryAssigned = CategoryAssignedValue.Assigned;
            checkList.Add("showWithCategoriesOnly=true");
            p.AdditionalPropertyId = 55;
            checkList.Add("additionalPropertyId=55");
            p.AdditionalPropertyType = AdditionalPropertyLoadTypes.News;
            checkList.Add("additionalPropertyType=" + ((int)AdditionalPropertyLoadTypes.News).ToString());
            p.Snaps = ParamHideValue.Exclusive;
            checkList.Add("showSnapOnly=true");
            p.AllLanguages = true;
            checkList.Add("allLanguages=true");
            CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);
        }

        [Test]
        public void DocInfoLoadParams()
        {
            List<string> checkList = new();
            string errMsg;
            DocInfoLoadParams p = new();

            //Default params (no properties set)
            CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);

            //Check all parameters
            checkList = new List<string>();
            p = new MatterialCore.Params.Doc.DocInfoLoadParams();
            checkList.Add("loadRoleRights=false");
            checkList.Add("loadCategoriesPublicOnly=false");
            checkList.Add("loadCategories=false");
            checkList.Add("loadResponsibles=false");
            checkList.Add("loadAuthors=false");
            checkList.Add("loadLastAuthorOnly=false");
            checkList.Add("loadFollowers=false");
            checkList.Add("loadAmIFollowing=false");
            checkList.Add("loadMarkedAsHelpfulBy=false");
            checkList.Add("loadAttachments=false");
            checkList.Add("loadLanguageAttachments=false");
            checkList.Add("loadDocumentAttachments=false");
            checkList.Add("loadRelatedDocumentIds=false");
            checkList.Add("loadAdditionalProperties=false");
            checkList.Add("loadExtensionValues=false");
            checkList.Add("loadSnapFlag=false");
            checkList.Add("loadComments=false");
            checkList.Add("attachmentOrderBy=name");
            checkList.Add("attachmentOrderDir=asc");
            p.DebugUrlParameter = true; CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);

            //Some more options set
            p = new MatterialCore.Params.Doc.DocInfoLoadParams();
            checkList = new List<string>();
            p.RoleRights = true;
            checkList.Add("loadRoleRights=true");
            p.CategoriesPublicOnly = true;
            checkList.Add("loadCategoriesPublicOnly=true");
            p.Categories = true;
            checkList.Add("loadCategories=true");
            p.Responsibles = true;
            checkList.Add("loadResponsibles=true");
            p.Authors = true;
            checkList.Add("loadAuthors=true");
            p.LastAuthorOnly = true;
            checkList.Add("loadLastAuthorOnly=true");
            p.Followers = true;
            checkList.Add("loadFollowers=true");
            p.AmIFollowing = true;
            checkList.Add("loadAmIFollowing=true");
            p.MarkedAsHelpfulBy = true;
            checkList.Add("loadMarkedAsHelpfulBy=true");
            p.Attachments = true;
            checkList.Add("loadAttachments=true");
            p.LanguageAttachments = true;
            checkList.Add("loadLanguageAttachments=true");
            p.DocumentAttachments = true;
            checkList.Add("loadDocumentAttachments=true");
            p.RelatedDocumentIds = true;
            checkList.Add("loadRelatedDocumentIds=true");
            p.AdditionalProperties = true;
            checkList.Add("loadAdditionalProperties=true");
            p.ExtensionValues = true;
            checkList.Add("loadExtensionValues=true");
            p.SnapFlag = true;
            checkList.Add("loadSnapFlag=true");
            p.Comments = true;
            checkList.Add("loadComments=true");
            p.AttachmentOrderBy = AttachmentsOrderBy.Id;
            checkList.Add("attachmentOrderBy=id");
            p.AttachmentOrderDir = OrderDirection.desc;
            checkList.Add("attachmentOrderDir=desc");
            CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);
        }

        [Test]
        public void DocScopeParams()
        {
            List<string> checkList = new();
            string errMsg;
            DocScopeParams p = new();

            //Check default (no properties set)
            CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);

            //check all properties expkicit
            p = new DocScopeParams();
            checkList = new List<string>();
            checkList.Add("languageKey=en");
            checkList.Add("showBios=false");
            checkList.Add("showInvalid=false");
            checkList.Add("hideSnap=false");
            checkList.Add("showTemplates=false");
            checkList.Add("showArchived=false");
            checkList.Add("hideLanguageVersionReviewed=false");
            checkList.Add("hideLanguageVersionReady=false");
            checkList.Add("hideLanguageVersionCurrentlyInProcessing=false");
            checkList.Add("hideLanguageVersionReviewRequested=false");
            checkList.Add("hideLandscape=false");
            p.DebugUrlParameter = true; CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);

            //Some more properties set
            p = new DocScopeParams();
            checkList = new List<string>();
            p.LanguageKey = LangKey.es;
            checkList.Add("languageKey=es");
            p.Invalid = ParamShowValue.Exclusive;
            checkList.Add("showInvalidOnly=true");
            p.Bios = ParamShowValue.Exclusive;
            checkList.Add("showBiosOnly=true");
            p.Templates = ParamShowValue.Exclusive;
            checkList.Add("showTemplatesOnly=true");
            p.Archived = ParamShowValue.Include;
            checkList.Add("showArchived=true");
            p.Landscape = ParamHideValue.Exclusive;
            checkList.Add("showLandscapeOnly=true");
            p.Reviewed = ParamHideValue.Exclusive;
            checkList.Add("showLanguageVersionReviewedOnly=true");
            p.Published = ParamHideValue.Exclusive;
            checkList.Add("showLanguageVersionReadyOnly=true");
            p.InProcessing = ParamHideValue.Exclude;
            checkList.Add("hideLanguageVersionCurrentlyInProcessing=true");
            p.ReviewRequested = ParamHideValue.Exclusive;
            checkList.Add("showLanguageVersionReviewRequestedOnly=true");
            p.Snaps = ParamHideValue.Exclusive;
            checkList.Add("showSnapOnly=true");

            CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);
        }

        [Test]
        public void DocUpdateParams()
        {
            List<string> checkList = new();
            string errMsg;
            DocUpdateParams p = new();

            //Default test (no properties set)
            p = new DocUpdateParams();
            checkList = new List<string>();
            CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);

            //all params explicit
            p = new DocUpdateParams();
            checkList = new List<string>();
            checkList.Add("contextToken=xxxx");
            checkList.Add("ignoreCategories=false");
            checkList.Add("ignoreRelatedDocumentIds=false");
            checkList.Add("ignoreExtensionValues=false");
            checkList.Add("ignoreAttachments=false");
            checkList.Add("ignoreRoleRights=false");
            checkList.Add("ignoreResponsibles=false");
            checkList.Add("uniqueLockId=xxxx");
            checkList.Add("publishRequest=None");
            checkList.Add("validBeginInSecondsRequest=0");
            p.DebugUrlParameter = true; CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg, true);
            Assert.That(errMsg, Is.Empty, errMsg);

            //set some properties
            p = new DocUpdateParams();
            checkList = new List<string>();
            p.ContextToken = "test";
            checkList.Add("contextToken=test");
            p.UniqueLockId = "aaa";
            checkList.Add("uniqueLockId=aaa");
            p.PublishRequestType = PublishRequestTypes.Reviewed;
            checkList.Add("publishRequest=reviewed");
            p.ValidBeginInSeconds = 12345;
            checkList.Add("validBeginInSecondsRequest=12345");
            p.IgnoreCategories = true;
            checkList.Add("ignoreCategories=true");
            p.IgnoreRelatedDocumentIds = true;
            checkList.Add("ignoreRelatedDocumentIds=true");
            p.IgnoreExtensionValues = true;
            checkList.Add("ignoreExtensionValues=true");
            p.IgnoreAttachments = true;
            checkList.Add("ignoreAttachments=true");
            p.IgnoreRoleRights = true;
            checkList.Add("ignoreRoleRights=true");
            p.IgnoreResponsibles = true;
            checkList.Add("ignoreResponsibles=true");
            CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);
        }

        [Test]
        public void HighlightFields()
        {
            //testing properties and url params
            List<string> checkList = new();
            string errMsg;

            //default url params
            HighlightFields p = new();
            checkList = new List<string>();
            CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);

            //all
            p = new HighlightFields();
            checkList = new List<string>();
            checkList.Add("highlightField=field");
            p.DebugUrlParameter = true; CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);

            p = new HighlightFields();
            checkList = new List<string>();
            p.Add("Field1");
            p.Add("Field2");
            checkList.Add("highlightField=Field1");
            checkList.Add("highlightField=Field2");
            CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);
        }

        [Test]
        public void LoadDocByIdParams()
        {
            //Testing if properties generate the correct url-params
            List<string> checkList = new();
            string errMsg;
            LoadDocByIdParams p = new();
            //Default (no properties set)
            checkList.Add("languageKey=en");
            CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);

            //All params
            p = new LoadDocByIdParams();
            checkList = new List<string>();
            checkList.Add("languageKey=en");
            checkList.Add("updateReadTime=en");
            checkList.Add("attachmentOrderBy=name");
            checkList.Add("attachmentOrderDir=asc");
            checkList.Add("languagePrefer=false");
            checkList.Add("languageExclude=false");
            checkList.Add("loadRoleRights=false");
            checkList.Add("loadCategoriesPublicOnly=false");
            checkList.Add("loadCategories=false");
            checkList.Add("loadResponsibles=false");
            checkList.Add("loadAuthors=false");
            checkList.Add("loadLastAuthorOnly=false");
            checkList.Add("loadFollowers=false");
            checkList.Add("loadAmIFollowing=false");
            checkList.Add("loadMarkedAsHelpfulBy=false");
            checkList.Add("loadAttachments=false");
            checkList.Add("loadLanguageAttachments=false");
            checkList.Add("loadDocumentAttachments=false");
            checkList.Add("loadRelatedDocumentIds=false");
            checkList.Add("loadAdditionalProperties=false");
            checkList.Add("loadExtensionValues=false");
            checkList.Add("loadSnapFlag=false");
            checkList.Add("loadComments=false");

            p.DebugUrlParameter = true; CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);

            //More properties set
            p = new LoadDocByIdParams();
            checkList = new List<string>();

            p.LanguagePrefer = true;
            checkList.Add("languagePrefer=true");
            p.LanguageExclude = true;
            checkList.Add("languageExclude=true");
            p.LanguageKey = LangKey.de;
            checkList.Add("languageKey=de");
            p.UpdateReadTime = LangKey.es;
            checkList.Add("updateReadTime=es");
            p.LoadOptions.RoleRights = true;
            checkList.Add("loadRoleRights=true");
            p.LoadOptions.CategoriesPublicOnly = true;
            checkList.Add("loadCategoriesPublicOnly=true");
            p.LoadOptions.Categories = true;
            checkList.Add("loadCategories=true");
            p.LoadOptions.Responsibles = true;
            checkList.Add("loadResponsibles=true");
            p.LoadOptions.Authors = true;
            checkList.Add("loadAuthors=true");
            p.LoadOptions.LastAuthorOnly = true;
            checkList.Add("loadLastAuthorOnly=true");
            p.LoadOptions.Followers = true;
            checkList.Add("loadFollowers=true");
            p.LoadOptions.AmIFollowing = true;
            checkList.Add("loadAmIFollowing=true");
            p.LoadOptions.MarkedAsHelpfulBy = true;
            checkList.Add("loadMarkedAsHelpfulBy=true");
            p.LoadOptions.AttachmentOrderBy = AttachmentsOrderBy.CreateTime;
            checkList.Add("attachmentOrderBy=createtime");
            p.LoadOptions.AttachmentOrderDir = OrderDirection.desc;
            checkList.Add("attachmentOrderDir=desc");
            p.LoadOptions.Attachments = true;
            checkList.Add("loadAttachments=true");
            p.LoadOptions.LanguageAttachments = true;
            checkList.Add("loadLanguageAttachments=true");
            p.LoadOptions.DocumentAttachments = true;
            checkList.Add("loadDocumentAttachments=true");
            p.LoadOptions.RelatedDocumentIds = true;
            checkList.Add("loadRelatedDocumentIds=true");
            p.LoadOptions.AdditionalProperties = true;
            checkList.Add("loadAdditionalProperties=true");
            p.LoadOptions.ExtensionValues = true;
            checkList.Add("loadExtensionValues=true");
            p.LoadOptions.SnapFlag = true;
            checkList.Add("loadSnapFlag=true");
            p.LoadOptions.Comments = true;
            checkList.Add("loadComments=true");
            CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);
        }

        [Test]
        public void LoadDocParams()
        {
            //Testing if properties generate the correct url-params
            List<string> checkList = new();
            string errMsg;
            LoadDocParams p = new();

            //Default (no properties set)
            checkList.Add("offset=0");
            checkList.Add("limit=" + Matterial.DefaultSearchResultLimit);
            checkList.Add("languageKey=en");
            CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);

            //All params
            p = new LoadDocParams();
            checkList = new List<string>();
            checkList.Add("languageKey=en");
            checkList.Add("showBios=false");
            checkList.Add("showInvalid=false");
            checkList.Add("hideSnap=false");
            checkList.Add("showTemplates=false");
            checkList.Add("showArchived=false");
            checkList.Add("hideLanguageVersionReviewed=false");
            checkList.Add("hideLanguageVersionReady=false");
            checkList.Add("hideLanguageVersionCurrentlyInProcessing=false");
            checkList.Add("hideLanguageVersionReviewRequested=false");
            checkList.Add("hideLandscape=false");
            checkList.Add("categoryIdsAnd=0");
            checkList.Add("categoryIdsOr=0");
            checkList.Add("roleRightRoleId=0");
            checkList.Add("roleRightType=0");
            checkList.Add("read=");
            checkList.Add("markedHelpfulByContactId=0");
            checkList.Add("responsible=false");
            checkList.Add("responsibleContactId=0");
            checkList.Add("authorship=false");
            checkList.Add("authorshipAccountId=0");
            checkList.Add("additionalPropertyId=0");
            checkList.Add("additionalPropertyType=-1");
            checkList.Add("lastChangedSince=0");
            checkList.Add("lastChangedSinceDays=0");
            checkList.Add("following=false");
            checkList.Add("followerAccountId=0");
            checkList.Add("offset=0");
            checkList.Add("limit=20");
            checkList.Add("allLanguages=false");
            checkList.Add("languagePrefer=false");
            checkList.Add("languageExclude=false");
            checkList.Add("orderBy=languageVersionTitle");
            checkList.Add("orderDir=asc");
            checkList.Add("relatedDocumentId=0");
            checkList.Add("categoryFollowerAccountId=0");
            checkList.Add("categoryFollowing=false");
            checkList.Add("additionalPropertyFollowerAccountId=0");
            checkList.Add("additionalPropertyFollowing=false");
            checkList.Add("documentExpiresInDays=-1");
            checkList.Add("documentExpiresOn=0");
            checkList.Add("documentExpires=false");
            checkList.Add("documentHasArchivedBegin=false");
            checkList.Add("mimeTypeId=-1");
            checkList.Add("mimeType=");
            checkList.Add("mentionedAccountIdInComment=0");
            checkList.Add("mentionedAccountIdInCommentUnread=0");
            checkList.Add("attachmentOrderBy=name");
            checkList.Add("attachmentOrderDir=asc");
            checkList.Add("loadRoleRights=false");
            checkList.Add("loadCategoriesPublicOnly=false");
            checkList.Add("loadCategories=false");
            checkList.Add("loadResponsibles=false");
            checkList.Add("loadAuthors=false");
            checkList.Add("loadLastAuthorOnly=false");
            checkList.Add("loadFollowers=false");
            checkList.Add("loadAmIFollowing=false");
            checkList.Add("loadMarkedAsHelpfulBy=false");
            checkList.Add("loadAttachments=false");
            checkList.Add("loadLanguageAttachments=false");
            checkList.Add("loadDocumentAttachments=false");
            checkList.Add("loadRelatedDocumentIds=false");
            checkList.Add("loadAdditionalProperties=false");
            checkList.Add("loadExtensionValues=false");
            checkList.Add("loadSnapFlag=false");
            checkList.Add("loadComments=false");
            checkList.Add("count=false");
            checkList.Add("documentId=0");
            checkList.Add("documentLanguageVersionId=0");
            checkList.Add("allVersions=false");
            checkList.Add("clipBoard=false");
            checkList.Add("updateReadTime=false");
            checkList.Add("showRemoved=false");

            p.DebugUrlParameter = true; CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);

            //Check follower and lastchanged and expires
            checkList = new List<string>();
            p = new LoadDocParams();
            checkList.Add("offset=0");
            checkList.Add("limit=" + Matterial.DefaultSearchResultLimit);
            checkList.Add("languageKey=en");
            p.FollowerAccountId = 10;
            checkList.Add("followerAccountId=10");
            p.CategoryFollowerAccountId = 99;
            checkList.Add("categoryFollowerAccountId=99");
            p.AdditionalPropertyFollowerAccountId = 77;
            checkList.Add("additionalPropertyFollowerAccountId=77");
            p.LastChangedSinceDays = 10;
            checkList.Add("lastChangedSinceDays=10");
            p.ResponsibleContactId = 44;
            checkList.Add("responsibleContactId=44");
            p.AuthorshipAccountId = 22;
            checkList.Add("authorshipAccountId=22");
            p.ExpiresInDays = 2;
            checkList.Add("documentExpiresInDays=2");
            CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);

            //Now set properties
            checkList = new List<string>();
            p = new LoadDocParams();
            p.AllLanguages = true;
            checkList.Add("allLanguages=true");
            p.LanguagePrefer = true;
            checkList.Add("languagePrefer=true");
            p.LanguageExclude = true;
            checkList.Add("languageExclude=true");
            p.LanguageKey = LangKey.es;
            checkList.Add("languageKey=es");
            p.CategoryAndIds = new List<long> { 1, 2, 3 };
            checkList.Add("categoryIdsAnd=1");
            checkList.Add("categoryIdsAnd=2");
            checkList.Add("categoryIdsAnd=3");
            p.CategoryOrIds = new List<long> { 5, 6, 7 };
            checkList.Add("categoryIdsOr=5");
            checkList.Add("categoryIdsOr=6");
            checkList.Add("categoryIdsOr=7");
            p.RoleRightRoleId = 4;
            checkList.Add("roleRightRoleId=4");
            p.RoleRightType = RoleRightFilterTypes.EDIT;
            checkList.Add("roleRightType=" + ((int)RoleRightFilterTypes.EDIT).ToString());
            p.ReadStatus = ReadStatuses.Unread;
            checkList.Add("read=false");
            p.RelatedDocumentId = 234;
            checkList.Add("relatedDocumentId=234");
            p.Invalid = ParamShowValue.Exclusive;
            checkList.Add("showInvalidOnly=true");
            p.Bios = ParamShowValue.Exclusive;
            checkList.Add("showBiosOnly=true");
            p.Templates = ParamShowValue.Exclusive;
            checkList.Add("showTemplatesOnly=true");
            p.Archived = ParamShowValue.Include;
            checkList.Add("showArchived=true");
            p.Landscape = ParamHideValue.Exclusive;
            checkList.Add("showLandscapeOnly=true");
            p.FollowerAccountId = 10;
            //checkList.Add("followerAccountId=10");
            p.Following = true;
            checkList.Add("following=true");
            p.CategoryFollowerAccountId = 99;
            //checkList.Add("categoryFollowerAccountId=99");
            p.CategoryFollowing = true;
            checkList.Add("categoryFollowing=true");
            p.AdditionalPropertyFollowerAccountId = 77;
            //checkList.Add("additionalPropertyFollowerAccountId=77");
            p.AdditionalPropertyFollowing = true;
            checkList.Add("additionalPropertyFollowing=true");
            p.MarkedHelpfulByContactId = 12;
            checkList.Add("markedHelpfulByContactId=12");
            p.LastChangedSinceDays = 10;
            //checkList.Add("lastChangedSinceDays=10");
            p.LastChangedSince = Matterial.UnixTimestamp();
            checkList.Add("lastChangedSince=" + Matterial.UnixTimestamp());
            p.ResponsibleContactId = 44;
            //checkList.Add("responsibleContactId=44");
            p.Responsible = true;
            checkList.Add("responsible=true");
            p.AuthorshipAccountId = 22;
            //checkList.Add("authorshipAccountId=22");
            p.Authorship = true;
            checkList.Add("authorship=true");
            p.ExpiresInDays = 2;
            //checkList.Add("documentExpiresInDays=2");
            p.ExpiresOn = 6765765;
            checkList.Add("documentExpiresOn=6765765");
            p.Expires = true;
            checkList.Add("documentExpires=true");
            p.HasArchivedBegin = true;
            checkList.Add("documentHasArchivedBegin=true");
            p.Reviewed = ParamHideValue.Exclusive;
            checkList.Add("showLanguageVersionReviewedOnly=true");
            p.Published = ParamHideValue.Exclusive;
            checkList.Add("showLanguageVersionReadyOnly=true");
            p.InProcessing = ParamHideValue.Exclude;
            checkList.Add("hideLanguageVersionCurrentlyInProcessing=true");
            p.ReviewRequested = ParamHideValue.Exclusive;
            checkList.Add("showLanguageVersionReviewRequestedOnly=true");
            p.AdditionalPropertyId = 55;
            checkList.Add("additionalPropertyId=55");
            p.AdditionalPropertyType = AdditionalPropertyLoadTypes.News;
            checkList.Add("additionalPropertyType=" + ((int)AdditionalPropertyLoadTypes.News).ToString());
            p.Snaps = ParamHideValue.Exclusive;
            checkList.Add("showSnapOnly=true");
            p.MimeTypeId = 99;
            checkList.Add("mimeTypeId=99");
            p.MimeType = "text";
            checkList.Add("mimeType=text");
            p.MentionedAccountIdInComment = 87;
            checkList.Add("mentionedAccountIdInComment=87");
            p.MentionedAccountIdInCommentUnread = 44;
            checkList.Add("mentionedAccountIdInCommentUnread=44");
            p.LoadOptions.RoleRights = true;
            checkList.Add("loadRoleRights=true");
            p.LoadOptions.CategoriesPublicOnly = true;
            checkList.Add("loadCategoriesPublicOnly=true");
            p.LoadOptions.Categories = true;
            checkList.Add("loadCategories=true");
            p.LoadOptions.Responsibles = true;
            checkList.Add("loadResponsibles=true");
            p.LoadOptions.Authors = true;
            checkList.Add("loadAuthors=true");
            p.LoadOptions.LastAuthorOnly = true;
            checkList.Add("loadLastAuthorOnly=true");
            p.LoadOptions.Followers = true;
            checkList.Add("loadFollowers=true");
            p.LoadOptions.MarkedAsHelpfulBy = true;
            checkList.Add("loadMarkedAsHelpfulBy=true");
            p.LoadOptions.AttachmentOrderBy = AttachmentsOrderBy.CreateTime;
            checkList.Add("attachmentOrderBy=createtime");
            p.LoadOptions.AttachmentOrderDir = OrderDirection.desc;
            checkList.Add("attachmentOrderDir=desc");
            p.LoadOptions.Attachments = true;
            checkList.Add("loadAttachments=true");
            p.LoadOptions.LanguageAttachments = true;
            checkList.Add("loadLanguageAttachments=true");
            p.LoadOptions.DocumentAttachments = true;
            checkList.Add("loadDocumentAttachments=true");
            p.LoadOptions.RelatedDocumentIds = true;
            checkList.Add("loadRelatedDocumentIds=true");
            p.LoadOptions.AdditionalProperties = true;
            checkList.Add("loadAdditionalProperties=true");
            p.LoadOptions.ExtensionValues = true;
            checkList.Add("loadExtensionValues=true");
            p.LoadOptions.SnapFlag = true;
            checkList.Add("loadSnapFlag=true");
            p.LoadOptions.Comments = true;
            checkList.Add("loadComments=true");
            p.Count = true;
            checkList.Add("count=true");
            p.OrderBy = DocLoadOrderBy.ClickCount;
            checkList.Add("orderBy=clickCount");
            p.OrderDir = OrderDirection.desc;
            checkList.Add("orderDir=desc");
            p.Offset = 1;
            checkList.Add("offset=1");
            p.Limit = 12;
            checkList.Add("limit=12");
            p.Removed = ParamShowValue.Include;
            checkList.Add("showRemoved=true");

            CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);
        }

        [Test]
        public void LoadDocTrashParams()
        {
            //GetDocumentsTrashParams testing properties and url params
            List<string> checkList = new();
            string errMsg;

            LoadDocTrashParams p = new();
            //Default (no properties set)
            checkList.Add("offset=0");
            checkList.Add("limit=" + Matterial.DefaultSearchResultLimit);
            checkList.Add("languageKey=en");
            CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);

            //All params explicit
            checkList = new List<string>();
            p = new LoadDocTrashParams();
            checkList.Add("languageKey=en");
            checkList.Add("showBios=false");
            checkList.Add("showInvalid=false");
            checkList.Add("hideSnap=false");
            checkList.Add("showTemplates=false");
            checkList.Add("showArchived=false");
            checkList.Add("hideLanguageVersionReviewed=false");
            checkList.Add("hideLanguageVersionReady=false");
            checkList.Add("hideLanguageVersionCurrentlyInProcessing=false");
            checkList.Add("hideLanguageVersionReviewRequested=false");
            checkList.Add("hideLandscape=false");
            checkList.Add("categoryIdsAnd=0");
            checkList.Add("categoryIdsOr=0");
            checkList.Add("roleRightRoleId=0");
            checkList.Add("roleRightType=0");
            checkList.Add("read=");
            checkList.Add("markedHelpfulByContactId=0");
            checkList.Add("responsible=false");
            checkList.Add("responsibleContactId=0");
            checkList.Add("authorship=false");
            checkList.Add("authorshipAccountId=0");
            checkList.Add("additionalPropertyId=0");
            checkList.Add("additionalPropertyType=-1");
            checkList.Add("lastChangedSince=0");
            checkList.Add("lastChangedSinceDays=0");
            checkList.Add("following=false");
            checkList.Add("followerAccountId=0");
            checkList.Add("offset=0");
            checkList.Add("limit=20");
            checkList.Add("allLanguages=false");
            checkList.Add("languagePrefer=false");
            checkList.Add("languageExclude=false");
            checkList.Add("orderBy=languageVersionTitle");
            checkList.Add("orderDir=asc");
            checkList.Add("relatedDocumentId=0");
            checkList.Add("categoryFollowerAccountId=0");
            checkList.Add("categoryFollowing=false");
            checkList.Add("additionalPropertyFollowerAccountId=0");
            checkList.Add("additionalPropertyFollowing=false");
            checkList.Add("documentExpiresInDays=-1");
            checkList.Add("documentExpiresOn=0");
            checkList.Add("documentExpires=false");
            checkList.Add("documentHasArchivedBegin=false");
            checkList.Add("mimeTypeId=-1");
            checkList.Add("mimeType=");
            checkList.Add("mentionedAccountIdInComment=0");
            checkList.Add("mentionedAccountIdInCommentUnread=0");
            checkList.Add("attachmentOrderBy=name");
            checkList.Add("attachmentOrderDir=asc");
            checkList.Add("loadRoleRights=false");
            checkList.Add("loadCategoriesPublicOnly=false");
            checkList.Add("loadCategories=false");
            checkList.Add("loadResponsibles=false");
            checkList.Add("loadAuthors=false");
            checkList.Add("loadLastAuthorOnly=false");
            checkList.Add("loadFollowers=false");
            checkList.Add("loadAmIFollowing=false");
            checkList.Add("loadMarkedAsHelpfulBy=false");
            checkList.Add("loadAttachments=false");
            checkList.Add("loadLanguageAttachments=false");
            checkList.Add("loadDocumentAttachments=false");
            checkList.Add("loadRelatedDocumentIds=false");
            checkList.Add("loadAdditionalProperties=false");
            checkList.Add("loadExtensionValues=false");
            checkList.Add("loadSnapFlag=false");
            checkList.Add("loadComments=false");
            checkList.Add("count=false");

            p.DebugUrlParameter = true; CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);

            //Check follower and lastchanged and expires
            checkList = new List<string>();
            p = new LoadDocTrashParams();
            checkList.Add("offset=0");
            checkList.Add("limit=" + Matterial.DefaultSearchResultLimit);
            checkList.Add("languageKey=en");
            p.FollowerAccountId = 10;
            checkList.Add("followerAccountId=10");
            p.CategoryFollowerAccountId = 99;
            checkList.Add("categoryFollowerAccountId=99");
            p.AdditionalPropertyFollowerAccountId = 77;
            checkList.Add("additionalPropertyFollowerAccountId=77");
            p.LastChangedSinceDays = 10;
            checkList.Add("lastChangedSinceDays=10");
            p.ResponsibleContactId = 44;
            checkList.Add("responsibleContactId=44");
            p.AuthorshipAccountId = 22;
            checkList.Add("authorshipAccountId=22");
            p.ExpiresInDays = 2;
            checkList.Add("documentExpiresInDays=2");
            CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);

            //Now set properties
            checkList = new List<string>();
            p = new LoadDocTrashParams();
            p.AllLanguages = true;
            checkList.Add("allLanguages=true");
            p.LanguagePrefer = true;
            checkList.Add("languagePrefer=true");
            p.LanguageExclude = true;
            checkList.Add("languageExclude=true");
            p.LanguageKey = LangKey.es;
            checkList.Add("languageKey=es");
            p.CategoryAndIds = new List<long> { 1, 2, 3 };
            checkList.Add("categoryIdsAnd=1");
            checkList.Add("categoryIdsAnd=2");
            checkList.Add("categoryIdsAnd=3");
            p.CategoryOrIds = new List<long> { 5, 6, 7 };
            checkList.Add("categoryIdsOr=5");
            checkList.Add("categoryIdsOr=6");
            checkList.Add("categoryIdsOr=7");
            p.RoleRightRoleId = 4;
            checkList.Add("roleRightRoleId=4");
            p.RoleRightType = RoleRightFilterTypes.EDIT;
            checkList.Add("roleRightType=" + ((int)RoleRightFilterTypes.EDIT).ToString());
            p.ReadStatus = ReadStatuses.Unread;
            checkList.Add("read=false");
            p.RelatedDocumentId = 234;
            checkList.Add("relatedDocumentId=234");
            p.Invalid = ParamShowValue.Exclusive;
            checkList.Add("showInvalidOnly=true");
            p.Bios = ParamShowValue.Exclusive;
            checkList.Add("showBiosOnly=true");
            p.Templates = ParamShowValue.Exclusive;
            checkList.Add("showTemplatesOnly=true");
            p.Archived = ParamShowValue.Include;
            checkList.Add("showArchived=true");
            p.Landscape = ParamHideValue.Exclusive;
            checkList.Add("showLandscapeOnly=true");
            p.FollowerAccountId = 10;
            //checkList.Add("followerAccountId=10");
            p.Following = true;
            checkList.Add("following=true");
            p.CategoryFollowerAccountId = 99;
            //checkList.Add("categoryFollowerAccountId=99");
            p.CategoryFollowing = true;
            checkList.Add("categoryFollowing=true");
            p.AdditionalPropertyFollowerAccountId = 77;
            //checkList.Add("additionalPropertyFollowerAccountId=77");
            p.AdditionalPropertyFollowing = true;
            checkList.Add("additionalPropertyFollowing=true");
            p.MarkedHelpfulByContactId = 12;
            checkList.Add("markedHelpfulByContactId=12");
            p.LastChangedSinceDays = 10;
            //checkList.Add("lastChangedSinceDays=10");
            p.LastChangedSince = Matterial.UnixTimestamp();
            checkList.Add("lastChangedSince=" + Matterial.UnixTimestamp());
            p.ResponsibleContactId = 44;
            //checkList.Add("responsibleContactId=44");
            p.Responsible = true;
            checkList.Add("responsible=true");
            p.AuthorshipAccountId = 22;
            //checkList.Add("authorshipAccountId=22");
            p.Authorship = true;
            checkList.Add("authorship=true");
            p.ExpiresInDays = 2;
            //checkList.Add("documentExpiresInDays=2");
            p.ExpiresOn = 6765765;
            checkList.Add("documentExpiresOn=6765765");
            p.Expires = true;
            checkList.Add("documentExpires=true");
            p.HasArchivedBegin = true;
            checkList.Add("documentHasArchivedBegin=true");
            p.Reviewed = ParamHideValue.Exclusive;
            checkList.Add("showLanguageVersionReviewedOnly=true");
            p.Published = ParamHideValue.Exclusive;
            checkList.Add("showLanguageVersionReadyOnly=true");
            p.InProcessing = ParamHideValue.Exclude;
            checkList.Add("hideLanguageVersionCurrentlyInProcessing=true");
            p.ReviewRequested = ParamHideValue.Exclusive;
            checkList.Add("showLanguageVersionReviewRequestedOnly=true");
            p.AdditionalPropertyId = 55;
            checkList.Add("additionalPropertyId=55");
            p.AdditionalPropertyType = AdditionalPropertyLoadTypes.News;
            checkList.Add("additionalPropertyType=" + ((int)AdditionalPropertyLoadTypes.News).ToString());
            p.Snaps = ParamHideValue.Exclusive;
            checkList.Add("showSnapOnly=true");
            p.MimeTypeId = 99;
            checkList.Add("mimeTypeId=99");
            p.MimeType = "text";
            checkList.Add("mimeType=text");
            p.MentionedAccountIdInComment = 87;
            checkList.Add("mentionedAccountIdInComment=87");
            p.MentionedAccountIdInCommentUnread = 44;
            checkList.Add("mentionedAccountIdInCommentUnread=44");
            p.LoadOptions.RoleRights = true;
            checkList.Add("loadRoleRights=true");
            p.LoadOptions.CategoriesPublicOnly = true;
            checkList.Add("loadCategoriesPublicOnly=true");
            p.LoadOptions.Categories = true;
            checkList.Add("loadCategories=true");
            p.LoadOptions.Responsibles = true;
            checkList.Add("loadResponsibles=true");
            p.LoadOptions.Authors = true;
            checkList.Add("loadAuthors=true");
            p.LoadOptions.LastAuthorOnly = true;
            checkList.Add("loadLastAuthorOnly=true");
            p.LoadOptions.Followers = true;
            checkList.Add("loadFollowers=true");
            p.LoadOptions.MarkedAsHelpfulBy = true;
            checkList.Add("loadMarkedAsHelpfulBy=true");
            p.LoadOptions.AttachmentOrderBy = AttachmentsOrderBy.CreateTime;
            checkList.Add("attachmentOrderBy=createtime");
            p.LoadOptions.AttachmentOrderDir = OrderDirection.desc;
            checkList.Add("attachmentOrderDir=desc");
            p.LoadOptions.Attachments = true;
            checkList.Add("loadAttachments=true");
            p.LoadOptions.LanguageAttachments = true;
            checkList.Add("loadLanguageAttachments=true");
            p.LoadOptions.DocumentAttachments = true;
            checkList.Add("loadDocumentAttachments=true");
            p.LoadOptions.RelatedDocumentIds = true;
            checkList.Add("loadRelatedDocumentIds=true");
            p.LoadOptions.AdditionalProperties = true;
            checkList.Add("loadAdditionalProperties=true");
            p.LoadOptions.ExtensionValues = true;
            checkList.Add("loadExtensionValues=true");
            p.LoadOptions.SnapFlag = true;
            checkList.Add("loadSnapFlag=true");
            p.LoadOptions.Comments = true;
            checkList.Add("loadComments=true");
            p.Count = true;
            checkList.Add("count=true");
            p.OrderBy = DocLoadOrderBy.ClickCount;
            checkList.Add("orderBy=clickCount");
            p.OrderDir = OrderDirection.desc;
            checkList.Add("orderDir=desc");
            p.Offset = 1;
            checkList.Add("offset=1");
            p.Limit = 12;
            checkList.Add("limit=12");

            CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);
        }

        [Test]
        public void MainFileLoadParams()
        {
            //testing properties and url params
            List<string> checkList = new();
            string errMsg;

            //default url params
            MainFileLoadParams p = new();
            checkList = new List<string>();
            checkList.Add("languageKey=en");
            CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);

            //All
            p = new MainFileLoadParams();
            checkList = new List<string>();
            checkList.Add("resolveCompounds=false");
            checkList.Add("asis=true");
            checkList.Add("languageKey=en");
            p.DebugUrlParameter = true; CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);

            p = new MainFileLoadParams();
            checkList = new List<string>();
            p.LanguageKey = LangKey.es;
            checkList.Add("languageKey=es");
            p.LoadFormat = LoadMainFileFormats.Thumbnail;
            checkList.Add("loadThumbnail=true");
            p.ResolveCompounds = true;
            checkList.Add("resolveCompounds=true");

            CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);
        }

        [Test]
        public void PropertyLoadByIdParams()
        {
            //testing properties and url params
            List<string> checkList = new();
            string errMsg;

            //default url params
            PropertyLoadByIdParams p = new();
            checkList = new List<string>();
            checkList.Add("languageKey=en");
            checkList.Add("offset=0");
            checkList.Add("limit=" + Matterial.DefaultSearchResultLimit);
            CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);

            //All
            p = new PropertyLoadByIdParams();
            checkList = new List<string>();
            checkList.Add("languageKey=en");
            checkList.Add("offset=0");
            checkList.Add("limit=20");
            checkList.Add("loadFollowingLanguageKey=en");
            p.DebugUrlParameter = true; CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);

            //specific ones
            p = new PropertyLoadByIdParams();
            checkList = new List<string>();
            p.LanguageKey = LangKey.es;
            checkList.Add("languageKey=es");
            p.Offset = 1;
            checkList.Add("offset=1");
            p.Limit = 10;
            checkList.Add("limit=10");
            p.LoadFollowingLanguageKey = LangKey.fr;
            checkList.Add("loadFollowingLanguageKey=fr");
            CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);
        }

        [Test]
        public void PropertyLoadParams()
        {
            //testing properties and url params
            List<string> checkList = new();
            string errMsg;

            //default url params
            PropertyLoadParams p = new();
            checkList = new List<string>();
            checkList.Add("languageKey=en");
            checkList.Add("offset=0");
            checkList.Add("limit=" + Matterial.DefaultSearchResultLimit);
            CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);

            //All
            p = new PropertyLoadParams();
            checkList = new List<string>();
            checkList.Add("languageKey=en");
            checkList.Add("offset=0");
            checkList.Add("limit=20");
            checkList.Add("loadFollowingLanguageKey=en");
            checkList.Add("orderDir=asc");
            checkList.Add("orderBy=id");
            checkList.Add("propertyType=0");
            p.DebugUrlParameter = true; CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);

            //specific ones
            p = new PropertyLoadParams();
            checkList = new List<string>();
            p.LanguageKey = LangKey.es;
            checkList.Add("languageKey=es");
            p.Offset = 1;
            checkList.Add("offset=1");
            p.Limit = 10;
            checkList.Add("limit=10");
            p.LoadFollowingLanguageKey = LangKey.fr;
            checkList.Add("loadFollowingLanguageKey=fr");
            p.OrderDir = OrderDirection.desc;
            checkList.Add("orderDir=desc");
            p.OrderBy = DocPropertyOrderBy.Description;
            checkList.Add("orderBy=description");
            p.PropertyType = AdditionalPropertyLoadTypes.Urgent;
            checkList.Add("propertyType=2");
            CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);
        }

        [Test]
        public void SavedSearchLoadParams()
        {
            List<string> checkList = new();
            string errMsg;

            //default url params
            SavedSearchLoadParams p = new();
            checkList = new List<string>();
            checkList.Add("languageKey=en");
            CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);

            //All params explicit
            p = new SavedSearchLoadParams();
            checkList = new List<string>();
            checkList.Add("languageKey=en");
            checkList.Add("onDashboard=debug");
            checkList.Add("hidePersonal=false");
            checkList.Add("orderBy=name");
            checkList.Add("orderDir=asc");
            p.DebugUrlParameter = true; CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);

            //Specific params set
            p = new SavedSearchLoadParams();
            checkList = new List<string>();
            p.LanguageKey = LangKey.fr;
            checkList.Add("languageKey=fr");
            p.OnDashboard = ParamHideBoolValue.Exclude;
            checkList.Add("onDashboard=false");
            p.Personal = ParamHideValue.Exclusive;
            checkList.Add("showPersonalOnly=true");
            p.OrderBy = SavedSearchesOrderBy.Id;
            checkList.Add("orderBy=id");
            p.OrderDir = OrderDirection.desc;
            checkList.Add("orderDir=desc");
            CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);
        }

        [Test]
        public void SavedSearchSearchParams()
        {
            List<string> checkList = new();
            string errMsg;

            //default url params
            SavedSearchSearchParams p = new();
            checkList = new List<string>();
            checkList.Add("languageKey=en");
            checkList.Add("offset=0");
            checkList.Add("limit=" + Matterial.DefaultSearchResultLimit);
            CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);

            //All params explicit
            p = new SavedSearchSearchParams();
            checkList = new List<string>();
            checkList.Add("languageKey=en");
            checkList.Add("offset=0");
            checkList.Add("limit=20");
            checkList.Add("loadRoleRights=false");
            checkList.Add("loadCategoriesPublicOnly=false");
            checkList.Add("loadCategories=false");
            checkList.Add("loadResponsibles=false");
            checkList.Add("loadAuthors=false");
            checkList.Add("loadLastAuthorOnly=false");
            checkList.Add("loadFollowers=false");
            checkList.Add("loadAmIFollowing=false");
            checkList.Add("loadMarkedAsHelpfulBy=false");
            checkList.Add("attachmentOrderBy=name");
            checkList.Add("attachmentOrderDir=asc");
            checkList.Add("loadAttachments=false");
            checkList.Add("loadLanguageAttachments=false");
            checkList.Add("loadDocumentAttachments=false");
            checkList.Add("loadRelatedDocumentIds=false");
            checkList.Add("loadAdditionalProperties=false");
            checkList.Add("loadExtensionValues=false");
            checkList.Add("loadSnapFlag=false");
            checkList.Add("loadComments=false");
            checkList.Add("highlightField=field");
            checkList.Add("languageAggregations=true");
            checkList.Add("categoryAggregations=true");
            checkList.Add("lastChangeAggregations=true");
            checkList.Add("count=false");

            p.DebugUrlParameter = true; CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);

            //Specific params set
            p = new SavedSearchSearchParams();
            checkList = new List<string>();
            p.LanguageKey = LangKey.fr;
            checkList.Add("languageKey=fr");
            p.Offset = 33;
            checkList.Add("offset=33");
            p.Limit = 2;
            checkList.Add("limit=2");
            p.LoadOptions.RoleRights = true;
            checkList.Add("loadRoleRights=true");
            p.LoadOptions.CategoriesPublicOnly = true;
            checkList.Add("loadCategoriesPublicOnly=true");
            p.LoadOptions.Categories = true;
            checkList.Add("loadCategories=true");
            p.LoadOptions.Responsibles = true;
            checkList.Add("loadResponsibles=true");
            p.LoadOptions.Authors = true;
            checkList.Add("loadAuthors=true");
            p.LoadOptions.LastAuthorOnly = true;
            checkList.Add("loadLastAuthorOnly=true");
            p.LoadOptions.Followers = true;
            checkList.Add("loadFollowers=true");
            p.LoadOptions.AmIFollowing = true;
            checkList.Add("loadAmIFollowing=true");
            p.LoadOptions.MarkedAsHelpfulBy = true;
            checkList.Add("loadMarkedAsHelpfulBy=true");
            p.LoadOptions.AttachmentOrderBy = AttachmentsOrderBy.Id;
            checkList.Add("attachmentOrderBy=id");
            p.LoadOptions.AttachmentOrderDir = OrderDirection.desc;
            checkList.Add("attachmentOrderDir=desc");
            p.LoadOptions.Attachments = true;
            checkList.Add("loadAttachments=true");
            p.LoadOptions.LanguageAttachments = true;
            checkList.Add("loadLanguageAttachments=true");
            p.LoadOptions.DocumentAttachments = true;
            checkList.Add("loadDocumentAttachments=true");
            p.LoadOptions.RelatedDocumentIds = true;
            checkList.Add("loadRelatedDocumentIds=true");
            p.LoadOptions.AdditionalProperties = true;
            checkList.Add("loadAdditionalProperties=true");
            p.LoadOptions.ExtensionValues = true;
            checkList.Add("loadExtensionValues=true");
            p.LoadOptions.SnapFlag = true;
            checkList.Add("loadSnapFlag=true");
            p.LoadOptions.Comments = true;
            checkList.Add("loadComments=true");
            p.HighlightFields = new HighlightFields { "field1", "field2" };
            checkList.Add("highlightField=field1");
            checkList.Add("highlightField=field2");
            p.Aggregation.Language = true;
            checkList.Add("languageAggregations=true");
            p.Aggregation.Category = true;
            checkList.Add("categoryAggregations=true");
            p.Aggregation.LastChange = true;
            checkList.Add("lastChangeAggregations=true");
            p.Count = true;
            checkList.Add("count=true");

            CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);
        }

        [Test]
        public void SearchAggregationParams()
        {
            List<string> checkList = new();
            string errMsg;

            //default url params
            SearchAggregationParams p = new();
            CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);

            //check all properties expkicit
            p = new SearchAggregationParams();
            checkList = new List<string>();
            checkList.Add("languageAggregations=true");
            checkList.Add("categoryAggregations=true");
            checkList.Add("lastChangeAggregations=true");
            p.DebugUrlParameter = true; CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);

            //Now some properties set
            p = new SearchAggregationParams();
            checkList = new List<string>();
            p.Language = true;
            checkList.Add("languageAggregations=true");
            p.Category = true;
            checkList.Add("categoryAggregations=true");
            p.LastChange = true;
            checkList.Add("lastChangeAggregations=true");
            CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);
        }

        [Test]
        public void SearchDocParams()
        {
            List<string> checkList = new();
            string errMsg;
            SearchDocParams p = new();

            //Default test (no properties set)
            p = new SearchDocParams();
            checkList = new List<string>();
            checkList.Add("offset=0");
            checkList.Add("limit=" + Matterial.DefaultSearchResultLimit);
            CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);

            //check all properties expkicit
            p = new SearchDocParams();
            checkList = new List<string>();
            checkList.Add("languageKey=en");
            checkList.Add("showBios=false");
            checkList.Add("showInvalid=false");
            checkList.Add("hideSnap=false");
            checkList.Add("showTemplates=false");
            checkList.Add("showArchived=false");
            checkList.Add("hideLanguageVersionReviewed=false");
            checkList.Add("hideLanguageVersionReady=false");
            checkList.Add("hideLanguageVersionCurrentlyInProcessing=false");
            checkList.Add("hideLanguageVersionReviewRequested=false");
            checkList.Add("hideLandscape=false");
            checkList.Add("categoryIdsAnd=0");
            checkList.Add("categoryIdsOr=0");
            checkList.Add("roleRightRoleId=0");
            checkList.Add("roleRightType=0");
            checkList.Add("read=");
            checkList.Add("markedHelpfulByContactId=0");
            checkList.Add("responsible=false");
            checkList.Add("responsibleContactId=0");
            checkList.Add("authorship=false");
            checkList.Add("authorshipAccountId=0");
            checkList.Add("additionalPropertyId=0");
            checkList.Add("additionalPropertyType=-1");
            checkList.Add("lastChangedSince=0");
            checkList.Add("lastChangedSinceDays=0");
            checkList.Add("following=false");
            checkList.Add("followerAccountId=0");
            checkList.Add("offset=0");
            checkList.Add("limit=20");
            checkList.Add("orderBy=");
            checkList.Add("orderDir=asc");
            checkList.Add("highlightField=field");
            checkList.Add("showRemoved=false");
            checkList.Add("languageAggregations=true");
            checkList.Add("categoryAggregations=true");
            checkList.Add("lastChangeAggregations=true");
            checkList.Add("allLanguages=false");
            p.DebugUrlParameter = true; CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);

            //Follower test
            p = new SearchDocParams();
            checkList = new List<string>();
            checkList.Add("offset=0");
            checkList.Add("limit=" + Matterial.DefaultSearchResultLimit);
            p.FollowerAccountId = 10;
            checkList.Add("followerAccountId=10");
            p.ResponsibleContactId = 44;
            checkList.Add("responsibleContactId=44");
            p.AuthorshipAccountId = 22;
            checkList.Add("authorshipAccountId=22");
            CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);

            //Last changed test
            p = new SearchDocParams();
            checkList = new List<string>();
            p.LastChangedSinceDays = 10;
            checkList.Add("lastChangedSinceDays=10");
            checkList.Add("offset=0");
            checkList.Add("limit=" + Matterial.DefaultSearchResultLimit);
            //p.LastChangedSince = Matterial.GetUnixTimestamp();
            //checkList.Add("lastChangedSince=" + Matterial.GetUnixTimestamp());
            CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);

            p = new SearchDocParams();
            checkList = new List<string>();
            p.LanguageKey = LangKey.es;
            checkList.Add("languageKey=es");
            p.CategoryAndIds = new List<long> { 1, 2, 3 };
            checkList.Add("categoryIdsAnd=1");
            checkList.Add("categoryIdsAnd=2");
            checkList.Add("categoryIdsAnd=3");
            p.CategoryOrIds = new List<long> { 5, 6, 7 };
            checkList.Add("categoryIdsOr=5");
            checkList.Add("categoryIdsOr=6");
            checkList.Add("categoryIdsOr=7");
            p.RoleRightRoleId = 4;
            checkList.Add("roleRightRoleId=4");
            p.RoleRightType = RoleRightFilterTypes.EDIT;
            checkList.Add("roleRightType=" + ((int)RoleRightFilterTypes.EDIT).ToString());
            p.ReadStatus = ReadStatuses.Unread;
            checkList.Add("read=false");
            p.Invalid = ParamShowValue.Exclusive;
            checkList.Add("showInvalidOnly=true");
            p.Bios = ParamShowValue.Exclusive;
            checkList.Add("showBiosOnly=true");
            p.Templates = ParamShowValue.Exclusive;
            checkList.Add("showTemplatesOnly=true");
            p.Archived = ParamShowValue.Include;
            checkList.Add("showArchived=true");
            p.Removed = ParamShowValue.Include;
            checkList.Add("showRemoved=true");
            p.Landscape = ParamHideValue.Exclusive;
            checkList.Add("showLandscapeOnly=true");
            //p.FollowerAccountId = 10;
            //checkList.Add("followerAccountId=10");
            p.Following = true;
            checkList.Add("following=true");
            p.MarkedHelpfulByContactId = 12;
            checkList.Add("markedHelpfulByContactId=12");
            //p.LastChangedSinceDays = 10;
            //checkList.Add("lastChangedSinceDays=10");
            p.LastChangedSince = Matterial.UnixTimestamp();
            checkList.Add("lastChangedSince=" + Matterial.UnixTimestamp());
            //p.ResponsibleContactId = 44;
            //checkList.Add("responsibleContactId=44");
            p.Responsible = true;
            checkList.Add("responsible=true");
            //p.AuthorshipAccountId = 22;
            //checkList.Add("authorshipAccountId=22");
            p.Authorship = true;
            checkList.Add("authorship=true");
            p.Reviewed = ParamHideValue.Exclusive;
            checkList.Add("showLanguageVersionReviewedOnly=true");
            p.Published = ParamHideValue.Exclusive;
            checkList.Add("showLanguageVersionReadyOnly=true");
            p.InProcessing = ParamHideValue.Exclude;
            checkList.Add("hideLanguageVersionCurrentlyInProcessing=true");
            p.ReviewRequested = ParamHideValue.Exclusive;
            checkList.Add("showLanguageVersionReviewRequestedOnly=true");
            p.AdditionalPropertyId = 55;
            checkList.Add("additionalPropertyId=55");
            p.AdditionalPropertyType = AdditionalPropertyLoadTypes.News;
            checkList.Add("additionalPropertyType=" + ((int)AdditionalPropertyLoadTypes.News).ToString());
            p.Snaps = ParamHideValue.Exclusive;
            checkList.Add("showSnapOnly=true");
            p.OrderBy = DocSearchOrderBy.LastChangeInSeconds;
            checkList.Add("orderBy=languageVersionLastChangeInSeconds");
            p.OrderDir = OrderDirection.desc;
            checkList.Add("orderDir=desc");
            p.Offset = 1;
            checkList.Add("offset=1");
            p.Limit = 10;
            checkList.Add("limit=10");
            p.Aggregation.Language = true;
            checkList.Add("languageAggregations=true");
            p.Aggregation.Category = true;
            checkList.Add("categoryAggregations=true");
            p.Aggregation.LastChange = true;
            checkList.Add("lastChangeAggregations=true");
            p.HighlightFields = new HighlightFields { "name1", "name2" };
            checkList.Add("highlightField=name1");
            checkList.Add("highlightField=name2");
            p.AllLanguages = true;
            checkList.Add("allLanguages=true");
            CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);
        }

        [Test]
        public void UpdateContextParams()
        {
            List<string> checkList = new();
            string errMsg;
            UpdateContextParams p = new();

            //Default test (no properties set)
            p = new UpdateContextParams();
            checkList = new List<string>();
            CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);

            //all params explicit
            p = new UpdateContextParams();
            checkList = new List<string>();
            checkList.Add("contextToken=xxxx");
            p.DebugUrlParameter = true; CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);

            //Set a context
            p = new UpdateContextParams();
            checkList = new List<string>();
            p.ContextToken = "abcdefg";
            checkList.Add("contextToken=abcdefg");
            CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);
        }

        [Test]
        public void UpdateLockParams()
        {
            List<string> checkList = new();
            string errMsg;
            UpdateLockParams p = new();

            //Default test (no properties set)
            p = new UpdateLockParams();
            checkList = new List<string>();
            CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);

            //all params explicit
            p = new UpdateLockParams();
            checkList = new List<string>();
            checkList.Add("contextToken=xxxx");
            checkList.Add("uniqueLockId=xxxx");
            checkList.Add("ignoreCategories=false");
            checkList.Add("ignoreRelatedDocumentIds=false");
            checkList.Add("ignoreExtensionValues=false");
            checkList.Add("ignoreAttachments=false");
            checkList.Add("ignoreRoleRights=false");
            checkList.Add("ignoreResponsibles=false");
            p.DebugUrlParameter = true; CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);

            //Set a context
            p = new UpdateLockParams();
            checkList = new List<string>();
            p.ContextToken = "abcdefg";
            checkList.Add("contextToken=abcdefg");
            p.UniqueLockId = "rrrrr";
            checkList.Add("uniqueLockId=rrrrr");
            p.IgnoreCategories = true;
            checkList.Add("ignoreCategories=true");
            p.IgnoreRelatedDocumentIds = true;
            checkList.Add("ignoreRelatedDocumentIds=true");
            p.IgnoreExtensionValues = true;
            checkList.Add("ignoreExtensionValues=true");
            p.IgnoreAttachments = true;
            checkList.Add("ignoreAttachments=true");
            p.IgnoreRoleRights = true;
            checkList.Add("ignoreRoleRights=true");
            p.IgnoreResponsibles = true;
            checkList.Add("ignoreResponsibles=true");

            CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);
        }

        [Test]
        public void UpdateTemplateParams()
        {
            List<string> checkList = new();
            string errMsg;
            UpdateTemplateParams p = new();

            //Default test (no properties set)
            p = new UpdateTemplateParams();
            checkList = new List<string>();
            CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);

            //all params explicit
            p = new UpdateTemplateParams();
            checkList = new List<string>();
            checkList.Add("contextToken=xxxx");
            checkList.Add("uniqueLockId=xxxx");
            checkList.Add("publishRequest=None");
            checkList.Add("ignoreCategories=false");
            checkList.Add("ignoreRelatedDocumentIds=false");
            checkList.Add("ignoreExtensionValues=false");
            checkList.Add("ignoreAttachments=false");
            checkList.Add("ignoreRoleRights=false");
            checkList.Add("ignoreResponsibles=false");
            p.DebugUrlParameter = true; CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);

            //set some properties
            p = new UpdateTemplateParams();
            checkList = new List<string>();
            p.ContextToken = "test";
            checkList.Add("contextToken=test");
            p.UniqueLockId = "aaa";
            checkList.Add("uniqueLockId=aaa");
            p.PublishRequestType = PublishRequestTypes.Reviewed;
            checkList.Add("publishRequest=reviewed");
            p.IgnoreCategories = true;
            checkList.Add("ignoreCategories=true");
            p.IgnoreRelatedDocumentIds = true;
            checkList.Add("ignoreRelatedDocumentIds=true");
            p.IgnoreExtensionValues = true;
            checkList.Add("ignoreExtensionValues=true");
            p.IgnoreAttachments = true;
            checkList.Add("ignoreAttachments=true");
            p.IgnoreRoleRights = true;
            checkList.Add("ignoreRoleRights=true");
            p.IgnoreResponsibles = true;
            checkList.Add("ignoreResponsibles=true");

            CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);
        }

        [Test]
        public void GetPersonByIdParams()
        {
            List<string> checkList = new();
            string errMsg;
            GetPersonByIdParams p = new();

            //Default test (no properties set)
            p = new GetPersonByIdParams();
            checkList = new List<string>();
            CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);

            //Force all parameters
            p = new GetPersonByIdParams();
            checkList = new List<string>();
            checkList.Add("loadClients=false");
            checkList.Add("loadAddresses=false");
            checkList.Add("loadCommunicationData=false");
            checkList.Add("loadRoles=false");
            checkList.Add("loadContactImages=false");
            checkList.Add("loadBioDocumentLanguageKey=en");
            p.DebugUrlParameter = true; CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);

            p = new GetPersonByIdParams();
            checkList = new List<string>();
            p.Clients = true;
            checkList.Add("loadClients=true");
            p.Addresses = true;
            checkList.Add("loadAddresses=true");
            p.CommunicationData = true;
            checkList.Add("loadCommunicationData=true");
            p.Roles = true;
            checkList.Add("loadRoles=true");
            p.ContactImages = true;
            checkList.Add("loadContactImages=true");
            p.BioDocumentLanguageKey = LangKey.es;
            checkList.Add("loadBioDocumentLanguageKey=es");

            CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);
        }

        [Test]
        public void GetPersonParams()
        {
            List<string> checkList = new();
            string errMsg;
            GetPersonParams p = new();

            //Default test (no properties set)
            p = new GetPersonParams();
            checkList = new List<string>();
            checkList.Add("offset=0");
            checkList.Add("limit=" + Matterial.DefaultSearchResultLimit);
            CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);

            ///Force all parameters
            p = new GetPersonParams();
            checkList = new List<string>();
            checkList.Add("roleId=0");
            checkList.Add("hideInstanceOwner=false");
            checkList.Add("hideDemo=false");
            checkList.Add("hideLimited=false");
            checkList.Add("hideActive=false");
            checkList.Add("offset=0");
            checkList.Add("limit=20");
            checkList.Add("orderDir=asc");
            checkList.Add("accountId=0");
            checkList.Add("contactId=0");
            checkList.Add("accountLogin=");
            checkList.Add("includeSystemUser=false");
            checkList.Add("followedCategoryId=0");
            checkList.Add("followedAdditionalPropertyId=0");
            checkList.Add("followedDocumentId=0");
            checkList.Add("followedLanguageId=0");
            checkList.Add("markedAsHelpfulDocumentId=0");
            checkList.Add("markedAsHelpfulDocumentLanguageVersionId=0");
            checkList.Add("markedAsHelpfulLanguageId=0");
            checkList.Add("responsibleForDocumentId=false");
            checkList.Add("authorshipForDocumentId=false");
            checkList.Add("authorshipForLanguageVersionId=false");
            checkList.Add("authorshipForLanguageId=false");
            checkList.Add("orderBy=lastName");
            checkList.Add("count=false");
            checkList.Add("loadClients=false");
            checkList.Add("loadAddresses=false");
            checkList.Add("loadCommunicationData=false");
            checkList.Add("loadRoles=false");
            checkList.Add("loadContactImages=false");
            checkList.Add("loadBioDocumentLanguageKey=en");

            p.DebugUrlParameter = true; CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);

            p = new GetPersonParams();
            checkList = new List<string>();
            p.Count = true;
            checkList.Add("count=true");
            p.LoadOptions.Clients = true;
            checkList.Add("loadClients=true");
            p.LoadOptions.Addresses = true;
            checkList.Add("loadAddresses=true");
            p.LoadOptions.CommunicationData = true;
            checkList.Add("loadCommunicationData=true");
            p.LoadOptions.Roles = true;
            checkList.Add("loadRoles=true");
            p.LoadOptions.ContactImages = true;
            checkList.Add("loadContactImages=true");
            p.LoadOptions.BioDocumentLanguageKey = LangKey.es;
            checkList.Add("loadBioDocumentLanguageKey=es");
            p.RoleId = 1;
            checkList.Add("roleId=1");
            p.InstanceOwner = ParamHideValue.Exclude;
            checkList.Add("hideInstanceOwner=true");
            p.DemoUser = ParamHideValue.Exclude;
            checkList.Add("hideDemo=true");
            p.LimitedUser = ParamHideValue.Exclude;
            checkList.Add("hideLimited=true");
            p.Active = ParamHideValue.Exclude;
            checkList.Add("hideActive=true");
            p.Offset = 1;
            checkList.Add("offset=1");
            p.Limit = 10;
            checkList.Add("limit=10");
            p.OrderDir = OrderDirection.desc;
            checkList.Add("orderDir=desc");
            p.AccountId = 1;
            checkList.Add("accountId=1");
            p.ContactId = 1;
            checkList.Add("contactId=1");
            p.AccountLogin = "test";
            checkList.Add("accountLogin=test");
            p.IncludeSystemUser = true;
            checkList.Add("includeSystemUser=true");
            p.FollowedCategoryId = 1;
            checkList.Add("followedCategoryId=1");
            p.FollowedAdditionalPropertyId = 1;
            checkList.Add("followedAdditionalPropertyId=1");
            p.FollowedDocumentId = 1;
            checkList.Add("followedDocumentId=1");
            p.FollowedLanguageId = 1;
            checkList.Add("followedLanguageId=1");
            p.MarkedAsHelpfulDocumentId = 1;
            checkList.Add("markedAsHelpfulDocumentId=1");
            p.MarkedAsHelpfulDocumentLanguageVersionId = 1;
            checkList.Add("markedAsHelpfulDocumentLanguageVersionId=1");
            p.MarkedAsHelpfulLanguageId = 1;
            checkList.Add("markedAsHelpfulLanguageId=1");
            p.ResponsibleForDocumentId = true;
            checkList.Add("responsibleForDocumentId=true");
            p.AuthorshipForDocumentId = true;
            checkList.Add("authorshipForDocumentId=true");
            p.AuthorshipForLanguageVersionId = true;
            checkList.Add("authorshipForLanguageVersionId=true");
            p.AuthorshipForLanguageId = true;
            checkList.Add("authorshipForLanguageId=true");
            p.OrderBy = PersonOrderBy.CreateTime;
            checkList.Add("orderBy=createTime");
            CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);
        }

        [Test]
        public void PersonAutocompleteParams()
        {
            List<string> checkList = new();
            string errMsg;
            PersonAutocompleteParams p = new();

            //Default test (no properties set)
            p = new PersonAutocompleteParams();
            checkList = new List<string>();
            CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);

            //Force all parameters
            p = new PersonAutocompleteParams();
            checkList = new List<string>();
            checkList.Add("roleId=0");
            checkList.Add("hideInstanceOwner=false");
            checkList.Add("hideDemo=false");
            checkList.Add("hideLimited=false");
            checkList.Add("hideActive=false");
            p.DebugUrlParameter = true; CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);

            p = new PersonAutocompleteParams();
            checkList = new List<string>();
            p.RoleId = 1;
            checkList.Add("roleId=1");
            p.InstanceOwner = ParamHideValue.Exclude;
            checkList.Add("hideInstanceOwner=true");
            p.DemoUser = ParamHideValue.Exclude;
            checkList.Add("hideDemo=true");
            p.LimitedUser = ParamHideValue.Exclude;
            checkList.Add("hideLimited=true");
            p.Active = ParamHideValue.Exclude;
            checkList.Add("hideActive=true");
            CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);
        }

        [Test]
        public void PersonExportParams()
        {
            List<string> checkList = new();
            string errMsg;
            PersonExportParams p = new();

            //Default test (no properties set)
            p = new PersonExportParams();
            checkList = new List<string>();
            checkList.Add("offset=0");
            checkList.Add("limit=" + Matterial.DefaultSearchResultLimit);
            CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);

            //Force all parameters
            p = new PersonExportParams();
            checkList = new List<string>();
            checkList.Add("roleId=0");
            checkList.Add("hideInstanceOwner=false");
            checkList.Add("hideDemo=false");
            checkList.Add("hideLimited=false");
            checkList.Add("hideActive=false");
            checkList.Add("offset=0");
            checkList.Add("limit=20");
            checkList.Add("orderDir=asc");
            checkList.Add("accountId=0");
            checkList.Add("contactId=0");
            checkList.Add("accountLogin=");
            checkList.Add("includeSystemUser=false");
            checkList.Add("followedCategoryId=0");
            checkList.Add("followedAdditionalPropertyId=0");
            checkList.Add("followedDocumentId=0");
            checkList.Add("followedLanguageId=0");
            checkList.Add("markedAsHelpfulDocumentId=0");
            checkList.Add("markedAsHelpfulDocumentLanguageVersionId=0");
            checkList.Add("markedAsHelpfulLanguageId=0");
            checkList.Add("responsibleForDocumentId=false");
            checkList.Add("authorshipForDocumentId=false");
            checkList.Add("authorshipForLanguageVersionId=false");
            checkList.Add("authorshipForLanguageId=false");
            checkList.Add("orderBy=lastName");

            p.DebugUrlParameter = true; CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);

            p = new PersonExportParams();
            checkList = new List<string>();
            p.RoleId = 1;
            checkList.Add("roleId=1");
            p.InstanceOwner = ParamHideValue.Exclude;
            checkList.Add("hideInstanceOwner=true");
            p.DemoUser = ParamHideValue.Exclude;
            checkList.Add("hideDemo=true");
            p.LimitedUser = ParamHideValue.Exclude;
            checkList.Add("hideLimited=true");
            p.Active = ParamHideValue.Exclude;
            checkList.Add("hideActive=true");
            p.Offset = 1;
            checkList.Add("offset=1");
            p.Limit = 10;
            checkList.Add("limit=10");
            p.OrderDir = OrderDirection.desc;
            checkList.Add("orderDir=desc");
            p.AccountId = 1;
            checkList.Add("accountId=1");
            p.ContactId = 1;
            checkList.Add("contactId=1");
            p.AccountLogin = "test";
            checkList.Add("accountLogin=test");
            p.IncludeSystemUser = true;
            checkList.Add("includeSystemUser=true");
            p.FollowedCategoryId = 1;
            checkList.Add("followedCategoryId=1");
            p.FollowedAdditionalPropertyId = 1;
            checkList.Add("followedAdditionalPropertyId=1");
            p.FollowedDocumentId = 1;
            checkList.Add("followedDocumentId=1");
            p.FollowedLanguageId = 1;
            checkList.Add("followedLanguageId=1");
            p.MarkedAsHelpfulDocumentId = 1;
            checkList.Add("markedAsHelpfulDocumentId=1");
            p.MarkedAsHelpfulDocumentLanguageVersionId = 1;
            checkList.Add("markedAsHelpfulDocumentLanguageVersionId=1");
            p.MarkedAsHelpfulLanguageId = 1;
            checkList.Add("markedAsHelpfulLanguageId=1");
            p.ResponsibleForDocumentId = true;
            checkList.Add("responsibleForDocumentId=true");
            p.AuthorshipForDocumentId = true;
            checkList.Add("authorshipForDocumentId=true");
            p.AuthorshipForLanguageVersionId = true;
            checkList.Add("authorshipForLanguageVersionId=true");
            p.AuthorshipForLanguageId = true;
            checkList.Add("authorshipForLanguageId=true");
            p.OrderBy = PersonOrderBy.CreateTime;
            checkList.Add("orderBy=createTime");
            CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);
        }

        [Test]
        public void SearchPersonParams()
        {
            List<string> checkList = new();
            string errMsg;
            SearchPersonParams p = new();

            //Default test (no properties set)
            p = new SearchPersonParams();
            checkList = new List<string>();
            checkList.Add("offset=0");
            checkList.Add("limit=" + Matterial.DefaultSearchResultLimit);
            CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);

            p = new SearchPersonParams();
            checkList = new List<string>();
            checkList.Add("roleId=0");
            checkList.Add("hideInstanceOwner=false");
            checkList.Add("hideDemo=false");
            checkList.Add("hideLimited=false");
            checkList.Add("hideActive=false");
            checkList.Add("offset=0");
            checkList.Add("limit=20");
            checkList.Add("orderDir=asc");
            checkList.Add("orderBy=lastName");

            p.DebugUrlParameter = true; CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);

            p = new SearchPersonParams();
            checkList = new List<string>();
            p.RoleId = 1;
            checkList.Add("roleId=1");
            p.InstanceOwner = ParamHideValue.Exclude;
            checkList.Add("hideInstanceOwner=true");
            p.DemoUser = ParamHideValue.Exclude;
            checkList.Add("hideDemo=true");
            p.LimitedUser = ParamHideValue.Exclude;
            checkList.Add("hideLimited=true");
            p.Active = ParamHideValue.Exclude;
            checkList.Add("hideActive=true");
            p.Offset = 1;
            checkList.Add("offset=1");
            p.Limit = 10;
            checkList.Add("limit=10");
            p.OrderDir = OrderDirection.desc;
            checkList.Add("orderDir=desc");
            p.OrderBy = PersonSearchOrderBy.Id;
            checkList.Add("orderBy=id");
            CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);
        }

        [Test]
        public void ActivityParams()
        {
            List<string> checkList = new();
            string errMsg;
            ActivityParams p = new();

            //Default test (no properties set)
            p = new ActivityParams();
            checkList = new List<string>();
            checkList.Add("offset=0");
            checkList.Add("limit=" + Matterial.DefaultSearchResultLimit);
            CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);

            //force all params
            p = new ActivityParams();
            checkList = new List<string>();
            checkList.Add("languageKey=en");
            checkList.Add("offset=0");
            checkList.Add("limit=20");
            checkList.Add("messageType=");
            checkList.Add("personAccountId=0");
            checkList.Add("documentId=0");
            checkList.Add("documentLanguageVersionId=0");
            checkList.Add("documentLanguageVersionLanguageKey=");
            checkList.Add("taskId=0");
            checkList.Add("notificationStatus=-1");
            checkList.Add("count=false");
            checkList.Add("orderBy=date");
            checkList.Add("orderDir=asc");
            p.DebugUrlParameter = true; CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);

            //set params
            p = new ActivityParams();
            checkList = new List<string>();
            p.Offset = 1;
            checkList.Add("offset=1");
            p.Limit = 10;
            checkList.Add("limit=10");
            p.Count = true;
            checkList.Add("count=true");
            p.OrderDir = OrderDirection.desc;
            checkList.Add("orderDir=desc");
            p.OrderBy = ActivityOrderBy.Id;
            checkList.Add("orderBy=id");
            p.DocumentId = 2;
            checkList.Add("documentId=2");
            p.DocumentLanguageVersionId = 1;
            checkList.Add("documentLanguageVersionId=1");
            p.LanguageKey = LangKey.es;
            checkList.Add("languageKey=es");
            p.MessageType = ActivityMessageType.Document_Read;
            checkList.Add("messageType=DOCUMENT_READ");
            p.PersonAccountId = 1;
            checkList.Add("personAccountId=1");
            p.DocumentLanguageVersionLanguageKey = "es";
            checkList.Add("documentLanguageVersionLanguageKey=es");
            p.TaskId = 5;
            checkList.Add("taskId=5");
            p.NotificationStatus = NotificationStatuses.Status_Opened;
            checkList.Add("notificationStatus=3");
            p.DebugUrlParameter = true; CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);
        }

        [Test]
        public void CategoryByIdBaseParams()
        {
            List<string> checkList = new();
            string errMsg;
            CategoryByIdBaseParams p = new();

            //Default test (no properties set)
            p = new CategoryByIdBaseParams();
            checkList = new List<string>();
            checkList.Add("languageKey=en");
            CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);

            //all params explicit
            p = new CategoryByIdBaseParams();
            checkList = new List<string>();
            checkList.Add("languageKey=en");
            checkList.Add("loadUsage=false");
            checkList.Add("loadFollowingLanguageKey=en");

            p.DebugUrlParameter = true; CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);

            //set some properties
            p = new CategoryByIdBaseParams();
            checkList = new List<string>();
            p.LanguageKey = LangKey.fr;
            checkList.Add("languageKey=fr");
            p.LoadUsage = true;
            checkList.Add("loadUsage=true");
            p.LoadFollowingLanguageKey = LangKey.es;
            checkList.Add("loadFollowingLanguageKey=es");
            CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);
        }

        [Test]
        public void CategoryByTypeIdParams()
        {
            List<string> checkList = new();
            string errMsg;
            CategoryByTypeIdParams p = new();

            //Default test (no properties set)
            p = new CategoryByTypeIdParams();
            checkList = new List<string>();
            checkList.Add("languageKey=en");
            checkList.Add("offset=0");
            checkList.Add("limit=" + Matterial.DefaultSearchResultLimit);
            //checkList.Add("categoryId=0");
            //checkList.Add("categoryId=0");
            CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);

            //all params explicit
            p = new CategoryByTypeIdParams();
            checkList = new List<string>();
            checkList.Add("languageKey=en");
            checkList.Add("loadUsage=false");
            checkList.Add("loadFollowingLanguageKey=en");
            checkList.Add("offset=0");
            checkList.Add("limit=" + Matterial.DefaultSearchResultLimit);
            checkList.Add("orderBy=name");
            checkList.Add("orderDir=asc");
            checkList.Add("quick=debug");
            p.DebugUrlParameter = true; CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);

            //set some properties
            p = new CategoryByTypeIdParams();
            checkList = new List<string>();
            p.LanguageKey = LangKey.fr;
            checkList.Add("languageKey=fr");
            p.LoadUsage = true;
            checkList.Add("loadUsage=true");
            p.LoadFollowingLanguageKey = LangKey.es;
            checkList.Add("loadFollowingLanguageKey=es");
            p.Offset = 1;
            checkList.Add("offset=1");
            p.Limit = 10;
            checkList.Add("limit=10");
            p.OrderBy = CategoryOrderBy.Id;
            checkList.Add("orderBy=id");
            p.OrderDir = OrderDirection.desc;
            checkList.Add("orderDir=desc");
            p.Quick = ParamHideBoolValue.Exclusive;
            checkList.Add("quick=true");
            CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);
        }

        [Test]
        public void CategoryParams()
        {
            List<string> checkList = new();
            string errMsg;
            CategoryParams p = new();

            //Default test (no properties set)
            p = new CategoryParams();
            checkList = new List<string>();
            checkList.Add("languageKey=en");
            checkList.Add("offset=0");
            checkList.Add("limit=" + Matterial.DefaultSearchResultLimit);
            //checkList.Add("categoryId=0");
            //checkList.Add("categoryId=0");
            CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);

            //all params explicit
            p = new CategoryParams();
            checkList = new List<string>();
            checkList.Add("languageKey=en");
            checkList.Add("loadUsage=false");
            checkList.Add("loadFollowingLanguageKey=en");
            checkList.Add("offset=0");
            checkList.Add("limit=" + Matterial.DefaultSearchResultLimit);
            checkList.Add("orderBy=name");
            checkList.Add("orderDir=asc");
            checkList.Add("personal=debug");
            checkList.Add("categoryId=0");
            checkList.Add("categoryTypeId=0");
            checkList.Add("quick=debug");
            p.DebugUrlParameter = true; CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);

            //set some properties
            p = new CategoryParams();
            checkList = new List<string>();
            p.LanguageKey = LangKey.fr;
            checkList.Add("languageKey=fr");
            p.LoadUsage = true;
            checkList.Add("loadUsage=true");
            p.LoadFollowingLanguageKey = LangKey.es;
            checkList.Add("loadFollowingLanguageKey=es");
            p.Offset = 1;
            checkList.Add("offset=1");
            p.Limit = 10;
            checkList.Add("limit=10");
            p.OrderBy = CategoryOrderBy.Id;
            checkList.Add("orderBy=id");
            p.OrderDir = OrderDirection.desc;
            checkList.Add("orderDir=desc");
            p.Personal = ParamHideBoolValue.Exclude;
            checkList.Add("personal=false");
            p.CategoryId = 1;
            checkList.Add("categoryId=1");
            p.CategoryTypeId = 1;
            checkList.Add("categoryTypeId=1");
            p.Quick = ParamHideBoolValue.Exclusive;
            checkList.Add("quick=true");
            CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);
        }

        [Test]
        public void CategoryTypeParams()
        {
            List<string> checkList = new();
            string errMsg;
            CategoryTypeParams p = new();

            //Default test (no properties set)
            p = new CategoryTypeParams();
            checkList = new List<string>();
            checkList.Add("languageKey=en");
            CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);

            //all params explicit
            p = new CategoryTypeParams();
            checkList = new List<string>();
            checkList.Add("personal=debug");
            checkList.Add("categoryTypeId=0");
            checkList.Add("quick=debug");
            checkList.Add("orderBy=name");
            checkList.Add("orderDir=asc");
            checkList.Add("languageKey=en");
            p.DebugUrlParameter = true; CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg, true);
            Assert.That(errMsg, Is.Empty, errMsg);

            //set some properties
            p = new CategoryTypeParams();
            checkList = new List<string>();
            p.LanguageKey = LangKey.fr;
            checkList.Add("languageKey=fr");
            p.OrderBy = CategoryOrderBy.Id;
            checkList.Add("orderBy=id");
            p.OrderDir = OrderDirection.desc;
            checkList.Add("orderDir=desc");
            p.Personal = ParamHideBoolValue.Exclude;
            checkList.Add("personal=false");
            p.CategoryTypeId = 1;
            checkList.Add("categoryTypeId=1");
            p.Quick = ParamHideBoolValue.Exclusive;
            checkList.Add("quick=true");
            CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);
        }

        [Test]
        public void ClientParams()
        {
            List<string> checkList = new();
            string errMsg;
            ClientParams p = new();

            //Default test (no properties set)
            p = new ClientParams();
            checkList = new List<string>();
            checkList.Add("offset=0");
            checkList.Add("limit=" + Matterial.DefaultSearchResultLimit);
            CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);

            //force all params
            p = new ClientParams();
            checkList = new List<string>();
            checkList.Add("offset=0");
            checkList.Add("limit=20");
            checkList.Add("accountId=0");
            checkList.Add("loadClientPreferences=false");
            checkList.Add("orderBy=name");
            checkList.Add("orderDir=asc");
            p.DebugUrlParameter = true; CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);

            //set params
            p = new ClientParams();
            checkList = new List<string>();
            p.Offset = 1;
            checkList.Add("offset=1");
            p.Limit = 10;
            checkList.Add("limit=10");
            p.AccountId = 3;
            checkList.Add("accountId=3");
            p.LoadClientPreferences = true;
            checkList.Add("loadClientPreferences=true");
            p.OrderBy = ClientOrderBy.Id;
            checkList.Add("orderBy=id");
            p.OrderDir = OrderDirection.desc;
            checkList.Add("orderDir=desc");
            p.DebugUrlParameter = true; CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);
        }

        [Test]
        public void CommentParams()
        {
            List<string> checkList = new();
            string errMsg;
            CommentParams p = new();

            //Default test (no properties set)
            p = new CommentParams();
            checkList = new List<string>();
            checkList.Add("offset=0");
            checkList.Add("limit=" + Matterial.DefaultSearchResultLimit);
            CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);

            //force all params
            p = new CommentParams();
            checkList = new List<string>();
            checkList.Add("offset=0");
            checkList.Add("limit=20");
            checkList.Add("orderBy=createtime");
            checkList.Add("orderDir=asc");
            checkList.Add("count=false");
            checkList.Add("documentId=0");
            checkList.Add("contactId=0");
            checkList.Add("documentLanguageVersionId=0");
            checkList.Add("languageKey=en");
            checkList.Add("mentionedAccountId=0");
            p.DebugUrlParameter = true; CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);

            //set params
            p = new CommentParams();
            checkList = new List<string>();
            p.Offset = 1;
            checkList.Add("offset=1");
            p.Limit = 10;
            checkList.Add("limit=10");
            p.Count = true;
            checkList.Add("count=true");
            p.OrderBy = CommentOrderBy.Createtime;
            checkList.Add("orderBy=createtime");
            p.OrderDir = OrderDirection.desc;
            checkList.Add("orderDir=desc");
            p.DocumentId = 2;
            checkList.Add("documentId=2");
            p.ContactId = 2;
            checkList.Add("contactId=2");
            p.DocumentLanguageVersionId = 1;
            checkList.Add("documentLanguageVersionId=1");
            p.LanguageKey = LangKey.es;
            checkList.Add("languageKey=es");
            p.MentionedAccountId = 4;
            checkList.Add("mentionedAccountId=4");
            p.DebugUrlParameter = true; CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);
        }

        [Test]
        public void InstanceParams()
        {
            List<string> checkList = new();
            string errMsg;
            InstanceParams p = new();

            //Default test (no properties set)
            p = new InstanceParams();
            checkList = new List<string>();
            checkList.Add("offset=0");
            checkList.Add("limit=" + Matterial.DefaultSearchResultLimit);
            CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);

            //force all params
            p = new InstanceParams();
            checkList = new List<string>();
            checkList.Add("offset=0");
            checkList.Add("limit=20");
            checkList.Add("dataSourceId=0");
            checkList.Add("active=false");
            checkList.Add("dataSourceName=");
            checkList.Add("dataSourceReference=");
            checkList.Add("count=false");
            checkList.Add("orderBy=active");
            checkList.Add("orderDir=asc");
            p.DebugUrlParameter = true; CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);

            //set params
            p = new InstanceParams();
            checkList = new List<string>();
            p.Offset = 1;
            checkList.Add("offset=1");
            p.Limit = 10;
            checkList.Add("limit=10");
            p.DataSourceId = 3;
            checkList.Add("dataSourceId=3");
            p.Active = true;
            checkList.Add("active=true");
            p.DataSourceName = "test";
            checkList.Add("dataSourceName=test");
            p.DataSourceReference = "ref";
            checkList.Add("dataSourceReference=ref");
            p.Count = true;
            checkList.Add("count=true");
            p.OrderBy = InstanceOrderBy.DisplayName;
            checkList.Add("orderBy=displayName");
            p.OrderDir = OrderDirection.desc;
            checkList.Add("orderDir=desc");
            p.DebugUrlParameter = true; CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);
        }

        [Test]
        public void LanguageParams()
        {
            List<string> checkList = new();
            string errMsg;
            LanguageParams p = new();

            //Default test (no properties set)
            p = new LanguageParams();
            checkList = new List<string>();
            CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);

            //force all params
            p = new LanguageParams();
            checkList = new List<string>();
            checkList.Add("active=false");
            checkList.Add("orderBy=prio");
            checkList.Add("orderDir=asc");
            p.DebugUrlParameter = true; CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);

            //set params
            p = new LanguageParams();
            checkList = new List<string>();
            p.Active = true;
            checkList.Add("active=true");
            p.OrderDir = OrderDirection.desc;
            checkList.Add("orderDir=desc");
            p.OrderBy = LanguageOrderBy.Id;
            checkList.Add("orderBy=id");
            p.DebugUrlParameter = true; CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);
        }

        [Test]
        public void RoleParams()
        {
            List<string> checkList = new();
            string errMsg;
            RoleParams p = new();

            //Default test (no properties set)
            p = new RoleParams();
            checkList = new List<string>();
            checkList.Add("offset=0");
            checkList.Add("limit=" + Matterial.DefaultSearchResultLimit);
            CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);

            //force all params
            p = new RoleParams();
            checkList = new List<string>();
            checkList.Add("offset=0");
            checkList.Add("limit=20");
            checkList.Add("accountId=0");
            checkList.Add("entityTypeId=-1");
            checkList.Add("count=false");
            checkList.Add("orderBy=type");
            checkList.Add("orderDir=asc");
            p.DebugUrlParameter = true; CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg, true);
            Assert.That(errMsg, Is.Empty, errMsg);

            //set params
            p = new RoleParams();
            checkList = new List<string>();
            p.OrderDir = OrderDirection.desc;
            checkList.Add("orderDir=desc");
            p.OrderBy = RoleOrderBy.Id;
            checkList.Add("orderBy=id");
            p.EntityTypeIds.Add(RoleEntityType.Functional);
            checkList.Add("entityTypeId=21");
            p.Offset = 1;
            checkList.Add("offset=1");
            p.Limit = 10;
            checkList.Add("limit=10");
            p.AccountId = 9;
            checkList.Add("accountId=9");
            p.Count = true;
            checkList.Add("count=true");
            p.DebugUrlParameter = true; CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);
        }

        [Test]
        public void TaskParams()
        {
            List<string> checkList = new();
            string errMsg;
            TaskParams p = new();

            //Default test (no properties set)
            p = new TaskParams();
            checkList = new List<string>();
            checkList.Add("offset=0");
            checkList.Add("limit=" + Matterial.DefaultSearchResultLimit);
            CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);

            //force all params
            p = new TaskParams();
            checkList = new List<string>();
            checkList.Add("documentId=0");
            checkList.Add("documentLanguageVersionId=0");
            checkList.Add("authorAccountId=0");
            checkList.Add("accountId=0");
            checkList.Add("roleId=0");
            checkList.Add("hideSnap=false");
            checkList.Add("hideTasksOfRemovedDocuments=false");
            checkList.Add("statusId=1");
            checkList.Add("statusIdExclude=1");
            checkList.Add("orderBy=date");
            checkList.Add("orderDir=asc");
            checkList.Add("count=false");
            checkList.Add("offset=0");
            checkList.Add("limit=20");
            p.DebugUrlParameter = true; CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);

            //Set values
            p = new TaskParams();
            checkList = new List<string>();
            p.DocumentId = 22;
            checkList.Add("documentId=22");
            p.DocumentLanguageVersionId = 9;
            checkList.Add("documentLanguageVersionId=9");
            p.LanguageKey = LangKey.fr;
            checkList.Add("languageKey=fr");
            p.AuthorAccountId = 88;
            checkList.Add("authorAccountId=88");
            p.AccountId = 77;
            checkList.Add("accountId=77");
            p.RoleId = 66;
            checkList.Add("roleId=66");
            p.Snap = ParamHideValue.Exclusive;
            checkList.Add("showSnapOnly=true");
            p.HideTasksOfRemovedDocuments = true;
            checkList.Add("hideTasksOfRemovedDocuments=true");
            p.StatusIds.Add(3);
            checkList.Add("statusId=3");
            p.StatusIdExcludes.Add(4);
            checkList.Add("statusIdExclude=4");
            p.OrderBy = TaskOrderBy.Id;
            checkList.Add("orderBy=id");
            p.OrderDir = OrderDirection.desc;
            checkList.Add("orderDir=desc");
            p.Count = true;
            checkList.Add("count=true");
            p.Offset = 1;
            checkList.Add("offset=1");
            p.Limit = 10;
            checkList.Add("limit=10");
            CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);
        }

        [Test]
        public void TaskStatusParams()
        {
            List<string> checkList = new();
            string errMsg;
            TaskStatusParams p = new();

            //Default test (no properties set)
            p = new TaskStatusParams();
            checkList = new List<string>();
            checkList.Add("languageKey=en");
            checkList.Add("orderBy=type");
            CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);

            //force all params
            p = new TaskStatusParams();
            checkList = new List<string>();
            checkList.Add("orderBy=type");
            checkList.Add("orderDir=asc");
            checkList.Add("languageKey=en");
            p.DebugUrlParameter = true; CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg, true);
            Assert.That(errMsg, Is.Empty, errMsg);

            //set params
            p = new TaskStatusParams();
            checkList = new List<string>();
            p.OrderDir = OrderDirection.desc;
            checkList.Add("orderDir=desc");
            p.OrderBy = TaskStatusOrderBy.Id;
            checkList.Add("orderBy=id");
            p.LanguageKey = LangKey.es;
            checkList.Add("languageKey=es");
            p.DebugUrlParameter = true; CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);
        }

        [Test]
        public void TrackingParams()
        {
            List<string> checkList = new();
            string errMsg;
            TrackingParams p = new();

            //Default test (no properties set)
            p = new TrackingParams();
            checkList = new List<string>();
            checkList.Add("offset=0");
            checkList.Add("limit=" + Matterial.DefaultSearchResultLimit);
            CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);

            //force all params
            p = new TrackingParams();
            checkList = new List<string>();
            checkList.Add("offset=0");
            checkList.Add("limit=20");
            checkList.Add("orderBy=date");
            checkList.Add("orderDir=asc");
            checkList.Add("count=false");
            p.DebugUrlParameter = true; CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);

            //set params
            p = new TrackingParams();
            checkList = new List<string>();
            p.Offset = 1;
            checkList.Add("offset=1");
            p.Limit = 10;
            checkList.Add("limit=10");
            p.Count = true;
            checkList.Add("count=true");
            p.OrderBy = TrackingOrderBy.Id;
            checkList.Add("orderBy=id");
            p.OrderDir = OrderDirection.desc;
            checkList.Add("orderDir=desc");
            p.DebugUrlParameter = true; CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);
        }

        [Test]
        public void TrackingSearchParams()
        {
            List<string> checkList = new();
            string errMsg;
            TrackingSearchParams p = new();

            //Default test (no properties set)
            p = new TrackingSearchParams();
            checkList = new List<string>();
            checkList.Add("offset=0");
            checkList.Add("limit=" + Matterial.DefaultSearchResultLimit);
            CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);

            //force all params
            p = new TrackingSearchParams();
            checkList = new List<string>();
            checkList.Add("offset=0");
            checkList.Add("limit=20");
            checkList.Add("orderBy=date");
            checkList.Add("orderDir=asc");
            p.DebugUrlParameter = true; CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg, true);
            Assert.That(errMsg, Is.Empty, errMsg);

            //set params
            p = new TrackingSearchParams();
            checkList = new List<string>();
            p.Offset = 1;
            checkList.Add("offset=1");
            p.Limit = 10;
            checkList.Add("limit=10");
            p.OrderBy = TrackingOrderBy.Id;
            checkList.Add("orderBy=id");
            p.OrderDir = OrderDirection.desc;
            checkList.Add("orderDir=desc");
            p.DebugUrlParameter = true; CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);
        }

        [Test]
        public void UpdatePersonParams()
        {
            List<string> checkList = new();
            string errMsg;
            UpdatePersonParams p = new();

            //Default test (no properties set)
            p = new UpdatePersonParams();
            checkList = new List<string>();
            CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);

            //force all params
            p = new UpdatePersonParams();
            checkList = new List<string>();
            checkList.Add("ignoreAddreses=false");
            checkList.Add("ignoreClients=false");
            checkList.Add("ignoreCommuncationData=false");
            checkList.Add("ignoreContactImages=false");
            p.DebugUrlParameter = true; CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg, true);
            Assert.That(errMsg, Is.Empty, errMsg);

            //set params
            p = new UpdatePersonParams();
            checkList = new List<string>();
            p.IgnoreAddreses = true;
            checkList.Add("ignoreAddreses=true");
            p.IgnoreClients = true;
            checkList.Add("ignoreClients=true");
            p.IgnoreCommuncationData = true;
            checkList.Add("ignoreCommuncationData=true");
            p.IgnoreContactImages = true;
            checkList.Add("ignoreContactImages=true");
            p.DebugUrlParameter = true; CheckUrlParams(p.UrlParameter().ToString(), checkList, out errMsg);
            Assert.That(errMsg, Is.Empty, errMsg);
        }
    }
}