﻿using MatterialCore.Entity;
using MatterialCore.Interfaces;

using NUnit.Framework;

using MatterialCore.Tests.Helper;

namespace MatterialCore.Tests.Integration
{
    internal class Indexing : TestBase
    {
        #region Setup

        [OneTimeSetUp] //once per class
        public void Setup()
        {
        }

        [OneTimeTearDown] //once per class
        public void TearDown()
        {
        }

        [SetUp] //once per test
        public void SetupTest()
        {
        }

        [TearDown] //once per test
        public void TearDownTest()
        {
        }

        #endregion Setup

        [Test]
        public void IndexDocumentTest()
        {
            //Create controller for document
            using TestSession cf = new();
            IDocument myController = cf.ApiSession.Document;
            Document createdDoc = TestHelper.CreateDefaultDoc(this, myController);
            myController.IndexDocument(createdDoc.Id);
        }

        [Test]
        public void IndexDocumenLanguageVersionTest()
        {
            //Create controller for document
            using TestSession cf = new();
            IDocument myController = cf.ApiSession.Document;
            Document createdDoc = TestHelper.CreateDefaultDoc(this, myController);
            myController.IndexDocument(createdDoc.Id, createdDoc.LanguageVersionId);
        }

        [Test]
        public void ReindexPersons()
        {
            //Create controller
            using TestSession cf = new();
            IPerson myController = cf.ApiSession.Person;
            Assert.That(myController, Is.Not.Null);

            myController.ReindexPersons();
        }

        [Test]
        public void ReindexDocuments()
        {
            //Create controller for document
            using TestSession cf = new();
            IDocument myController = cf.ApiSession.Document;
            myController.ReindexAll();
        }

        [Test]
        public void ReindexTrackingItems()
        {
            //Create controller for document
            using TestSession cf = new();
            ITrackingItem myController = cf.ApiSession.TrackingItem;
            myController.Reindex();
        }
    }
}