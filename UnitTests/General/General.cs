using MatterialCore;
using MatterialCore.Entity;
using MatterialCore.Enums;
using MatterialCore.Interfaces;
using MatterialCore.Params;
using MatterialCore.Tests;

using NUnit.Framework;

using System.Collections.Generic;
using System.IO;

using MatterialCore.Tests.Helper;

namespace MatterialCore.Tests.Integration
{
    internal class General : TestBase
    {
        #region Setup

        [OneTimeSetUp] //once per class
        public void Setup()
        {
        }

        [OneTimeTearDown] //once per class
        public void TearDown()
        {
        }

        [SetUp] //once per test
        public void SetupTest()
        {
        }

        [TearDown] //once per test
        public void TearDownTest()
        {
        }

        #endregion Setup

        #region MultiTest

        //[Test]
        //public void DoS()
        //{
        //    UserCredentials _credentials = new UserCredentials();
        //    int numLoops = 1000;

        //    for (int i = 0; i < numLoops; i++)
        //    {
        //        try
        //        {
        //            _ = Matterial.Connect("DoS-Test", _credentials.Password, _credentials.ServerUrl);
        //        }
        //        catch { }
        //    }
        //    if (Debugger.IsAttached)
        //    {
        //        Debugger.Break();
        //    }
        //}

        #endregion MultiTest

        [Test]
        [Ignore("Not executed")]
        public void SendLoginLink()
        {
            bool b = Matterial.SendLoginLink("myuser@mydomain.com", Matterial.DefaultLanguage);
            Assert.That(b, Is.True);
        }

        [Test]
        public void Connect()
        {
            using Session session = new();
            IApiSession apiSession = session.CreateSession();
            Assert.That(apiSession.IsLoggedIn, Is.True, "The connect failed.");
        }

        [Test]
        public void ConnectFail()
        {
            //Test where login should fail
            UserCredentials userCred = new(BaseConfig.ApiUserName, "1234");
            try
            {
                using Session session = new(userCred);
                IApiSession apiSession = session.CreateSession();
                Assert.That(apiSession.IsLoggedIn, Is.False, "The connect succeeded but it should have failed.");
            }
            catch
            {
            }
        }

        [Test]
        public void GetLoginData()
        {
            using Session session = new();
            IApiSession apiSession = session.CreateSession();
            Assert.That(apiSession.IsLoggedIn, Is.True, "The connect failed.");
            LoginData ld = apiSession.LoginData().Result;
            Assert.That(ld, Is.Not.Null);
            Assert.Multiple(() =>
            {
                Assert.That(ld.Client.Name, Is.EqualTo("DEFAULT"));
                Assert.That(ld.AvailableInstances, Is.Not.Empty, "No datasources found.");
            });
        }

        [Test]
        public void Conversion()
        {
            using Session session = new();
            IApiSession apiSession = session.CreateSession();
            Assert.That(apiSession.IsLoggedIn, Is.True, "The connect failed.");
            string mdFile = Path.Combine(BaseConfig.TestFilesDir(), "default.md");
            string md = File.ReadAllText(mdFile);
            string html = apiSession.Convert(ConversionAction.MarkdownToHtml, md).Result;
            File.WriteAllText(Path.Combine(BaseConfig.WorkFolder(), "Test.html"), html);
            //Now backwards
            string md2 = apiSession.Convert(ConversionAction.HtmlToMarkdown, html).Result;
            File.WriteAllText(Path.Combine(BaseConfig.WorkFolder(), "Test.md"), md2);
        }

        [Test]
        public void GetLicenseUsage()
        {
            using TestSession cf = new();
            ILicense myController = cf.ApiSession.License;
            LicenseUsage usage = myController.GetUsage().Result;
            Assert.That(usage, Is.Not.Null);
        }

        [Test]
        public void GetActivities()
        {
            using TestSession cf = new();
            IActivity myController = cf.ApiSession.Activity;
            ListResult<Activity> list = myController.Load(new ActivityParams()).Result;
            Assert.That(list, Is.Not.Null);
        }

        [Test]
        public void GetActivityById()
        {
            using TestSession cf = new();
            IActivity myController = cf.ApiSession.Activity;
            ListResult<Activity> list = myController.Load(new ActivityParams()).Result;
            Assert.That(list, Is.Not.Null);
            Assert.That(list.Results, Is.Not.Empty);
            long id = list.Results[0].Id;
            Activity a = myController.Load(id).Result;
            Assert.That(a, Is.Not.Null);
        }

        [Test]
        public void UnconstraintPreferenceMap()
        {
            using TestSession cf = new();
            IInstance c = cf.ApiSession.Instance;
            PreferenceMapContainer m = c.GetUnrestrictedPreferences().Result;
            Assert.That(m, Is.Not.Null);
            Assert.That(m.Preferences, Is.Not.Empty, "The preference map container is empty!");
        }

        [Test]
        [TestCase(UnrestrictedPreferences.Demo_Host_Name)]
        [TestCase(UnrestrictedPreferences.Inhouse_Enabled)]
        [TestCase(UnrestrictedPreferences.NTML_Supported)]
        [TestCase(UnrestrictedPreferences.OAuth_Cobot_Enabled)]
        [TestCase(UnrestrictedPreferences.OAuth_Google_Enabled)]
        [TestCase(UnrestrictedPreferences.OAuth_Microsoft_Enabled)]
        public void UnconstraintPreferenceKey(UnrestrictedPreferences pref)
        {
            using TestSession cf = new();
            IInstance c = cf.ApiSession.Instance;
            string prefValue = c.GetUnrestrictedPreference(pref).Result;
            Assert.That(string.IsNullOrEmpty(prefValue), Is.False, "The value pf the preference os null");
        }

        [Test]
        public void ChangeClient()
        {
            using TestSession cf = new();
            IClient cc = cf.ApiSession.Client;
            List<Client> clients = cc.Load(new ClientParams()).Result;
            LoginData ld = cf.ApiSession.ChangeClient(clients[0].Id).Result;
        }

        [Test]
        public void ChangePassword()
        {
            using Session session = new();
            IApiSession apiSession = session.CreateSession();
            UserCredentials cred = new();
            string newPwd = "newPwd";
            PasswordContainer pwc = new(BaseConfig.ApiUserPassword, newPwd, newPwd);
            LoginData ld = apiSession.ChangePassword(pwc).Result;
            pwc = new PasswordContainer(newPwd, BaseConfig.ApiUserPassword, BaseConfig.ApiUserPassword);
            ld = apiSession.ChangePassword(pwc).Result;
        }

        [Test]
        public void RightsCheck()
        {
            using Session session = new();
            IApiSession apiSession = session.CreateSession();
            bool b = apiSession.DisableRightsCheck().Result;
            Assert.That(b, Is.True);
            b = apiSession.EnableRightsCheck().Result;
            Assert.That(b, Is.True);
        }

        [Test]
        public void DocumentIndexingEnable()
        {
            using Session session = new();
            IApiSession apiSession = session.CreateSession();
            bool b = apiSession.DisableDocumentIndexing().Result;
            Assert.That(b, Is.True);
            b = apiSession.EnableDocumentIndexing().Result;
            Assert.That(b, Is.True);
        }

        [Test]
        public void PersonIndexingEnable()
        {
            using Session session = new();
            IApiSession apiSession = session.CreateSession();
            bool b = apiSession.DisablePersonIndexing().Result;
            Assert.That(b, Is.True);
            b = apiSession.EnablePersonIndexing().Result;
            Assert.That(b, Is.True);
        }

        [Test]
        public void ChangeInstance()
        {
            using Session session = new();
            IApiSession apiSession = session.CreateSession();
            //Set available instances+		AccountSettings	Count = 27	System.Collections.Generic.Dictionary<string, object>

            LoginData ld = apiSession.LoginData().Result;
            Assert.That(ld.AvailableInstances, Has.Count.GreaterThan(1), "There is only one instance available.");

            _ = apiSession.ChangeInstance(ld.AvailableInstances[1].Id);
            //Change back
            _ = apiSession.ChangeInstance(ld.AvailableInstances[0].Id);
        }

        [Test]
        public void UpdateAccountSettings()
        {
            using Session session = new();
            IApiSession apiSession = session.CreateSession();
            //Load current settings
            LoginData ld = apiSession.LoginData().Result;
            LangKey content = apiSession.DefaultContentLanguage;
            LangKey ui = apiSession.DefaultUiLanguage;
            //Update Assount settings
            ld.AccountSettings["accountSetting.language.content"] = LangKey.es.ToLangString();
            ld.AccountSettings["accountSetting.language.ui"] = LangKey.es.ToLangString();
            apiSession.UpdateAccountSettings(ld.AccountSettings).Wait();

            ld = apiSession.LoginData(true).Result;
            Assert.Multiple(() =>
            {
                Assert.That(apiSession.DefaultContentLanguage, Is.EqualTo(LangKey.es));
                Assert.That(apiSession.DefaultUiLanguage, Is.EqualTo(LangKey.es));
            });

            //Store back
            ld.AccountSettings["accountSetting.language.content"] = content.ToLangString();
            ld.AccountSettings["accountSetting.language.ui"] = ui.ToLangString();
            apiSession.UpdateAccountSettings(ld.AccountSettings).Wait();
            ld = apiSession.LoginData(true).Result;
            Assert.Multiple(() =>
            {
                Assert.That(apiSession.DefaultContentLanguage, Is.EqualTo(content));
                Assert.That(apiSession.DefaultUiLanguage, Is.EqualTo(ui));
            });
        }

        [Test]
        public void SetDefaultLanguageSettings()
        {
            using Session session = new();
            IApiSession apiSession = session.CreateSession();
            LangKey content = apiSession.DefaultContentLanguage;
            LangKey ui = apiSession.DefaultUiLanguage;
            apiSession.DefaultContentLanguage = LangKey.fr;
            apiSession.DefaultUiLanguage = LangKey.fr;
            LoginData ld = apiSession.LoginData(true).Result;
            Assert.Multiple(() =>
            {
                Assert.That(apiSession.DefaultContentLanguage, Is.EqualTo(LangKey.fr));
                Assert.That(apiSession.DefaultUiLanguage, Is.EqualTo(LangKey.fr));
            });
            apiSession.DefaultContentLanguage = content;
            apiSession.DefaultUiLanguage = ui;
        }

        [Test]
        public void Version()
        {
            Matterial.BasePath = BaseConfig.DefaultServerUrl;
            MtrVersion version = Matterial.Version();
            Assert.That(version.Major, Is.GreaterThan(0));
        }


        [Test]
        public void ConnectMS_OAuth()
        {
            //using Session session = new Session();
            //IApiSession apiSession = session.CreateMSOAuthSession();

        }
    }
}