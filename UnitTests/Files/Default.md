# AccountSetting
Account settings contain all settings related to a user account. They are stored as a key-value pair.

[Json Schema](https://gitlab.com/matterial/matterial-api/-/blob/develop/src/resources/json-schema/AccountSetting.json)
[Json Example](https://gitlab.com/matterial/matterial-api/-/blob/develop/src/resources/json-sample/AccountSetting.json)
[Xml Example](https://gitlab.com/matterial/matterial-api/-/blob/develop/src/resources/xml-sample/AccountSetting.xml)
[Java](https://gitlab.com/matterial/matterial-api/-/blob/develop/src/main/java/com/matterial/mtr/api/object/AccountSetting.java)


| Property | Data type | Min/Max | Updateable / Required on create |
| --- | --- | --- | --- |
| key | String |  | No/No |
| value | Object |  | No/No |


