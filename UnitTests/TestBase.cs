﻿using MatterialCore.Entity;
using MatterialCore.Enums;
using MatterialCore.Interfaces;
using MatterialCore.Params;
using MatterialCore.Params.Doc;

using System;
using System.Collections.Generic;
using System.Diagnostics;

using MatterialCore.Tests.Helper;

namespace MatterialCore.Tests
{
    public abstract class TestBase : IDisposable
    {
        protected List<EntityBase> CreatedEntities = new();

        public virtual void AddToCreated(EntityBase entity)
        {
            if (CreatedEntities is null)
            {
                CreatedEntities = new List<EntityBase>();
            }
            if (!(entity is null))
            {
                CreatedEntities.Add(entity);
            }
        }

        public virtual void AddToCreated(List<Document> entityList)
        {
            foreach (var item in entityList)
            {
                AddToCreated(item);
            }
        }

        public void CleanUp()
        {
            using Session session = new();
            IApiSession apiSession = session.CreateSession();
            IDocument c = apiSession.Document;
            //delete all created docs
            foreach (var item in CreatedEntities)
            {
                if (item is Document)
                {
                    try
                    {
                        long docId = ((Document)item).Id;
                        _ = c.UnlockByDocId(docId).Result;
                        _ = c.Remove(docId, true).Result;
                    }
                    catch
                    {
                    }
                }
            }
            IProperty p = apiSession.Property;
            //delete all created AdditionalProperties
            foreach (var item in CreatedEntities)
            {
                if (item is AdditionalProperty)
                {
                    _ = p.Remove(((AdditionalProperty)item).Id).Result;
                }
            }

            //Roles
            using TestSession cf = new();
            IRole r = cf.ApiSession.Role;
            foreach (var item in CreatedEntities)
            {
                if (item is Role)
                {
                    _ = r.Remove(((Role)item).Id).Result;
                }
            }
        }

        public virtual void DeleteAll()
        {
            ///Documents
            DeleteAllDocumentRelatedItems(false);
            DeleteAllDocumentRelatedItems(true);
            DeleteAllAdditionalProperties();
            DeleteAllCategories();
            //DeleteAllRoles();
            DeletePersons();
        }

        public virtual void DeletePersons()
        {
            //Persons
            using TestSession cf = new(true);
            IPerson pc = cf.ApiSession.Person;
            foreach (var item in CreatedEntities)
            {
                if (item is Person)
                {
                    _ = pc.Remove(((Person)item).AccountId).Result;
                }
            }
        }

        public virtual void DeleteAllRoles()
        {
            using TestSession cf = new();
            IRole myController = cf.ApiSession.Role;
            List<RoleEntityType> ls = new()
            {
                RoleEntityType.Functional,
                RoleEntityType.ReviewGroup,
                RoleEntityType.WorkGroup
            };

            ListResult<Role> roles = myController.Load(new RoleParams { EntityTypeIds = ls }).Result;
            if (!(roles is null))
            {
                DeleteRoles(myController, roles.Results);
            }
        }

        public virtual void DeleteRoles(IRole myController, List<Role> roles)
        {
            foreach (var item in roles)
            {
                try
                {
                    _ = myController.Remove(item.Id).Result;
                }
                catch (Exception ex)
                {
                    _ = ex.Message;
                    if (Debugger.IsAttached)
                        Debugger.Break();
                }
            }
        }

        public virtual void DeleteAllDocumentRelatedItems(bool asInstanceOwner)
        {
            //return;
            using TestSession cf = new(asInstanceOwner);
            IDocument myController = cf.ApiSession.Document;
            LoadDocParams loadParams = new()
            {
                Limit = 5000,
                Offset = 0,
                Removed = ParamShowValue.Include,
                Templates = ParamShowValue.Include,
                Archived = ParamShowValue.Include,
                AllLanguages=true
            };

            ListResult<Document> docs = myController.Load(loadParams).Result;
            DeleteDocs(docs, myController);
            DeleteAllSavedSearches(myController, asInstanceOwner);
        }

        public virtual void DeleteDocs(ListResult<Document> docs, IDocument c)
        {
            Debugger.Log(1, "Info", $"Deleting {docs.Results.Count} documents...\n");
            int j = 0;
            foreach (var item in docs.Results)
            {
                j++;
                Debugger.Log(1, "Info", $"Deleting {j}/{docs.Results.Count}...\n");
                _ = c.UnlockByDocId(item.Id).Result;
                _ = c.Remove(item.Id, true).Result;
            }
        }

        public virtual void DeleteAllAdditionalProperties()
        {
            using TestSession cf = new(true);
            IProperty myController = cf.ApiSession.Property;
            List<AdditionalProperty> propertyList = myController.Load(new PropertyLoadParams()).Result;
            DeleteProperties(propertyList, myController);
        }

        public virtual void DeleteAllCategories()
        {
            using TestSession cf = new();
            ICategory myController = cf.ApiSession.Category;
            List<CategoryType> propertyList = myController.LoadTypes(new CategoryTypeParams()).Result;
            DeleteCategoryTypes(propertyList, myController);
        }

        public virtual void DeleteCategoryTypes(List<CategoryType> categoryTypes, ICategory c)
        {
            foreach (var item in categoryTypes)
            {
                try
                {
                    _ = c.RemoveByTypeId(item.Id).Result;
                }
                catch (Exception)
                {
                }

                c.RemoveType(item.Id);
            }
        }

        public virtual void DeleteProperties(List<AdditionalProperty> items, IProperty c)
        {
            foreach (var item in items)
            {
                if (item.PropertyType == AdditionalPropertyTypes.Custom)
                {
                    _ = c.Remove(item.Id).Result;
                }
            }
        }

        public virtual void DeleteAllSavedSearches(IDocument myController, bool restoreDefaults)
        {
            SavedSearchLoadParams lp = new() { Personal = ParamHideValue.Include, OnDashboard = ParamHideBoolValue.Include };
            List<SavedSearch> savedSearches = myController.SavedSearches(lp).Result;
            foreach (var item in savedSearches)
            {
                _ = myController.RemoveSavedSearch(item.Id).Result;
            }
            //Restore
            if (restoreDefaults)
            {
                _ = myController.RestoreDefaultSavedSearches().Result;
            }
        }

        #region IDisposable Support

        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // dispose managed state (managed objects).
                    CleanUp();
                }
                disposedValue = true;
            }
        }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
        }

        #endregion IDisposable Support
    }
}