﻿using MatterialCore.Entity;
using MatterialCore.Enums;
using MatterialCore.Interfaces;
using MatterialCore.Params;

using NUnit.Framework;

using System;

using MatterialCore.Tests.Helper;

namespace MatterialCore.Tests.Integration
{
    internal class TaskTests : TestBase
    {
        #region Setup

        [OneTimeSetUp] //once per class
        public void Setup()
        {
        }

        [OneTimeTearDown] //once per class
        public void TearDown()
        {
        }

        [SetUp] //once per test
        public void SetupTest()
        {
        }

        [TearDown] //once per test
        public void TearDownTest()
        {
        }

        #endregion Setup

        [Test]
        public void CreateController()
        {
            //Create controller
            using TestSession cf = new();
            IMtrTask myController = cf.ApiSession.Task;
            Assert.That(myController, Is.Not.Null);
        }

        [Test]
        public void Load()
        {
            using TestSession cf = new();
            IMtrTask myController = cf.ApiSession.Task;
            Assert.That(myController, Is.Not.Null);
            MtrTask t = TestHelper.CreateTask(this, cf.ApiSession.LoggedOnPerson().RolePersonal, TaskStatusId.Open, myController, "My new Task " + DateTime.Now.ToString());
            ListResult<MtrTask> tasks = myController.Load(new TaskParams()).Result;
            bool found = false;
            foreach (var item in tasks.Results)
            {
                if (item.Id == t.Id)
                {
                    found = true;
                    break;
                }
            }
            Assert.That(found, Is.True);
        }

        [Test]
        public void GetById()
        {
            using TestSession cf = new();
            IMtrTask myController = cf.ApiSession.Task;
            Assert.That(myController, Is.Not.Null);
            MtrTask l = TestHelper.CreateTask(this, cf.ApiSession.LoggedOnPerson().RolePersonal, TaskStatusId.Open, myController, "My new Task " + DateTime.Now.ToString());
            MtrTask r = myController.LoadById(l.Id).Result;
            Assert.That(r.Id, Is.EqualTo(l.Id));
        }

        [Test]
        public void Create()
        {
            using TestSession cf = new();
            IMtrTask myController = cf.ApiSession.Task;
            Assert.That(myController, Is.Not.Null);
            MtrTask newTask = TestHelper.CreateTask(this, cf.ApiSession.LoggedOnPerson().RolePersonal, TaskStatusId.Open, myController, "My new Task " + DateTime.Now.ToString());
            Assert.That(newTask.Id, Is.GreaterThan(0));
        }

        [Test]
        public void CreatePeriodicTask()
        {
            using TestSession cf = new();
            IMtrTask myController = cf.ApiSession.Task;
            Assert.That(myController, Is.Not.Null);
            MtrTask t = new()
            {
                Description = "My new Task " + DateTime.Now.ToString(),
                AssignedRole = cf.ApiSession.LoggedOnPerson().RolePersonal,
                TaskStatusId = TaskStatusId.Open,
                ResubmissionTimePeriod = 30,
                ResubmissionTimeUnit = ResubmissionTimeUnits.Days
            };
            MtrTask retVal = myController.Create(t).Result;
            if (retVal.Id > 0)
            {
                AddToCreated(retVal);
            }

            Assert.Multiple(() =>
            {
                Assert.That(retVal.Id, Is.GreaterThan(0));
                Assert.That(retVal.ResubmissionTimeUnit, Is.EqualTo(t.ResubmissionTimeUnit));
                Assert.That(retVal.ResubmissionTimePeriod, Is.EqualTo(t.ResubmissionTimePeriod));
            });
        }

        [Test]
        public void Update()
        {
            using TestSession cf = new();
            IMtrTask myController = cf.ApiSession.Task;
            Assert.That(myController, Is.Not.Null);
            MtrTask newTask = TestHelper.CreateTask(this, cf.ApiSession.LoggedOnPerson().RolePersonal, TaskStatusId.Open, myController, "My new Task " + DateTime.Now.ToString());
            string newName = "Task " + Guid.NewGuid().ToString();
            newTask.Description = newName;
            MtrTask updateTask = myController.Update(newTask).Result;
            Assert.That(updateTask.Id, Is.EqualTo(newTask.Id));
            MtrTask updateTask2 = myController.LoadById(newTask.Id).Result;
            Assert.That(updateTask2.Description, Is.EqualTo(newName));
        }
    }
}