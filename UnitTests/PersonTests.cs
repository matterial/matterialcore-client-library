﻿using MatterialCore.Entity;
using MatterialCore.Enums;
using MatterialCore.Interfaces;
using MatterialCore.Params.Person;

using NUnit.Framework;

using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;

using MatterialCore.Tests.Helper;

namespace MatterialCore.Tests.Integration
{
    internal class PersonTests : TestBase
    {
        #region Setup

        [OneTimeSetUp] //once per class
        public void Setup()
        {
        }

        [OneTimeTearDown] //once per class
        public void TearDown()
        {
        }

        [SetUp] //once per test
        public void SetupTest()
        {
        }

        [TearDown] //once per test
        public void TearDownTest()
        {
        }

        #endregion Setup

        [Test]
        public void CreateController()
        {
            //Create controller
            using TestSession cf = new();
            IPerson myController = cf.ApiSession.Person;
            Assert.That(myController, Is.Not.Null);
        }

        [TestCase("")]
        [TestCase("*")]
        public void SearchPerson(string query)
        {
            //Create controller
            using TestSession cf = new();
            IPerson myController = cf.ApiSession.Person;
            Assert.That(myController, Is.Not.Null);

            SearchResult<Person> persons = myController.Search(query, new SearchPersonParams()).Result;
            if (!String.IsNullOrEmpty(query))
            {
                Assert.That(persons.Results, Is.Not.Empty);
            }
            else
            {
                Assert.That(persons.Results, Is.Empty);
            }
        }

        [Test]
        public void SearchPersonWithOptions()
        {
            //Create controller
            using TestSession cf = new();
            IPerson myController = cf.ApiSession.Person;
            Assert.That(myController, Is.Not.Null);
            SearchPersonParams p = new()
            {
                InstanceOwner = ParamHideValue.Exclusive,
            };
            SearchResult<Person> persons = myController.Search("*", p).Result;
            Assert.That(persons.Results, Has.Count.GreaterThanOrEqualTo(2));
        }

        [Test]
        public void SearchAutocomplete()
        {
            //Create controller
            using TestSession cf = new();
            IPerson myController = cf.ApiSession.Person;
            Assert.That(myController, Is.Not.Null);
            PersonAutocompleteParams p = new()
            {
                InstanceOwner = ParamHideValue.Exclusive,
            };
            List<SearchAutocompleteSuggest> suggests = myController.SearchAutocomplete("api", p).Result;
            Assert.Multiple(() =>
            {
                Assert.That(suggests, Has.Count.EqualTo(1));
                Assert.That(suggests[0].Value, Is.EqualTo("apitest"));
            });
        }

        [Test]
        public void LoadPerson()
        {
            //Create controller
            using TestSession cf = new();
            IPerson myController = cf.ApiSession.Person;
            Assert.That(myController, Is.Not.Null);
            GetPersonParams p = new();
            ListResult<Person> result = myController.Load(p).Result;
            Assert.That(result.Results, Is.Not.Empty);
        }

        [Test]
        public void LoadByAccountId()
        {
            //Create controller
            using TestSession cf = new();
            IPerson myController = cf.ApiSession.Person;
            Assert.That(myController, Is.Not.Null);
            GetPersonByIdParams p = new();
            long accountId = cf.ApiSession.LoggedOnPerson().AccountId;
            Person item = myController.LoadByAccountId(accountId, p).Result;
            Assert.That(item, Is.Not.Null);
            p.Addresses = true;
            p.Clients = true;
            p.CommunicationData = true;
            p.ContactImages = true;
            p.Roles = true;
            item = myController.LoadByAccountId(accountId, p).Result;
        }

        [Test]
        public void LoadByContactId()
        {
            //Create controller
            using TestSession cf = new();
            IPerson myController = cf.ApiSession.Person;
            Assert.That(myController, Is.Not.Null);
            GetPersonByIdParams p = new();
            long accountId = cf.ApiSession.LoggedOnPerson().AccountId;
            Person item = myController.LoadByContactId(accountId, p).Result;
            Assert.That(item, Is.Not.Null);
            p.Addresses = true;
            p.Clients = true;
            p.CommunicationData = true;
            p.ContactImages = true;
            p.Roles = true;
            item = myController.LoadByContactId(accountId, p).Result;
        }

        [Test]
        public void Update()
        {
            //Create controller
            using TestSession cf = new();
            IPerson myController = cf.ApiSession.Person;
            Assert.That(myController, Is.Not.Null);
            long accountId = cf.ApiSession.LoggedOnPerson().AccountId;
            //First load person to update
            Person updateItem = myController.LoadByContactId(accountId, new GetPersonByIdParams { Addresses = true }).Result;
            //Set new values
            updateItem.FirstName = "Firsty";
            updateItem.Addresses = new List<Address>
            {
                new() { City = "New York", HouseNumber = "111", Street = "Broadway" }
            };
            //Update
            Person item = myController.Update(updateItem).Result;
            Assert.That(item, Is.Not.Null);
            //Reload and check
            updateItem = myController.LoadByContactId(accountId, new GetPersonByIdParams { Addresses = true }).Result;
            Assert.Multiple(() =>
            {
                Assert.That(updateItem.FirstName, Is.EqualTo("Firsty"));
                Assert.That(updateItem.Addresses[0].Street, Is.EqualTo("Broadway"));
            });
        }

        [Test]
        public void UpdatePersonalData()
        {
            //Updates data of a person for current logged-in account.
            //Personal-data includes Addresses, CommunicationData, and ContactImages
            using TestSession cf = new();

            //Load the person object again to get the current data
            Person reloadedPerson = cf.ApiSession.LoginData(true).Result.Person;

            //Clear current contact images, addresses and communicastion data
            reloadedPerson.ContactImages = new List<ContactImage>();
            reloadedPerson.Addresses = new List<Address>();
            reloadedPerson.CommunicationData = new List<CommunicationData>();
            //update the person to clear everything
            LoginData updatedLoginDate = cf.ApiSession.UpdatePersonalData().Result;
            //Reload to get current data
            reloadedPerson = reloadedPerson = cf.ApiSession.LoginData(true).Result.Person;
            Assert.Multiple(() =>
            {
                Assert.That(reloadedPerson.Addresses, Is.Empty);
                Assert.That(reloadedPerson.CommunicationData, Is.Empty);
                Assert.That(reloadedPerson.ContactImages, Is.Empty);
            });
            //Upload 2 contact images
            string filePath = Path.Combine(BaseConfig.TestFilesDir(), "ContactImage1.png");
            bool b = cf.ApiSession.UploadContactImage(filePath, false).Result;
            filePath = Path.Combine(BaseConfig.TestFilesDir(), "ContactImage2.png");
            b = cf.ApiSession.UploadContactImage(filePath, false).Result;
            //Reload person
            reloadedPerson = cf.ApiSession.LoginData(true).Result.Person;
            //Use the last contact image
            long contactImageId = reloadedPerson.ContactImages[^1].Id;
            reloadedPerson.ContactImage = new ContactImage(contactImageId);
            //A one address
            reloadedPerson.Addresses.Add(new Address { City = "Dallas", Street = "1st street" });
            //A one email
            reloadedPerson.CommunicationData.Add(new CommunicationData { Description = "Test email address", Value = "Test@test.de", EntityTypeId = CommunicationEntityType.Email });
            //New mobile number
            reloadedPerson.CommunicationData.Add(new CommunicationData { Description = "my private mobile", Value = "212-33445566", EntityTypeId = CommunicationEntityType.Phone });
            //Save
            Person updatedPerson = cf.ApiSession.UpdatePersonalData().Result.Person;
            //Reload person
            reloadedPerson = cf.ApiSession.LoginData(true).Result.Person;

            Assert.Multiple(() =>
            {
                //Now check address
                Assert.That(updatedPerson.Addresses, Has.Count.EqualTo(1));
                Assert.That(updatedPerson.Addresses[0].City, Is.EqualTo("Dallas"));
                Assert.That(updatedPerson.Addresses[0].Street, Is.EqualTo("1st street"));
                //Check Contact Image
                Assert.That(contactImageId, Is.EqualTo(updatedPerson.ContactImage.Id));
                //Check comm data
                Assert.That(updatedPerson.CommunicationData, Has.Count.EqualTo(2));
                Assert.That(updatedPerson.CommunicationData[0].Value, Is.EqualTo("Test@test.de"));
                Assert.That(updatedPerson.CommunicationData[1].Value, Is.EqualTo("212-33445566"));
            });
        }

        [Test]
        public void Create()
        {
            using TestSession cf = new();
            IPerson myController = cf.ApiSession.Person;
            Assert.That(myController, Is.Not.Null);
            //Delete first
            ListResult<Person> p = myController.Load(new GetPersonParams { AccountLogin = "john.dilbert@dilbert.com" }).Result;
            if (p.Results.Count > 0)
            {
                AddToCreated(p.Results[0]);
                DeletePersons();
                CreatedEntities.Remove(p.Results[0]);
            }

            //Create new person
            Person newPerson = new()
            {
                FirstName = "John",
                LastName = "Dilbert",
                AccountLogin = "john.dilbert@dilbert.com" //This is required when creating a new one.
            };
            //create the person
            Person createdPerson = myController.Create(newPerson).Result;
            Assert.That(createdPerson, Is.Not.Null);
            AddToCreated(createdPerson);
        }

        [Test]
        [Ignore("Race time condition due to server converting in background task")]
        public void ReConvertAndGetContactImage()
        {
            //Create controller
            using TestSession cf = new();
            IPerson myController = cf.ApiSession.Person;
            Assert.That(myController, Is.Not.Null);
            long contactId = cf.ApiSession.LoggedOnPerson().ContactId;
            string filePath = Path.Combine(BaseConfig.TestFilesDir(), "ContactImage1.png");
            bool b = myController.UploadContactImage(contactId, filePath, true).Result;
            Thread.Sleep(1000);
            int i = 0;
            b = false;
            do
            {
                try
                {
                    b = myController.ReConvertContactImage(contactId).Result;
                }
                catch (Exception)
                {
                    Thread.Sleep(1000);
                    i++;
                }
            } while (!b && (i <= 15));
            Assert.That(b, Is.True, "ReConvertContactImage did not succeed.");

            Person reloadedPerson = myController.LoadByContactId(cf.ApiSession.LoggedOnPerson().ContactId, new GetPersonByIdParams { ContactImages = true }).Result;
            b = false;
            i = 0;
            do
            {
                try
                {
                    using Stream imageStream = myController.GetContactImage(reloadedPerson.ContactId, reloadedPerson.ContactImage.Id, ContactImageFormats.Thumbnail).Result;
                    TestHelper.SaveStreamToFile(imageStream, Path.Combine(BaseConfig.WorkFolder(), "thumbnail.png"));
                    b = true;
                }
                catch (Exception)
                {
                    Thread.Sleep(1000);
                    i++;
                }
            } while (!b && (i <= 15));

            Assert.That(b, Is.True, "Getting stream did not succeed.");
        }

        [Test]
        public void ConvertAllPersonsContactImage()
        {
            using TestSession cf = new();
            IPerson myController = cf.ApiSession.Person;
            Assert.That(myController, Is.Not.Null);
            long l = myController.ConvertAllPersonsContactImage(true).Result;
            Assert.That(l, Is.GreaterThan(0));
        }

        [Test]
        public void UploadContactImage()
        {
            //Create controller
            using TestSession cf = new();
            IPerson myController = cf.ApiSession.Person;
            Assert.That(myController, Is.Not.Null);
            long contactId = cf.ApiSession.LoggedOnPerson().ContactId;
            string filePath = Path.Combine(BaseConfig.TestFilesDir(), "ContactImage1.png");
            bool b = myController.UploadContactImage(contactId, filePath, true).Result;
            Assert.That(b, Is.True);
        }
    }
}