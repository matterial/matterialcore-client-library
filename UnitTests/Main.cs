﻿using NUnit.Framework;

namespace MatterialCore.Tests.Integration
{
    [SetUpFixture]
    internal class MainTest : TestBase
    {
        [OneTimeSetUp] //once per run
        public void Setup() { }

        [OneTimeTearDown] //once per run
        public void TearDown()
        {
            //Delete all
            DeleteAll();
        }
    }
}