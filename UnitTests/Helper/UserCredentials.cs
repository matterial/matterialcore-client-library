﻿using MatterialCore;
using MatterialCore.Enums;

using System.Security;

namespace MatterialCore.Tests.Helper
{
    internal class UserCredentials
    {
        public string User { get; }

        public SecureString Password { get; }

        public string ServerUrl { get; private set; }

        public LangKey Language { get; set; }

        public UserCredentials(string user, string password)
        {
            User = user;
            Password = TestHelper.CreateSecureString(password);
            ServerUrl = BaseConfig.DefaultServerUrl;
            Language = Matterial.DefaultLanguage;
        }

        public UserCredentials(bool useInstanceOwner)
        {
            if (useInstanceOwner)
            {
                User = BaseConfig.InstanceOwnerUserName;
                Password = TestHelper.CreateSecureString(BaseConfig.InstanceOwnerPassword);
                ServerUrl = BaseConfig.DefaultServerUrl;
                Language = Matterial.DefaultLanguage;
            }
            else
            {
                User = BaseConfig.ApiUserName;
                Password = TestHelper.CreateSecureString(BaseConfig.ApiUserPassword);
                ServerUrl = BaseConfig.DefaultServerUrl;
                Language = Matterial.DefaultLanguage;
            }
        }

        public UserCredentials(string user, string password, string serverUrl)
        {
            User = user;
            Password = TestHelper.CreateSecureString(password);
            ServerUrl = serverUrl;
            Language = Matterial.DefaultLanguage;
        }

        public UserCredentials(string user, string password, string serverUrl, LangKey language)
        {
            User = user;
            Password = TestHelper.CreateSecureString(password);
            ServerUrl = serverUrl;
            Language = language;
        }

        public UserCredentials()
        {
            User = BaseConfig.ApiUserName;
            Password = TestHelper.CreateSecureString(BaseConfig.ApiUserPassword);
            ServerUrl = BaseConfig.DefaultServerUrl;
            Language = Matterial.DefaultLanguage;
        }
    }
}