﻿using System;
using System.IO;

namespace MatterialCore.Tests.Helper
{
    internal static class BaseConfig
    {
        public static string TestFilesDir()
        {
            string p = RootDir() + "\\Files";
            return p;
        }

        public static string TestUploadFilesDir()
        {
            string p = RootDir() + "\\Files\\Upload";
            return p;
        }

        public static string RootDir()
        {
            string execPath = AppDomain.CurrentDomain.BaseDirectory;
            execPath = execPath.TrimEnd(Path.DirectorySeparatorChar);
            DirectoryInfo d = new(execPath);
            string p = Directory.GetParent(Directory.GetParent(Directory.GetParent(d.ToString()).ToString()).ToString()).ToString() + "";

            return p;
        }

        public static string WorkFolder()
        {
            string p = RootDir() + "\\Work";
            if (!Directory.Exists(p))
            {
                Directory.CreateDirectory(p);
            }
            return p;
        }

        public static string UniqueName()
        {
            return Path.GetRandomFileName().Replace('.', '_');
        }

        public static string UniqueWorkFolder()
        {
            string p = Path.Combine(WorkFolder(), Path.GetRandomFileName().Replace('.', '_'));
            if (!Directory.Exists(p))
            {
                Directory.CreateDirectory(p);
            }
            return p;
        }

        public static string DefaultServerUrl
        {
            get
            {
                return TestCrendentials.Url;
            }
        }

        //ApiUserName
        public static string ApiUserName
        {
            get
            {
                return TestCrendentials.ApiUserName;
            }
        }

        //Api user password
        public static string ApiUserPassword
        {
            get
            {
                return TestCrendentials.ApiUserPassword;
            }
        }

        //InstanceOwner
        public static string InstanceOwnerUserName
        {
            get
            {
                return TestCrendentials.InstanceOwnerUserName;
            }
        }

        //InstanceOwner user password
        public static string InstanceOwnerPassword
        {
            get
            {
                return TestCrendentials.InstanceOwnerPassword;
            }
        }
    }
}