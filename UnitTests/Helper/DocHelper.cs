﻿using MatterialCore.Entity;
using MatterialCore.Enums;
using MatterialCore.Interfaces;
using MatterialCore.Params.Doc;

using NUnit.Framework;

using System;
using System.Collections.Generic;
using System.IO;

namespace MatterialCore.Tests.Helper
{
    public enum TestDocType : int
    {
        Document, Template, Snap
    }

    internal class DocHelper
    {
        private TestBase testClass = null;
        private IDocument myController = null;

        public DocHelper(TestBase testClass, IDocument myController)
        {
            Title = "My new doc " + DateTime.Now.ToString("yyyy-mm-dd HH:mm:ss");
            Abstract = "Abstract text of " + Title;
            this.myController = myController;
            this.testClass = testClass;
            UnlockAfterCreation = true;
            PublishRequest = PublishRequestTypes.Unreviewed;
            DocumentContent = File.ReadAllText(Path.Combine(BaseConfig.TestFilesDir(), "Default.md"));
            AttachmentFiles = new List<string>();
        }

        public Document Create(string textContent = "", DocumentContentMediaType mediaType = DocumentContentMediaType.Markdown)
        {
            Document newDoc = new()
            {
                LanguageVersionTitle = Title,
                LanguageVersionAbstract = Abstract
            };
            return Create(new DocUpdateParams(), newDoc, DocType, PublishRequest, UnlockAfterCreation, DocumentContent, DocumentContentType);
        }

        public Document Create(PublishRequestTypes requestType, string textContent = "", DocumentContentMediaType mediaType = DocumentContentMediaType.Markdown)
        {
            Document newDoc = new()
            {
                LanguageVersionTitle = Title,
                LanguageVersionAbstract = Abstract
            };
            return Create(new DocUpdateParams(), newDoc, DocType, requestType, UnlockAfterCreation, "", DocumentContentMediaType.Markdown);
        }

        public Document Create(TestDocType testDocType, PublishRequestTypes requestType, string textContent = "", DocumentContentMediaType mediaType = DocumentContentMediaType.Markdown)
        {
            string prefix = "";
            switch (testDocType)
            {
                case TestDocType.Template:
                    prefix = "Template - ";
                    break;

                case TestDocType.Snap:
                    prefix = "Snap - ";
                    break;
            }
            Document newDoc = new()
            {
                LanguageVersionTitle = prefix + Title,
                LanguageVersionAbstract = Abstract
            };
            return Create(new DocUpdateParams(), newDoc, testDocType, requestType, UnlockAfterCreation, textContent, mediaType);
        }

        public Document Create(DocUpdateParams createParams)
        {
            Document newDoc = new()
            {
                LanguageVersionTitle = Title,
                LanguageVersionAbstract = Abstract
            };
            return Create(createParams, newDoc, DocType, PublishRequest, UnlockAfterCreation, DocumentContent, DocumentContentType);
        }

        public Document Create(DocUpdateParams createParams, Document newDoc)
        {
            return Create(createParams, newDoc, DocType, PublishRequest, UnlockAfterCreation, DocumentContent, DocumentContentType);
        }

        public Document Create(DocUpdateParams createParams, Document newDoc, PublishRequestTypes requestType)
        {
            return Create(createParams, newDoc, DocType, requestType, UnlockAfterCreation, DocumentContent, DocumentContentType);
        }

        public Document Create(DocUpdateParams createParams, Document newDoc, TestDocType testDocType, PublishRequestTypes requestType, bool unlockAfterCreation, string textContent, DocumentContentMediaType mediaType)
        {
            //Define unlock id
            LockId = Guid.NewGuid().ToString();
            if (String.IsNullOrEmpty(textContent))
            {
                textContent = File.ReadAllText(Path.Combine(BaseConfig.TestFilesDir(), "Default.md"));
            }
            createParams.UniqueLockId = LockId;
            createParams.PublishRequestType = requestType;
            //Do we have attachments?
            if (AttachmentFiles.Count > 0)
            {
                //Create temp uploads of each
                List<UploadFile> uploadFiles = new();
                createParams.ContextToken = Guid.NewGuid().ToString();
                foreach (var item in AttachmentFiles)
                {
                    UploadFile uf = new()
                    {
                        FilePath = item,
                        FileType = TempFileType.Attachment
                    };
                    uploadFiles.Add(uf);
                }
                List<TempFileDescriptor> tfdList = myController.UploadFile(uploadFiles, createParams.ContextToken, newDoc.LanguageVersionLanguageKey.ToLanguageKey()).Result;
            }

            Document createdDoc = null;
            switch (testDocType)
            {
                case TestDocType.Document:
                    createdDoc = myController.Create(newDoc, createParams, textContent, mediaType).Result;
                    break;

                case TestDocType.Template:
                    createdDoc = myController.CreateTemplate(newDoc, createParams, textContent, mediaType).Result;
                    break;

                case TestDocType.Snap:
                    createdDoc = myController.CreateSnap(newDoc, createParams, textContent, mediaType).Result;
                    break;
            }
            if (AttachmentFiles.Count > 0)
                _ = myController.RemoveTempfiles(createParams.ContextToken);

            Assert.That(createdDoc.Id, Is.GreaterThan(0));
            //Unlock after creation?
            if (unlockAfterCreation)
            {
                _ = myController.Unlock(LockId).Result;
                LockId = string.Empty;
            }
            this.testClass.AddToCreated(createdDoc);
            return createdDoc;
        }

        private string Title { get; set; }

        private string Abstract { get; set; }

        public string LockId { get; private set; }

        public string DocumentContent { get; set; }

        public DocumentContentMediaType DocumentContentType { get; set; }

        public PublishRequestTypes PublishRequest { get; set; }

        public TestDocType DocType { get; set; }

        public bool UnlockAfterCreation { get; set; }

        public List<string> AttachmentFiles { get; set; }
    }
}