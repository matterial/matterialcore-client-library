﻿using MatterialCore.Interfaces;

using System;

namespace MatterialCore.Tests.Helper
{
    public enum ConnectionType
    {
        Default=0,
        MSOAuth = 1
    }
    internal class TestSession : IDisposable
    {
        private Session _Session = null;
        public IApiSession ApiSession = null;
        private readonly UserCredentials _credentials = null;
        private bool _disposed;

        public ConnectionType ConnectionType { get; private set; }

        public void Dispose()
        {
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
            {
                if (!(_Session is null))
                {
                    _Session.Dispose();
                    _Session = null;
                    if (!(ApiSession is null))
                    {
                        ApiSession.Dispose();
                        ApiSession = null;
                    }
                }
            }
            _disposed = true;
        }
        
        public TestSession()
        {
            ConnectionType=ConnectionType.Default;
            _credentials = new UserCredentials();
            CreateSession();
        }
        public TestSession(ConnectionType connectionType)
        {
            ConnectionType = connectionType;
            _credentials = new UserCredentials();
            CreateSession();
            
            
        }
        public TestSession(bool useInstanceOwner)
        {
            ConnectionType= ConnectionType.Default;
            if (useInstanceOwner)
            {
                _credentials = new UserCredentials(useInstanceOwner);
            }
            else
            {
                _credentials = new UserCredentials();
            }
            CreateSession();
        }

        public TestSession(UserCredentials credentials)
        {
            ConnectionType = ConnectionType.Default;
            _credentials = credentials;
            CreateSession();
        }

        private void CreateSession()
        {
            if (!(_Session is null))
                return;

            if (ConnectionType == ConnectionType.Default)
            {
                _Session = new Session(_credentials);
                ApiSession = _Session.CreateSession();
            }
            else
            {
                throw new NotImplementedException();
                //_Session = new Session();
                //ApiSession = _Session.CreateMSOAuthSession();
            }
            
        }
    }
}