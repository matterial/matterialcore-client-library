﻿using MatterialCore;
using MatterialCore.Entity;
using MatterialCore.Enums;
using MatterialCore.Interfaces;
using MatterialCore.Params.Doc;

using NUnit.Framework;

using System;
using System.Collections.Generic;
using System.IO;
using System.Security;

namespace MatterialCore.Tests.Helper
{
    public static class TestHelper
    {
        //private const bool skipCleanWorkFolder = false;

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification = "<Pending>")]
        public static void CleanWorkFolder(string folderPath = "", bool skipWorkFolderCreation = false)
        {
            if (skipWorkFolderCreation)
            {
                return;
            }
            else
            {
                if (String.IsNullOrEmpty(folderPath))
                {
                    folderPath = BaseConfig.WorkFolder();
                }
                if (Directory.Exists(folderPath))
                {
                    foreach (string folder in Directory.EnumerateDirectories(folderPath))
                    {
                        CleanWorkFolder(folder, true);
                    }
                    foreach (string file in Directory.EnumerateFiles(folderPath))
                    {
                        try
                        {
                            File.Delete(file);
                        }
                        catch
                        { }
                    }
                    try
                    {
                        if (skipWorkFolderCreation)
                            Directory.Delete(folderPath);
                    }
                    catch
                    { }
                }
                else
                {
                    if (!skipWorkFolderCreation)
                        Directory.CreateDirectory(BaseConfig.WorkFolder());
                }
            }
        }

        public static SecureString CreateSecureString(string pw)
        {
            SecureString s = new();
            foreach (var item in pw.ToCharArray())
            {
                s.AppendChar(item);
            }
            s.MakeReadOnly();
            return s;
        }

        public static long CreateCategoryType(TestBase testClass, ICategory myController, CategoryType newItem)
        {
            long? newTypeId = myController.CreateType(newItem, Matterial.DefaultLanguage).Result;
            Assert.That(newTypeId, Is.Not.Null);
            Assert.That(newTypeId, Is.GreaterThan(0));
            return newTypeId ?? 0;
        }

        public static long CreateCategoryType(TestBase testClass, ICategory myController)
        {
            CategoryType newItem = new()
            {
                Name = "CategoryType " + DateTime.Now.ToString()
            };
            return CreateCategoryType(testClass, myController, newItem);
        }

        public static Document CreateDefaultDoc(TestBase testClass, IDocument myController)
        {
            return CreateDefaultDoc(testClass, myController, out _);
        }

        public static long CreateCategory(TestBase testClass, ICategory myController, Category newItem)
        {
            long? newTypeId = myController.Create(newItem, Matterial.DefaultLanguage).Result;
            Assert.That(newTypeId, Is.Not.Null);
            Assert.That(newTypeId, Is.GreaterThan(0));
            return newTypeId ?? 0;
        }

        public static long CreateCategory(TestBase testClass, ICategory myController, long categoryTypeId)
        {
            Category newItem = new()
            {
                Name = "Category " + DateTime.Now.ToString(),
                TypeId = categoryTypeId
            };
            return CreateCategory(testClass, myController, newItem);
        }

        public static Document CreateDefaultDoc(TestBase testClass, IDocument myController, out string unlockId, bool unlockIt, PublishRequestTypes requestType = PublishRequestTypes.Unreviewed)
        {
            //Default Title
            string docTitle = "My Title " + DateTime.Now.ToString();
            Document newDoc = new(docTitle, Matterial.DefaultLanguage);
            Assert.That(newDoc, Is.Not.Null);
            return CreateDefaultDoc(testClass, myController, newDoc, out unlockId, unlockIt, requestType);
        }

        public static Document CreateDefaultDoc(TestBase testClass, IDocument myController, out string unlockId, bool unlockIt = true)
        {
            //Default Title
            string docTitle = "My Title " + DateTime.Now.ToString();
            Document newDoc = new(docTitle, Matterial.DefaultLanguage);
            Assert.That(newDoc, Is.Not.Null);
            Document createdDoc = CreateDefaultDoc(testClass, myController, newDoc, out unlockId, unlockIt);
            return createdDoc;
        }

        public static Document CreateDefaultDoc(TestBase testClass, IDocument myController, Document newDoc, out string lockId, bool unlockIt = true, PublishRequestTypes requestType = PublishRequestTypes.Unreviewed)
        {
            //Define unlock id
            string defaultDocContent = File.ReadAllText(Path.Combine(BaseConfig.TestFilesDir(), "default.md"));
            string unlockId = Guid.NewGuid().ToString();
            DocUpdateParams createParams = new()
            {
                UniqueLockId = unlockId
            };

            createParams.PublishRequestType = requestType;

            Document createdDoc = myController.Create(newDoc, createParams, defaultDocContent).Result;
            Assert.That(createdDoc.Id, Is.GreaterThan(0));
            //Unlock after creation?
            lockId = createParams.UniqueLockId;
            if (unlockIt)
            {
                _ = myController.Unlock(unlockId).Result;
                lockId = string.Empty;
            }
            testClass.AddToCreated(createdDoc);
            return createdDoc;
        }

        public static void SaveStreamToFile(Stream stream, string filePath)
        {
            using FileStream fStream = new(filePath, FileMode.Create);
            stream.Seek(0, SeekOrigin.Begin);
            stream.CopyTo(fStream);
            fStream.Close();
        }

        public static long CreateRole(TestBase testClass, IRole myController, Role newRole)
        {
            return myController.Create(newRole).Result;
        }

        public static long CreateRole(TestBase testClass, IRole myController, string name, RoleEntityType roleType = RoleEntityType.Functional)
        {
            Role r = new()
            {
                EntityTypeId = roleType,
                Name = name
            };
            long l = myController.Create(r).Result;
            if (l > 0)
            {
                r.Id = l;
                testClass.AddToCreated(r);
            }
            return l;
        }

        public static MtrTask CreateTask(TestBase testClass, IMtrTask myController, MtrTask newTask)
        {
            return myController.Create(newTask).Result;
        }

        public static MtrTask CreateTask(TestBase testClass, Role assignedRole, TaskStatusId statusId, IMtrTask myController, string description)
        {
            MtrTask t = new()
            {
                Description = description,
                AssignedRole = assignedRole,
                TaskStatusId = statusId
            };
            MtrTask retVal = myController.Create(t).Result;
            if (retVal.Id > 0)
            {
                testClass.AddToCreated(retVal);
            }
            return retVal;
        }

        public static List<string> GetAttachments(int maxNumber = 10, string searchPattern = "*")
        {
            List<string> retVal = new();
            List<string> files = new(Directory.EnumerateFiles(BaseConfig.TestUploadFilesDir(), (searchPattern == "*" ? "" : searchPattern)));
            foreach (var item in files)
            {
                retVal.Add(item);
                if (retVal.Count >= maxNumber)
                    break;
            }
            return retVal;
        }
    }
}