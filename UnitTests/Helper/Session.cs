﻿using MatterialCore;
using MatterialCore.Interfaces;

using System;

namespace MatterialCore.Tests.Helper
{
    internal class Session : IDisposable
    {
        private IApiSession _Session = null;
        private bool _disposed;
        private readonly UserCredentials _credentials = null;

        public Session(UserCredentials credentials)
        {
            _credentials = credentials;
        }

        public Session()
        {
            _credentials = new UserCredentials();
        }

        public void Dispose()
        {
            Dispose(true);
        }

        public IApiSession CreateSession()
        {
            if (!(_Session is null))
            {
                return _Session;
            }

            _Session = Matterial.Connect(_credentials.User, _credentials.Password, _credentials.ServerUrl);
            return _Session;
        }

        //public IApiSession CreateMSOAuthSession()
        //{
        //    if (!(_Session is null))
        //    {
        //        return _Session;
        //    }

        //    _Session = Matterial.ConnectMSOAuth();
        //    return _Session;
        //}
        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
            {
                if (!(_Session is null))
                {
                    _Session.Dispose();
                    _Session = null;
                }
            }
            _disposed = true;
        }
    }
}