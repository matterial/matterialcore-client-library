﻿using MatterialCore.Entity;
using MatterialCore.Interfaces;
using MatterialCore.Params;

using NUnit.Framework;

using MatterialCore.Tests.Helper;

namespace MatterialCore.Tests.Integration
{
    internal class CommentTests : TestBase
    {
        #region Setup

        [OneTimeSetUp] //once per class
        public void Setup()
        {
        }

        [OneTimeTearDown] //once per class
        public void TearDown()
        {
        }

        [SetUp] //once per test
        public void SetupTest()
        {
        }

        [TearDown] //once per test
        public void TearDownTest()
        {
        }

        #endregion Setup

        [Test]
        public void CreateController()
        {
            //Create controller
            using TestSession cf = new();
            IComment myController = cf.ApiSession.Comment;
            Assert.That(myController, Is.Not.Null);
        }

        [Test]
        public void CreateCommentAndGetById()
        {
            //Create controller
            using TestSession cf = new();
            IComment myController = cf.ApiSession.Comment;
            IDocument docController = cf.ApiSession.Document;
            Assert.That(myController, Is.Not.Null);
            Document doc = TestHelper.CreateDefaultDoc(this, docController);
            Comment newComment = new()
            {
                Text = "This is the actual comment"
            };
            long? l = myController.Create(doc.LanguageVersionId, newComment).Result;
            Assert.That(l, Is.Not.Null);
            Comment c = myController.Load(l ?? 0).Result;
            Assert.Multiple(() =>
            {
                Assert.That(c, Is.Not.Null);
                Assert.That(newComment.Text, Is.EqualTo(c.Text));
            });
        }

        [Test]
        public void GetComments()
        {
            using TestSession cf = new();
            IComment myController = cf.ApiSession.Comment;
            IDocument docController = cf.ApiSession.Document;
            Document doc = TestHelper.CreateDefaultDoc(this, docController);
            Comment newComment = new()
            {
                Text = "This is the actual comment"
            };
            long? l = myController.Create(doc.LanguageVersionId, newComment).Result;
            newComment = new Comment
            {
                Text = "This is the actual comment 2"
            };
            l = myController.Create(doc.LanguageVersionId, newComment).Result;

            ListResult<Comment> comments = myController.Load(new CommentParams()).Result;
            Assert.That(comments.Results, Has.Count.GreaterThanOrEqualTo(2));
        }

        [Test]
        public void UpdateComment()
        {
            using TestSession cf = new();
            IComment myController = cf.ApiSession.Comment;
            IDocument docController = cf.ApiSession.Document;
            Document doc = TestHelper.CreateDefaultDoc(this, docController);
            Comment newComment = new()
            {
                Text = "This is the actual comment"
            };
            long? l = myController.Create(doc.LanguageVersionId, newComment).Result;
            Comment createdComment = myController.Load(l ?? 0).Result;
            createdComment.Text = "This is the updated text comment";

            l = myController.Update(createdComment).Result;
            Assert.That(l, Is.GreaterThan(0));
        }

        [Test]
        public void RemoveComment()
        {
            using TestSession cf = new();
            IComment myController = cf.ApiSession.Comment;
            IDocument docController = cf.ApiSession.Document;
            Document doc = TestHelper.CreateDefaultDoc(this, docController);
            Comment newComment = new()
            {
                Text = "This is the actual comment"
            };
            long? l = myController.Create(doc.LanguageVersionId, newComment).Result;

            bool b = myController.Remove(l ?? 0).Result;
            Assert.That(b, Is.True);
        }
    }
}