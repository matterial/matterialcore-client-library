﻿using MatterialCore.Entity;
using MatterialCore.Enums;
using MatterialCore.Interfaces;
using MatterialCore.Params;

using NUnit.Framework;

using System;
using System.Collections.Generic;

using MatterialCore.Tests.Helper;

namespace MatterialCore.Tests.Integration
{
    internal class RoleTests : TestBase
    {
        #region Setup

        [OneTimeSetUp] //once per class
        public void Setup()
        {
        }

        [OneTimeTearDown] //once per class
        public void TearDown()
        {
        }

        [SetUp] //once per test
        public void SetupTest()
        {
        }

        [TearDown] //once per test
        public void TearDownTest()
        {
        }

        #endregion Setup

        [Test]
        public void CreateController()
        {
            //Create controller
            using TestSession cf = new();
            IRole myController = cf.ApiSession.Role;
            Assert.That(myController, Is.Not.Null);
        }

        [Test]
        public void Load()
        {
            using TestSession cf = new();
            IRole myController = cf.ApiSession.Role;
            Assert.That(myController, Is.Not.Null);
            long l = TestHelper.CreateRole(this, myController, "My new role " + DateTime.Now.ToString());
            ListResult<Role> roles = myController.Load(new RoleParams()).Result;
            bool found = false;
            foreach (var item in roles.Results)
            {
                if (item.Id == l)
                {
                    found = true;
                    break;
                }
            }
            Assert.That(found, Is.True);
        }

        [Test]
        public void GetById()
        {
            using TestSession cf = new();
            IRole myController = cf.ApiSession.Role;
            Assert.That(myController, Is.Not.Null);
            long l = TestHelper.CreateRole(this, myController, "My new role " + DateTime.Now.ToString());
            Role r = myController.LoadById(l).Result;
            Assert.That(r.Id, Is.EqualTo(l));
        }

        [Test]
        public void Create()
        {
            using TestSession cf = new();
            IRole myController = cf.ApiSession.Role;
            Assert.That(myController, Is.Not.Null);
            long l = TestHelper.CreateRole(this, myController, "My new role " + DateTime.Now.ToString());
            Assert.That(l, Is.GreaterThan(0));
            l = TestHelper.CreateRole(this, myController, "My new group " + DateTime.Now.ToString(), RoleEntityType.WorkGroup);
            Assert.That(l, Is.GreaterThan(0));
            l = TestHelper.CreateRole(this, myController, "My new review group " + DateTime.Now.ToString(), RoleEntityType.ReviewGroup);
            Assert.That(l, Is.GreaterThan(0));
            l = TestHelper.CreateRole(this, myController, "My new personal group " + DateTime.Now.ToString(), RoleEntityType.Personal);
            Assert.That(l, Is.GreaterThan(0));
        }

        [Test]
        public void Update()
        {
            using TestSession cf = new();
            IRole myController = cf.ApiSession.Role;
            Assert.That(myController, Is.Not.Null);
            long l = TestHelper.CreateRole(this, myController, "My new role " + DateTime.Now.ToString());
            string newName = "Role " + Guid.NewGuid().ToString();
            Role updateRole = new()
            {
                Name = newName
            };
            long newl = myController.Update(l, updateRole).Result;
            Assert.That(newl, Is.EqualTo(l));
            updateRole = myController.LoadById(newl).Result;
            Assert.That(updateRole.Name, Is.EqualTo(newName));
        }

        [Test]
        public void Assign()
        {
            using TestSession cf = new();
            IRole myController = cf.ApiSession.Role;
            Assert.That(myController, Is.Not.Null);
            long l = TestHelper.CreateRole(this, myController, "My new role " + DateTime.Now.ToString());
            long accounId = cf.ApiSession.LoggedOnPerson().AccountId;
            List<Person> result = myController.Assign(l, accounId).Result;
            Assert.That(result, Is.Not.Null);
        }

        [Test]
        public void AssignMany()
        {
            using TestSession cf = new();
            IRole myController = cf.ApiSession.Role;
            Assert.That(myController, Is.Not.Null);
            long l = TestHelper.CreateRole(this, myController, "My new role " + DateTime.Now.ToString());
            long l2 = TestHelper.CreateRole(this, myController, "My new role " + DateTime.Now.ToString());
            long accounId = cf.ApiSession.LoggedOnPerson().AccountId;
            long accointId2 = 2;
            List<Person> result = myController.Assign(new List<long> { l, l2 }, new List<long> { accounId, accointId2 }).Result;
            Assert.That(result, Is.Not.Null);
        }

        [Test]
        public void UnAssing()
        {
            using TestSession cf = new();
            IRole myController = cf.ApiSession.Role;
            Assert.That(myController, Is.Not.Null);
            long l = TestHelper.CreateRole(this, myController, "My new role " + DateTime.Now.ToString());
            long accounId = cf.ApiSession.LoggedOnPerson().AccountId;
            List<Person> result = myController.UnAssign(l, accounId).Result;
            Assert.That(result, Is.Not.Null);
        }

        [Test]
        public void UnAssingMany()
        {
            using TestSession cf = new();
            IRole myController = cf.ApiSession.Role;
            Assert.That(myController, Is.Not.Null);
            long l = TestHelper.CreateRole(this, myController, "My new role " + DateTime.Now.ToString());
            long l2 = TestHelper.CreateRole(this, myController, "My new role " + DateTime.Now.ToString());
            long accounId = cf.ApiSession.LoggedOnPerson().AccountId;
            long accointId2 = 2;
            List<Person> result = myController.Assign(new List<long> { l, l2 }, new List<long> { accounId, accointId2 }).Result;
            Assert.That(result, Is.Not.Null);
            result = myController.UnAssign(new List<long> { l, l2 }, new List<long> { accounId, accointId2 }).Result;
            Assert.That(result, Is.Not.Null);
        }
    }
}