﻿using MatterialCore;
using MatterialCore.Entity;
using MatterialCore.Interfaces;
using MatterialCore.Params;

using Microsoft.VisualBasic;

using NUnit.Framework;

using System.Collections.Generic;
using System.Linq;

using MatterialCore.Tests.Helper;

namespace MatterialCore.Tests.Integration
{
    internal class CategoryTests : TestBase
    {
        #region Setup

        [OneTimeSetUp] //once per class
        public void Setup()
        {
            //Create a category type
            //Create controller
            using TestSession cf = new();
            ICategory myController = cf.ApiSession.Category;
            long categoryTypeId = TestHelper.CreateCategoryType(this, myController);
            categoryType = myController.LoadType(categoryTypeId, Matterial.DefaultLanguage).Result;
        }

        [OneTimeTearDown] //once per class
        public void TearDown()
        {
        }

        [SetUp] //once per test
        public void SetupTest()
        {
        }

        [TearDown] //once per test
        public void TearDownTest()
        {
        }

        #endregion Setup

        private CategoryType categoryType = null;

        [Test]
        public void CreateController()
        {
            //Create controller
            using TestSession cf = new();
            ICategory myController = cf.ApiSession.Category;
            Assert.That(myController, Is.Not.Null);
        }

        [Test]
        public void LoadCategories()
        {
            //Create controller
            using TestSession cf = new();
            ICategory myController = cf.ApiSession.Category;
            Category c = new();

            //Create at least one
            long newId = TestHelper.CreateCategory(this, myController, categoryType.Id);
            CategoryParams p = new();
            List<Category> result = myController.Load(p).Result;
            Assert.That(result, Is.Not.Null);
            Assert.That(result.FirstOrDefault(item => item.Id == newId), Is.Not.Null);
        }

        [Test]
        public void LoadCategoryByType()
        {
            //Create controller
            using TestSession cf = new();
            ICategory myController = cf.ApiSession.Category;
            //Create at least one
            long newId = TestHelper.CreateCategory(this, myController, categoryType.Id);

            CategoryByTypeIdParams p = new();
            List<Category> result = myController.LoadByType(categoryType.Id, p).Result;
            Assert.That(result, Is.Not.Null);
            Assert.That(result.FirstOrDefault(item => item.Id == newId), Is.Not.Null);
        }

        [Test]
        public void LoadCategoryById()
        {
            //Create controller
            using TestSession cf = new();
            ICategory myController = cf.ApiSession.Category;
            //Create at least one
            long newId = TestHelper.CreateCategory(this, myController, categoryType.Id);

            CategoryByIdBaseParams p = new();
            Category result = myController.Load(newId, p).Result;
            Assert.Multiple(() =>
            {
                Assert.That(result, Is.Not.Null);
                Assert.That(newId, Is.EqualTo(result.Id));
            });
        }

        [Test]
        public void LoadCategoryTypeById()
        {
            //Create controller for category
            using TestSession cf = new();
            ICategory myController = cf.ApiSession.Category;
            //load by id
            CategoryType ct = myController.LoadType(categoryType.Id, Matterial.DefaultLanguage).Result;
            Assert.Multiple(() =>
            {
                Assert.That(ct.Id, Is.EqualTo(categoryType.Id));
                Assert.That(ct.Name, Is.EqualTo(categoryType.Name));
            });
        }

        [Test]
        public void LoadCategoryTypes()
        {
            //Create controller for category
            using TestSession cf = new();
            ICategory myController = cf.ApiSession.Category;

            //load
            CategoryTypeParams p = new();
            List<CategoryType> ct = myController.LoadTypes(p).Result;
            Assert.That(ct, Has.Count.GreaterThan(1));
        }

        [Test]
        public void UpdateCategoryType()
        {
            //Create controller for category
            using TestSession cf = new();
            ICategory myController = cf.ApiSession.Category;

            //update
            categoryType.Name = "New name";
            categoryType.Description = "New description";

            long? ct = myController.UpdateType(categoryType, Matterial.DefaultLanguage).Result;
            Assert.That(ct, Is.GreaterThan(1));
        }

        [Test]
        public void CreateCategoryType()
        {
            //Create controller
            using TestSession cf = new();
            ICategory myController = cf.ApiSession.Category;
            CategoryType newItem = new()
            {
                Name = "My new Category Type" + DateAndTime.Now.ToString()
            };
            long? newId = myController.CreateType(newItem, Matterial.DefaultLanguage).Result;
            Assert.That(newId, Is.Not.Null);
            Assert.That(newId, Is.GreaterThan(0));
        }

        [Test]
        public void IsFollowing()
        {
            //Create controller
            using TestSession cf = new();
            ICategory myController = cf.ApiSession.Category;

            //Create category for the type
            Category newItem = new()
            {
                Name = "My new Category " + DateAndTime.Now.ToString(),
                TypeId = categoryType.Id
            };
            long newId = TestHelper.CreateCategory(this, myController, newItem);
            Assert.That(newId, Is.AtLeast(1));
            //Now check following
            bool b = myController.IsFollowing(newId, Matterial.DefaultLanguage).Result;
        }

        [Test]
        public void CreateCategory()
        {
            //Create controller
            using TestSession cf = new();
            ICategory myController = cf.ApiSession.Category;
            //Create category for the type
            Category newItem = new()
            {
                Name = "My new Category " + DateAndTime.Now.ToString(),
                TypeId = categoryType.Id
            };
            long newId = TestHelper.CreateCategory(this, myController, newItem);
            Assert.That(newId, Is.AtLeast(1));
        }

        [Test]
        public void UpdateCategory()
        {
            //Create controller
            using TestSession cf = new();
            ICategory myController = cf.ApiSession.Category;
            //Create category for the type
            Category newItem = new()
            {
                Name = "My new Category " + DateAndTime.Now.ToString(),
                TypeId = categoryType.Id
            };
            long newId = TestHelper.CreateCategory(this, myController, newItem);
            Assert.That(newId, Is.AtLeast(1));

            //update
            newItem = myController.Load(newId).Result;
            newItem.Name = "New name";
            newItem.Description = "New description";
            long? updatedId = myController.Update(newItem, Matterial.DefaultLanguage).Result;
            Assert.That(updatedId, Is.EqualTo(newId));
        }

        [Test]
        public void FollowCategory()
        {
            //Create controller
            using TestSession cf = new();
            ICategory myController = cf.ApiSession.Category;
            //Create category for the type
            Category newItem = new()
            {
                Name = "My new Category " + DateAndTime.Now.ToString(),
                TypeId = categoryType.Id
            };
            long newId = TestHelper.CreateCategory(this, myController, newItem);
            Assert.That(newId, Is.AtLeast(1));

            //follow
            bool b = myController.Follow(newId, Matterial.DefaultLanguage).Result;
            Assert.That(b, Is.True);
            b = myController.IsFollowing(newId, Matterial.DefaultLanguage).Result;
            Assert.That(b, Is.True);
        }

        [Test]
        public void UnFollowCategory()
        {
            //Create controller
            using TestSession cf = new();
            ICategory myController = cf.ApiSession.Category;
            //Create category for the type
            Category newItem = new()
            {
                Name = "My new Category " + DateAndTime.Now.ToString(),
                TypeId = categoryType.Id
            };
            long newId = TestHelper.CreateCategory(this, myController, newItem);
            Assert.That(newId, Is.AtLeast(1));

            //follow
            bool b = myController.Follow(newId, Matterial.DefaultLanguage).Result;
            Assert.That(b, Is.True);
            b = myController.IsFollowing(newId, Matterial.DefaultLanguage).Result;
            Assert.That(b, Is.True);
            b = myController.UnFollow(newId, Matterial.DefaultLanguage).Result;
            Assert.That(b, Is.True);
            b = myController.IsFollowing(newId, Matterial.DefaultLanguage).Result;
            Assert.That(b, Is.False);
        }

        [Test]
        public void FollowAndUnFollowCategoryOtherAccount()
        {
            //Create controller
            using TestSession cf = new();
            ICategory myController = cf.ApiSession.Category;
            //Create category for the type
            Category newItem = new()
            {
                Name = "My new Category " + DateAndTime.Now.ToString(),
                TypeId = categoryType.Id
            };
            long newId = TestHelper.CreateCategory(this, myController, newItem);
            Assert.That(newId, Is.AtLeast(1));
            IPerson pc = cf.ApiSession.Person;
            ListResult<Person> persons = pc.Load(new MatterialCore.Params.Person.GetPersonParams()).Result;
            Person otherPerson = persons.Results.FirstOrDefault(item => item.AccountId != cf.ApiSession.LoggedOnPerson().AccountId);
            Assert.That(otherPerson, Is.Not.Null);

            //follow
            bool b = myController.Follow(newId, otherPerson.AccountId, Matterial.DefaultLanguage).Result;
            Assert.That(b, Is.True);
            b = myController.UnFollow(newId, otherPerson.AccountId, Matterial.DefaultLanguage).Result;
            Assert.That(b, Is.True);
        }

        [Test]
        public void AssignCategory()
        {
            //Create controller for document
            using TestSession cfd = new();
            IDocument myControllerDoc = cfd.ApiSession.Document;
            //Create default doc
            Document createdDoc = TestHelper.CreateDefaultDoc(this, myControllerDoc);
            Document createdDoc2 = TestHelper.CreateDefaultDoc(this, myControllerDoc);

            //Create controller
            using TestSession cf = new();
            ICategory myController = cf.ApiSession.Category;
            //Create category for the type
            Category newItem = new()
            {
                Name = "My new Category " + DateAndTime.Now.ToString(),
                TypeId = categoryType.Id
            };
            long newId = TestHelper.CreateCategory(this, myController, newItem);
            Assert.That(newId, Is.AtLeast(1));

            //assign
            List<long> docIds = new()
            {
                createdDoc.Id,
                createdDoc2.Id
            };
            bool b = myController.Assign(newId, docIds).Result;
            Assert.That(b, Is.True);
        }

        [Test]
        public void AssignQuickCategory()
        {
            //Create controller for document
            using TestSession cfd = new();
            IDocument myControllerDoc = cfd.ApiSession.Document;
            //Create default doc
            Document createdDoc = TestHelper.CreateDefaultDoc(this, myControllerDoc);
            Document createdDoc2 = TestHelper.CreateDefaultDoc(this, myControllerDoc);

            //Create controller
            using TestSession cf = new();
            ICategory myController = cf.ApiSession.Category;
            //Create category for the type
            Category newItem = new()
            {
                Name = "My new Category " + DateAndTime.Now.ToString(),
                TypeId = categoryType.Id
            };
            long newId = TestHelper.CreateCategory(this, myController, newItem);
            Assert.That(newId, Is.AtLeast(1));

            //assign
            List<long> docIds = new()
            {
                createdDoc.Id,
                createdDoc2.Id
            };
            bool b = myController.AssignQuickCategory(docIds).Result;
            Assert.That(b, Is.True);
        }

        [Test]
        public void UnAssignCategory()
        {
            //Create controller for document
            using TestSession cfd = new();
            IDocument myControllerDoc = cfd.ApiSession.Document;
            //Create default doc
            Document createdDoc = TestHelper.CreateDefaultDoc(this, myControllerDoc);
            Document createdDoc2 = TestHelper.CreateDefaultDoc(this, myControllerDoc);

            //Create controller
            using TestSession cf = new();
            ICategory myController = cf.ApiSession.Category;
            //Create category for the type
            Category newItem = new()
            {
                Name = "My new Category " + DateAndTime.Now.ToString(),
                TypeId = categoryType.Id
            };
            long newId = TestHelper.CreateCategory(this, myController, newItem);
            Assert.That(newId, Is.AtLeast(1));

            //assign
            List<long> docIds = new()
            {
                createdDoc.Id,
                createdDoc2.Id
            };
            bool b = myController.Assign(newId, docIds).Result;
            Assert.That(b, Is.True);
            b = myController.UnAssign(newId, docIds).Result;
            Assert.That(b, Is.True);
        }

        [Test]
        public void UnAssignQuickCategory()
        {
            //Create controller for document
            using TestSession cfd = new();
            IDocument myControllerDoc = cfd.ApiSession.Document;
            //Create default doc
            Document createdDoc = TestHelper.CreateDefaultDoc(this, myControllerDoc);
            Document createdDoc2 = TestHelper.CreateDefaultDoc(this, myControllerDoc);

            //Create controller
            using TestSession cf = new();
            ICategory myController = cf.ApiSession.Category;
            //Create category for the type
            Category newItem = new()
            {
                Name = "My new Category " + DateAndTime.Now.ToString(),
                TypeId = categoryType.Id
            };
            long newId = TestHelper.CreateCategory(this, myController, newItem);
            Assert.That(newId, Is.AtLeast(1));

            //assign
            List<long> docIds = new()
            {
                createdDoc.Id,
                createdDoc2.Id
            };
            bool b = myController.AssignQuickCategory(docIds).Result;
            Assert.That(b, Is.True);
            b = myController.UnAssignQuickCategory(docIds).Result;
            Assert.That(b, Is.True);
        }
    }
}