﻿using MatterialCore;
using MatterialCore.Entity;
using MatterialCore.Enums;
using MatterialCore.Interfaces;
using MatterialCore.Params.Doc;

using NUnit.Framework;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

using MatterialCore.Tests.Helper;

namespace MatterialCore.Tests.Integration
{
    internal class SearchDocs : TestBase
    {
        #region Setup

        [OneTimeSetUp] //once per class
        public void Setup()
        {
        }

        [OneTimeTearDown] //once per class
        public void TearDown()
        {
        }

        [SetUp] //once per test
        public void SetupTest()
        {
        }

        [TearDown] //once per test
        public void TearDownTest()
        {
        }

        #endregion Setup

        [TestCase("")]
        [TestCase("*")]
        public void DoSearch(string query)
        {
            using TestSession cf = new();
            IDocument myController = cf.ApiSession.Document;
            Assert.That(myController, Is.Not.Null);
            TestHelper.CreateDefaultDoc(this, myController, out string s);
            TestHelper.CreateDefaultDoc(this, myController, out string s2);
            //wait just a bit
            Thread.Sleep(2000);
            SearchResult<Document> docs = myController.Search(query).Result;
            Assert.That(docs, Is.Not.Null);
            if (String.IsNullOrEmpty(query))
            {
                Assert.That(docs.TotalHits, Is.EqualTo(0));
            }
        }
        [TestCase(CategoryAssignedValue.Any)]
        [TestCase(CategoryAssignedValue.Assigned)]
        [TestCase(CategoryAssignedValue.NoneAssigned)]
        public void SearchHasCategory(CategoryAssignedValue val)
        {
            using TestSession cf = new();
            IDocument myController = cf.ApiSession.Document;
            Assert.That(myController, Is.Not.Null);
            string docTitle =  Guid.NewGuid().ToString();
            Document newDoc1 = new(docTitle, Matterial.DefaultLanguage);
            var Doc1 = TestHelper.CreateDefaultDoc(this, myController, newDoc1, out string s );

            Document newDoc2 = new(docTitle, Matterial.DefaultLanguage);
            var Doc2 = TestHelper.CreateDefaultDoc(this, myController, newDoc2,out string s2);

            //Create at least one category
            ICategory catController = cf.ApiSession.Category;
            Category c = new();
            long categoryTypeId = TestHelper.CreateCategoryType(this, catController);
             var categoryType = catController.LoadType(categoryTypeId, Matterial.DefaultLanguage).Result;
            long newId = TestHelper.CreateCategory(this, catController, categoryType.Id);

            //wait just a bit
            Thread.Sleep(2000);

            //Update documemnt 2
            //Lock it
            string lockId = Guid.NewGuid().ToString();
            DocumentLock docLock = myController.Lock(Doc2.Id, lockId).Result;
            Assert.That(docLock, Is.Not.Null);
            Assert.That(docLock.LockedForMe, Is.True);

            //Update it
            Doc2.Categories.Add(new Category(newId));
            DocUpdateParams p = new()
            {
                PublishRequestType = PublishRequestTypes.Reviewed,
                UniqueLockId = lockId
            };
            Document updatedDoc = myController.Update(Doc2, p).Result;
            Assert.That(updatedDoc, Is.Not.Null);

            string query = docTitle;
            SearchDocParams options = new(Matterial.DefaultLanguage);
            //options.AuthorshipAccountId = 3;
            options.HasCategoryAssigned = val;
            SearchResult<Document> docs = myController.Search(query, options).Result;
            Assert.That(docs, Is.Not.Null);

            switch (val)
            {
                case CategoryAssignedValue.NoneAssigned:
                    Assert.That(docs.TotalHits, Is.EqualTo(1));
                    Assert.That(docs.Results.Any(d => d.Source.Id == Doc1.Id), Is.True);
                    Assert.That(docs.Results.Any(d => d.Source.Id == Doc2.Id), Is.False);
                    break;
                case CategoryAssignedValue.Any:
                    Assert.That(docs.TotalHits, Is.EqualTo(2));
                    Assert.That(docs.Results.Any(d => d.Source.Id == Doc1.Id), Is.True);
                    Assert.That(docs.Results.Any(d => d.Source.Id == Doc2.Id), Is.True);
                    break;
                case CategoryAssignedValue.Assigned:
                    Assert.That(docs.TotalHits, Is.EqualTo(1));
                    Assert.That(docs.Results.Any(d => d.Source.Id == Doc2.Id), Is.True);
                    Assert.That(docs.Results.Any(d => d.Source.Id == Doc1.Id), Is.False);
                    break;
                default:
                    break;
            }
            
        }

        [Test]
        public void SavedSearches()
        {
            using TestSession cf = new();
            IDocument myController = cf.ApiSession.Document;
            Assert.That(myController, Is.Not.Null);

            List<SavedSearch> savedSearches = myController.SavedSearches(null).Result;
            Assert.That(savedSearches, Is.Not.Null);

            foreach (var item in savedSearches)
            {
                SavedSearch s = myController.SavedSearchById(item.Id, Matterial.DefaultLanguage).Result;
                Assert.That(s.Name, Is.EqualTo(item.Name));
            }
        }

        [Test]
        public void CreateSavedSearch()
        {
            using TestSession cf = new();
            IDocument myController = cf.ApiSession.Document;
            Assert.That(myController, Is.Not.Null);
            SavedSearch s = new()
            {
                DashboardPrio = 1,
                Name = "My new saved search " + DateTime.Now.ToString(),
                DashboardStyle = DashboardStyles.GridDark
            };
            SearchDocParams p = new()
            {
                OrderDir = OrderDirection.desc,
                OrderBy = DocSearchOrderBy.SumRating
            };
            long newId = myController.CreateSavedSearch(s, "*", p).Result;
            Assert.That(newId, Is.GreaterThan(0));
        }

        [Test]
        public void SetDashboardSavedSearchPrios()
        {
            using TestSession cf = new();
            IDocument myController = cf.ApiSession.Document;
            Assert.That(myController, Is.Not.Null);
            //Delete all existing
            DeleteAllSavedSearches(myController, false);

            //Create 10 saved searches with ascending prio
            List<long> ids = new();
            SavedSearch s;
            SearchDocParams sdp = new()
            {
                OrderDir = OrderDirection.desc,
                OrderBy = DocSearchOrderBy.SumRating
            };
            int numToCreate = 8;
            for (int i = 1; i <= numToCreate; i++)
            {
                //Create saved searches
                s = new SavedSearch
                {
                    DashboardPrio = 1,
                    DashboardStyle = (DashboardStyles)i,
                    Name = "Prio " + (i) + " saved search " + DateTime.Now.ToString()
                };

                long newId = myController.CreateSavedSearch(s, "*", sdp).Result;
                Assert.That(newId, Is.GreaterThan(0));
            }

            //load
            SavedSearchLoadParams p = new()
            {
                OrderBy = SavedSearchesOrderBy.Id,
                OrderDir = OrderDirection.asc
            };
            List<SavedSearch> searches = myController.SavedSearches(p).Result;
            // Now re-order id list descending
            foreach (var item in searches)
            {
                ids.Add(item.Id);
            }
            //Set dashboard prio
            long l = myController.SetDashboardPrios(ids).Result;
            Assert.That(l, Is.EqualTo(numToCreate));
            //load again
            p = new SavedSearchLoadParams
            {
                OrderBy = SavedSearchesOrderBy.Id,
                OrderDir = OrderDirection.asc
            };
            searches = myController.SavedSearches(p).Result;
            Assert.That(searches, Has.Count.EqualTo(numToCreate));
            //Now iterate thru and check
            for (int i = 0; i < numToCreate; i++)
            {
                Assert.That(searches[i].DashboardPrio, Is.EqualTo(numToCreate - i));
            }

            //Now upside down
            ids = new List<long>(ids.OrderByDescending(i => i));
            //Set dashboard prio
            l = myController.SetDashboardPrios(ids).Result;
            Assert.That(l, Is.EqualTo(numToCreate));
            //Re-load
            searches = myController.SavedSearches(p).Result;
            Assert.That(searches, Has.Count.EqualTo(numToCreate));
            //Check
            for (int i = 0; i < numToCreate; i++)
            {
                Assert.That(searches[i].DashboardPrio, Is.EqualTo(i + 1));
            }
        }

        [Test]
        public void UpdateSavedSearch()
        {
            using TestSession cf = new();
            IDocument myController = cf.ApiSession.Document;
            Assert.That(myController, Is.Not.Null);
            SavedSearch s = new()
            {
                DashboardPrio = 1,
                Name = "My new saved search " + DateTime.Now.ToString(),
                DashboardStyle = DashboardStyles.GridDark
            };
            SearchDocParams p = new()
            {
                OrderDir = OrderDirection.desc,
                OrderBy = DocSearchOrderBy.SumRating
            };
            long newSavedSearchId = myController.CreateSavedSearch(s, "*", p).Result;
            Assert.That(newSavedSearchId, Is.GreaterThan(0));
            s = myController.SavedSearchById(newSavedSearchId, Matterial.DefaultLanguage).Result;

            //Now update

            s.DashboardPrio = 3;
            s.Name = "My updated saved search " + DateTime.Now.ToString();
            s.DashboardStyle = DashboardStyles.ColumnsLight;

            p.OrderDir = OrderDirection.asc;
            p.OrderBy = DocSearchOrderBy.LastChangeInSeconds;
            p.CopyToSavedSearch(s, "saved");
            newSavedSearchId = myController.UpdateSavedSearch(s).Result;
            Assert.That(newSavedSearchId, Is.GreaterThan(0));
        }

        [Test]
        public void ExecuteSavedSearch()
        {
            using TestSession cf = new();
            IDocument myController = cf.ApiSession.Document;
            Assert.That(myController, Is.Not.Null);

            //Create some documents
            Document createdDoc1 = TestHelper.CreateDefaultDoc(this, myController);
            Document createdDoc2 = TestHelper.CreateDefaultDoc(this, myController);
            Document createdDoc3 = TestHelper.CreateDefaultDoc(this, myController);

            //Create saved search
            SavedSearch s = new()
            {
                DashboardPrio = 1,
                Name = "My new saved search " + DateTime.Now.ToString(),
                DashboardStyle = DashboardStyles.GridDark
            };
            SearchDocParams p = new()
            {
                OrderDir = OrderDirection.desc,
                OrderBy = DocSearchOrderBy.Title,
                Bios = ParamShowValue.Exclusive
            };
            long newSavedSearchId = myController.CreateSavedSearch(s, "*", p).Result;
            Assert.That(newSavedSearchId, Is.GreaterThan(0));
            s = myController.SavedSearchById(newSavedSearchId, Matterial.DefaultLanguage).Result;

            SavedSearchSearchParams dParams = new(0, 10);

            //Run saved search
            ListResult<SearchResultEntry<Document>> docs = myController.Search(newSavedSearchId, dParams).Result;
            Assert.That(docs, Is.Not.Null);
        }

        [Test]
        public void SearchAutocompleteTest()
        {
            using TestSession cf = new();
            IDocument myController = cf.ApiSession.Document;
            Assert.That(myController, Is.Not.Null);

            //Create some documents
            Document createdDoc1 = TestHelper.CreateDefaultDoc(this, myController);
            Document createdDoc2 = TestHelper.CreateDefaultDoc(this, myController);
            Document createdDoc3 = TestHelper.CreateDefaultDoc(this, myController);
            //Wait a bit so index is created
            Thread.Sleep(2000);
            DocAutocompleteParams p = new();

            List<SearchAutocompleteSuggest> result = myController.SearchAutocomplete("My tit", p).Result;
            Assert.That(result, Is.Not.Null);
            Assert.Multiple(() =>
            {
                Assert.That(result, Is.Not.Empty);
                Assert.That(result[0].Value, Is.EqualTo("my title"));
            });
        }
    }
}