﻿using MatterialCore;
using MatterialCore.Entity;
using MatterialCore.Enums;
using MatterialCore.Interfaces;
using MatterialCore.Params;
using MatterialCore.Params.Doc;

using NUnit.Framework;

using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;

using MatterialCore.Tests.Helper;

namespace MatterialCore.Tests.Integration
{
    internal class CommonDoc : TestBase
    {
        #region Setup

        [OneTimeSetUp] //once per class
        public void Setup()
        { }

        [OneTimeTearDown] //once per class
        public void TearDown()
        { }

        [SetUp] //once per test
        public void SetupTest()
        { }

        [TearDown] //once per test
        public void TearDownTest()
        { }

        #endregion Setup

        [Test]
        public void LoadDocument()
        {
            //Create controller for document
            using TestSession cf = new();
            IDocument myController = cf.ApiSession.Document;
            //Create default doc
            Document createdDoc = TestHelper.CreateDefaultDoc(this, myController, out string unlockId);
        }

        [Test]
        public void Locking()
        {
            //Lock / unlock / update lock
            //Create controller for document
            using TestSession cf = new();
            IDocument myController = cf.ApiSession.Document;
            //Create default doc
            Document createdDoc = TestHelper.CreateDefaultDoc(this, myController, out string unlockId);

            //Lock document
            string lockId = Guid.NewGuid().ToString();
            DocumentLock docLock = myController.Lock(createdDoc.Id, lockId).Result;
            Assert.That(docLock, Is.Not.Null);
            Assert.Multiple(() =>
            {
                Assert.That(docLock.LockedForMe, Is.True);
                Assert.That(createdDoc.Id, Is.EqualTo(docLock.DocumentId));
            });
            long timeStamp = docLock.LockTimestampInSeconds;
            //Update lock
            Thread.Sleep(1000);
            docLock = myController.UpdateLock(createdDoc.Id, lockId).Result;
            Assert.That(docLock, Is.Not.Null);
            Assert.That(docLock.LockTimestampInSeconds, Is.Not.EqualTo(timeStamp));

            //Unlock by lock ID
            bool b = myController.Unlock(lockId).Result;
            Assert.That(b, Is.True);

            //Lock again
            docLock = myController.Lock(createdDoc.Id, lockId).Result;
            Assert.That(docLock, Is.Not.Null);
            Assert.Multiple(() =>
            {
                Assert.That(docLock.LockedForMe, Is.True);
                Assert.That(createdDoc.Id, Is.EqualTo(docLock.DocumentId));
            });
            //Unlock by doc ID
            b = myController.UnlockByDocId(createdDoc.Id).Result;
            Assert.That(b, Is.True);
        }

        [Test]
        public void GetDuplicate()
        {
            //Create controller for document
            using TestSession cf = new();
            IDocument myController = cf.ApiSession.Document;
            //Create default doc
            Document createdDoc = TestHelper.CreateDefaultDoc(this, myController, out string unlockId);

            DocumentDuplicate dupl = myController.Duplicate(createdDoc.Id, DuplicateMode.Default).Result;
            Assert.That(dupl, Is.Not.Null);
            Assert.Multiple(() =>
            {
                Assert.That(dupl.Documents, Has.Count.EqualTo(1));
                Assert.That(dupl.OriginId, Is.EqualTo(createdDoc.Id));
            });
        }

        [Test]
        public void SaveDuplicate()
        {
            //Create controller for document
            using TestSession cf = new();
            IDocument myController = cf.ApiSession.Document;
            //Create default doc
            Document createdDoc = TestHelper.CreateDefaultDoc(this, myController, out string unlockId);
            DocumentDuplicate dupl = myController.Duplicate(createdDoc.Id, DuplicateMode.Default).Result;
            Assert.That(dupl, Is.Not.Null);
            Assert.Multiple(() =>
            {
                Assert.That(dupl.Documents, Has.Count.EqualTo(1));
                Assert.That(dupl.OriginId, Is.EqualTo(createdDoc.Id));
            });

            //Load all duplicates
            DocumentDuplicates duplicates = myController.GetDuplicates().Result;
            Assert.That(dupl, Is.Not.Null);
            List<Document> duplicatedDocs = myController.SaveDuplicates(duplicates).Result;
            AddToCreated(duplicatedDocs);
        }

        [Test]
        public void Duplicates()
        {
            //Create controller for document
            using TestSession cf = new();
            IDocument myController = cf.ApiSession.Document;
            //Create default doc
            Document createdDoc = TestHelper.CreateDefaultDoc(this, myController, out string unlockId);
            Document createdDoc2 = TestHelper.CreateDefaultDoc(this, myController, out string unlockId2);

            //FirstDuplicate
            DocumentDuplicate dupl = myController.Duplicate(createdDoc.Id, DuplicateMode.Default).Result;
            Assert.That(dupl, Is.Not.Null);
            Assert.Multiple(() =>
            {
                Assert.That(dupl.Documents, Has.Count.EqualTo(1));
                Assert.That(dupl.OriginId, Is.EqualTo(createdDoc.Id));
            });
            //Second duplicate
            dupl = myController.Duplicate(createdDoc2.Id, DuplicateMode.Default).Result;
            Assert.That(dupl, Is.Not.Null);
            Assert.Multiple(() =>
            {
                Assert.That(dupl.Documents, Has.Count.EqualTo(1));
                Assert.That(dupl.OriginId, Is.EqualTo(createdDoc2.Id));
            });

            //Now get all duplicates
            DocumentDuplicates duplicates = myController.GetDuplicates().Result;
            Assert.Multiple(() =>
            {
                Assert.That(dupl, Is.Not.Null);
                Assert.That(duplicates.Map, Has.Count.EqualTo(2));
            });
        }

        [Test]
        public void RemoveDuplicates()
        {
            //Create controller for document
            using TestSession cf = new();
            IDocument myController = cf.ApiSession.Document;
            //Create default doc
            Document createdDoc = TestHelper.CreateDefaultDoc(this, myController, out string unlockId);
            Document createdDoc2 = TestHelper.CreateDefaultDoc(this, myController, out string unlockId2);

            //FirstDuplicate
            DocumentDuplicate dupl = myController.Duplicate(createdDoc.Id, DuplicateMode.Default).Result;
            Assert.That(dupl, Is.Not.Null);
            Assert.Multiple(() =>
            {
                Assert.That(dupl.Documents, Has.Count.EqualTo(1));
                Assert.That(dupl.OriginId, Is.EqualTo(createdDoc.Id));
            });
            //Second duplicate
            dupl = myController.Duplicate(createdDoc2.Id, DuplicateMode.Default).Result;
            Assert.That(dupl, Is.Not.Null);
            Assert.Multiple(() =>
            {
                Assert.That(dupl.Documents, Has.Count.EqualTo(1));
                Assert.That(dupl.OriginId, Is.EqualTo(createdDoc2.Id));
            });

            //Now get all duplicates
            DocumentDuplicates duplicates = myController.GetDuplicates().Result;
            Assert.Multiple(() =>
            {
                Assert.That(dupl, Is.Not.Null);
                Assert.That(duplicates.Map, Has.Count.EqualTo(2));
            });
            //Remove all
            int i = myController.RemoveDuplicates(duplicates).Result;
            Assert.That(i, Is.EqualTo(2));
            //Check if removed
            duplicates = myController.GetDuplicates().Result;
            Assert.Multiple(() =>
            {
                Assert.That(dupl, Is.Not.Null);
                Assert.That(duplicates.Map, Is.Empty);
            });
        }

        [Test]
        public void AddToClipboard()
        {
            //Create controller for document
            using TestSession cf = new();
            IDocument myController = cf.ApiSession.Document;
            //Create default doc
            Document createdDoc = TestHelper.CreateDefaultDoc(this, myController);
            Document createdDoc2 = TestHelper.CreateDefaultDoc(this, myController);
            List<DocumentClipBoardEntry> docs = new()
            {
                new DocumentClipBoardEntry( createdDoc.Id, createdDoc.LanguageVersionLanguageId),
                new DocumentClipBoardEntry( createdDoc2.Id, createdDoc2.LanguageVersionLanguageId)
            };
            int i = myController.AddToClipboard(docs).Result;
            Assert.That(i, Is.EqualTo(docs.Count));
        }

        [Test]
        public void RemoveFromClipboard()
        {
            //Create controller for document
            using TestSession cf = new();
            IDocument myController = cf.ApiSession.Document;
            //Create default doc
            Document createdDoc = TestHelper.CreateDefaultDoc(this, myController);
            Document createdDoc2 = TestHelper.CreateDefaultDoc(this, myController);
            List<DocumentClipBoardEntry> docs = new()
            {
                new DocumentClipBoardEntry( createdDoc.Id, createdDoc.LanguageVersionLanguageId),
                new DocumentClipBoardEntry( createdDoc2.Id, createdDoc2.LanguageVersionLanguageId)
            };
            int i = myController.AddToClipboard(docs).Result;
            Assert.That(docs, Has.Count.EqualTo(i));
            //Now remove
            i = myController.RemoveFromClipboard(docs).Result;
            Assert.That(docs, Has.Count.EqualTo(i));
        }

        [Test]
        public void ClearClipboard()
        {
            //Create controller for document
            using TestSession cf = new();
            IDocument myController = cf.ApiSession.Document;
            //Clear first if there are docu on clipboard
            LoadDocParams p = new()
            {
                ClipBoard = true
            };
            ListResult<Document> docList = myController.Load(p).Result;
            if (docList.Results.Count > 0)
            {
                int j = myController.ClearClipboard().Result;
            }
            //Clear first
            int i = 0;
            try
            {
                i = myController.ClearClipboard().Result;
            }
            catch (Exception)
            {
            }

            //Create default doc
            Document createdDoc = TestHelper.CreateDefaultDoc(this, myController);
            Document createdDoc2 = TestHelper.CreateDefaultDoc(this, myController);
            List<DocumentClipBoardEntry> docs = new()
            {
                new DocumentClipBoardEntry( createdDoc.Id, createdDoc.LanguageVersionLanguageId),
                new DocumentClipBoardEntry( createdDoc2.Id, createdDoc2.LanguageVersionLanguageId)
            };
            i = myController.AddToClipboard(docs).Result;
            Assert.That(i, Is.EqualTo(docs.Count));
            //Now clear
            i = myController.ClearClipboard().Result;
            Assert.That(docs, Has.Count.EqualTo(i));
        }

        [Test]
        public void Trash()
        {
            //Create controller for document
            using TestSession cf = new();
            IDocument myController = cf.ApiSession.Document;
            //Create default doc
            Document createdDoc = TestHelper.CreateDefaultDoc(this, myController);
            Document createdDoc2 = TestHelper.CreateDefaultDoc(this, myController);

            DocumentBase lockDoc = myController.Remove(createdDoc.Id, false).Result;
            Assert.That(lockDoc is Document, Is.True);

            lockDoc = myController.Remove(createdDoc2.Id, false).Result;
            Assert.That(lockDoc is Document, Is.True);
        }

        [Test]
        public void UnTrash()
        {
            //Create controller for document
            using TestSession cf = new();
            IDocument myController = cf.ApiSession.Document;
            //Create default doc
            Document createdDoc = TestHelper.CreateDefaultDoc(this, myController);

            DocumentBase lockDoc = myController.Remove(createdDoc.Id, false).Result;
            Assert.That(lockDoc, Is.Not.Null);
            Assert.That(lockDoc is Document, Is.True);
            lockDoc = myController.Restore(createdDoc.Id).Result;
            Assert.That(lockDoc, Is.Not.Null);
            Assert.That(lockDoc is Document, Is.True);
        }

        [Test]
        public void RemoveSpecificVersion()
        {
            using TestSession cf = new();
            IDocument myController = cf.ApiSession.Document;
            //Create default doc
            Document createdDoc = TestHelper.CreateDefaultDoc(this, myController);
            long versionId = createdDoc.LanguageVersionId;
            createdDoc.LanguageVersionTitle = "Updated title";
            DocUpdateParams options = new()
            {
                UniqueLockId = Guid.NewGuid().ToString(),
                PublishRequestType = PublishRequestTypes.Unreviewed
            };
            DocumentLock l = myController.Lock(createdDoc.Id, options.UniqueLockId).Result;

            createdDoc = myController.Update(createdDoc, options).Result;
            _ = myController.Unlock(options.UniqueLockId).Result;
            DocumentBase ret = myController.Remove(createdDoc.Id, versionId).Result;
            Assert.That(ret is Document, Is.True);
        }

        [Test]
        public void ClearTrash()
        {
            //Create controller for document
            using TestSession cf = new();
            IDocument myController = cf.ApiSession.Document;
            //Create default doc
            Document createdDoc = TestHelper.CreateDefaultDoc(this, myController);
            DocumentBase lockDoc = myController.Remove(createdDoc.Id, false).Result;
            //Now clear
            int i = myController.ClearTrash(true).Result;
            Assert.That(i, Is.GreaterThanOrEqualTo(1));
        }

        [Test]
        public void GetRemovalQueueSize()
        {
            //Create controller for document
            using TestSession cf = new();
            IDocument myController = cf.ApiSession.Document;
            //Create default doc
            Document createdDoc = TestHelper.CreateDefaultDoc(this, myController);
            DocumentBase lockDoc = myController.Remove(createdDoc.Id, false).Result;
            //Get queue size
            int i = myController.GetRemovalQueueSize().Result;
        }

        [Test]
        public void Archive()
        {
            //Create controller for document
            using TestSession cf = new();
            IDocument myController = cf.ApiSession.Document;
            //Create default doc
            Document createdDoc = TestHelper.CreateDefaultDoc(this, myController);
            Document createdDoc2 = TestHelper.CreateDefaultDoc(this, myController);

            DocumentBase result = myController.Archive(createdDoc.Id).Result;
            Assert.That(result, Is.Not.Null);
            Assert.That(result is Document, Is.True);
            //Archive second document tomorrow
            result = myController.Archive(createdDoc2.Id, Matterial.UnixTimestamp(DateTime.UtcNow.AddDays(1))).Result;
            Assert.That(result, Is.Not.Null);
            Assert.That(result is Document, Is.True);
        }

        [Test]
        public void UnArchive()
        {
            //Create controller for document
            using TestSession cf = new();
            IDocument myController = cf.ApiSession.Document;
            //Create default doc
            Document createdDoc = TestHelper.CreateDefaultDoc(this, myController);

            DocumentBase result = myController.Archive(createdDoc.Id, Matterial.UnixTimestamp(DateTime.UtcNow.AddDays(1))).Result;
            Assert.That(result, Is.Not.Null);
            Assert.That(result is Document, Is.True);
            //Archive second document tomorrow
            result = myController.UnArchive(createdDoc.Id).Result;
            Assert.That(result, Is.Not.Null);
            Assert.That(result is Document, Is.True);
        }

        [Test]
        public void Rating()
        {
            //Create controller for document
            using TestSession cf = new();
            IDocument myController = cf.ApiSession.Document;
            //Create default doc
            Document createdDoc = TestHelper.CreateDefaultDoc(this, myController);
            bool b = myController.RateDocument(createdDoc.Id, createdDoc.LanguageVersionId).Result;
            Assert.That(b, Is.True);
            b = myController.IsRatedDocument(createdDoc.Id, createdDoc.LanguageVersionId).Result;
            Assert.That(b, Is.True);
            b = myController.UnRateDocument(createdDoc.Id, createdDoc.LanguageVersionId).Result;
            Assert.That(b, Is.True);
            b = myController.IsRatedDocument(createdDoc.Id, createdDoc.LanguageVersionId).Result;
            Assert.That(b, Is.False);
        }

        [Test]
        public void ChangeLog()
        {
            //Create controller for document
            using TestSession cf = new();
            IDocument myController = cf.ApiSession.Document;
            //Create default doc
            Document createdDoc = TestHelper.CreateDefaultDoc(this, myController);
            ChangeLogParams p = new(createdDoc.Id);
            ListResult<DocumentChangeLog> log = myController.GetChangeLog(p).Result;
            Assert.That(log, Is.Not.Null);
        }

        [Test]
        public void ChangeLogMap()
        {
            //Create controller for document
            using TestSession cf = new();
            IDocument myController = cf.ApiSession.Document;
            //Create default doc
            Document createdDoc = TestHelper.CreateDefaultDoc(this, myController);
            ChangeLogMapParams p = new(createdDoc.Id)
            {
                Count = true
            };
            DocumentChangeLogMap log = myController.GetChangeLogMap(p).Result;
            Assert.That(log, Is.Not.Null);
        }

        [Test]
        public void ExtensionValues()
        {
            //Create controller for document
            using TestSession cf = new();
            IDocument myController = cf.ApiSession.Document;
            //Create default doc
            List<ExtensionValue> extValues = myController.GetExtensionValues().Result;
            Assert.That(extValues, Is.Not.Null);
            Assert.That(extValues, Is.Not.Empty);
        }

        [Test]
        public void DeclineReview()
        {
            using TestSession cf = new();
            IDocument myController = cf.ApiSession.Document;
            //Create a doc with review request
            DocHelper dh = new(this, myController);
            Document newDoc = dh.Create(PublishRequestTypes.ReviewRequested);
            bool b = myController.DeclineReview(newDoc.Id, newDoc.LanguageVersionId).Result;
            Assert.That(b, Is.True);
        }

        [Test]
        public void DeclineReviewWithTask()
        {
            using TestSession cf = new();
            IDocument myController = cf.ApiSession.Document;
            //Create a doc with review request
            DocHelper dh = new(this, myController);
            Document newDoc = dh.Create(PublishRequestTypes.ReviewRequested);
            MtrTask newTask = new()
            {
                Description = "Review " + newDoc.LanguageVersionTitle,
                AssignedRole = cf.ApiSession.LoggedOnPerson().RolePersonal,
                TaskStatusId = TaskStatusId.Open
            };
            bool b = myController.DeclineReview(newDoc.Id, newDoc.LanguageVersionId, newTask).Result;
            Assert.That(b, Is.True);
        }

        [Test]
        public void ReconvertDocument()
        {
            using TestSession cf = new();
            IDocument myController = cf.ApiSession.Document;
            DocHelper dh = new(this, myController);
            Document newDoc = dh.Create();
            int i = myController.ReconvertDocument(newDoc).Result;
            Assert.That(i, Is.GreaterThan(0));
        }

        [Test]
        public void ReconvertAll()
        {
            using TestSession cf = new();
            IDocument myController = cf.ApiSession.Document;
            DocHelper dh = new(this, myController);
            _ = dh.Create();
            _ = dh.Create();
            int i = myController.ReconvertAll(true).Result;
            Assert.That(i, Is.GreaterThan(0));
        }

        [Test]
        public void GetAttachments()
        {
            using TestSession cf = new();
            IDocument myController = cf.ApiSession.Document;
            DocHelper dh = new(this, myController);
            //Create doc with attachments
            List<string> docAttachments = TestHelper.GetAttachments();
            dh.AttachmentFiles = docAttachments;
            Document newDoc = dh.Create();
            AttachmentsLoadParams lp = new();
            List<Attachment> attachList = myController.GetAttachments(newDoc.Id, lp).Result;
            bool foundAll = true;
            foreach (var item in docAttachments)
            {
                bool found = false;
                foreach (var attach in attachList)
                {
                    if (attach.Name == Path.GetFileName(item))
                    {
                        found = true;
                        break;
                    }
                }
                foundAll = found && foundAll;
            }
            Assert.That(foundAll, Is.True);
        }

        [Test]
        public void GetAttachment()
        {
            using TestSession cf = new();
            IDocument myController = cf.ApiSession.Document;
            DocHelper dh = new(this, myController);
            //Create doc with attachments
            List<string> docAttachments = TestHelper.GetAttachments(1);
            dh.AttachmentFiles = docAttachments;
            Document newDoc = dh.Create();
            AttachmentLoadParams lp = new();
            Stream aStream = myController.GetAttachment(newDoc.Id, newDoc.Attachments[0].Id, lp).Result;
            Assert.That(aStream, Is.Not.Null);
            string fPath = Path.Combine(BaseConfig.WorkFolder(), newDoc.Attachments[0].Name);
            TestHelper.SaveStreamToFile(aStream, fPath);
            Assert.That(File.Exists(fPath), Is.True);
        }

        [Test]
        public void GetAttachmentByVersion()
        {
            using TestSession cf = new();
            IDocument myController = cf.ApiSession.Document;
            DocHelper dh = new(this, myController);
            //Create doc with attachments
            List<string> docAttachments = TestHelper.GetAttachments(1);
            dh.AttachmentFiles = docAttachments;
            Document newDoc = dh.Create();
            AttachmentLoadParams lp = new();
            Stream aStream = myController.GetAttachment(newDoc.Id, newDoc.LanguageVersionId, newDoc.Attachments[0].Id, lp).Result;
            Assert.That(aStream, Is.Not.Null);
            string fPath = Path.Combine(BaseConfig.WorkFolder(), newDoc.Attachments[0].Name);
            TestHelper.SaveStreamToFile(aStream, fPath);
            Assert.That(File.Exists(fPath), Is.True);
        }

        [Test]
        public void ExecuteBatchAction()
        {
            //Create controller for document
            using TestSession cf = new();
            IDocument myController = cf.ApiSession.Document;
            myController.ExecuteBatchAction(BatchActions.ARCHIVE, new BatchActionAdditionalData());
        }

        [Test]
        public void CreateMultipleVersionAndLanguages()
        {
            using TestSession cf = new();
            IDocument myController = cf.ApiSession.Document;
            Document createdDoc = TestHelper.CreateDefaultDoc(this, myController);
            long docId = createdDoc.Id;
            //Lock new document
            DocUpdateParams options = new()
            {
                UniqueLockId = Guid.NewGuid().ToString(),
                PublishRequestType = PublishRequestTypes.Unreviewed
            };
            DocumentLock l = myController.Lock(createdDoc.Id, options.UniqueLockId).Result;
            ILanguage lc = cf.ApiSession.Language;
            LanguageParams p = new();
            List<Language> languages = lc.Load(p).Result;
            //Create versions for each language
            int docsCreated = 1;
            foreach (var lang in languages)
            {
                if (lang.Active)
                {
                    createdDoc.LanguageVersionLanguageKey = lang.I18nKey;

                    for (int i = 0; i < 5; i++)
                    {
                        createdDoc.LanguageVersionTitle = createdDoc.LanguageVersionLanguageKey + " version " + (i + 1);
                        createdDoc = myController.Update(createdDoc, options).Result;
                        docsCreated++;
                    }
                }
            }
            //Unloack
            _ = myController.Unlock(options.UniqueLockId).Result;
            //Check results
            LoadDocParams loadP = new();
            loadP.DocumentIds.Add(docId);
            loadP.AllLanguages = true;
            loadP.AllVersions = true;
            ListResult<Document> docs = myController.Load(loadP).Result;
            Assert.That(docs.Results, Has.Count.EqualTo(docsCreated));

            loadP.AllVersions = false;
            docs = myController.Load(loadP).Result;
        }

        [Test]
        public void CreateMultipleDocsAndVersionsAndLanguages()
        {
            using TestSession cf = new();
            IDocument myController = cf.ApiSession.Document;
            ILanguage lc = cf.ApiSession.Language;
            //Activate all languages
            LanguageParams p = new();
            List<Language> languages = lc.Load(p).Result;

            int numDocsToCreate = 3;
            int docsCreated = 0;
            for (int k = 0; k < numDocsToCreate; k++)
            {
                Document createdDoc = TestHelper.CreateDefaultDoc(this, myController);
                long docId = createdDoc.Id;
                docsCreated++;
                //Lock new document
                DocUpdateParams options = new()
                {
                    UniqueLockId = Guid.NewGuid().ToString(),
                    PublishRequestType = PublishRequestTypes.Unreviewed
                };
                DocumentLock l = myController.Lock(createdDoc.Id, options.UniqueLockId).Result;

                //Create versions for each language

                foreach (var lang in languages)
                {
                    if (lang.Active)
                    {
                        createdDoc.LanguageVersionLanguageKey = lang.I18nKey;

                        for (int i = 0; i < 5; i++)
                        {
                            createdDoc.LanguageVersionTitle = createdDoc.LanguageVersionLanguageKey + " version " + (i + 1);
                            createdDoc = myController.Update(createdDoc, options).Result;
                            docsCreated++;
                        }
                    }
                }
                //Unloack
                _ = myController.Unlock(options.UniqueLockId).Result;

                //Check results
                LoadDocParams loadP = new();
                loadP.DocumentIds.Add(docId);
                loadP.AllLanguages = true;
                loadP.AllVersions = true;
                ListResult<Document> docs = myController.Load(loadP).Result;
                Assert.That(docs.Results, Has.Count.EqualTo(docsCreated));
                docsCreated = 0;
            }
        }

        [Test]
        public void CreateAndUpdate()
        {
            using TestSession cf = new();
            IDocument myController = cf.ApiSession.Document;
            List<UploadFile> uf = new();
            List<string> files = new(Directory.EnumerateFiles(BaseConfig.TestUploadFilesDir()));
            foreach (var item in files)
            {
                uf.Add(new UploadFile(item, Path.GetFileNameWithoutExtension(item), TempFileType.Attachment));
            }
            string contextToken = Guid.NewGuid().ToString();
            List<TempFileDescriptor> tempFiles = myController.UploadFile(uf, contextToken, Matterial.DefaultLanguage).Result;
            Assert.That(tempFiles, Has.Count.EqualTo(uf.Count));
            Document newDoc = new("Doc with Attachments" + DateTime.Now.ToString(), Matterial.DefaultLanguage)
            {
                LanguageVersionAbstract = "The abstract is here"
            };

            string unlockId = Guid.NewGuid().ToString();
            Document uploadedDoc = myController.Create(newDoc, new DocUpdateParams { ContextToken = tempFiles[0].ContextToken, UniqueLockId = unlockId, PublishRequestType = PublishRequestTypes.Unreviewed }).Result;
            Assert.That(uploadedDoc, Is.Not.Null);
            Document updateDoc = new(uploadedDoc.Id)
            {
                LanguageVersionTitle = "New title"
            };
            Document docUpdated = myController.Update(updateDoc, new DocUpdateParams { UniqueLockId = unlockId }).Result;
            Document reloadedDoc = myController.Load(uploadedDoc.Id, Matterial.DefaultLanguage.ToString()).Result;
        }
    }
}