﻿using MatterialCore;
using MatterialCore.Entity;
using MatterialCore.Enums;
using MatterialCore.Interfaces;
using MatterialCore.Params.Doc;

using NUnit.Framework;

using System.Collections.Generic;
using System.IO;
using System.Threading;

using MatterialCore.Tests.Helper;

namespace MatterialCore.Tests.Integration
{
    internal class Load : TestBase
    {
        #region Setup

        [OneTimeSetUp] //once per class
        public void Setup()
        {
        }

        [OneTimeTearDown] //once per class
        public void TearDown()
        {
        }

        [SetUp] //once per test
        public void SetupTest()
        {
        }

        [TearDown] //once per test
        public void TearDownTest()
        {
        }

        #endregion Setup

        [Test]
        public void LoadDoc()
        {
            //Create controller for document
            using TestSession cf = new();
            IDocument myController = cf.ApiSession.Document;
            //Create default doc
            Document createdDoc = TestHelper.CreateDefaultDoc(this, myController);

            //Load it
            Document loadedDoc = myController.Load(createdDoc.Id, createdDoc.LanguageVersionLanguageKey).Result;
            Assert.That(loadedDoc, Is.Not.Null);
            Assert.That(loadedDoc.Id, Is.EqualTo(createdDoc.Id));
        }

        [Test]
        public void LoadMultipleDocs()
        {
            //Create controller for document
            using TestSession cf = new();
            IDocument myController = cf.ApiSession.Document;
            //Create default doc
            Document createdDoc1 = TestHelper.CreateDefaultDoc(this, myController, out string unlockId1);
            Document createdDoc2 = TestHelper.CreateDefaultDoc(this, myController, out string unlockId2);
            Document createdDoc3 = TestHelper.CreateDefaultDoc(this, myController, out string unlockId3);
            //Load it
            LoadDocParams p = new();
            p.DocumentIds.Add(createdDoc1.Id);
            p.DocumentIds.Add(createdDoc2.Id);
            p.DocumentIds.Add(createdDoc3.Id);

            p.OrderBy = DocLoadOrderBy.Id;
            p.OrderDir = OrderDirection.asc;
            p.LanguageKey = Matterial.DefaultLanguage;

            ListResult<Document> loadedDocList = myController.Load(p).Result;
            Assert.Multiple(() =>
            {
                Assert.That(loadedDocList, Is.Not.Null);
                Assert.That(p.DocumentIds, Has.Count.EqualTo(loadedDocList.Results.Count));
            });
            int i = 0;
            foreach (var item in loadedDocList.Results)
            {
                Assert.That(item.Id, Is.EqualTo(p.DocumentIds[i]));
                i++;
            }
            //Different order
            p.OrderDir = OrderDirection.desc;
            loadedDocList = myController.Load(p).Result;
            Assert.Multiple(() =>
            {
                Assert.That(loadedDocList, Is.Not.Null);
                Assert.That(p.DocumentIds, Has.Count.EqualTo(loadedDocList.Results.Count));
            });
            i = p.DocumentIds.Count - 1;
            foreach (var item in loadedDocList.Results)
            {
                Assert.That(item.Id, Is.EqualTo(p.DocumentIds[i]));
                i--;
            }
        }

        [Test]
        public void LoadTrashedDocs()
        {
            //Create controller for document
            using TestSession cf = new();
            IDocument myController = cf.ApiSession.Document;
            //Create default doc
            Document createdDoc1 = TestHelper.CreateDefaultDoc(this, myController);
            Document createdDoc2 = TestHelper.CreateDefaultDoc(this, myController);
            Document createdDoc3 = TestHelper.CreateDefaultDoc(this, myController);
            List<long> idList = new()
            {
                createdDoc1.Id,
                createdDoc2.Id,
                createdDoc3.Id
            };

            //Load it
            LoadDocTrashParams p = new()
            {
                OrderBy = DocLoadOrderBy.Id,
                OrderDir = OrderDirection.asc,
                AllLanguages = true
            };

            DocumentBase lockDoc = myController.Remove(createdDoc1.Id, false).Result;
            Assert.That(lockDoc is Document, Is.True);
            lockDoc = myController.Remove(createdDoc2.Id, false).Result; ;
            Assert.That(lockDoc is Document, Is.True);
            lockDoc = myController.Remove(createdDoc3.Id, false).Result; ;
            Assert.That(lockDoc is Document, Is.True);

            ListResult<Document> loadedDocList = myController.LoadTrash(p).Result;
            Assert.That(loadedDocList, Is.Not.Null);
            Assert.That(loadedDocList.Results, Has.Count.EqualTo(idList.Count));
            int i = 0;
            foreach (var item in loadedDocList.Results)
            {
                Assert.That(item.Id, Is.EqualTo(idList[i]));
                i++;
            }
            //Different order
            p.OrderDir = OrderDirection.desc;
            loadedDocList = myController.LoadTrash(p).Result;
            Assert.That(loadedDocList, Is.Not.Null);
            Assert.That(loadedDocList.Results, Has.Count.EqualTo(idList.Count));
            i = idList.Count - 1;
            foreach (var item in loadedDocList.Results)
            {
                Assert.That(item.Id, Is.EqualTo(idList[i]));
                i--;
            }
        }

        private void GetFile(LoadMainFileFormats format, IDocument myController, Document doc)
        {
            bool b = true;
            int i = 0;
            long id = doc.Id;
            LangKey lang = doc.LanguageVersionLanguageKey.ToLanguageKey();
            string extension = "md";
            switch (format)
            {
                case LoadMainFileFormats.Pdf:
                    extension = "pdf";
                    break;

                case LoadMainFileFormats.Thumbnail:
                    extension = "png";
                    break;

                case LoadMainFileFormats.Html:
                    extension = "html";
                    break;
            }
            string outFile = Path.Combine(BaseConfig.WorkFolder(), "MainFile." + extension);
            if (File.Exists(outFile))
            {
                File.Delete(outFile);
            }
            bool success = false;
            do
            {
                try
                {
                    using Stream myStream = myController.GetMainFile(id, new MainFileLoadParams { LanguageKey = lang, LoadFormat = format }).Result;
                    TestHelper.SaveStreamToFile(myStream, outFile);
                }
                catch (System.Exception)
                {
                }
                if (File.Exists(outFile) && new FileInfo(outFile).Length > 0)
                {
                    b = false;
                    success = true;
                }
                else
                {
                    Thread.Sleep(1000);
                    if (i > 10)
                        b = false;
                }
                i++;
            } while (b);
            Assert.That(success, Is.True, "Failed to load " + extension + " file after " + i + " seconds.");
        }

        [Test]
        [Ignore("Ignore due to race time condition")]
        public void GetMainFile()
        {
            //Create controller for document
            using TestSession cf = new();
            IDocument myController = cf.ApiSession.Document;
            Document newDoc = TestHelper.CreateDefaultDoc(this, myController);
            bool keepGoing = true;
            int i = 0;
            do
            {
                Thread.Sleep(1000);
                newDoc = myController.Load(newDoc.Id, newDoc.LanguageVersionLanguageKey).Result;
                if (!string.IsNullOrEmpty(newDoc.LanguageVersionCasIdPdf) && !string.IsNullOrEmpty(newDoc.LanguageVersionCasIdThumbnail))
                {
                    keepGoing = false;
                }
                i++;
            } while (keepGoing && i < 60);
            Assert.That(keepGoing, Is.False, "The background conversion to PDF did not finish after " + i + " seconds.");

            GetFile(LoadMainFileFormats.Pdf, myController, newDoc);
            GetFile(LoadMainFileFormats.Thumbnail, myController, newDoc);
            GetFile(LoadMainFileFormats.AsIs, myController, newDoc);
            GetFile(LoadMainFileFormats.Html, myController, newDoc);
        }
    }
}