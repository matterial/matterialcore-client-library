﻿using MatterialCore;
using MatterialCore.Entity;
using MatterialCore.Interfaces;
using MatterialCore.Params.Doc;

using NUnit.Framework;

using System;
using System.Collections.Generic;

using MatterialCore.Tests.Helper;

namespace MatterialCore.Tests.Integration
{
    internal class Properties : TestBase
    {
        #region Setup

        [OneTimeSetUp] //once per class
        public void Setup()
        {
        }

        [OneTimeTearDown] //once per class
        public void TearDown()
        {
        }

        [SetUp] //once per test
        public void SetupTest()
        {
        }

        [TearDown] //once per test
        public void TearDownTest()
        {
        }

        #endregion Setup

        [Test]
        public void CreateController()
        {
            using TestSession cf = new();
            IProperty myController = cf.ApiSession.Property;
            Assert.That(myController, Is.Not.Null);
        }

        [Test]
        public void CreateProperty()
        {
            using TestSession cf = new();
            IProperty myController = cf.ApiSession.Property;
            Assert.That(myController, Is.Not.Null);
            //New property
            string propName = "My Property " + DateTime.Now.ToString();
            AdditionalProperty prop = new(propName, propName + " - Description text");
            long newPropId = myController.Create(prop, Matterial.DefaultLanguage).Result;
            Assert.That(newPropId, Is.GreaterThan(0));
            AdditionalProperty newProp = myController.Load(newPropId, new PropertyLoadByIdParams()).Result;
            AddToCreated(newProp);
        }

        [Test]
        public void UpdateProperty()
        {
            using TestSession cf = new();
            IProperty myController = cf.ApiSession.Property;
            Assert.That(myController, Is.Not.Null);
            //New property
            string propName = "My Property " + DateTime.Now.ToString();
            AdditionalProperty prop = new(propName, propName + " - Description text");
            long newPropId = myController.Create(prop, Matterial.DefaultLanguage).Result;
            Assert.That(newPropId, Is.GreaterThan(0));
            AdditionalProperty newProp = myController.Load(newPropId, new PropertyLoadByIdParams(Matterial.DefaultLanguage)).Result;
            AddToCreated(newProp);

            //Now update the name and description
            newProp.Name = "test " + Guid.NewGuid().ToString();
            newProp.Description = "Descr " + Guid.NewGuid().ToString();
            newPropId = myController.Update(newProp.Id, newProp, Matterial.DefaultLanguage).Result;
            Assert.That(newPropId, Is.GreaterThan(0));
            AdditionalProperty updProp = myController.Load(newPropId, new PropertyLoadByIdParams(Matterial.DefaultLanguage)).Result;
            Assert.Multiple(() =>
            {
                //Compare
                Assert.That(updProp.Name, Is.EqualTo(newProp.Name));
                Assert.That(updProp.Description, Is.EqualTo(newProp.Description));
            });
        }

        [Test]
        public void FollowProperty()
        {
            using TestSession cf = new();
            IProperty myController = cf.ApiSession.Property;
            Assert.That(myController, Is.Not.Null);
            //New property
            string propName = "My Property " + DateTime.Now.ToString();
            AdditionalProperty prop = new(propName, propName + " - Description text");
            long newPropId = myController.Create(prop, Matterial.DefaultLanguage).Result;
            Assert.That(newPropId, Is.GreaterThan(0));
            AdditionalProperty newProp = myController.Load(newPropId, new PropertyLoadByIdParams(Matterial.DefaultLanguage)).Result;
            AddToCreated(newProp);

            //Now follow
            bool b = myController.Follow(newPropId, Matterial.DefaultLanguage).Result;
            Assert.That(b, Is.True);
            b = myController.IsFollowing(newPropId, Matterial.DefaultLanguage).Result;
            Assert.That(b, Is.True);
            b = myController.UnFollow(newPropId, Matterial.DefaultLanguage).Result;
            Assert.That(b, Is.True);
            b = myController.IsFollowing(newPropId, Matterial.DefaultLanguage).Result;
            Assert.That(b, Is.False);
        }

        [Test]
        public void SetAdditionalProperties()
        {
            using TestSession cf = new();
            IProperty myController = cf.ApiSession.Property;
            Assert.That(myController, Is.Not.Null);
            //New property
            string propName = "My Property " + DateTime.Now.ToString();
            AdditionalProperty prop = new(propName, propName + " - Description text");
            long newPropId = myController.Create(prop, Matterial.DefaultLanguage).Result;
            Assert.That(newPropId, Is.GreaterThan(0));
            AdditionalProperty newProp = myController.Load(newPropId, new PropertyLoadByIdParams()).Result;
            AddToCreated(newProp);

            IDocument docControl = cf.ApiSession.Document;
            Document doc = TestHelper.CreateDefaultDoc(this, docControl);
            List<AdditionalProperty> props = new() { newProp };

            Document d = docControl.SetAdditionalProperties(doc.Id, props).Result;
            Assert.That(d.AdditionalProperties, Is.Not.Empty);
        }
    }
}