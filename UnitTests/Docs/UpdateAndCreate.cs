﻿using MatterialCore;
using MatterialCore.Entity;
using MatterialCore.Enums;
using MatterialCore.Interfaces;
using MatterialCore.Params.Doc;
using MatterialCore.Params.Person;

using NUnit.Framework;

using System;
using System.Collections.Generic;
using System.IO;

using MatterialCore.Tests.Helper;

namespace MatterialCore.Tests.Integration
{
    internal class UpdateAndCreate : TestBase
    {
        #region Setup

        [OneTimeSetUp] //once per class
        public void Setup()
        {
        }

        [OneTimeTearDown] //once per class
        public void TearDown()
        {
        }

        [SetUp] //once per test
        public void SetupTest()
        {
        }

        [TearDown] //once per test
        public void TearDownTest()
        {
        }

        #endregion Setup

        [Test]
        public void CreateDocument()
        {
            using TestSession cf = new();
            IDocument myController = cf.ApiSession.Document;
            Document newDoc = TestHelper.CreateDefaultDoc(this, myController);
            Assert.That(newDoc, Is.Not.Null);
        }

        [Test]
        public void CreateSnap()
        {
            //Create controller for document
            using TestSession cf = new();
            IDocument myController = cf.ApiSession.Document;
            //Create snap
            string docTitle = "My new Snap " + DateTime.Now.ToString();
            Document newDoc = new(docTitle, Matterial.DefaultLanguage);
            Assert.That(newDoc, Is.Not.Null);
            UpdateContextParams p = new() { ContextToken = Guid.NewGuid().ToString() };

            Document newSnap = myController.CreateSnap(newDoc, p).Result;
            AddToCreated(newSnap);

            Assert.That(newSnap, Is.Not.Null);
            Assert.That(newSnap.Snap, Is.True);
        }

        [Test]
        public void UnSnap()
        {
            //Create controller for document
            using TestSession cf = new();
            IDocument myController = cf.ApiSession.Document;
            //Create snap
            string docTitle = "My new Snap " + DateTime.Now.ToString();
            Document newDoc = new(docTitle, Matterial.DefaultLanguage);
            Assert.That(newDoc, Is.Not.Null);
            UpdateContextParams p = new() { ContextToken = Guid.NewGuid().ToString() };

            Document newSnap = myController.CreateSnap(newDoc, p).Result;
            AddToCreated(newSnap);

            Assert.That(newSnap, Is.Not.Null);
            Assert.That(newSnap.Snap, Is.True);
            //Unsnap
            bool b = myController.Unsnap(newSnap).Result;
            Assert.That(b, Is.True);
        }

        [Test]
        public void CreateAndUpdateTemplate()
        {
            //Create controller for document
            using TestSession cf = new();
            IDocument myController = cf.ApiSession.Document;
            //Create template
            string docTitle = "My new template " + DateTime.Now.ToString();
            Document newDoc = new(docTitle, Matterial.DefaultLanguage);
            Assert.That(newDoc, Is.Not.Null);
            UpdateTemplateParams p = new()
            {
                ContextToken = Guid.NewGuid().ToString(),
                PublishRequestType = PublishRequestTypes.Unreviewed,
                UniqueLockId = Guid.NewGuid().ToString()
            };

            Document newTemplate = myController.CreateTemplate(newDoc, p).Result;
            newTemplate.LanguageVersionTitle = "Updated title of template";
            Document updatedTemplate = myController.UpdateTemplate(newTemplate, new UpdateTemplateParams { UniqueLockId = p.UniqueLockId, PublishRequestType = PublishRequestTypes.Unreviewed }).Result;
            _ = myController.Unlock(p.UniqueLockId).Result;
            AddToCreated(newTemplate);

            Assert.That(newTemplate, Is.Not.Null);
            Assert.That(newTemplate.Template, Is.True);
        }

        [Test]
        public void UpdateDocument()
        {
            //Create controller for document
            using TestSession cf = new();
            IDocument myController = cf.ApiSession.Document;
            //Create new doc first
            Document newDoc = TestHelper.CreateDefaultDoc(this, myController);
            Assert.That(newDoc, Is.Not.Null);

            //Lock it
            string lockId = Guid.NewGuid().ToString();
            DocumentLock docLock = myController.Lock(newDoc.Id, lockId).Result;
            Assert.That(docLock, Is.Not.Null);
            Assert.That(docLock.LockedForMe, Is.True);

            //Update it
            newDoc.LanguageVersionTitle = "Updated document " + DateTime.Now.ToString();
            DocUpdateParams p = new()
            {
                PublishRequestType = PublishRequestTypes.Reviewed,
                UniqueLockId = lockId
            };
            Document updatedDoc = myController.Update(newDoc, p).Result;
            Assert.That(updatedDoc, Is.Not.Null);
            Assert.That(updatedDoc.LanguageVersionTitle, Is.EqualTo(newDoc.LanguageVersionTitle));
            //Unlock
            bool b = myController.Unlock(lockId).Result;
            Assert.That(b, Is.True);
        }

        [Test]
        public void MarkAsReviewed()
        {
            using TestSession cf = new();
            IDocument myController = cf.ApiSession.Document;
            Document newDoc = TestHelper.CreateDefaultDoc(this, myController, out string lockId, false, PublishRequestTypes.ReviewRequested);
            bool b = myController.MarkAsReviewed(newDoc.Id, newDoc.LanguageVersionId).Result;
            Assert.That(b, Is.True);
        }

        [Test]
        public void UpdateBio()
        {
            //Create controller for document
            using TestSession cf = new();
            IDocument myController = cf.ApiSession.Document;
            IPerson pc = cf.ApiSession.Person;

            string cvContent = "This is the cv content";
            bool b = cf.ApiSession.UploadBioDocument(cvContent, LangKey.en).Result;

            Person p = pc.LoadByAccountId(cf.ApiSession.LoggedOnPerson().AccountId, new GetPersonByIdParams { BioDocumentLanguageKey = LangKey.en }).Result;
            using Stream str = myController.GetMainFile(p.BioDocument.Id, new MainFileLoadParams { LanguageKey = LangKey.en }).Result;
            string outFile = Path.Combine(BaseConfig.WorkFolder(), "mainFile.md");
            TestHelper.SaveStreamToFile(str, outFile);
            string fileContent = File.ReadAllText(outFile);
            Assert.That(fileContent, Is.EqualTo(cvContent));
        }

        [Test]
        public void UploadAttachmentFile()
        {
            //Create controller for document
            using TestSession cf = new();
            IDocument myController = cf.ApiSession.Document;

            List<UploadFile> uf = new();
            List<string> files = new(Directory.EnumerateFiles(BaseConfig.TestUploadFilesDir()));
            foreach (var item in files)
            {
                uf.Add(new UploadFile(item, Path.GetFileNameWithoutExtension(item), TempFileType.Attachment));
            }
            string contextToken = Guid.NewGuid().ToString();
            List<TempFileDescriptor> tempFiles = myController.UploadFile(uf, contextToken, Matterial.DefaultLanguage).Result;
            Assert.That(tempFiles, Has.Count.EqualTo(uf.Count));
            Document newDoc = new("Doc with Attachments" + DateTime.Now.ToString(), Matterial.DefaultLanguage);
            string unlockId = Guid.NewGuid().ToString();
            Document uploadedDoc = myController.Create(newDoc, new DocUpdateParams { ContextToken = tempFiles[0].ContextToken, UniqueLockId = unlockId, PublishRequestType = PublishRequestTypes.Unreviewed }).Result;
            Assert.That(uploadedDoc, Is.Not.Null);
        }

        [Test]
        public void UploadDocumentText()
        {
            //Create controller for document
            using TestSession cf = new();
            IDocument myController = cf.ApiSession.Document;

            string content = "The text content";
            string contextToken = Guid.NewGuid().ToString();
            TempFileDescriptor tempFile = myController.UploadText(content, contextToken, Matterial.DefaultLanguage).Result;
            Assert.That(tempFile, Is.Not.Null);
            Document newDoc = new("Doc with Text doc " + DateTime.Now.ToString(), Matterial.DefaultLanguage);
            string unlockId = Guid.NewGuid().ToString();
            Document uploadedDoc = myController.Create(newDoc, new DocUpdateParams { ContextToken = tempFile.ContextToken, UniqueLockId = unlockId, PublishRequestType = PublishRequestTypes.Unreviewed }).Result;
            Assert.That(uploadedDoc, Is.Not.Null);
        }

        [Test]
        public void GetTempFile()
        {
            //Create controller for document
            using TestSession cf = new();
            IDocument myController = cf.ApiSession.Document;

            List<UploadFile> uf = new();
            List<string> files = new(Directory.EnumerateFiles(BaseConfig.TestUploadFilesDir()));
            foreach (var item in files)
            {
                uf.Add(new UploadFile(item, Path.GetFileNameWithoutExtension(item), TempFileType.Attachment));
            }
            string contextToken = Guid.NewGuid().ToString();
            List<TempFileDescriptor> tempFiles = myController.UploadFile(uf, contextToken, Matterial.DefaultLanguage).Result;
            Assert.That(tempFiles, Has.Count.EqualTo(uf.Count));
            using Stream stream = myController.GetTempFile(tempFiles[0], true).Result;
            string extension = MatterialCore.Matterial.GetExtension(tempFiles[0].MimeType);
            TestHelper.SaveStreamToFile(stream, Path.Combine(BaseConfig.WorkFolder(), tempFiles[0].FileName) + extension);
        }

        [Test]
        public void RemoveTempFiles()
        {
            //Create controller for document
            using TestSession cf = new();
            IDocument myController = cf.ApiSession.Document;

            List<UploadFile> uf = new();
            List<string> files = new(Directory.EnumerateFiles(BaseConfig.TestUploadFilesDir()));
            foreach (var item in files)
            {
                uf.Add(new UploadFile(item, Path.GetFileNameWithoutExtension(item), TempFileType.Attachment));
            }
            string contextToken = Guid.NewGuid().ToString();
            List<TempFileDescriptor> tempFiles = myController.UploadFile(uf, contextToken, Matterial.DefaultLanguage).Result;
            Assert.That(tempFiles, Has.Count.EqualTo(uf.Count));

            myController.RemoveTempfiles(contextToken);
        }

        [Test]
        public void RemoveTempFile()
        {
            //Create controller for document
            using TestSession cf = new();
            IDocument myController = cf.ApiSession.Document;

            List<UploadFile> uf = new();
            List<string> files = new(Directory.EnumerateFiles(BaseConfig.TestUploadFilesDir()));
            foreach (var item in files)
            {
                uf.Add(new UploadFile(item, Path.GetFileNameWithoutExtension(item), TempFileType.Attachment));
                break;
            }
            string contextToken = Guid.NewGuid().ToString();
            List<TempFileDescriptor> tempFiles = myController.UploadFile(uf, contextToken, Matterial.DefaultLanguage).Result;
            Assert.That(tempFiles, Has.Count.EqualTo(uf.Count));

            myController.RemoveTempfile(TempFileType.Attachment, tempFiles[0].ContextToken, tempFiles[0].FileToken, Matterial.DefaultLanguage);
        }

        [Test]
        public void RemoveTempFileByDescriptor()
        {
            //Create controller for document
            using TestSession cf = new();
            IDocument myController = cf.ApiSession.Document;

            List<UploadFile> uf = new();
            List<string> files = new(Directory.EnumerateFiles(BaseConfig.TestUploadFilesDir()));
            foreach (var item in files)
            {
                uf.Add(new UploadFile(item, Path.GetFileNameWithoutExtension(item), TempFileType.Attachment));
                break;
            }
            string contextToken = Guid.NewGuid().ToString();
            List<TempFileDescriptor> tempFiles = myController.UploadFile(uf, contextToken, Matterial.DefaultLanguage).Result;
            Assert.That(tempFiles, Has.Count.EqualTo(uf.Count));

            myController.RemoveTempfile(tempFiles[0]);
        }
    }
}