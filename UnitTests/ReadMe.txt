﻿

##########   Matterial Core Library UnitTests   ############

Before you can compile and run the UnitTests, please add a new class TestCredentials.cs to the project with the following content
(edit the credentials as needed):

namespace MatterialCore.Tests.Helper
{ 
    internal static class TestCrendentials
    {
        public const string ApiUserName = "user@mycompany.com";
        public const string ApiUserPassword = "password";"

        public const string Url = "https://my.matterial.com";
    }
}