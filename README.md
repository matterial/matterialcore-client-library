# MatterialCore Client Library

The matterial core client library is a library written in C# 8.0. (.Net Standard 2.1). The library encapsulates the [REST Interface](https://www.matterial.com/de/documentation/api/) of the matterial appliaction.

## Library Dependencies
.Net Standard Library 2.1
JSON.Net 12.0.3 (Newtonsoft.Json)

The [Nullable Reference Types](https://docs.microsoft.com/en-us/dotnet/csharp/whats-new/csharp-8#nullable-reference-types) feature is enabled.

You can find the full documentation and example code [here](https://www.matterial.com/documentation/api/655#intro).

