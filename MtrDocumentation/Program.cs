﻿using MatterialCore;
using MatterialCore.Entity;

using Newtonsoft.Json;

using System;
using System.Collections.Generic;

using System.IO;
using System.Reflection;

using static MatterialCore.MtrPropertyAttribute;

namespace MtrDocumentation
{
    /// <summary>
    /// This console application creates a documentation in Markdown which is based on the
    /// Mtr-Attributes used in the source code of the Matterial Core Library.
    /// </summary>
    internal static class Program
    {
        #region private fields

        private const string GitBaseUrl = "https://gitlab.com/matterial/matterial-api/-/blob";
        private const string GitBranch = "develop";
        private const string JSonSchemaPath = "src/resources/json-schema";
        private const string JSonExamplePath = "src/resources/json-sample";
        private const string XmlExamplePath = "src/resources/xml-sample";
        private const string JavaPath = "src/main/java/com/matterial/mtr/api/object";
        private const string OutputFileName = @"..\..\..\MtrClassDocumentation.md";
        private const string DefaultVersionImplemented = "";

        #endregion private fields

        private static void Main()
        {
            List<string> fileContent = new();
            GetAttribute(typeof(Activity), fileContent);
            GetAttribute(typeof(AdditionalProperty), fileContent);
            GetAttribute(typeof(Address), fileContent);
            GetAttribute(typeof(Aggregation), fileContent);
            GetAttribute(typeof(Attachment), fileContent);
            GetAttribute(typeof(BatchActionAdditionalData), fileContent);
            GetAttribute(typeof(Category), fileContent);
            GetAttribute(typeof(CategoryType), fileContent);
            GetAttribute(typeof(Client), fileContent);
            GetAttribute(typeof(Comment), fileContent);
            GetAttribute(typeof(CommunicationData), fileContent);
            GetAttribute(typeof(ContactImage), fileContent);
            GetAttribute(typeof(Credential), fileContent);
            GetAttribute(typeof(CredentialWithInvitationText), fileContent);
            GetAttribute(typeof(Document), fileContent);
            GetAttribute(typeof(DocumentChangeLog), fileContent);
            GetAttribute(typeof(DocumentChangeLogMap), fileContent);
            GetAttribute(typeof(DocumentChangeLogs), fileContent);
            GetAttribute(typeof(DocumentClipBoardEntry), fileContent);
            GetAttribute(typeof(DocumentDuplicate), fileContent);
            GetAttribute(typeof(DocumentForPdfConversion), fileContent);
            GetAttribute(typeof(DocumentLock), fileContent);
            GetAttribute(typeof(ExtensionValue), fileContent);
            GetAttribute(typeof(Highlight), fileContent);
            GetAttribute(typeof(Instance), fileContent);
            GetAttribute(typeof(Language), fileContent);
            GetAttribute(typeof(LoginData), fileContent);
            GetAttribute(typeof(MtrVersion), fileContent);
            GetAttribute(typeof(PasswordContainer), fileContent);
            GetAttribute(typeof(PreferenceMapContainer), fileContent);
            GetAttribute(typeof(Permissions), fileContent);
            GetAttribute(typeof(Person), fileContent);
            GetAttribute(typeof(Role), fileContent);
            GetAttribute(typeof(RoleRight), fileContent);
            GetAttribute(typeof(SavedSearch), fileContent);
            GetAttribute(typeof(SavedSearchParameter), fileContent);
            GetAttribute(typeof(SearchAutocompleteSuggest), fileContent);
            GetAttribute(typeof(MtrTask), fileContent);
            GetAttribute(typeof(MtrTaskStatus), fileContent);
            GetAttribute(typeof(TempFileDescriptor), fileContent);
            GetAttribute(typeof(TrackingItem), fileContent);

            WriteToFile(fileContent);
        }

        #region Extensions

        public static string ToLowerFirstChar(this string input)
        {
            string newString = input;
            if (!String.IsNullOrEmpty(newString) && Char.IsUpper(newString[0]))
                newString = Char.ToLower(newString[0]) + newString.Substring(1);
            return newString;
        }

        public static string ToYesNo(this bool me)
        {
            return me ? "Yes" : "No";
        }

        public static string ToMtrType(this Type me)
        {
            var nullableType = Nullable.GetUnderlyingType(me);
            bool isNullableType = nullableType != null;
            string retVal = me.Name;
            if (isNullableType)
                retVal = nullableType.Name;

            if (retVal == "Int64" || retVal == "Int32")
            {
                return "Number";
            }

            return retVal;
        }

        public static string ToFriendlyString(this MtrDataTypes me)
        {
            switch (me)
            {
                case MtrDataTypes.String:
                case MtrDataTypes.Boolean:
                case MtrDataTypes.Number:
                case MtrDataTypes.Permissions:
                case MtrDataTypes.Role:
                case MtrDataTypes.Person:
                case MtrDataTypes.PreferenceMapContainer:
                    return me.ToString();

                case MtrDataTypes.UnixTimeStamp:
                    return "Number";

                case MtrDataTypes.ListOfLong:
                    return "Array\\<Number>";

                case MtrDataTypes.ListOfLanguage:
                    return "Array\\<Language>";

                case MtrDataTypes.ListOfInstance:
                    return "Array\\<Instance>";

                case MtrDataTypes.ListOfDocument:
                    return "Array\\<Document>";

                case MtrDataTypes.ListOfDocumentChangeLog:
                    return "Array\\<DocumentChangeLog>";

                case MtrDataTypes.ListOfCategory:
                    return "Array\\<Category>";

                case MtrDataTypes.ListOfComment:
                    return "Array\\<Comment>";

                case MtrDataTypes.ListOfAttachment:
                    return "Array\\<Attachment>";

                case MtrDataTypes.ListOfPerson:
                    return "Array\\<Person>";

                case MtrDataTypes.ListOfAddress:
                    return "Array\\<Address>";

                case MtrDataTypes.ListOfClient:
                    return "Array\\<Client>";

                case MtrDataTypes.ListOfCommunicationData:
                    return "Array\\<CommunicationData>";

                case MtrDataTypes.ListOfContactImage:
                    return "Array\\<ContactImage>";

                case MtrDataTypes.ListOfRole:
                    return "Array\\<Role>";

                case MtrDataTypes.ListOfSavedSearchParameter:
                    return "Array\\<SavedSearchParameter>";

                case MtrDataTypes.ListOfRoleRight:
                    return "Array\\<RoleRight>";

                case MtrDataTypes.ListOfTempFileDescriptor:
                    return "Array\\<TempFileDescriptor>";

                case MtrDataTypes.ListOfString:
                    return "Array\\<String>";

                case MtrDataTypes.DictionaryStringObject:
                    return "Map\\<String, object>";

                case MtrDataTypes.ListOfCredentials:
                    return "Array\\<Credentials>";

                case MtrDataTypes.ListOfAdditionalProperties:
                    return "Array\\<AdditionalProperties>";

                case MtrDataTypes.ListOfExtensionValue:
                    return "Array\\<ExtensionValue>";

                case MtrDataTypes.DictionaryLongDocumentChangeLogs:
                    return "Map\\<Number, DocumentChangeLogs>";

                case MtrDataTypes.DictionaryStringDocumentDuplicate:
                    return "Map\\<String, DocumentDuplicate>";

                case MtrDataTypes.DictionaryStringLong:
                    return "Map\\<String, Number>";

                case MtrDataTypes.DictionaryStringString:
                    return "Map\\<String, String>";

                case MtrDataTypes.DictionaryLongLong:
                    return "Map\\<Number, Number>";

                case MtrDataTypes.ContactImage:
                    return "ContactImage";

                case MtrDataTypes.Document:
                    return "Document";

                case MtrDataTypes.DashboardStyles:
                    return "DashboardStyles";
            }
            return "Unknown";
        }

        #endregion Extensions

        public static void GetAttribute(Type t, List<string> lines)
        {
            List<string> unDocumented = new();
            lines.Add("# " + t.Name);
            MtrClassAttribute classAttribute = (MtrClassAttribute)Attribute.GetCustomAttribute(t, typeof(MtrClassAttribute));
            if (!(classAttribute is null))
            {
                if (!String.IsNullOrEmpty(classAttribute.Description))
                {
                    lines.Add(String.Format("{0}", classAttribute.Description));
                }
                if (!String.IsNullOrEmpty(classAttribute.VersionImplemented))
                {
                    lines.Add(String.Format("Implemented since version: {0}", classAttribute.VersionImplemented));
                }
            }

            lines.Add("");
            lines.Add("[Json Schema](" + CombineUri(GitBaseUrl, GitBranch, JSonSchemaPath, t.Name + ".json") + ")");
            lines.Add("");
            lines.Add("[Json Example](" + CombineUri(GitBaseUrl, GitBranch, JSonExamplePath, t.Name + ".json") + ")");
            lines.Add("");
            lines.Add("[Xml Example](" + CombineUri(GitBaseUrl, GitBranch, XmlExamplePath, t.Name + ".xml") + ")");
            lines.Add("");
            lines.Add("[Java](" + CombineUri(GitBaseUrl, GitBranch, JavaPath, t.Name + ".java") + ")");
            lines.Add("");

            lines.Add("");

            string formatString = "| {0} | {1} | {2} | {3} | {4} |";
            string HeaderString = "| Property | Data type | Min/Max | Updateable / Required on create | Version implemented |";
            string headerSubString = "| --- | --- | --- | --- | --- |";
            lines.Add(HeaderString);
            lines.Add(headerSubString);
            int mainInsertIndex = lines.Count;
            int offset = 0;
            // Get instance of the attribute.
            PropertyInfo[] pInfo = t.GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
            foreach (var item in pInfo)
            {
                int insertIndex = lines.Count;
                bool insertInDocumentedArea = false;
                if (String.Compare(item.Name, "id", true) == 0)
                {
                    insertIndex = mainInsertIndex;
                    insertInDocumentedArea = true;
                }
                else
                {
                    if (String.Compare(item.Name, "name", true) == 0)
                    {
                        insertIndex = (mainInsertIndex + 1 - offset) >= lines.Count - 1 ? lines.Count : (mainInsertIndex + 1 - offset);
                        insertInDocumentedArea = true;
                        offset = 1;
                    }
                    else
                    {
                        if (String.Compare(item.Name, "description", true) == 0)
                        {
                            insertIndex = (mainInsertIndex + 2 + offset) >= lines.Count - 1 ? lines.Count : (mainInsertIndex + 2 + offset);
                            insertInDocumentedArea = true;
                            offset = 1;
                        }
                    }
                }
                MtrPropertyAttribute MyAttribute = (MtrPropertyAttribute)Attribute.GetCustomAttribute(item, typeof(MtrPropertyAttribute));
                JsonPropertyAttribute jsonAttrib = (JsonPropertyAttribute)Attribute.GetCustomAttribute(item, typeof(JsonPropertyAttribute));
                string version = DefaultVersionImplemented;
                if (MyAttribute != null)
                {
                    version = MyAttribute.VersionImplemented ?? DefaultVersionImplemented;
                    string minMaxStr = MyAttribute.Min ?? string.Empty;
                    if (String.IsNullOrEmpty(minMaxStr))
                    {
                        if (!String.IsNullOrEmpty(MyAttribute.Max))
                        {
                            minMaxStr = "0/" + MyAttribute.Max;
                        }
                    }
                    else
                    {
                        minMaxStr = MyAttribute.Min + "/" + MyAttribute.Max;
                    }
                    lines.Insert(insertIndex, String.Format(formatString, jsonAttrib.PropertyName, MyAttribute.MtrDataType.ToFriendlyString(), minMaxStr, MyAttribute.Updateable.ToYesNo() + "/" + MyAttribute.RequiredOnCreate.ToYesNo(),version));
                    //lines.Add("| " + jsonAttrib.PropertyName + " ||||");
                    insertIndex++;
                    string descrStr = string.Empty;
                    if (!String.IsNullOrEmpty(MyAttribute.Description))
                    {
                        descrStr = String.Format("{0}", MyAttribute.Description);
                        lines.Insert(insertIndex, "|| *" + descrStr + "* |||");
                    }
                    if (!String.IsNullOrEmpty(MyAttribute.Note))
                    {
                        descrStr = "**Note**: *" + String.Format("{0}", MyAttribute.Note);
                        lines.Insert(insertIndex, "|| " + descrStr + "* |||");
                    }
                }
                else
                {
                    if (!(jsonAttrib is null))
                    {
                        //No documentation attributes
                        if (insertInDocumentedArea)
                        {
                            lines.Insert(insertIndex, String.Format(formatString, jsonAttrib.PropertyName, item.PropertyType.ToMtrType(), "", "No/No", version) );
                        }
                        else
                        {
                            unDocumented.Add(String.Format(formatString, jsonAttrib.PropertyName, item.PropertyType.ToMtrType(), "", "No/No", version));
                        }
                    }
                }
            }

            //Copy undocumented to the end
            if (unDocumented.Count > 0)
            {
                foreach (var item in unDocumented)
                {
                    lines.Add(item);
                }
            }

            lines.Add("");
            lines.Add("");
        }

        private static string CombineUri(params string[] uriParts)
        {
            string uri = string.Empty;
            if (uriParts != null && uriParts.Length > 0)
            {
                char[] trims = new char[] { '\\', '/' };
                uri = (uriParts[0] ?? string.Empty).TrimEnd(trims);
                for (int i = 1; i < uriParts.Length; i++)
                {
                    if (!String.IsNullOrEmpty(uriParts[i]))
                    {
                        uri = string.Format("{0}/{1}", uri.TrimEnd(trims), (uriParts[i] ?? string.Empty).TrimStart(trims));
                    }
                }
            }
            return uri;
        }

        private static void WriteToFile(List<string> fileContent)
        {
            using TextWriter tw = new StreamWriter(OutputFileName);
            foreach (String s in fileContent)
                tw.WriteLine(s);
            //Show with Windows Explorer
            string cmd = $"/select,\"{OutputFileName}\"";
            System.Diagnostics.Process.Start("Explorer.exe", cmd);
        }
    }
}