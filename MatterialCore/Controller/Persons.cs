﻿using MatterialCore.Entity;
using MatterialCore.Enums;
using MatterialCore.Interfaces;
using MatterialCore.Params;
using MatterialCore.Params.Doc;
using MatterialCore.Params.Person;

using Newtonsoft.Json;

using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;

namespace MatterialCore.Controller
{
    ///<inheritdoc cref="IPerson"/>
    internal class Persons : Entities, IPerson
    {
        private enum LoadType : int
        {
            ByAccountId,
            ByContactId
        }

        private enum ExportFormat : int
        {
            Csv,
            VCard,
            VCards
        }

        private enum FlagTypes : int
        {
            SetInstanceOwner,
            UnSetInstanceOwner,
            SetDemo,
            UnSetDemo
        }

        public Persons(string sessionId, string basePath)
        {
            UpdateSession(sessionId);
            BasePath = basePath;
        }

        #region Search

        public async Task<SearchResult<Person>?> Search(string query, SearchPersonParams options)
        {
            string path = ApiSession.CombineUri(Person.RESTBasePath(), Const.SEARCH);
            ParameterCollection pc = new ParameterCollection(options.UrlParameter())
            {
                {Const.PARAM_QUERY, query }
            };
            HttpResponseMessage response = await Get(path, ExpectedResponse.Json, pc);
            EnsureSuccessStatusCode(response);
            return JsonConvert.DeserializeObject<SearchResult<Person>>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);
        }

        public async Task<List<SearchAutocompleteSuggest>?> SearchAutocomplete(string query, PersonAutocompleteParams options)
        {
            string path = ApiSession.CombineUri(Person.RESTBasePath(), Const.SEARCH, Const.AUTOCOMPLETE);
            ParameterCollection pc = new ParameterCollection(options.UrlParameter())
            {
                {Const.PARAM_QUERY, query }
            };
            HttpResponseMessage response = await Get(path, ExpectedResponse.Json, pc);
            EnsureSuccessStatusCode(response);
            return JsonConvert.DeserializeObject<List<SearchAutocompleteSuggest>>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);
        }

        #endregion Search

        #region Load

        private async Task<Person?> LoadInternal(LoadType loadType, long id, GetPersonByIdParams options)
        {
            CheckId(id, "account id");
            string path = Person.RESTBasePath();

            switch (loadType)
            {
                case LoadType.ByAccountId:
                    path = ApiSession.CombineUri(path, Const.BY_ACCOUNT, id.ToString());
                    break;

                case LoadType.ByContactId:
                    path = ApiSession.CombineUri(path, Const.BY_CONTACT, id.ToString());
                    break;
            }
            HttpResponseMessage response = await Get(path, ExpectedResponse.Json, options.UrlParameter());
            EnsureSuccessStatusCode(response);
            return JsonConvert.DeserializeObject<Person>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);
        }

        public async Task<Person?> LoadByAccountId(long accountId)
        {
            return await LoadInternal(LoadType.ByAccountId, accountId, new GetPersonByIdParams());
        }

        public async Task<Person?> LoadByAccountId(long accountId, GetPersonByIdParams options)
        {
            return await LoadInternal(LoadType.ByAccountId, accountId, options);
        }

        public async Task<Person?> LoadByContactId(long contactId)
        {
            return await LoadInternal(LoadType.ByContactId, contactId, new GetPersonByIdParams());
        }

        public async Task<Person?> LoadByContactId(long contactId, GetPersonByIdParams options)
        {
            return await LoadInternal(LoadType.ByContactId, contactId, options);
        }

        public async Task<ListResult<Person>?> Load(GetPersonParams options)
        {
            string path = Person.RESTBasePath();
            HttpResponseMessage response = await Get(path, ExpectedResponse.Json, options.UrlParameter());
            EnsureSuccessStatusCode(response);
            return JsonConvert.DeserializeObject<ListResult<Person>>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);
        }

        #endregion Load

        #region Update

        public async Task<Person?> Update(Person person)
        {
            return await Update(person, "");
        }

        private async Task<Person?> Update(Person person, string contextToken, bool isPersonalData, UpdatePersonParams? options)
        {
            string path = ApiSession.CombineUri(Person.RESTBasePath(), Const.BY_ACCOUNT, person.AccountId.ToString());
            UpdatePersonParams personParams = options ?? new UpdatePersonParams();
            if (isPersonalData)
            {
                path = ApiSession.CombineUri(Person.RESTBasePath(), Const.PERSONAL_DATA);
            }
            ParameterCollection pc = new ParameterCollection(personParams.UrlParameter())
            {
                {Const.PARAM_CONTEXT_TOKEN ,contextToken }
            };
            HttpResponseMessage response = await Put(path, ExpectedResponse.Json, person, pc);
            EnsureSuccessStatusCode(response);
            if (isPersonalData)
            {
                LoginData? ld = JsonConvert.DeserializeObject<LoginData>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);
                if (!(ld is null) && !(ld.Person is null))
                {
                    return ld.Person;
                }
                return null;
            }
            return JsonConvert.DeserializeObject<Person>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);
        }

        public async Task<Person?> Update(Person person, string contextToken)
        {
            return await Update(person, contextToken, false, null);
        }

        public async Task<Person?> Update(Person person, string contextToken, UpdatePersonParams options)
        {
            return await Update(person, contextToken, false, options);
        }

        private async Task<bool> SetFlag(FlagTypes flagType, long accountId)
        {
            CheckId(accountId, "account id");
            HttpResponseMessage response = new HttpResponseMessage();
            string path = ApiSession.CombineUri(Person.RESTBasePath(), Const.BY_ACCOUNT, accountId.ToString());
            switch (flagType)
            {
                case FlagTypes.SetInstanceOwner:
                    path = ApiSession.CombineUri(path, Const.INSTANCE_OWNER);
                    response = await Put(path, ExpectedResponse.Plain);
                    break;

                case FlagTypes.UnSetInstanceOwner:
                    path = ApiSession.CombineUri(path, Const.INSTANCE_OWNER);
                    response = await Delete(path, ExpectedResponse.Plain);
                    break;

                case FlagTypes.SetDemo:
                    path = ApiSession.CombineUri(path, Const.DEMO);
                    response = await Put(path, ExpectedResponse.Plain);
                    break;

                case FlagTypes.UnSetDemo:
                    path = ApiSession.CombineUri(path, Const.DEMO);
                    response = await Delete(path, ExpectedResponse.Plain);
                    break;
            }
            EnsureSuccessStatusCode(response);
            string s = await response.Content.ReadAsStringAsync();
            if (int.TryParse(s, out int i))
            {
                return (i > 0);
            }
            return false;
        }

        public async Task<bool> SetDemoUser(long accountId, bool demoUser)
        {
            if (demoUser)
            {
                return await SetFlag(FlagTypes.SetDemo, accountId);
            }
            else
            {
                return await SetFlag(FlagTypes.UnSetDemo, accountId);
            }
        }

        public async Task<bool> SetInstanceOwner(long accountId, bool instanceOwner)
        {
            if (instanceOwner)
            {
                return await SetFlag(FlagTypes.SetInstanceOwner, accountId);
            }
            else
            {
                return await SetFlag(FlagTypes.UnSetInstanceOwner, accountId);
            }
        }

        public async Task<bool> UploadContactImage(long contactId, string filePath, bool setActive = true)
        {
            CheckId(contactId, "contct id");
            if (!File.Exists(filePath ?? string.Empty))
            {
                throw new ArgumentException("File " + filePath + " not found.");
            }
            string fPath = filePath ?? string.Empty;
            //Load person first
            GetPersonByIdParams opt = new GetPersonByIdParams
            {
                ContactImages = true
            };

            Person? p = await LoadByContactId(contactId, opt);
            if (p is null)
            {
                return false;
            }
            return await UploadContactImage(p, fPath, setActive);
        }

        public async Task<bool> UploadContactImage(Person p, string filePath, bool setActive = true)
        {
            if (!File.Exists(filePath ?? string.Empty))
            {
                throw new ArgumentException("File " + filePath + " not found.");
            }
            //Set the context token
            string contextToken = Guid.NewGuid().ToString();
            //Create temp file controller
            ITempFile c = new TempFiles(SessionId, BasePath);
            UploadFile u = new UploadFile
            {
                FilePath = filePath,
                FileType = TempFileType.ContactImage
            };
            TempFileDescriptor? tfd = await c.UploadFile(u, contextToken, Matterial.DefaultLanguage);
            if (!(tfd is null))
            {
                if (setActive)
                {
                    p.ContactImage = new ContactImage { Reference = tfd.FileToken };
                }
                //Make sure we ignore the other parts of the person's data
                UpdatePersonParams updateParams = new UpdatePersonParams { IgnoreAddreses = true, IgnoreClients = true, IgnoreCommuncationData = true };
                _ = await Update(p, contextToken, true, updateParams);
                return true;
            }
            return false;
        }

        #endregion Update

        #region Export

        private async Task<string> Export(ExportFormat format, long id)
        {
            string path = ApiSession.CombineUri(Person.RESTBasePath());
            switch (format)
            {
                case ExportFormat.Csv:
                    path = ApiSession.CombineUri(path, Const.CSV);
                    break;

                case ExportFormat.VCard:
                    CheckId(id, "account id");
                    path = ApiSession.CombineUri(path, Const.BY_CONTACT, id.ToString(), Const.VCARD);
                    break;

                case ExportFormat.VCards:
                    path = ApiSession.CombineUri(path, Const.VCARD);
                    break;
            }
            HttpResponseMessage response = await Get(path, ExpectedResponse.Plain);
            EnsureSuccessStatusCode(response);
            return await response.Content.ReadAsStringAsync();
        }

        public async Task<string?> GetCsvFile(PersonExportParams options)
        {
            return await Export(ExportFormat.Csv, 0);
        }

        public async Task<string?> GetVCards(PersonExportParams options)
        {
            return await Export(ExportFormat.VCards, 0);
        }

        public async Task<string?> GetVCard(long contactId)
        {
            return await Export(ExportFormat.VCard, contactId);
        }

        #endregion Export

        #region Create

        public async Task<Person?> Create(Person person)
        {
            return await Create(person, string.Empty);
        }

        public async Task<Person?> Create(Person person, string contextToken)
        {
            string path = Person.RESTBasePath();
            ParameterCollection pc = new ParameterCollection
            {
                {Const.PARAM_CONTEXT_TOKEN ,contextToken }
            };
            HttpResponseMessage response = await Post(path, ExpectedResponse.Json, person, pc);
            EnsureSuccessStatusCode(response);
            return JsonConvert.DeserializeObject<Person>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);
        }

        #endregion Create

        #region Remove

        public async Task<bool> Remove(long accountId)
        {
            CheckId(accountId, "account id");
            string path = ApiSession.CombineUri(Person.RESTBasePath(), Const.BY_ACCOUNT, accountId.ToString());
            HttpResponseMessage response = await Delete(path, ExpectedResponse.Plain);
            EnsureSuccessStatusCode(response);
            string s = await response.Content.ReadAsStringAsync();
            if (int.TryParse(s, out int i))
            {
                return (i > 0);
            }
            return false;
        }

        #endregion Remove

        #region Misc

        public async Task<bool> ReConvertContactImage(long contactId)
        {
            CheckId(contactId, "contact id");
            string path = ApiSession.CombineUri(Person.RESTBasePath(), Const.BY_CONTACT, contactId.ToString(), Const.CONVERT);
            HttpResponseMessage response = await Put(path, ExpectedResponse.Plain);
            EnsureSuccessStatusCode(response);
            string s = await response.Content.ReadAsStringAsync();
            if (int.TryParse(s, out int i))
            {
                return (i > 0);
            }
            return false;
        }

        public async Task<long> ConvertAllPersonsContactImage(bool regenerate)
        {
            string path = ApiSession.CombineUri(Person.RESTBasePath(), Const.CONVERT);
            ParameterCollection pc = new ParameterCollection();
            if (regenerate)
            {
                pc.Add(Const.PARAM_REGENERATE, regenerate.ToString().ToLower());
            }
            HttpResponseMessage response = await Put(path, ExpectedResponse.Plain, pc);
            EnsureSuccessStatusCode(response);
            string s = await response.Content.ReadAsStringAsync();
            if (int.TryParse(s, out int i))
            {
                return i;
            }
            return 0;
        }

        public async Task<Stream> GetContactImage(long contactId, long contactImageId, ContactImageFormats format)
        {
            CheckId(contactId, "contact id");
            CheckId(contactImageId, "contact image id");
            string path = ApiSession.CombineUri(Person.RESTBasePath(), Const.BY_CONTACT, contactId.ToString(), Const.CONTACT_IMAGE, contactImageId.ToString());
            ParameterCollection pc = new ParameterCollection();
            switch (format)
            {
                case ContactImageFormats.Pdf:
                    pc.Add(Const.PARAM_LOAD_PDF, "true");
                    break;

                case ContactImageFormats.Thumbnail:
                    pc.Add(Const.PARAM_LOAD_THUMBNAIL, "true");
                    break;
            }
            HttpResponseMessage response = await Get(path, ExpectedResponse.Nothing, pc);
            EnsureSuccessStatusCode(response);
            return await response.Content.ReadAsStreamAsync();
        }

        public async Task ReindexPersons()
        {
            string path = ApiSession.CombineUri(Person.RESTBasePath(), Const.REINDEX_ALL);
            HttpResponseMessage response = await Post(path, ExpectedResponse.Nothing);
            EnsureSuccessStatusCode(response);
        }

        #endregion Misc
    }
}