﻿using MatterialCore.Entity;
using MatterialCore.Interfaces;

using Newtonsoft.Json;

using System.Net.Http;
using System.Threading.Tasks;

namespace MatterialCore.Controller
{
    ///<inheritdoc cref="ILicense"/>
    internal class Licenses : Entities, ILicense
    {
        public Licenses(string sessionId, string basePath)
        {
            UpdateSession(sessionId);
            BasePath = basePath;
        }

        public async Task<License?> CheckLicense(License item)
        {
            string path = ApiSession.CombineUri(License.RESTBasePath(), Const.CHECK);
            HttpResponseMessage response = await Put(path, ExpectedResponse.Json, item);
            EnsureSuccessStatusCode(response);
            return JsonConvert.DeserializeObject<License>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);
        }

        public async Task<License?> GenerateLicense(License item)
        {
            string path = ApiSession.CombineUri(License.RESTBasePath(), Const.GENERATE);
            HttpResponseMessage response = await Put(path, ExpectedResponse.Json, item);
            EnsureSuccessStatusCode(response);
            return JsonConvert.DeserializeObject<License>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);
        }

        public async Task<License?> GetLicense()
        {
            string path = ApiSession.CombineUri(License.RESTBasePath());
            HttpResponseMessage response = await Get(path, ExpectedResponse.Json);
            EnsureSuccessStatusCode(response);
            return JsonConvert.DeserializeObject<License>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);
        }

        public async Task<LicenseUsage?> GetUsage()
        {
            string path = ApiSession.CombineUri(License.RESTBasePath(), Const.USAGE);
            HttpResponseMessage response = await Get(path, ExpectedResponse.Json);
            EnsureSuccessStatusCode(response);
            return JsonConvert.DeserializeObject<LicenseUsage>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);
        }

        public async Task<License?> UpdateLicense(License item)
        {
            string path = ApiSession.CombineUri(License.RESTBasePath());
            HttpResponseMessage response = await Put(path, ExpectedResponse.Json, item);
            EnsureSuccessStatusCode(response);
            return JsonConvert.DeserializeObject<License>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);
        }
    }
}