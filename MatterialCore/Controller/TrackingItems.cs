﻿using MatterialCore.Entity;
using MatterialCore.Interfaces;
using MatterialCore.Params;

using Newtonsoft.Json;

using System.Net.Http;
using System.Threading.Tasks;

namespace MatterialCore.Controller
{
    ///<inheritdoc cref="ITrackingItem"/>
    internal class TrackingItems : Entities, ITrackingItem
    {
        public TrackingItems(string sessionId, string basePath)
        {
            UpdateSession(sessionId);
            BasePath = basePath;
        }

        public async Task<ListResult<TrackingItem>?> Load(TrackingParams options)
        {
            string path = ApiSession.CombineUri(TrackingItem.RESTBasePath());
            HttpResponseMessage response = await Get(path, ExpectedResponse.Json, options.UrlParameter());
            EnsureSuccessStatusCode(response);
            return JsonConvert.DeserializeObject<ListResult<TrackingItem>>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);
        }

        public async Task Reindex()
        {
            string path = ApiSession.CombineUri(TrackingItem.RESTBasePath(), Const.SEARCH, Const.REINDEX_ALL);
            HttpResponseMessage response = await Post(path, ExpectedResponse.Nothing);
            EnsureSuccessStatusCode(response);
        }

        public async Task<SearchResult<TrackingItem>?> Search(string query, TrackingSearchParams options)
        {
            string path = ApiSession.CombineUri(TrackingItem.RESTBasePath(), Const.SEARCH);
            ParameterCollection pc = new ParameterCollection(options.UrlParameter())
            {
                {Const.PARAM_QUERY, query }
            };
            HttpResponseMessage response = await Get(path, ExpectedResponse.Json, pc);
            EnsureSuccessStatusCode(response);
            return JsonConvert.DeserializeObject<SearchResult<TrackingItem>>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);
        }
    }
}