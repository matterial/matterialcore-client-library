﻿using MatterialCore.Entity;
using MatterialCore.Enums;
using MatterialCore.Interfaces;
using MatterialCore.Params;
using MatterialCore.Params.Doc;

//using System.Text.Json;
using Newtonsoft.Json;

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace MatterialCore.Controller
{
    ///<inheritdoc cref="IDocument"/>
    internal class Documents : Entities, IDocument
    {
        private enum NewDocumentType : int
        {
            Document = 0,
            Snap = 1,
            Template = 2
        }

        private enum UpdateDocumentType : int
        {
            Document = 0,
            Template = 1,
            Bio = 2
        }

        private enum SavedSearchActions : int
        {
            Create,
            Update,
            Remove
        }

        private readonly TempFiles tempFileController;

        public Documents(string sessionId, string basePath)
        {
            UpdateSession(sessionId);
            BasePath = basePath;
            tempFileController = new TempFiles(sessionId, basePath);
        }

        #region Create

        public async Task<Document?> Create(Document newDocument, DocUpdateParams options)
        {
            return await CreateNewDocument(newDocument, options, NewDocumentType.Document);
        }

        public async Task<Document?> Create(Document newDocument, DocUpdateParams options, string documentContent, DocumentContentMediaType mediaType = DocumentContentMediaType.Markdown)
        {
            return await CreateNewDocument(newDocument, options, NewDocumentType.Document, documentContent);
        }

        public async Task<Document?> CreateSnap(Document newDocument, UpdateContextParams options)
        {
            return await CreateNewDocument(newDocument, options, NewDocumentType.Snap);
        }

        public async Task<Document?> CreateSnap(Document newDocument, UpdateContextParams options, string documentContent, DocumentContentMediaType mediaType = DocumentContentMediaType.Markdown)
        {
            return await CreateNewDocument(newDocument, options, NewDocumentType.Snap, documentContent);
        }

        public async Task<Document?> CreateTemplate(Document newDocument, UpdateTemplateParams options)
        {
            return await CreateNewDocument(newDocument, options, NewDocumentType.Template);
        }

        public async Task<Document?> CreateTemplate(Document newDocument, UpdateTemplateParams options, string documentContent, DocumentContentMediaType mediaType = DocumentContentMediaType.Markdown)
        {
            return await CreateNewDocument(newDocument, options, NewDocumentType.Template, documentContent);
        }

        private async Task<Document?> CreateNewDocument(Document newDocument, UpdateContextParams options, NewDocumentType docType, string documentContent = "", DocumentContentMediaType mediaType = DocumentContentMediaType.Markdown)
        {
            string path = Document.RESTBasePath();
            switch (docType)
            {
                case NewDocumentType.Document:
                    if (!(options is DocUpdateParams))
                    {
                        throw new ArgumentException("The options parameter must be the type DocumentUpdateParams.");
                    }
                    DocUpdateParams updParams = (DocUpdateParams)options;
                    if (String.IsNullOrEmpty(updParams.UniqueLockId))
                    {
                        throw new ArgumentException("The UniqueLockId property of DocumentCreateParams must not be empty.");
                    }
                    break;

                case NewDocumentType.Snap:
                    path = ApiSession.CombineUri(path, Const.SNAP);
                    break;

                case NewDocumentType.Template:
                    if (!(options is UpdateTemplateParams))
                    {
                        throw new ArgumentException("The options parameter must be the type DocumentUpdateTemplateParams.");
                    }
                    path = ApiSession.CombineUri(path, Const.TEMPLATE);
                    break;
            }
            //if a text content was passed, updload it as text
            if (!String.IsNullOrEmpty(documentContent))
            {
                if (String.IsNullOrEmpty(options.ContextToken))
                {
                    options.ContextToken = Guid.NewGuid().ToString();
                }
                LangKey lk = (newDocument.LanguageVersionLanguageKey is null) ? Matterial.DefaultLanguage : newDocument.LanguageVersionLanguageKey.ToLanguageKey();
                _ = await UploadText(documentContent, options.ContextToken, lk);
                newDocument.EditorType = EditorTypes.MARKDOWN;
                if (mediaType == DocumentContentMediaType.Html)
                {
                    newDocument.EditorType = EditorTypes.WYSIWYG;
                }
            }
            HttpResponseMessage response = await Post(path, ExpectedResponse.Json, newDocument, options.UrlParameter());
            EnsureSuccessStatusCode(response);
            return JsonConvert.DeserializeObject<Document>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);
        }

        #endregion Create

        #region Search

        public async Task<SearchResult<Document>?> Search(string searchTerm)
        {
            return await Search(searchTerm, null);
        }

        public async Task<SearchResult<Document>?> Search(string searchTerm, SearchDocParams? searchParameter)
        {
            string path = ApiSession.CombineUri(Document.RESTBasePath(), Const.SEARCH);
            //URL parameter
            ParameterCollection pc = new ParameterCollection();
            if (!(searchParameter is null))
            {
                //Some other parameters are used
                pc = new ParameterCollection(searchParameter.UrlParameter());
            }
            pc.Add(Const.PARAM_QUERY, searchTerm);
            HttpResponseMessage response = await Get(path, ExpectedResponse.Json, pc);
            EnsureSuccessStatusCode(response);
            return JsonConvert.DeserializeObject<SearchResult<Document>>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);
        }

        public async Task<List<SearchAutocompleteSuggest>?> SearchAutocomplete(string query, DocAutocompleteParams searchParameter)
        {
            string path = ApiSession.CombineUri(Document.RESTBasePath(), Const.SEARCH, Const.AUTOCOMPLETE);
            //URL parameter
            ParameterCollection pc = new ParameterCollection(searchParameter.UrlParameter())
            {
                { Const.PARAM_QUERY, query }
            };
            HttpResponseMessage response = await Get(path, ExpectedResponse.Json, pc);
            EnsureSuccessStatusCode(response);
            return JsonConvert.DeserializeObject<List<SearchAutocompleteSuggest>>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);
        }

        #endregion Search

        #region SavedSearches

        public async Task<ListResult<SearchResultEntry<Document>>?> Search(long savedSearchId, SavedSearchSearchParams searchParams)
        {
            CheckId(savedSearchId, nameof(savedSearchId));
            string path = ApiSession.CombineUri(Document.RESTBasePath(), Const.SAVED_SEARCH, savedSearchId.ToString(), Const.EXECUTE);
            HttpResponseMessage response = await Get(path, ExpectedResponse.Json, searchParams.UrlParameter());
            EnsureSuccessStatusCode(response);
            return JsonConvert.DeserializeObject<ListResult<SearchResultEntry<Document>>>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);
        }

        public async Task<List<SavedSearch>?> SavedSearches(SavedSearchLoadParams? loadParameter)
        {
            string path = ApiSession.CombineUri(Document.RESTBasePath(), Const.SAVED_SEARCH);

            if (loadParameter is null)
            {
                loadParameter = new SavedSearchLoadParams();
            }
            HttpResponseMessage response = await Get(path, ExpectedResponse.Json, loadParameter.UrlParameter());
            EnsureSuccessStatusCode(response);
            return JsonConvert.DeserializeObject<List<SavedSearch>>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);
        }

        public async Task<SavedSearch?> SavedSearchById(long savedSearchId, LangKey? languageKey)
        {
            CheckId(savedSearchId, nameof(savedSearchId));
            string path = ApiSession.CombineUri(Document.RESTBasePath(), Const.SAVED_SEARCH, savedSearchId.ToString());
            //URL parameter
            ParameterCollection pc = new ParameterCollection
            {
                languageKey.KeyAndValue()
            };
            HttpResponseMessage response = await Get(path, ExpectedResponse.Json, pc);
            EnsureSuccessStatusCode(response);
            return JsonConvert.DeserializeObject<SavedSearch>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);
        }

        public async Task<long> CreateSavedSearch(SavedSearch savedSearch, string searchTerm, SearchDocParams searchParams)
        {
            return await SavedSearchAction(SavedSearchActions.Create, null, savedSearch, searchTerm, searchParams);
        }

        public async Task<long> UpdateSavedSearch(SavedSearch savedSearch)
        {
            return await SavedSearchAction(SavedSearchActions.Update, null, savedSearch, null, null);
        }

        public async Task<bool> RemoveSavedSearch(long savedSearchId)
        {
            CheckId(savedSearchId, nameof(savedSearchId));
            long i = await SavedSearchAction(SavedSearchActions.Remove, savedSearchId, null, null, null);
            return (i > 0);
        }

        public async Task<bool> RestoreDefaultSavedSearches()
        {
            string path = ApiSession.CombineUri(Document.RESTBasePath(), Const.SAVED_SEARCH, Const.INITIAL);
            HttpResponseMessage response = await Put(path, ExpectedResponse.Plain);
            EnsureSuccessStatusCode(response);
            string s = response.Content.ReadAsStringAsync().Result;
            if (long.TryParse(s, out long i))
            {
                return (i > 0);
            }
            return false;
        }

        public async Task<long> SetDashboardPrios(List<long> savedSearchIdList)
        {
            string path = ApiSession.CombineUri(Document.RESTBasePath(), Const.SAVED_SEARCH, Const.PRIO);
            HttpResponseMessage response = await Put(path, ExpectedResponse.Plain, savedSearchIdList);
            EnsureSuccessStatusCode(response);
            string s = response.Content.ReadAsStringAsync().Result;
            if (long.TryParse(s, out long i))
            {
                return i;
            }
            return 0;
        }

        private async Task<long> SavedSearchAction(SavedSearchActions action, long? savedSearchId, SavedSearch? savedSearch, string? searchTerm, SearchDocParams? searchParams)
        {
            SavedSearch item = savedSearch ?? new SavedSearch();
            string query = searchTerm ?? string.Empty;
            SearchDocParams searchParam = searchParams ?? new SearchDocParams();
            string path = ApiSession.CombineUri(Document.RESTBasePath(), Const.SAVED_SEARCH);
            //long itemId = savedSearchId ?? 0;

            HttpResponseMessage? response;
            SavedSearch? returnedSavedSearch;
            switch (action)
            {
                case SavedSearchActions.Create:
                    searchParam.CopyToSavedSearch(item, query);
                    response = await Post(path, ExpectedResponse.Json, item);
                    EnsureSuccessStatusCode(response);
                    returnedSavedSearch = JsonConvert.DeserializeObject<SavedSearch>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);
                    if (!(returnedSavedSearch is null))
                    {
                        return returnedSavedSearch.Id;
                    }
                    return 0;

                case SavedSearchActions.Update:
                    CheckId(item.Id, "saved search to update");
                    path = ApiSession.CombineUri(path, item.Id.ToString());
                    response = await Put(path, ExpectedResponse.Json, item);
                    EnsureSuccessStatusCode(response);
                    returnedSavedSearch = JsonConvert.DeserializeObject<SavedSearch>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);
                    if (!(returnedSavedSearch is null))
                    {
                        return returnedSavedSearch.Id;
                    }
                    return 0;

                case SavedSearchActions.Remove:
                    CheckId(savedSearchId, "saved search to remove");
                    path = ApiSession.CombineUri(path, savedSearchId.ToString());
                    response = await Delete(path, ExpectedResponse.Plain);
                    EnsureSuccessStatusCode(response);
                    string s = response.Content.ReadAsStringAsync().Result;
                    if (long.TryParse(s, out long i))
                    {
                        return i;
                    }
                    throw new Exception("Removing saved search failed.");
            }
            throw new ArgumentException("Invalid action parameter.");
        }

        public string GetQueryString(SavedSearch savedSearch)
        {
            //Try to load the query string form the params of a saved search
            string? s = savedSearch.Params.FirstOrDefault(item => item.Key == Const.PARAM_QUERY).Value;
            if (String.IsNullOrEmpty(s))
            {
                return string.Empty;
            }
            return s;
        }

        #endregion SavedSearches

        #region Load

        public async Task<Document?> Load(long documentId, LoadDocByIdParams loadParams)
        {
            string path = Document.RESTBasePath();
            CheckId(documentId, "document id");
            path = ApiSession.CombineUri(path, documentId.ToString());
            //Get the options
            HttpResponseMessage response = await Get(path, ExpectedResponse.Json, loadParams?.UrlParameter());
            EnsureSuccessStatusCode(response);
            return JsonConvert.DeserializeObject<Document>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);
        }

        public async Task<Document?> Load(long documentId, string languageKey)
        {
            CheckId(documentId, "documentId");

            LangKey langKey = Matterial.DefaultLanguage;
            if (Enum.TryParse<LangKey>(languageKey, out LangKey k))
            {
                langKey = k;
            }
            LoadDocByIdParams loadParams = new LoadDocByIdParams()
            {
                LanguageKey = langKey
            };
            return await Load(documentId, loadParams);
        }

        public async Task<ListResult<Document>?> Load(LoadDocParams? loadParams)
        {
            string path = Document.RESTBasePath();
            if (!(loadParams is null))
            {
                path = ApiSession.CombineUri(path, loadParams.PathParameter);
            }
            //Get the options
            HttpResponseMessage response = await Get(path, ExpectedResponse.Json, loadParams?.UrlParameter());
            EnsureSuccessStatusCode(response);
            return JsonConvert.DeserializeObject<ListResult<Document>>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);
        }

        public async Task<ListResult<Document>?> LoadTrash(LoadDocTrashParams? loadParams)
        {
            string path = Document.RESTBasePath();
            if (!(loadParams is null))
            {
                path = ApiSession.CombineUri(path, loadParams.PathParameter);
            }
            //Get the options
            HttpResponseMessage response = await Get(path, ExpectedResponse.Json, loadParams?.UrlParameter());
            EnsureSuccessStatusCode(response);
            return JsonConvert.DeserializeObject<ListResult<Document>>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);
        }

        #endregion Load

        #region MainFile

        public async Task<Stream?> GetMainFile(long documentId, MainFileLoadParams loadParams)
        {
            return await GetMainFile(loadParams, documentId, 0);
        }

        public async Task<Stream?> GetMainFile(long documentId, long documentLanguageVersionId, MainFileLoadParams loadParams)
        {
            return await GetMainFile(loadParams, documentId, documentLanguageVersionId);
        }

        private async Task<Stream?> GetMainFile(MainFileLoadParams loadParams, long documentId, long documentLanguageVersionId)
        {
            CheckId(documentId, nameof(documentId));

            string path = ApiSession.CombineUri(Document.RESTBasePath(), documentId.ToString());
            if (documentLanguageVersionId == 0)
            {
                path = ApiSession.CombineUri(path, Const.FILE);
            }
            else
            {
                path = ApiSession.CombineUri(path, Const.VERSION, documentLanguageVersionId.ToString(), Const.FILE);
            }
            //Get the options
            HttpResponseMessage response = await Get(path, ExpectedResponse.Nothing, loadParams?.UrlParameter());
            EnsureSuccessStatusCode(response);
            return await response.Content.ReadAsStreamAsync();
        }

        public async Task<Stream?> CompareMainFile(long oldDocumentLanguageVersionId, long newDocumentLanguageVersionId)
        {
            CheckId(oldDocumentLanguageVersionId, nameof(oldDocumentLanguageVersionId));
            CheckId(newDocumentLanguageVersionId, nameof(newDocumentLanguageVersionId));

            string path = ApiSession.CombineUri(Document.RESTBasePath(), Const.COMPARE, Const.VERSION, oldDocumentLanguageVersionId.ToString(), newDocumentLanguageVersionId.ToString());

            //Get the options
            HttpResponseMessage response = await Get(path, ExpectedResponse.Nothing);
            EnsureSuccessStatusCode(response);
            return await response.Content.ReadAsStreamAsync();
        }

        #endregion MainFile

        #region TempFile

        public async Task<TempFileDescriptor?> UploadText(string text, string contextToken, LangKey languageKey)
        {
            return await tempFileController.UploadText(text, contextToken, languageKey);
        }

        public async Task<Stream?> GetTempFile(TempFileType tempFileType, string contextToken, string fileToken, LangKey languageKey, bool forceAttachment)
        {
            return await tempFileController.GetTempFile(tempFileType, contextToken, fileToken, languageKey, forceAttachment);
        }

        public async Task<Stream?> GetTempFile(TempFileDescriptor tempFileDescriptor, bool forceAttachment)
        {
            return await tempFileController.GetTempFile(tempFileDescriptor, forceAttachment);
        }

        public async Task<TempFileDescriptor?> UploadFile(UploadFile uploadFile, string contextToken, LangKey languageKey)
        {
            return await tempFileController.UploadFile(uploadFile, contextToken, languageKey);
        }

        public async Task<List<TempFileDescriptor>?> UploadFile(List<UploadFile> uploadFiles, string contextToken, LangKey languageKey)
        {
            return await tempFileController.UploadFile(uploadFiles, contextToken, languageKey);
        }

        public async Task RemoveTempfiles(string contextToken)
        {
            await tempFileController.RemoveTempfiles(contextToken);
        }

        public async Task RemoveTempfile(TempFileType tempFileType, string contextToken, string fileToken, LangKey languageKey)
        {
            await tempFileController.RemoveTempfile(tempFileType, contextToken, fileToken, languageKey);
        }

        public async Task RemoveTempfile(TempFileDescriptor tempFileDescriptor)
        {
            await tempFileController.RemoveTempfile(tempFileDescriptor);
        }

        #endregion TempFile

        #region Attachments

        public async Task<List<Attachment>?> GetAttachments(long documentId, AttachmentsLoadParams loadParams)
        {
            CheckId(documentId, nameof(documentId));
            string path = ApiSession.CombineUri(Document.RESTBasePath(), documentId.ToString(), Const.ATTACHMENT);
            HttpResponseMessage response = await Get(path, ExpectedResponse.Json, loadParams.UrlParameter());
            EnsureSuccessStatusCode(response);
            return JsonConvert.DeserializeObject<List<Attachment>>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);
        }

        private async Task<Stream?> GetAttachment(AttachmentLoadParams loadParams, long documentId, long documentLanguageVersionId, long attachmentId)
        {
            string path = ApiSession.CombineUri(Document.RESTBasePath(), documentId.ToString());
            if (documentLanguageVersionId == 0)
            {
                path = ApiSession.CombineUri(path, Const.ATTACHMENT, attachmentId.ToString());
            }
            else
            {
                path = ApiSession.CombineUri(path, Const.VERSION, documentLanguageVersionId.ToString(), Const.ATTACHMENT, attachmentId.ToString());
            }

            HttpResponseMessage response = await Get(path, ExpectedResponse.Nothing, loadParams?.UrlParameter());
            EnsureSuccessStatusCode(response);
            return await response.Content.ReadAsStreamAsync();
        }

        public async Task<Stream?> GetAttachment(long documentId, long documentLanguageVersionId, long attachmentId, AttachmentLoadParams loadParams)
        {
            CheckId(documentId, nameof(documentId));
            CheckId(documentLanguageVersionId, nameof(documentLanguageVersionId));
            CheckId(attachmentId, nameof(attachmentId));

            return await GetAttachment(loadParams, documentId, documentLanguageVersionId, attachmentId);
        }

        public async Task<Stream?> GetAttachment(long documentId, long attachmentId, AttachmentLoadParams loadParams)
        {
            CheckId(documentId, nameof(documentId));
            CheckId(attachmentId, nameof(attachmentId));

            return await GetAttachment(loadParams, documentId, 0, attachmentId);
        }

        #endregion Attachments

        #region Remove/Trash/Archive

        public async Task<DocumentBase?> Remove(long documentId, bool finallyRemove)
        {
            string path = Document.RESTBasePath();
            CheckId(documentId, nameof(documentId));

            HttpResponseMessage response = new HttpResponseMessage();
            if (finallyRemove)
            {
                //Put to trash first and then finally delete
                try
                {
                    path = ApiSession.CombineUri(path, documentId.ToString(), Const.TRASH);
                    response = await Put(path, ExpectedResponse.Json);
                    //finally delete
                    path = ApiSession.CombineUri(Document.RESTBasePath(), documentId.ToString());
                    response = await Delete(path, ExpectedResponse.Json);
                }
                catch
                {
                }
            }
            else
            {
                path = ApiSession.CombineUri(path, documentId.ToString(), Const.TRASH);
                response = await Put(path, ExpectedResponse.Json);
            }

            if (response.StatusCode == HttpStatusCode.Conflict)
            {
                //The document is locked
                return JsonConvert.DeserializeObject<DocumentLock>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);
            }
            EnsureSuccessStatusCode(response);
            Document? docResult = JsonConvert.DeserializeObject<Document>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);
            return docResult;
        }

        public async Task<DocumentBase?> Remove(long documentId, long documentLanguageVersionId)
        {
            CheckId(documentId, nameof(documentId));
            string path = ApiSession.CombineUri(Document.RESTBasePath(), documentId.ToString(), Const.VERSION, documentLanguageVersionId.ToString());
            HttpResponseMessage response = await Delete(path, ExpectedResponse.Json);
            if (response.StatusCode == HttpStatusCode.Conflict)
            {
                //The document is locked
                return JsonConvert.DeserializeObject<DocumentLock>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);
            }
            EnsureSuccessStatusCode(response);
            Document? docResult = JsonConvert.DeserializeObject<Document>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);
            return docResult;
        }

        public async Task<DocumentBase?> Remove(long documentId, LangKey languageKey)
        {
            CheckId(documentId, nameof(documentId));
            string path = ApiSession.CombineUri(Document.RESTBasePath(), documentId.ToString(), Const.LANGUAGE, (string)languageKey.KeyAndValue().Value);
            HttpResponseMessage response = await Delete(path, ExpectedResponse.Json);
            if (response.StatusCode == HttpStatusCode.Conflict)
            {
                //The document is locked
                return JsonConvert.DeserializeObject<DocumentLock>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);
            }
            EnsureSuccessStatusCode(response);
            Document? docResult = JsonConvert.DeserializeObject<Document>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);
            return docResult;
        }

        public async Task<DocumentBase?> Restore(long documentId)
        {
            CheckId(documentId, nameof(documentId));
            string path = ApiSession.CombineUri(Document.RESTBasePath(), documentId.ToString(), Const.UNTRASH);
            HttpResponseMessage response = await Put(path, ExpectedResponse.Json);
            if (response.StatusCode == HttpStatusCode.Conflict)
            {
                //The document is locked
                return JsonConvert.DeserializeObject<DocumentLock>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);
            }
            EnsureSuccessStatusCode(response);
            Document? docResult = JsonConvert.DeserializeObject<Document>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);
            return docResult;
        }

        private enum ArchiveActions : int
        {
            Archive,
            Unarchive
        }

        private async Task<DocumentBase?> ArchiveAction(ArchiveActions action, long documentId, long archivedBeginInSeconds)
        {
            CheckId(documentId, nameof(documentId));
            ParameterCollection pc = new ParameterCollection();
            string path = ApiSession.CombineUri(Document.RESTBasePath(), documentId.ToString());
            switch (action)
            {
                case ArchiveActions.Archive:
                    if (archivedBeginInSeconds > 0)
                    {
                        pc = new ParameterCollection
                        {
                            { Const.PARAM_ARCHIVED_BEGIN_IN_SECONDS_REQUEST, archivedBeginInSeconds.ToString() }
                        };
                    }
                    path = ApiSession.CombineUri(path, Const.ARCHIVE);
                    break;

                case ArchiveActions.Unarchive:
                    path = ApiSession.CombineUri(path, Const.UNARCHIVE);
                    break;

                default:
                    break;
            }
            HttpResponseMessage response = await Put(path, ExpectedResponse.Json, pc);
            if (response.StatusCode == HttpStatusCode.Conflict)
            {
                DocumentLock? docLock = JsonConvert.DeserializeObject<DocumentLock>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);
                return docLock;
            }
            EnsureSuccessStatusCode(response);
            Document? docResult = JsonConvert.DeserializeObject<Document>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);
            return docResult;
        }

        public async Task<DocumentBase?> Archive(long documentId, long archivedBeginInSeconds)
        {
            return await ArchiveAction(ArchiveActions.Archive, documentId, archivedBeginInSeconds);
        }

        public async Task<DocumentBase?> UnArchive(long documentId)
        {
            return await ArchiveAction(ArchiveActions.Unarchive, documentId, 0);
        }

        public async Task<DocumentBase?> Archive(long documentId)
        {
            return await ArchiveAction(ArchiveActions.Archive, documentId, 0);
        }

        public async Task<int> ClearTrash(bool ignoreRemoveTime)
        {
            string path = ApiSession.CombineUri(Document.RESTBasePath());
            ParameterCollection pc = new ParameterCollection
            {
                { Const.PARAM_IGNORE_REMOVE_TIME, ignoreRemoveTime.ToString().ToLower() }
            };
            HttpResponseMessage response = await Delete(path, ExpectedResponse.Plain, pc);
            EnsureSuccessStatusCode(response);
            return JsonConvert.DeserializeObject<int>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);
        }

        public async Task<int> GetRemovalQueueSize()
        {
            string path = ApiSession.CombineUri(Document.RESTBasePath(), Const.REMOVAL, Const.QUEUE);
            HttpResponseMessage response = await Get(path, ExpectedResponse.Plain);
            EnsureSuccessStatusCode(response);
            return JsonConvert.DeserializeObject<int>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);
        }

        public async Task<int> ReconvertDocument(Document document)
        {
            string path = ApiSession.CombineUri(Document.RESTBasePath(), document.Id.ToString(), Const.VERSION, document.LanguageVersionId.ToString(), Const.CONVERT);
            HttpResponseMessage response = await Put(path, ExpectedResponse.Plain);
            EnsureSuccessStatusCode(response);
            return JsonConvert.DeserializeObject<int>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);
        }

        public async Task<int> ReconvertAll(bool regenerate)
        {
            ParameterCollection pc = new ParameterCollection { { Const.PARAM_REGENERATE, regenerate.ToString().ToLower() } };
            string path = ApiSession.CombineUri(Document.RESTBasePath(), Const.CONVERT);
            HttpResponseMessage response = await Put(path, ExpectedResponse.Plain, pc);
            EnsureSuccessStatusCode(response);
            return JsonConvert.DeserializeObject<int>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);
        }

        #endregion Remove/Trash/Archive

        #region Locking

        public async Task<DocumentLock?> Lock(long documentId, string uniqueLockId)
        {
            CheckId(documentId, nameof(documentId));
            ParameterCollection pc = new ParameterCollection
            {
                { Const.PARAM_UNIQUE_LOCK_ID, uniqueLockId }
            };
            string path = ApiSession.CombineUri(Document.RESTBasePath(), Const.LOCK, documentId.ToString());
            HttpResponseMessage response = await Put(path, ExpectedResponse.Json, pc);
            EnsureSuccessStatusCode(response);
            return JsonConvert.DeserializeObject<DocumentLock>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);
        }

        public async Task<bool> Unlock(string uniqueLockId)
        {
            string path = ApiSession.CombineUri(Document.RESTBasePath(), Const.LOCK);
            ParameterCollection pc = new ParameterCollection
            {
                { Const.PARAM_UNIQUE_LOCK_ID, uniqueLockId }
            };

            HttpResponseMessage response = await Delete(path, ExpectedResponse.Plain, pc);
            EnsureSuccessStatusCode(response);
            bool retVal = false;
            if (int.TryParse(response.Content.ReadAsStringAsync().Result, out int i))
            {
                retVal = (i == 1);
            }
            return retVal;
        }

        public async Task<bool> UnlockByDocId(long documentId)
        {
            CheckId(documentId, nameof(documentId));
            string path = ApiSession.CombineUri(Document.RESTBasePath(), Const.LOCK, documentId.ToString());
            HttpResponseMessage response = await Delete(path, ExpectedResponse.Plain);
            if (response.StatusCode == HttpStatusCode.NotFound)
            {
                return false;
            }
            EnsureSuccessStatusCode(response);
            bool retVal = false;
            if (int.TryParse(response.Content.ReadAsStringAsync().Result, out int i))
            {
                retVal = (i == 1);
            }
            return retVal;
        }

        public async Task<DocumentLock?> UpdateLock(long documentId, string uniqueLockId)
        {
            return await Lock(documentId, uniqueLockId);
        }

        #endregion Locking

        #region Updating

        public async Task<Document?> Update(Document document, DocUpdateParams updateParams)
        {
            return await Update(document, updateParams, UpdateDocumentType.Document);
        }

        public async Task<Document?> UpdateTemplate(Document document, UpdateTemplateParams updateParams)
        {
            return await Update(document, updateParams, UpdateDocumentType.Template);
        }

        public async Task<Document?> UpdateBio(Document document, UpdateLockParams updateParams)
        {
            return await Update(document, updateParams, UpdateDocumentType.Template);
        }

        private async Task<Document?> Update(Document document, UpdateLockParams updateParams, UpdateDocumentType updateType)
        {
            string path = ApiSession.CombineUri(Document.RESTBasePath(), document.Id.ToString());
            switch (updateType)
            {
                case UpdateDocumentType.Template:
                    path = ApiSession.CombineUri(Document.RESTBasePath(), Const.TEMPLATE, document.Id.ToString());
                    break;

                case UpdateDocumentType.Bio:
                    path = ApiSession.CombineUri(Document.RESTBasePath(), Const.BIO, document.Id.ToString());
                    break;
            }
            HttpResponseMessage response = await Put(path, ExpectedResponse.Json, document, updateParams.UrlParameter());
            EnsureSuccessStatusCode(response);
            return JsonConvert.DeserializeObject<Document>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);
        }

        #endregion Updating

        #region Snap

        public async Task<bool> Unsnap(Document snap)
        {
            if (String.IsNullOrEmpty(snap.LanguageVersionLanguageKey))
            {
                throw new ArgumentException("Invalid ur not existing language key.");
            }
            string path = ApiSession.CombineUri(Document.RESTBasePath(), snap.Id.ToString(), Const.LANGUAGE, snap.LanguageVersionLanguageKey.ToLower(), Const.SNAP);
            HttpResponseMessage response = await Delete(path, ExpectedResponse.Plain);
            EnsureSuccessStatusCode(response);
            int i = JsonConvert.DeserializeObject<int>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);
            return (i > 0);
        }

        #endregion Snap

        #region Review

        private enum ReviewedAction
        {
            MarkAsReviewed,
            DeclineReview
        }

        public async Task<bool> MarkAsReviewed(long id, long languageVersionId)
        {
            return await DeclineReview(ReviewedAction.MarkAsReviewed, id, languageVersionId, null);
        }

        public async Task<bool> DeclineReview(long id, long languageVersionId)
        {
            return await DeclineReview(ReviewedAction.DeclineReview, id, languageVersionId, null);
        }

        public async Task<bool> DeclineReview(long id, long languageVersionId, MtrTask task)
        {
            return await DeclineReview(ReviewedAction.DeclineReview, id, languageVersionId, task);
        }

        private async Task<bool> DeclineReview(ReviewedAction action, long id, long languageVersionId, MtrTask? task)
        {
            CheckId(id, "document id");
            CheckId(languageVersionId, "language version id");
            string path = ApiSession.CombineUri(Document.RESTBasePath(), id.ToString(), Const.VERSION, languageVersionId.ToString());
            switch (action)
            {
                case ReviewedAction.MarkAsReviewed:
                    path = ApiSession.CombineUri(path, Const.REVIEW);
                    break;

                case ReviewedAction.DeclineReview:
                    path = ApiSession.CombineUri(path, Const.REVIEW_DECLINE);
                    break;
            }
            HttpResponseMessage? response; // = null;
            if (!(task is null))
            {
                if (task.AssignedRole is null)
                {
                    throw new ArgumentException("The AssignedRole of the given Task must not be null.");
                }
                if (task.AssignedRole.Id == 0)
                {
                    throw new ArgumentException("The AssignedRole ID of the given Task must not be less than 1.");
                }
                if (String.IsNullOrEmpty(task.AssignedRole.Description))
                {
                    throw new ArgumentException("The AssignedRole description of the given Task must not be null or empty.");
                }
                response = await Put(path, ExpectedResponse.Plain, task);
            }
            else
            {
                response = await Put(path, ExpectedResponse.Plain);
            }
            EnsureSuccessStatusCode(response);
            int i = JsonConvert.DeserializeObject<int>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);
            return (i > 0);
        }

        #endregion Review

        #region Additional Properties

        public async Task<Document?> SetAdditionalProperties(long id, List<AdditionalProperty> properties)
        {
            CheckId(id, nameof(id));
            string path = ApiSession.CombineUri(Document.RESTBasePath(), id.ToString(), Const.ADDITIONAL_PROPERTY);
            HttpResponseMessage response = await Put(path, ExpectedResponse.Json, properties);
            EnsureSuccessStatusCode(response);
            return JsonConvert.DeserializeObject<Document>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);
        }

        #endregion Additional Properties

        #region Duplicates

        public async Task<DocumentDuplicate?> Duplicate(long id, DuplicateMode mode)
        {
            CheckId(id, nameof(id));
            ParameterCollection pc = new ParameterCollection();

            if (mode != DuplicateMode.Default)
            {
                pc.Add(mode.KeyAndValue());
            }
            string path = ApiSession.CombineUri(Document.RESTBasePath(), id.ToString(), Const.DUPLICATE);
            HttpResponseMessage response = await Get(path, ExpectedResponse.Json, pc);
            EnsureSuccessStatusCode(response);
            return JsonConvert.DeserializeObject<DocumentDuplicate>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);
        }

        public async Task<List<Document>> SaveDuplicate(DocumentDuplicate documentDuplicate)
        {
            List<Document> createdDocs = new List<Document>();
            //Generate unique lock
            string unlockId = Guid.NewGuid().ToString();
            DocUpdateParams updateParamsp = new DocUpdateParams
            {
                // The context token is used to reference all uploaded files(main-file and attachments)
                ContextToken = documentDuplicate.ContextToken,
                UniqueLockId = unlockId
            };

            long firstDocId = 0;
            if (!(documentDuplicate.Documents is null))
            {
                for (int i = 0; i < documentDuplicate.Documents.Count; i++)
                {
                    Document? createdDoc;
                    //The first language gets created, all other get updated
                    if (i == 0)
                    {
                        //Need to create the first language
                        createdDoc = await Create(documentDuplicate.Documents[i], updateParamsp);
                        if (!(createdDoc is null))
                        {
                            firstDocId = createdDoc.Id;
                        }
                        else
                        {
                            throw new Exception("Saving duplicate as new document failed.");
                        }
                    }
                    else
                    {
                        //Updating further languages
                        long idDuplicate = documentDuplicate.Documents[i].Id;
                        //We need to set the id of the first created document
                        documentDuplicate.Documents[i].Id = firstDocId;
                        createdDoc = await Update(documentDuplicate.Documents[i], updateParamsp);
                        //Reset id back
                        documentDuplicate.Documents[i].Id = idDuplicate;
                    }
                    if (!(createdDoc is null))
                    {
                        _ = Unlock(unlockId).Result;
                        createdDocs.Add(createdDoc);
                    }
                }
            }
            return createdDocs;
        }

        public async Task<List<Document>> SaveDuplicates(DocumentDuplicates documentDuplicates)
        {
            //Saving duplicates as new documents.
            //If there are multiple lamnguages of one document id, the first one has to be saved
            //as a new document, all other s are updates on the existing document id
            DocUpdateParams p = new DocUpdateParams();
            List<Document> createdDocs = new List<Document>();
            //Generate unique lock
            string unlockId = Guid.NewGuid().ToString();
            p.UniqueLockId = unlockId;
            //Iterate thru duplicates
            if (!(documentDuplicates.Map is null))
            {
                foreach (KeyValuePair<string, DocumentDuplicate> item in documentDuplicates.Map)
                {
                    List<Document> l = await SaveDuplicate(item.Value);
                    //Copy to list
                    foreach (var doc in l)
                    {
                        createdDocs.Add(doc);
                    }
                }
            }
            return createdDocs;
        }

        public async Task<DocumentDuplicates?> GetDuplicates()
        {
            string path = ApiSession.CombineUri(Document.RESTBasePath(), Const.DUPLICATE);
            HttpResponseMessage response = await Get(path, ExpectedResponse.Json);
            EnsureSuccessStatusCode(response);
            //string s = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<DocumentDuplicates>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);
        }

        public async Task<int> RemoveDuplicate(string contextToken)
        {
            string path = ApiSession.CombineUri(Document.RESTBasePath(), contextToken, Const.DUPLICATE);
            HttpResponseMessage response = await Delete(path, ExpectedResponse.Plain);
            EnsureSuccessStatusCode(response);
            return JsonConvert.DeserializeObject<int>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);
        }

        public async Task<int> RemoveDuplicates(DocumentDuplicates duplicates)
        {
            int retval = 0;
            if (!(duplicates.Map is null))
            {
                foreach (KeyValuePair<string, DocumentDuplicate> item in duplicates.Map)
                {
                    retval += await RemoveDuplicate(item.Key);
                }
            }
            return retval;
        }

        #endregion Duplicates

        #region Clipboard

        private enum ClipboardAction : int
        {
            Add,
            Remove,
            Clear
        }

        public async Task<int> AddToClipboard(List<DocumentClipBoardEntry> clipboardEntries)
        {
            List<IEntity> e = new List<IEntity>();
            foreach (var item in clipboardEntries)
            {
                e.Add(item);
            }
            return await Clipboard(e, ClipboardAction.Add);
        }

        public async Task<int> RemoveFromClipboard(List<DocumentClipBoardEntry> clipboardEntries)
        {
            List<IEntity> e = new List<IEntity>();
            foreach (var item in clipboardEntries)
            {
                e.Add(item);
            }
            return await Clipboard(e, ClipboardAction.Remove);
        }

        public async Task<int> ClearClipboard()
        {
            return await Clipboard(null, ClipboardAction.Clear);
        }

        private async Task<int> Clipboard(List<IEntity>? clipboardEntries, ClipboardAction action)
        {
            string pathSuffix = "";
            switch (action)
            {
                case ClipboardAction.Add:
                    pathSuffix = Const.ADD;
                    break;

                case ClipboardAction.Remove:
                    pathSuffix = Const.REMOVE;
                    break;

                case ClipboardAction.Clear:
                    pathSuffix = "";
                    break;
            }
            string path = ApiSession.CombineUri(Document.RESTBasePath(), Const.CLIP_BOARD, pathSuffix);
            HttpResponseMessage? response = null;
            if (action == ClipboardAction.Clear)
            {
                response = await Delete(path, ExpectedResponse.Plain);
            }
            else
            {
                if (!(clipboardEntries is null))
                {
                    response = await Put(path, ExpectedResponse.Plain, clipboardEntries);
                }
            }

            if (!(response is null))
            {
                EnsureSuccessStatusCode(response);
                return JsonConvert.DeserializeObject<int>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);
            }
            return 0;
        }

        public async Task ExecuteBatchAction(BatchActions actionCode, BatchActionAdditionalData options)
        {
            string path = ApiSession.CombineUri(Document.RESTBasePath(), Const.BATCH_ACTION, ((int)actionCode).ToString());
            ParameterCollection pc = new ParameterCollection
            {
                { Const.PARAM_ACTION_CODE, actionCode.ToString() }
            };
            HttpResponseMessage response = await Put(path, ExpectedResponse.Plain, pc);
            EnsureSuccessStatusCode(response);
        }

        #endregion Clipboard

        #region Rating

        private enum RatingActions : int
        {
            Rate,
            Unrate,
            IsRated
        }

        private async Task<bool> RateDocument(RatingActions ratingAction, long id, long languageVersionId)
        {
            CheckId(id, "document id");
            CheckId(languageVersionId, "language version id");
            HttpResponseMessage? response = null;
            string path = ApiSession.CombineUri(Document.RESTBasePath(), id.ToString(), Const.VERSION, languageVersionId.ToString(), Const.RATING);
            switch (ratingAction)
            {
                case RatingActions.Rate:
                    response = await Put(path, ExpectedResponse.Nothing);
                    break;

                case RatingActions.Unrate:
                    response = await Delete(path, ExpectedResponse.Nothing);
                    break;

                case RatingActions.IsRated:
                    response = await Get(path, ExpectedResponse.Plain);
                    break;
            }
            if (!(response is null))
            {
                EnsureSuccessStatusCode(response);
                if (ratingAction == RatingActions.IsRated)
                {
                    string s = response.Content.ReadAsStringAsync().Result;
                    if (bool.TryParse(s, out bool b))
                    {
                        return b;
                    }
                }
            }
            return true;
        }

        public async Task<bool> RateDocument(long id, long languageVersionId)
        {
            return await RateDocument(RatingActions.Rate, id, languageVersionId);
        }

        public async Task<bool> UnRateDocument(long id, long languageVersionId)
        {
            return await RateDocument(RatingActions.Unrate, id, languageVersionId);
        }

        public async Task<bool> IsRatedDocument(long id, long languageVersionId)
        {
            return await RateDocument(RatingActions.IsRated, id, languageVersionId);
        }

        #endregion Rating

        #region ChangeLog

        public async Task<ListResult<DocumentChangeLog>?> GetChangeLog(ChangeLogParams loadParams)
        {
            string path = ApiSession.CombineUri(Document.RESTBasePath(), Const.CHANGE_LOG);
            CheckId(loadParams.DocumentId, "loadParams.DocumentId");

            HttpResponseMessage response = await Get(path, ExpectedResponse.Json, loadParams.UrlParameter());
            EnsureSuccessStatusCode(response);
            return JsonConvert.DeserializeObject<ListResult<DocumentChangeLog>>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);
        }

        public async Task<DocumentChangeLogMap?> GetChangeLogMap(ChangeLogMapParams loadParams)
        {
            CheckId(loadParams.DocumentId, "loadParams.DocumentId");
            string path = ApiSession.CombineUri(Document.RESTBasePath(), Const.CHANGE_LOG, Const.MAP);
            HttpResponseMessage response = await Get(path, ExpectedResponse.Json, loadParams.UrlParameter());
            EnsureSuccessStatusCode(response);
            return JsonConvert.DeserializeObject<DocumentChangeLogMap>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);
        }

        #endregion ChangeLog

        #region ExtensionValues

        public async Task<List<ExtensionValue>?> GetExtensionValues()
        {
            string path = ApiSession.CombineUri(Document.RESTBasePath(), Const.EXTENSION_VALUE, Const.AVAILABLE);
            HttpResponseMessage response = await Get(path, ExpectedResponse.Json);
            EnsureSuccessStatusCode(response);
            return JsonConvert.DeserializeObject<List<ExtensionValue>>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);
        }

        #endregion ExtensionValues

        #region Index

        public async Task ReindexAll()
        {
            string path = ApiSession.CombineUri(Document.RESTBasePath(), Const.SEARCH, Const.REINDEX_ALL);
            HttpResponseMessage response = await Post(path, ExpectedResponse.Nothing);
            EnsureSuccessStatusCode(response);
        }

        public async Task IndexDocument(long documentId)
        {
            string path = ApiSession.CombineUri(Document.RESTBasePath(), Const.SEARCH, Const.INDEX, documentId.ToString());
            HttpResponseMessage response = await Post(path, ExpectedResponse.Nothing);
            EnsureSuccessStatusCode(response);
        }

        //Task IndexDocument(long id, long languageVersionId);
        public async Task IndexDocument(long documentId, long languageVersionId)
        {
            string path = ApiSession.CombineUri(Document.RESTBasePath(), Const.SEARCH, Const.INDEX, documentId.ToString(), Const.VERSION, languageVersionId.ToString());
            HttpResponseMessage response = await Post(path, ExpectedResponse.Nothing);
            EnsureSuccessStatusCode(response);
        }

        #endregion Index
    }
}