﻿using MatterialCore.Entity;
using MatterialCore.Interfaces;
using MatterialCore.Params;

using Newtonsoft.Json;

using System.Net.Http;
using System.Threading.Tasks;

namespace MatterialCore.Controller
{
    ///<inheritdoc cref="IActivity"/>
    internal class Activities : Entities, IActivity
    {
        public Activities(string sessionId, string basePath)
        {
            UpdateSession(sessionId);
            BasePath = basePath;
        }

        public async Task<Activity?> Load(long id)
        {
            string path = ApiSession.CombineUri(Activity.RESTBasePath(), id.ToString());
            HttpResponseMessage response = await Get(path, ExpectedResponse.Json);
            EnsureSuccessStatusCode(response);
            return JsonConvert.DeserializeObject<Activity>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);
        }

        public async Task<ListResult<Activity>?> Load(ActivityParams options)
        {
            string path = ApiSession.CombineUri(Activity.RESTBasePath());
            HttpResponseMessage response = await Get(path, ExpectedResponse.Json, options.UrlParameter());
            EnsureSuccessStatusCode(response);
            return JsonConvert.DeserializeObject<ListResult<Activity>>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);
        }
    }
}