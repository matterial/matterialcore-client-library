﻿using MatterialCore.Entity;
using MatterialCore.Enums;
using MatterialCore.Interfaces;
using MatterialCore.Params;

using Newtonsoft.Json;

using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace MatterialCore.Controller
{
    ///<inheritdoc cref="IRole"/>
    internal class Roles : Entities, IRole
    {
        public Roles(string sessionId, string basePath)
        {
            UpdateSession(sessionId);
            BasePath = basePath;
        }

        private enum RoleActionTypes : int
        {
            GetById,
            Create,
            Update,
            Remove
        }

        private enum RoleAssignActionTypes : int
        {
            Assign,
            AssignMany,
            Unassign,
            UnassignMany
        }

        private async Task<List<Person>?> RoleAssignAction(RoleAssignActionTypes action, List<long>? ids, List<long>? accountIds)
        {
            HttpResponseMessage response; // = new HttpResponseMessage();
            ParameterCollection pc = new ParameterCollection();
            pc.AllowedDuplicateKeys.Add(Const.PARAM_ACCOUNT_ID);
            pc.AllowedDuplicateKeys.Add(Const.PARAM_ROLE_ID);

            if (!(accountIds is null))
            {
                foreach (var idItem in accountIds)
                {
                    CheckId(idItem, "account id");
                    pc.Add(Const.PARAM_ACCOUNT_ID, idItem.ToString());
                }
            }
            if (!(ids is null))
            {
                foreach (var idItem in ids)
                {
                    CheckId(idItem, "role id");
                    pc.Add(Const.PARAM_ROLE_ID, idItem.ToString());
                }
            }
            string path = Role.RESTBasePath();
            long id = (ids is null) ? 0 : ids[0];
            long accountId = (accountIds is null) ? 0 : accountIds[0];
            switch (action)
            {
                case Roles.RoleAssignActionTypes.Assign:
                    CheckId(id, "role id");
                    path = ApiSession.CombineUri(path, id.ToString(), Const.BY_ACCOUNT, accountId.ToString());
                    response = await Put(path, ExpectedResponse.Json);
                    break;

                case Roles.RoleAssignActionTypes.AssignMany:
                    path = ApiSession.CombineUri(path, Const.ASSIGN, Const.BY_ACCOUNT);
                    response = await Put(path, ExpectedResponse.Json, pc);
                    break;

                case Roles.RoleAssignActionTypes.Unassign:
                    CheckId(id, "role id");
                    path = ApiSession.CombineUri(path, id.ToString(), Const.BY_ACCOUNT, accountId.ToString());
                    response = await Delete(path, ExpectedResponse.Json);
                    break;

                case Roles.RoleAssignActionTypes.UnassignMany:
                    path = ApiSession.CombineUri(path, Const.ASSIGN, Const.BY_ACCOUNT);
                    response = await Delete(path, ExpectedResponse.Json, pc);
                    break;

                default:
                    throw new ArgumentException("Invalid action parameter.");
            }
            EnsureSuccessStatusCode(response);
            return JsonConvert.DeserializeObject<List<Person>>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);
        }

        private async Task<Role?> RoleAction(RoleActionTypes action, long id, Role? item)
        {
            HttpResponseMessage response;
            ParameterCollection pc = new ParameterCollection();
            pc.AllowedDuplicateKeys.Add(Const.PARAM_ACCOUNT_ID);
            pc.AllowedDuplicateKeys.Add(Const.PARAM_ROLE_ID);

            string path = Role.RESTBasePath();
            switch (action)
            {
                case Roles.RoleActionTypes.GetById:
                    CheckId(id, "role id");
                    path = ApiSession.CombineUri(path, id.ToString());
                    response = await Get(path, ExpectedResponse.Json);
                    break;

                case Roles.RoleActionTypes.Create:
                    if (item is null)
                        throw new ArgumentException("Invalid parameter: item must not be null");
                    response = await Post(path, ExpectedResponse.Json, item);
                    break;

                case Roles.RoleActionTypes.Update:
                    if (item is null)
                        throw new ArgumentException("Invalid parameter: item must not be null");
                    path = ApiSession.CombineUri(path, id.ToString());
                    response = await Put(path, ExpectedResponse.Json, item);
                    break;

                case Roles.RoleActionTypes.Remove:
                    path = ApiSession.CombineUri(path, id.ToString());
                    response = await Delete(path, ExpectedResponse.Plain);
                    break;

                default:
                    throw new ArgumentException("Invalid action parameter.");
            }

            EnsureSuccessStatusCode(response);
            Role? retval = new Role();
            switch (action)
            {
                case RoleActionTypes.GetById:
                case RoleActionTypes.Create:
                case RoleActionTypes.Update:
                    return JsonConvert.DeserializeObject<Role>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);

                case RoleActionTypes.Remove:
                    retval.Id = response.Content.ReadAsStringAsync().Result.ToInt();
                    return retval;
            }
            return retval;
        }

        public async Task<Role?> LoadById(long id)
        {
            return await RoleAction(RoleActionTypes.GetById, id, null);
        }

        public async Task<ListResult<Role>?> Load(RoleParams options)
        {
            string path = Role.RESTBasePath();
            HttpResponseMessage response = await Get(path, ExpectedResponse.Json, options.UrlParameter());
            EnsureSuccessStatusCode(response);
            return JsonConvert.DeserializeObject<ListResult<Role>>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);
        }

        public async Task<long> Create(Role newRole)
        {
            Role? r = await RoleAction(RoleActionTypes.Create, 0, newRole);
            if (!(r is null))
            {
                return r.Id;
            }
            return 0;
        }

        public async Task<long> Update(long id, Role updateRole)
        {
            Role? r = await RoleAction(RoleActionTypes.Update, id, updateRole);
            if (!(r is null))
            {
                return r.Id;
            }
            return 0;
        }

        public async Task<bool> Remove(long id)
        {
            Role? r = await RoleAction(RoleActionTypes.Remove, id, null);
            if (!(r is null))
            {
                return (r.Id > 0);
            }
            return false;
        }

        public async Task<List<Person>?> Assign(long id, long accountId)
        {
            return await RoleAssignAction(RoleAssignActionTypes.Assign, new List<long> { id }, new List<long> { accountId });
        }

        public async Task<List<Person>?> Assign(List<long> ids, List<long> accountIds)
        {
            return await RoleAssignAction(RoleAssignActionTypes.AssignMany, ids, accountIds);
        }

        public async Task<List<Person>?> UnAssign(long id, long accountId)
        {
            return await RoleAssignAction(RoleAssignActionTypes.Unassign, new List<long> { id }, new List<long> { accountId });
        }

        public async Task<List<Person>?> UnAssign(List<long> ids, List<long> accountIds)
        {
            return await RoleAssignAction(RoleAssignActionTypes.UnassignMany, ids, accountIds);
        }
    }
}