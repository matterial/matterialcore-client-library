﻿using MatterialCore.Entity;
using MatterialCore.Enums;
using MatterialCore.Interfaces;
using MatterialCore.Params;

using Newtonsoft.Json;

using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;

namespace MatterialCore.Controller
{
    ///<inheritdoc cref="IInstance"/>
    internal class Instances : Entities, IInstance
    {
        public Instances(string sessionId, string basePath)
        {
            UpdateSession(sessionId);
            BasePath = basePath;
        }

        public async Task<ListResult<Instance>?> Load(InstanceParams options)
        {
            string path = ApiSession.CombineUri(Instance.RESTBasePath(), Const.INSTANCE);
            HttpResponseMessage response = await Get(path, ExpectedResponse.Json, options.UrlParameter());
            EnsureSuccessStatusCode(response);
            return JsonConvert.DeserializeObject<ListResult<Instance>>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);
        }

        public async Task<Instance?> Load(long id)
        {
            string path = ApiSession.CombineUri(Instance.RESTBasePath(), Const.INSTANCE, id.ToString());
            HttpResponseMessage response = await Get(path, ExpectedResponse.Json);
            EnsureSuccessStatusCode(response);
            return JsonConvert.DeserializeObject<Instance>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);
        }

        private enum InstanceActions : int
        {
            Activate2FA,
            ActivateInstance,
            ActivateInstanceDays,
            Authorize2FA,
            Deactivate2FA,
            DeactivateInstance,
            Reinvite,
            RemoveInstance,
            RemoveInvitee
        }

        private async Task<LoginData?> InstanceAction(InstanceActions action, string? instanceName = null, int days = 0, string? code = null, long credentialId = 0, LangKey? languageKey = LangKey.en)
        {
            string path = ApiSession.CombineUri(Instance.RESTBasePath());
            HttpResponseMessage response = new HttpResponseMessage();
            switch (action)
            {
                case InstanceActions.Activate2FA:
                    path = ApiSession.CombineUri(path, Const.INSTANCE, Const.TWO_FACTOR_AUTH);
                    response = await Post(path, ExpectedResponse.Json);
                    break;

                case InstanceActions.ActivateInstance:
                    path = ApiSession.CombineUri(path, Const.INSTANCE, instanceName ?? "", Const.ACTIVATE, Const.UNLIMITED);
                    response = await Put(path, ExpectedResponse.Plain);
                    break;

                case InstanceActions.ActivateInstanceDays:
                    path = ApiSession.CombineUri(path, Const.INSTANCE, instanceName ?? "", Const.ACTIVATE, Const.DAYS, days.ToString());
                    response = await Put(path, ExpectedResponse.Plain);
                    break;

                case InstanceActions.Authorize2FA:
                    path = ApiSession.CombineUri(path, Const.INSTANCE, Const.TWO_FACTOR_AUTH);
                    response = await Put(path, ExpectedResponse.Json, code ?? "");
                    break;

                case InstanceActions.Deactivate2FA:
                    path = ApiSession.CombineUri(path, Const.INSTANCE, Const.TWO_FACTOR_AUTH);
                    response = await Delete(path, ExpectedResponse.Json);
                    break;

                case InstanceActions.DeactivateInstance:
                    path = ApiSession.CombineUri(path, Const.INSTANCE, instanceName ?? "", Const.DEACTIVATE);
                    response = await Put(path, ExpectedResponse.Plain);
                    break;

                case InstanceActions.Reinvite:
                    path = ApiSession.CombineUri(path, Const.CREDENTIAL, Const.INVITEE, credentialId.ToString());
                    ParameterCollection pc = new ParameterCollection
                    {
                        {languageKey.KeyAndValue() }
                    };
                    response = await Put(path, ExpectedResponse.Nothing, pc);
                    break;

                case InstanceActions.RemoveInstance:
                    path = ApiSession.CombineUri(path, Const.INSTANCE, instanceName ?? "");
                    response = await Delete(path, ExpectedResponse.Plain);
                    break;

                case InstanceActions.RemoveInvitee:
                    path = ApiSession.CombineUri(path, Const.CREDENTIAL, Const.INVITEE, credentialId.ToString());
                    response = await Delete(path, ExpectedResponse.Plain);
                    break;

                default:
                    break;
            }

            EnsureSuccessStatusCode(response);
            string retVal;
            switch (action)
            {
                case InstanceActions.ActivateInstance:
                case InstanceActions.ActivateInstanceDays:
                case InstanceActions.DeactivateInstance:
                case InstanceActions.RemoveInvitee:
                    retVal = await response.Content.ReadAsStringAsync();
                    if (int.TryParse(retVal, out _))
                    {
                        return new LoginData();
                    }
                    return null;

                case InstanceActions.RemoveInstance:
                    retVal = await response.Content.ReadAsStringAsync();
                    if (bool.TryParse(retVal, out bool b))
                    {
                        if (b)
                            return new LoginData();
                    }
                    return null;

                case InstanceActions.Activate2FA:
                case InstanceActions.Authorize2FA:
                case InstanceActions.Deactivate2FA:
                    return JsonConvert.DeserializeObject<LoginData>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);

                case InstanceActions.Reinvite:
                    return new LoginData();
            }
            return null;
        }

        public async Task<LoginData?> Activate2FA()
        {
            return await InstanceAction(InstanceActions.Activate2FA);
        }

        public async Task<bool> ActivateInstance(string instanceName)
        {
            LoginData? retVal = await InstanceAction(InstanceActions.ActivateInstance, instanceName);
            if (!(retVal is null))
                return true;
            return false;
        }

        public async Task<bool> ActivateInstance(string instanceName, int days)
        {
            LoginData? retVal = await InstanceAction(InstanceActions.ActivateInstanceDays, instanceName, days);
            if (!(retVal is null))
                return true;
            return false;
        }

        public async Task<LoginData?> Authorize2FA(string code)
        {
            return await InstanceAction(InstanceActions.Authorize2FA, null, 0, code);
        }

        public async Task<LoginData?> Deactivate2FA()
        {
            return await InstanceAction(InstanceActions.Deactivate2FA);
        }

        public async Task<bool> DeactivateInstance(string instanceName)
        {
            LoginData? retVal = await InstanceAction(InstanceActions.DeactivateInstance, instanceName);
            if (!(retVal is null))
                return true;
            return false;
        }

        public async Task<Stream?> Get2FAQRCode()
        {
            string path = ApiSession.CombineUri(Instance.RESTBasePath(), Const.TWO_FACTOR_AUTH, Const.QR_CODE);
            HttpResponseMessage response = await Get(path, ExpectedResponse.Nothing);
            EnsureSuccessStatusCode(response);
            return await response.Content.ReadAsStreamAsync();
        }

        public async Task<List<Credential>?> GetInvitees()
        {
            string path = ApiSession.CombineUri(Instance.RESTBasePath(), Const.CREDENTIAL, Const.INVITEE);
            HttpResponseMessage response = await Get(path, ExpectedResponse.Json);
            EnsureSuccessStatusCode(response);
            return JsonConvert.DeserializeObject<List<Credential>>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);
        }

        public async Task Reinvite(long credentialId, LangKey languageKey)
        {
            _ = await InstanceAction(InstanceActions.Reinvite, null, 0, null, credentialId, languageKey);
        }

        public async Task<bool> RemoveInstance(string instanceName)
        {
            LoginData? retVal = await InstanceAction(InstanceActions.RemoveInstance, instanceName);
            if (!(retVal is null))
                return true;
            return false;
        }

        public async Task<bool> RemoveInvitee(long credentialId)
        {
            CheckId(credentialId, "credential id");
            LoginData? retVal = await InstanceAction(InstanceActions.RemoveInvitee, null, 0, null, credentialId);
            if (!(retVal is null))
                return true;
            return false;
        }

        public async Task<long> StoreCredentials(CredentialWithInvitationText item, LangKey languageKey)
        {
            List<CredentialWithInvitationText> l = new List<CredentialWithInvitationText>
            {
                item
            };
            List<long>? retVal = await StoreCredentials(l, languageKey);
            if (!(retVal is null))
            {
                return retVal.Count;
            }
            return 0;
        }

        public async Task<List<long>?> StoreCredentials(List<CredentialWithInvitationText> items, LangKey languageKey)
        {
            List<IEntity> itemList = new List<IEntity>(items);
            string path = ApiSession.CombineUri(Instance.RESTBasePath(), Const.CREDENTIAL, Const.LIST);
            ParameterCollection pc = new ParameterCollection
            {
                languageKey.KeyAndValue()
            };
            HttpResponseMessage response = await Put(path, ExpectedResponse.Json, itemList, pc);
            EnsureSuccessStatusCode(response);
            return JsonConvert.DeserializeObject<List<long>>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);
        }

        public async Task<LoginData?> UpdateDisplayname(string displayName)
        {
            string path = ApiSession.CombineUri(Instance.RESTBasePath(), Const.INSTANCE, Const.DISPLAY_NAME, displayName);
            HttpResponseMessage response = await Put(path, ExpectedResponse.Json);
            EnsureSuccessStatusCode(response);
            return JsonConvert.DeserializeObject<LoginData>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);
        }

        public async Task<LoginData?> UpdateFavouriteInstance(Instance item)
        {
            string path = ApiSession.CombineUri(Instance.RESTBasePath(), Const.CREDENTIAL, Const.FAV_DATA_SOURCE);
            HttpResponseMessage response = await Put(path, ExpectedResponse.Json, item);
            EnsureSuccessStatusCode(response);
            return JsonConvert.DeserializeObject<LoginData>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);
        }

        public async Task<PreferenceMapContainer?> GetPreferences()
        {
            string path = ApiSession.CombineUri(Instance.RESTBasePath(), Const.PREFERENCE, Const.CACHE);
            HttpResponseMessage response = await Get(path, ExpectedResponse.Json);
            EnsureSuccessStatusCode(response);
            return JsonConvert.DeserializeObject<PreferenceMapContainer>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);
        }

        public async Task<string?> GetPreference(string key)
        {
            string path = ApiSession.CombineUri(Instance.RESTBasePath(), Const.PREFERENCE, Const.CACHE, Const.BY_PREFERENCE_KEY, key);
            HttpResponseMessage response = await Get(path, ExpectedResponse.Plain);
            EnsureSuccessStatusCode(response);
            return await response.Content.ReadAsStringAsync();
        }

        public async Task<PreferenceMapContainer?> GetUnrestrictedPreferences()
        {
            string path = ApiSession.CombineUri(Instance.RESTBasePath(), Const.PREFERENCE, Const.CACHE, Const.UNRESTRICTED);
            HttpResponseMessage response = await Get(path, ExpectedResponse.Json);
            EnsureSuccessStatusCode(response);
            return JsonConvert.DeserializeObject<PreferenceMapContainer>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);
        }

        public async Task<string?> GetUnrestrictedPreference(UnrestrictedPreferences key)
        {
            string path = ApiSession.CombineUri(Instance.RESTBasePath(), Const.PREFERENCE, Const.CACHE, Const.BY_UNRESTRICTED_KEY, key.ToKeyString());
            HttpResponseMessage response = await Get(path, ExpectedResponse.Plain);
            EnsureSuccessStatusCode(response);
            return await response.Content.ReadAsStringAsync();
        }

        public async Task ReinitPreferenceCache()
        {
            string path = ApiSession.CombineUri(Instance.RESTBasePath(), Const.PREFERENCE, Const.CACHE, Const.REINIT);
            HttpResponseMessage response = await Get(path, ExpectedResponse.Nothing);
            EnsureSuccessStatusCode(response);
        }

        public async Task StorePreference(string key, string value)
        {
            string path = ApiSession.CombineUri(Instance.RESTBasePath(), Const.PREFERENCE, key);
            HttpResponseMessage response = await Put(path, ExpectedResponse.Nothing, value);
            EnsureSuccessStatusCode(response);
        }
    }
}