﻿using MatterialCore.Entity;
using MatterialCore.Enums;
using MatterialCore.Interfaces;
using MatterialCore.Params;

using Newtonsoft.Json;

using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace MatterialCore.Controller
{
    ///<inheritdoc cref="IMtrTask"/>
    internal class MtrTasks : Entities, IMtrTask
    {
        public MtrTasks(string sessionId, string basePath)
        {
            UpdateSession(sessionId);
            BasePath = basePath;
        }

        private enum TaskActionTypes : int
        {
            GetById,
            Get,
            Create,
            Update,
            Remove
        }

        private async Task<MtrTask?> TaskAction(TaskActionTypes action, long id, MtrTask? item)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            string path = MtrTask.RESTBasePath();
            switch (action)
            {
                case TaskActionTypes.GetById:
                    CheckId(id, "task id");
                    path = ApiSession.CombineUri(path, id.ToString());
                    response = await Get(path, ExpectedResponse.Json);
                    break;

                case TaskActionTypes.Create:
                    response = await Post(path, ExpectedResponse.Json, item ?? new MtrTask());
                    break;

                case TaskActionTypes.Update:
                    CheckId(id, "task id");
                    path = ApiSession.CombineUri(path, id.ToString());
                    response = await Put(path, ExpectedResponse.Json, item ?? new MtrTask());
                    break;

                case TaskActionTypes.Remove:
                    CheckId(id, "task id");
                    path = ApiSession.CombineUri(path, id.ToString());
                    response = await Delete(path, ExpectedResponse.Plain);
                    break;

                default:
                    throw new ArgumentException("Invalid action parameter.");
            }
            EnsureSuccessStatusCode(response);
            MtrTask? retval = new MtrTask();
            switch (action)
            {
                case TaskActionTypes.GetById:
                case TaskActionTypes.Create:
                case TaskActionTypes.Update:
                    return JsonConvert.DeserializeObject<MtrTask>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);

                case TaskActionTypes.Remove:
                    retval.Id = response.Content.ReadAsStringAsync().Result.ToInt();
                    return retval;
            }
            return retval;
        }

        public async Task<MtrTask?> LoadById(long id)
        {
            return await TaskAction(TaskActionTypes.GetById, id, null);
        }

        public async Task<ListResult<MtrTask>?> Load(TaskParams options)
        {
            string path = MtrTask.RESTBasePath();
            HttpResponseMessage response = await Get(path, ExpectedResponse.Json, options.UrlParameter());
            EnsureSuccessStatusCode(response);
            return JsonConvert.DeserializeObject<ListResult<MtrTask>>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);
        }

        public async Task<MtrTask?> Create(MtrTask newTask)
        {
            return await TaskAction(TaskActionTypes.Create, 0, newTask);
        }

        public async Task<MtrTask?> Update(MtrTask updateTask)
        {
            return await TaskAction(TaskActionTypes.Update, updateTask.Id, updateTask);
        }

        public async Task<bool> Remove(long id)
        {
            MtrTask? r = await TaskAction(TaskActionTypes.Remove, id, null);
            if (!(r is null))
            {
                return (r.Id > 0);
            }
            return false;
        }

        public async Task<MtrTaskStatus?> GetStatusById(long id, LangKey languageKey)
        {
            CheckId(id, "task status id");
            string path = ApiSession.CombineUri(MtrTask.RESTBasePath(), Const.STATUS, id.ToString());
            ParameterCollection pc = new ParameterCollection
            {
                languageKey.KeyAndValue()
            };
            HttpResponseMessage response = await Get(path, ExpectedResponse.Json, pc);
            EnsureSuccessStatusCode(response);
            return JsonConvert.DeserializeObject<MtrTaskStatus>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);
        }

        public async Task<List<MtrTaskStatus>?> GetStatus(TaskStatusParams options)
        {
            string path = ApiSession.CombineUri(MtrTask.RESTBasePath(), Const.STATUS);
            HttpResponseMessage response = await Get(path, ExpectedResponse.Json, options.UrlParameter());
            EnsureSuccessStatusCode(response);
            return JsonConvert.DeserializeObject<List<MtrTaskStatus>>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);
        }
    }
}