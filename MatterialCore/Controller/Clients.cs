﻿using MatterialCore.Entity;
using MatterialCore.Enums;
using MatterialCore.Interfaces;
using MatterialCore.Params;

using Newtonsoft.Json;

using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;

namespace MatterialCore.Controller
{
    ///<inheritdoc cref="IClient"/>
    internal class Clients : Entities, IClient
    {
        public Clients(string sessionId, string basePath)
        {
            UpdateSession(sessionId);
            BasePath = basePath;
        }

        public async Task<Client?> Load(long id, bool loadClientPreferences = false)
        {
            string path = ApiSession.CombineUri(Client.RESTBasePath(), id.ToString());
            ParameterCollection pc = new ParameterCollection();
            if (loadClientPreferences)
            {
                pc.Add(Const.PARAM_LOAD_CLIENT_PREFERENCES, "true");
            }
            HttpResponseMessage response = await Get(path, ExpectedResponse.Json, pc);
            EnsureSuccessStatusCode(response);
            return JsonConvert.DeserializeObject<Client>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);
        }

        public async Task<List<Client>?> Load(ClientParams options)
        {
            string path = ApiSession.CombineUri(Client.RESTBasePath());
            HttpResponseMessage response = await Get(path, ExpectedResponse.Json, options.UrlParameter());
            EnsureSuccessStatusCode(response);
            return JsonConvert.DeserializeObject<List<Client>>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);
        }

        public async Task<long> Create(Client item)
        {
            string path = ApiSession.CombineUri(Client.RESTBasePath());
            HttpResponseMessage response = await Post(path, ExpectedResponse.Json, item);
            EnsureSuccessStatusCode(response);
            Client? cl = JsonConvert.DeserializeObject<Client>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);
            if (!(cl is null))
                return cl.Id;
            return 0;
        }

        public async Task<long> Update(Client item)
        {
            string path = ApiSession.CombineUri(Client.RESTBasePath(), item.Id.ToString());
            HttpResponseMessage response = await Put(path, ExpectedResponse.Json, item);
            EnsureSuccessStatusCode(response);
            Client? cl = JsonConvert.DeserializeObject<Client>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);
            if (!(cl is null))
                return cl.Id;
            return 0;
        }

        public async Task<bool> Remove(long id)
        {
            string path = ApiSession.CombineUri(Client.RESTBasePath(), id.ToString());
            HttpResponseMessage response = await Delete(path, ExpectedResponse.Plain);
            EnsureSuccessStatusCode(response);
            string retVal = response.Content.ReadAsStringAsync().Result;
            return retVal.ToBool();
        }

        public async Task<Stream?> GetLogo(LoadSize size)
        {
            string path = ApiSession.CombineUri(Client.RESTBasePath(), Const.LOGO);
            ParameterCollection pc = new ParameterCollection
            {
                size.KeyAndValue()
            };
            HttpResponseMessage response = await Delete(path, ExpectedResponse.Nothing, pc);
            EnsureSuccessStatusCode(response);
            return await response.Content.ReadAsStreamAsync();
        }

        public async Task<Client?> UpdateLogo(string contextToken, Client item)
        {
            string path = ApiSession.CombineUri(Client.RESTBasePath(), Const.LOGO);
            ParameterCollection pc = new ParameterCollection
            {
                { Const.PARAM_CONTEXT_TOKEN, contextToken }
            };
            HttpResponseMessage response = await Put(path, ExpectedResponse.Json, item, pc);
            EnsureSuccessStatusCode(response);
            return JsonConvert.DeserializeObject<Client>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);
        }

        public async Task<bool> RemoveLogo()
        {
            string path = ApiSession.CombineUri(Client.RESTBasePath(), Const.LOGO);
            HttpResponseMessage response = await Delete(path, ExpectedResponse.Plain);
            EnsureSuccessStatusCode(response);
            string retVal = response.Content.ReadAsStringAsync().Result;
            return retVal.ToBool();
        }

        public async Task<string?> GetPreference(string key)
        {
            string path = ApiSession.CombineUri(Client.RESTBasePath(), Const.PREFERENCE, key);
            HttpResponseMessage response = await Get(path, ExpectedResponse.Plain);
            EnsureSuccessStatusCode(response);
            return await response.Content.ReadAsStringAsync();
        }

        public async Task<PreferenceMapContainer?> GetPreferences()
        {
            string path = ApiSession.CombineUri(Client.RESTBasePath(), Const.PREFERENCE);
            HttpResponseMessage response = await Get(path, ExpectedResponse.Json);
            EnsureSuccessStatusCode(response);
            return JsonConvert.DeserializeObject<PreferenceMapContainer>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);
        }

        public async Task<bool> StorePreference(string key, string preference)
        {
            string path = ApiSession.CombineUri(Client.RESTBasePath(), Const.PREFERENCE, key);
            HttpResponseMessage response = await Put(path, ExpectedResponse.Plain, preference);
            EnsureSuccessStatusCode(response);
            string retVal = response.Content.ReadAsStringAsync().Result;
            if (int.TryParse(retVal, out int i))
            {
                return (i > 0);
            }
            return false;
        }

        public async Task<bool> RemovePreference(string key)
        {
            string path = ApiSession.CombineUri(Client.RESTBasePath(), Const.PREFERENCE, key);
            HttpResponseMessage response = await Delete(path, ExpectedResponse.Plain);
            EnsureSuccessStatusCode(response);
            string retVal = response.Content.ReadAsStringAsync().Result;
            if (int.TryParse(retVal, out int i))
            {
                return (i > 0);
            }
            return false;
        }
    }
}