﻿using MatterialCore.Entity;
using MatterialCore.Interfaces;
using MatterialCore.Params;

using Newtonsoft.Json;

using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace MatterialCore.Controller
{
    ///<inheritdoc cref="IComment"/>
    internal class Comments : Entities, IComment
    {
        public Comments(string sessionId, string basePath)
        {
            UpdateSession(sessionId);
            BasePath = basePath;
        }

        public async Task<long?> Create(long documentLanguageVersionId, Comment newItem)
        {
            string path = ApiSession.CombineUri(Comment.RESTBasePath());
            newItem.DocumentLanguageVersionId = documentLanguageVersionId;
            HttpResponseMessage response = await Post(path, ExpectedResponse.Json, newItem);
            EnsureSuccessStatusCode(response);
            Comment? c = JsonConvert.DeserializeObject<Comment>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);
            if (!(c is null))
            {
                return c.Id;
            }
            return null;
        }

        public async Task<Comment?> Load(long id)
        {
            string path = ApiSession.CombineUri(Comment.RESTBasePath(), id.ToString());
            HttpResponseMessage response = await Get(path, ExpectedResponse.Json);
            EnsureSuccessStatusCode(response);
            return JsonConvert.DeserializeObject<Comment>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);
        }

        public async Task<ListResult<Comment>?> Load(CommentParams options)
        {
            string path = ApiSession.CombineUri(Comment.RESTBasePath());
            HttpResponseMessage response = await Get(path, ExpectedResponse.Json, options.UrlParameter());
            EnsureSuccessStatusCode(response);
            return JsonConvert.DeserializeObject<ListResult<Comment>>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);
        }

        public async Task<bool> Remove(long id)
        {
            string path = ApiSession.CombineUri(Comment.RESTBasePath(), id.ToString());
            HttpResponseMessage response = await Delete(path, ExpectedResponse.Plain);
            EnsureSuccessStatusCode(response);
            string s = response.Content.ReadAsStringAsync().Result;
            if (long.TryParse(s, out long i))
            {
                return (i > 0);
            }
            throw new Exception("Removing item with id " + id + " failed.");
        }

        public async Task<long?> Update(Comment newItem)
        {
            CheckId(newItem.Id, "comment id");
            CheckId(newItem.DocumentLanguageVersionId, "document language version id");
            string path = ApiSession.CombineUri(Comment.RESTBasePath(), newItem.Id.ToString());
            HttpResponseMessage response = await Put(path, ExpectedResponse.Json, newItem);
            EnsureSuccessStatusCode(response);
            Comment? c = JsonConvert.DeserializeObject<Comment>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);
            if (!(c is null))
            {
                return c.Id;
            }
            return null;
        }
    }
}