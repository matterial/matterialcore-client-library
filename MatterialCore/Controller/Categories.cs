﻿using MatterialCore.Entity;
using MatterialCore.Enums;
using MatterialCore.Interfaces;
using MatterialCore.Params;

using Newtonsoft.Json;

using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace MatterialCore.Controller
{
    ///<inheritdoc cref="ICategory"/>
    internal class Categories : Entities, ICategory
    {
        public Categories(string sessionId, string basePath)
        {
            UpdateSession(sessionId);
            BasePath = basePath;
        }

        #region Load

        public async Task<List<Category>?> Load(CategoryParams options)
        {
            string path = ApiSession.CombineUri(Category.RESTBasePath());
            HttpResponseMessage response = await Get(path, ExpectedResponse.Json, options.UrlParameter());
            EnsureSuccessStatusCode(response);
            return JsonConvert.DeserializeObject<List<Category>>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);
        }

        public async Task<Category?> Load(long id)
        {
            return await Load(id, new CategoryByIdBaseParams());
        }

        public async Task<Category?> Load(long id, CategoryByIdBaseParams options)
        {
            CheckId(id, nameof(id));
            string path = ApiSession.CombineUri(Category.RESTBasePath(), id.ToString());
            HttpResponseMessage response = await Get(path, ExpectedResponse.Json, options.UrlParameter());
            EnsureSuccessStatusCode(response);
            return JsonConvert.DeserializeObject<Category>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);
        }

        public async Task<List<Category>?> LoadByType(long typeId)
        {
            return await LoadByType(typeId, new CategoryByTypeIdParams());
        }

        public async Task<List<Category>?> LoadByType(long typeId, CategoryByTypeIdParams options)
        {
            CheckId(typeId, nameof(typeId));
            string path = ApiSession.CombineUri(Category.RESTBasePath(), Const.BY_TYPE, typeId.ToString());
            HttpResponseMessage response = await Get(path, ExpectedResponse.Json, options.UrlParameter());
            EnsureSuccessStatusCode(response);
            return JsonConvert.DeserializeObject<List<Category>>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);
        }

        public async Task<CategoryType?> LoadType(long typeId, LangKey languageKey)
        {
            CheckId(typeId, nameof(typeId));
            string path = ApiSession.CombineUri(Category.RESTBasePath(), Const.TYPE, typeId.ToString());
            ParameterCollection pc = new ParameterCollection
            {
                {languageKey.KeyAndValue() }
            };
            HttpResponseMessage response = await Get(path, ExpectedResponse.Json, pc);
            EnsureSuccessStatusCode(response);
            return JsonConvert.DeserializeObject<CategoryType>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);
        }

        public async Task<List<CategoryType>?> LoadTypes(CategoryTypeParams options)
        {
            string path = ApiSession.CombineUri(Category.RESTBasePath(), Const.TYPE);
            HttpResponseMessage response = await Get(path, ExpectedResponse.Json, options.UrlParameter());
            EnsureSuccessStatusCode(response);
            return JsonConvert.DeserializeObject<List<CategoryType>>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);
        }

        #endregion Load

        #region Create

        public async Task<long> CreateType(CategoryType category, LangKey languageKey)
        {
            string path = ApiSession.CombineUri(Category.RESTBasePath(), Const.TYPE);
            ParameterCollection pc = new ParameterCollection
            {
                {languageKey.KeyAndValue() }
            };
            HttpResponseMessage response = await Post(path, ExpectedResponse.Json, category, pc);
            EnsureSuccessStatusCode(response);
            CategoryType? c = JsonConvert.DeserializeObject<CategoryType>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);
            if (!(c is null))
            {
                return c.Id;
            }
            return 0;
        }

        public async Task<long> Create(Category category, LangKey languageKey)
        {
            CheckId(category.TypeId, "category type id");
            string path = ApiSession.CombineUri(Category.RESTBasePath());
            ParameterCollection pc = new ParameterCollection
            {
                {languageKey.KeyAndValue() }
            };
            HttpResponseMessage response = await Post(path, ExpectedResponse.Json, category, pc);
            EnsureSuccessStatusCode(response);
            Category? c = JsonConvert.DeserializeObject<Category>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);
            if (!(c is null))
            {
                return c.Id;
            }
            return 0;
        }

        public async Task<long> Update(Category category, LangKey languageKey)
        {
            CheckId(category.Id, nameof(category.Id));
            string path = ApiSession.CombineUri(Category.RESTBasePath(), category.Id.ToString());
            ParameterCollection pc = new ParameterCollection
            {
                {languageKey.KeyAndValue() }
            };
            HttpResponseMessage response = await Put(path, ExpectedResponse.Json, category, pc);
            EnsureSuccessStatusCode(response);
            Category? c = JsonConvert.DeserializeObject<Category>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);
            if (!(c is null))
            {
                return c.Id;
            }
            return 0;
        }

        public async Task<long> UpdateType(CategoryType categoryType, LangKey languageKey)
        {
            CheckId(categoryType.Id, nameof(categoryType.Id));
            string path = ApiSession.CombineUri(Category.RESTBasePath(), Const.TYPE, categoryType.Id.ToString());
            ParameterCollection pc = new ParameterCollection
            {
                {languageKey.KeyAndValue() }
            };
            HttpResponseMessage response = await Put(path, ExpectedResponse.Json, categoryType, pc);
            EnsureSuccessStatusCode(response);
            CategoryType? c = JsonConvert.DeserializeObject<CategoryType>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);
            if (!(c is null))
            {
                return c.Id;
            }
            return 0;
        }

        #endregion Create

        #region Remove

        private enum RemovalType : int
        {
            Category,
            CategoryByTypeiD,
            CategoryType
        }

        private async Task<long> RemoveInternal(RemovalType type, long id)
        {
            string path = ApiSession.CombineUri(Category.RESTBasePath(), id.ToString());
            switch (type)
            {
                case Categories.RemovalType.Category:
                    CheckId(id, "category id");

                    break;

                case Categories.RemovalType.CategoryType:
                    CheckId(id, "category type id");
                    path = ApiSession.CombineUri(Category.RESTBasePath(), Const.TYPE, id.ToString());
                    break;

                case RemovalType.CategoryByTypeiD:
                    CheckId(id, "category type id");
                    path = ApiSession.CombineUri(Category.RESTBasePath(), Const.BY_TYPE, id.ToString());
                    break;
            }
            HttpResponseMessage response = await Delete(path, ExpectedResponse.Plain);
            EnsureSuccessStatusCode(response);
            string s = response.Content.ReadAsStringAsync().Result;
            if (long.TryParse(s, out long i))
            {
                return i;
            }
            throw new Exception("Removing item with id " + id + " failed.");
        }

        public async Task Remove(long id)
        {
            await RemoveInternal(RemovalType.Category, id);
        }

        public async Task<long> RemoveByTypeId(long typeId)
        {
            return await RemoveInternal(RemovalType.CategoryByTypeiD, typeId);
        }

        public async Task RemoveType(long typeId)
        {
            await RemoveInternal(RemovalType.CategoryType, typeId);
        }

        #endregion Remove

        #region Follow

        private enum FollowingActions : int
        {
            IsFollowing,
            Follow,
            FolowOtherAcount,
            UnFollow,
            UnfollowOtherAccount
        }

        private async Task<bool> FollowingAction(FollowingActions action, long categoryId, long? otherAccountId, LangKey languageKey)
        {
            string path = ApiSession.CombineUri(Category.RESTBasePath());
            ParameterCollection pc = new ParameterCollection
            {
                {languageKey.KeyAndValue() }
            };

            switch (action)
            {
                case FollowingActions.IsFollowing:
                    path = ApiSession.CombineUri(path, categoryId.ToString(), Const.FOLLOW);
                    break;

                case FollowingActions.Follow:
                    path = ApiSession.CombineUri(path, categoryId.ToString(), Const.FOLLOW);
                    break;

                case FollowingActions.FolowOtherAcount:
                    path = ApiSession.CombineUri(path, categoryId.ToString(), Const.FOLLOW, Const.BY_ACCOUNT, otherAccountId.ToString());
                    break;

                case FollowingActions.UnFollow:
                    path = ApiSession.CombineUri(path, categoryId.ToString(), Const.UNFOLLOW);

                    break;

                case FollowingActions.UnfollowOtherAccount:
                    path = ApiSession.CombineUri(path, categoryId.ToString(), Const.UNFOLLOW, Const.BY_ACCOUNT, otherAccountId.ToString());

                    break;

                default:
                    break;
            }

            HttpResponseMessage? response;
            if (action == FollowingActions.IsFollowing)
            {
                response = await Get(path, ExpectedResponse.Plain, pc);
            }
            else
            {
                response = await Put(path, ExpectedResponse.Plain, pc);
            }
            EnsureSuccessStatusCode(response);
            string s = response.Content.ReadAsStringAsync().Result;
            if (action == FollowingActions.IsFollowing)
            {
                if (bool.TryParse(s, out bool b))
                {
                    return b;
                }
            }
            else
            {
                if (int.TryParse(s, out int i))
                {
                    return (i > 0);
                }
            }

            return false;
        }

        public async Task<bool> IsFollowing(long id, LangKey languageKey)
        {
            return await FollowingAction(FollowingActions.IsFollowing, id, null, languageKey);
        }

        public async Task<bool> Follow(long id, LangKey languageKey)
        {
            return await FollowingAction(FollowingActions.Follow, id, null, languageKey);
        }

        public async Task<bool> Follow(long id, long accountId, LangKey languageKey)
        {
            return await FollowingAction(FollowingActions.FolowOtherAcount, id, accountId, languageKey);
        }

        public async Task<bool> UnFollow(long id, LangKey languageKey)
        {
            return await FollowingAction(FollowingActions.UnFollow, id, null, languageKey);
        }

        public async Task<bool> UnFollow(long id, long accountId, LangKey languageKey)
        {
            return await FollowingAction(FollowingActions.UnfollowOtherAccount, id, accountId, languageKey);
        }

        #endregion Follow

        #region Assign

        private async Task<bool> AssignInternal(long id, List<long> documentIds, bool quick = false, bool remove = false)
        {
            if (documentIds.Count == 0)
            {
                throw new ArgumentException("The parameter documentIds is invalid.");
            }
            string path;
            if (quick)
            {
                path = ApiSession.CombineUri(Category.RESTBasePath(), Const.QUICK, remove ? Const.UNASSIGN : Const.ASSIGN);
            }
            else
            {
                CheckId(id, nameof(id));
                path = ApiSession.CombineUri(Category.RESTBasePath(), id.ToString(), remove ? Const.UNASSIGN : Const.ASSIGN);
            }

            ParameterCollection pc = new ParameterCollection();
            pc.AllowedDuplicateKeys.Add(Const.PARAM_DOCUMENT_ID);
            foreach (var item in documentIds)
            {
                CheckId(item, "documentId");
                pc.Add(Const.PARAM_DOCUMENT_ID, item.ToString());
            }
            HttpResponseMessage response = await Put(path, ExpectedResponse.Plain, pc);
            string s = response.Content.ReadAsStringAsync().Result;
            if (int.TryParse(s, out int i))
            {
                return (i > 0);
            }
            return false;
        }

        public async Task<bool> Assign(long id, List<long> documentIds)
        {
            return await AssignInternal(id, documentIds, false, false);
        }

        public async Task<bool> Assign(long id, long documentId)
        {
            List<long> docIds = new List<long> { documentId };
            return await AssignInternal(id, docIds, false);
        }

        public async Task<bool> AssignQuickCategory(List<long> documentIds)
        {
            return await AssignInternal(0, documentIds, true);
        }

        public async Task<bool> AssignQuickCategory(long documentId)
        {
            List<long> docIds = new List<long> { documentId };
            return await AssignInternal(0, docIds, true);
        }

        public async Task<bool> UnAssign(long id, List<long> documentIds)
        {
            return await AssignInternal(id, documentIds, false, true);
        }

        public async Task<bool> UnAssign(long id, long documentId)
        {
            List<long> docIds = new List<long> { documentId };
            return await AssignInternal(id, docIds, false, true);
        }

        public async Task<bool> UnAssignQuickCategory(List<long> documentIds)
        {
            return await AssignInternal(0, documentIds, true, true);
        }

        public async Task<bool> UnAssignQuickCategory(long documentId)
        {
            List<long> docIds = new List<long> { documentId };
            return await AssignInternal(0, docIds, true, true);
        }

        #endregion Assign
    }
}