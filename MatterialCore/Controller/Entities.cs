﻿using System;

//using System.Text.Json;

namespace MatterialCore.Controller
{
    internal abstract class Entities : RestClient
    {
        protected virtual void CheckId(long? id, string propertyName)
        {
            if (id is null)
            {
                throw new ArgumentException("Invalid argument " + propertyName + ". It must not be null.");
            }

            if (id < 1)
            {
                throw new ArgumentException("Invalid argument " + propertyName + ". It must be greater than 0.");
            }
        }
    }
}