﻿using MatterialCore.Entity;
using MatterialCore.Enums;
using MatterialCore.Interfaces;
using MatterialCore.Params;

using Newtonsoft.Json;

using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace MatterialCore.Controller
{
    ///<inheritdoc cref="ILanguage"/>
    internal class Languages : Entities, ILanguage
    {
        public Languages(string sessionId, string basePath)
        {
            UpdateSession(sessionId);
            BasePath = basePath;
        }

        public async Task<Language?> Load(long id)
        {
            string path = ApiSession.CombineUri(Language.RESTBasePath(), id.ToString());
            HttpResponseMessage response = await Get(path, ExpectedResponse.Json);
            EnsureSuccessStatusCode(response);
            return JsonConvert.DeserializeObject<Language>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);
        }

        public async Task<List<Language>?> Load(LanguageParams options)
        {
            string path = ApiSession.CombineUri(Language.RESTBasePath());
            HttpResponseMessage response = await Get(path, ExpectedResponse.Json, options.UrlParameter());
            EnsureSuccessStatusCode(response);
            return JsonConvert.DeserializeObject<List<Language>>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);
        }

        public async Task<bool> Aktivate(long id)
        {
            string path = ApiSession.CombineUri(Language.RESTBasePath(), id.ToString(), Const.ACTIVATE);
            HttpResponseMessage response = await Put(path, ExpectedResponse.Plain);
            EnsureSuccessStatusCode(response);
            string retVal = response.Content.ReadAsStringAsync().Result;
            if (retVal.ToInt() > 0)
                return true;
            return false;
        }

        public async Task<bool> Deaktivate(long id)
        {
            string path = ApiSession.CombineUri(Language.RESTBasePath(), id.ToString(), Const.DEACTIVATE);
            HttpResponseMessage response = await Put(path, ExpectedResponse.Plain);
            EnsureSuccessStatusCode(response);
            string retVal = response.Content.ReadAsStringAsync().Result;
            if (retVal.ToInt() > 0)
                return true;
            return false;
        }
    }
}