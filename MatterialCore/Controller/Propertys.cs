﻿using MatterialCore.Entity;
using MatterialCore.Enums;
using MatterialCore.Interfaces;
using MatterialCore.Params;
using MatterialCore.Params.Doc;

using Newtonsoft.Json;

using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace MatterialCore.Controller
{
    ///<inheritdoc cref="IProperty"/>
    internal class Propertys : Entities, IProperty
    {
        private enum ByIdActions : int
        {
            Follow,
            Unfollow,
            FollowOther,
            UnfollowOther,
            IsFollowing,
            Remove,
            Create,
            Update,
            Get,
            GetById
        }

        public Propertys(string sessionId, string basePath)
        {
            UpdateSession(sessionId);
            BasePath = basePath;
        }

        private async Task<HttpResponseMessage?> ByIdAction(ByIdActions action, long? propertyId, LangKey? langParam,
            long? otherAccountId, AdditionalProperty? property, PropertyLoadByIdParams? getByIdParams, PropertyLoadParams? loadByFilterParams)
        {
            //This function handles the different AdditionalProperty requests.
            string path = ApiSession.CombineUri(Document.RESTBasePath(), Const.ADDITIONAL_PROPERTY);
            ParameterCollection pc = new ParameterCollection();

            HttpResponseMessage? response = null;
            LangKey langKey = langParam ?? Matterial.DefaultLanguage;
            long otherId = otherAccountId ?? 0;
            AdditionalProperty item = property ?? new AdditionalProperty();
            PropertyLoadByIdParams loadByIdParams = getByIdParams ?? new PropertyLoadByIdParams();
            PropertyLoadParams loadParams = loadByFilterParams ?? new PropertyLoadParams();
            long id = propertyId ?? 0;

            switch (action)
            {
                case ByIdActions.IsFollowing:
                    path = ApiSession.CombineUri(path, id.ToString(), Const.FOLLOW);
                    pc.Add(langKey.KeyAndValue());
                    break;

                case ByIdActions.Follow:
                    path = ApiSession.CombineUri(path, id.ToString(), Const.FOLLOW);
                    pc.Add(langKey.KeyAndValue());
                    break;

                case ByIdActions.Unfollow:
                    path = ApiSession.CombineUri(path, id.ToString(), Const.UNFOLLOW);
                    pc.Add(langKey.KeyAndValue());
                    break;

                case ByIdActions.FollowOther:
                    path = ApiSession.CombineUri(path, id.ToString(), Const.FOLLOW, Const.BY_ACCOUNT, otherId.ToString());
                    pc.Add(langKey.KeyAndValue());
                    break;

                case ByIdActions.UnfollowOther:
                    path = ApiSession.CombineUri(path, id.ToString(), Const.UNFOLLOW, Const.BY_ACCOUNT, otherId.ToString());
                    pc.Add(langKey.KeyAndValue());
                    break;

                case ByIdActions.Remove:
                    path = ApiSession.CombineUri(path, id.ToString());
                    break;

                case ByIdActions.Create:
                    pc.Add(langKey.KeyAndValue());
                    break;

                case ByIdActions.Update:
                    path = ApiSession.CombineUri(path, id.ToString());
                    pc.Add(langKey.KeyAndValue());
                    break;

                case ByIdActions.Get:
                    pc.Copy(loadParams.UrlParameter());
                    break;

                case ByIdActions.GetById:
                    pc.Copy(loadByIdParams.UrlParameter());
                    path = ApiSession.CombineUri(path, id.ToString());
                    break;
            }

            switch (action)
            {
                case ByIdActions.IsFollowing:
                    response = await Get(path, ExpectedResponse.Plain, pc);
                    break;

                case ByIdActions.Follow:
                case ByIdActions.Unfollow:
                case ByIdActions.FollowOther:
                case ByIdActions.UnfollowOther:
                    response = await Put(path, ExpectedResponse.Plain, pc);
                    break;

                case ByIdActions.Remove:
                    response = await Delete(path, ExpectedResponse.Plain);
                    break;

                case ByIdActions.Create:
                    response = await Post(path, ExpectedResponse.Json, item, pc);
                    break;

                case ByIdActions.Update:
                    response = await Put(path, ExpectedResponse.Json, item, pc);
                    break;

                case ByIdActions.Get:
                case ByIdActions.GetById:
                    response = await Get(path, ExpectedResponse.Json, pc);
                    break;
            }

            return response;
        }

        public async Task<long> Create(AdditionalProperty newItem, LangKey langParams)
        {
            HttpResponseMessage? response = await ByIdAction(ByIdActions.Create, null, langParams, null, newItem, null, null);
            if (!(response is null))
            {
                EnsureSuccessStatusCode(response);
                //Returns a new AdditionalProperty
                AdditionalProperty? retVal = JsonConvert.DeserializeObject<AdditionalProperty>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);
                if (!(retVal is null))
                {
                    return retVal.Id;
                }
            }
            return 0;
        }

        public async Task<bool> Follow(long id, LangKey langParam)
        {
            return await FollowAction(id, null, langParam, false);
        }

        public async Task<bool> Follow(long id, long accountId, LangKey langParam)
        {
            return await FollowAction(id, accountId, langParam, false);
        }

        public async Task<bool> UnFollow(long id, LangKey langParam)
        {
            return await FollowAction(id, null, langParam, true);
        }

        public async Task<bool> UnFollow(long id, long accountId, LangKey langParam)
        {
            return await FollowAction(id, accountId, langParam, true);
        }

        private async Task<bool> FollowAction(long id, long? accountId, LangKey langParam, bool unFollow)
        {
            bool retVal = false;
            ByIdActions action;
            if ((accountId is null) || (accountId < 1))
            {
                if (unFollow)
                {
                    //Make the current user un-follow the additional property
                    action = ByIdActions.Unfollow;
                }
                else
                {
                    //Make the current user follower of the additional property
                    action = ByIdActions.Follow;
                }
            }
            else
            {
                if (unFollow)
                {
                    //Make other user accoint un-follow the additional property
                    action = ByIdActions.UnfollowOther;
                }
                else
                {
                    //Make other user account follow the additional property
                    action = ByIdActions.FollowOther;
                }
            }
            HttpResponseMessage? response = await ByIdAction(action, id, langParam, accountId, null, null, null);
            if (!(response is null))
            {
                EnsureSuccessStatusCode(response);
                if (int.TryParse(response.Content.ReadAsStringAsync().Result, out int i))
                {
                    retVal = (i != 0);
                }
            }
            return retVal;
        }

        public async Task<AdditionalProperty?> Load(long id, PropertyLoadByIdParams loadParams)
        {
            //Returns a  AdditionalProperty by id
            AdditionalProperty? retVal = null;
            HttpResponseMessage? response = await ByIdAction(ByIdActions.GetById, id, null, null, null, loadParams, null);
            if (!(response is null))
            {
                EnsureSuccessStatusCode(response);
                retVal = JsonConvert.DeserializeObject<AdditionalProperty>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);
            }
            return retVal;
        }

        public async Task<bool> IsFollowing(long id, LangKey langParam)
        {
            //Check if the current user is following and AdditionalProperty
            bool retVal = false;
            HttpResponseMessage? response = await ByIdAction(ByIdActions.IsFollowing, id, langParam, null, null, null, null);
            if (!(response is null))
            {
                EnsureSuccessStatusCode(response);
                if (bool.TryParse(response.Content.ReadAsStringAsync().Result, out bool b))
                {
                    retVal = b;
                }
            }
            return retVal;
        }

        public async Task<List<AdditionalProperty>?> Load(PropertyLoadParams loadParams)
        {
            //Loads AdditionalProperties
            List<AdditionalProperty>? retVal = null;
            HttpResponseMessage? response = await ByIdAction(ByIdActions.Get, null, null, null, null, null, loadParams);
            if (!(response is null))
            {
                EnsureSuccessStatusCode(response);
                retVal = JsonConvert.DeserializeObject<List<AdditionalProperty>>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);
            }
            return retVal;
        }

        public async Task<long> Update(long id, AdditionalProperty item, LangKey langParam)
        {
            HttpResponseMessage? response = await ByIdAction(ByIdActions.Update, id, langParam, null, item, null, null);
            if (!(response is null))
            {
                EnsureSuccessStatusCode(response);
                //Returns a  AdditionalProperty by id
                AdditionalProperty? retVal = JsonConvert.DeserializeObject<AdditionalProperty>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);
                if (!(retVal is null))
                {
                    return retVal.Id;
                }
            }
            return 0;
        }

        public async Task<bool> Remove(long id)
        {
            //remove an AdditionalProperty
            bool retVal = false;
            HttpResponseMessage? response = await ByIdAction(ByIdActions.Remove, id, null, null, null, null, null);
            if (!(response is null))
            {
                EnsureSuccessStatusCode(response);
                if (int.TryParse(response.Content.ReadAsStringAsync().Result, out int i))
                {
                    retVal = (i != 0);
                }
            }
            return retVal;
        }
    }
}