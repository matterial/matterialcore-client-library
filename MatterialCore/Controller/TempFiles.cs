﻿using MatterialCore.Entity;
using MatterialCore.Enums;
using MatterialCore.Interfaces;
using MatterialCore.Params;
using MatterialCore.Params.Doc;

using Newtonsoft.Json;

using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace MatterialCore.Controller
{
    internal class TempFiles : Entities, ITempFile
    {
        public TempFiles(string sessionId, string basePath)
        {
            UpdateSession(sessionId);
            BasePath = basePath;
        }

        private void CheckTempDescriptorValid(TempFileDescriptor tempFileDescriptor)
        {
            if (String.IsNullOrEmpty(tempFileDescriptor.ContextToken))
            {
                throw new ArgumentException("The ContextToken of the TempFileDescriptor must not be null or empty");
            }
            if (String.IsNullOrEmpty(tempFileDescriptor.FileToken))
            {
                throw new ArgumentException("The FileToken of the TempFileDescriptor must not be null or empty");
            }
        }

        public async Task<Stream?> GetTempFile(TempFileType tempFileType, string contextToken, string fileToken, LangKey languageKey, bool forceAttachment)
        {
            string path = ApiSession.CombineUri(Const.MTR_BACKEND, Const.APPLICATION_PATH, Const.TEMP_FILE, tempFileType.PathParameterValue(), contextToken, fileToken);
            ParameterCollection pc = new ParameterCollection
            {
                {Const.PARAM_FORCE_ATTACHMENT, forceAttachment.ToString().ToLower() },
                {languageKey.KeyAndValue()  }
            };
            HttpResponseMessage response = await Get(path, ExpectedResponse.Nothing, pc);
            EnsureSuccessStatusCode(response);
            return await response.Content.ReadAsStreamAsync();
        }

        public async Task<Stream?> GetTempFile(TempFileDescriptor tempFileDescriptor, bool forceAttachment)
        {
            CheckTempDescriptorValid(tempFileDescriptor);
            TempFileType tempFileType = TempFileType.Attachment;

            if (Enum.TryParse<TempFileType>(tempFileDescriptor.FileType, out TempFileType t))
            {
                tempFileType = t;
            }
            LangKey languageKey = Matterial.DefaultLanguage;
            if (Enum.TryParse<LangKey>(tempFileDescriptor.LanguageKey, out LangKey l))
            {
                languageKey = l;
            }
            return await GetTempFile(tempFileType, tempFileDescriptor.ContextToken ?? "", tempFileDescriptor.FileToken ?? "", languageKey, forceAttachment);
        }

        public async Task<TempFileDescriptor?> UploadFile(UploadFile uploadFile, string contextToken, LangKey languageKey)
        {
            List<UploadFile> uploadFiles = new List<UploadFile> { uploadFile };
            CheckFiles(uploadFiles);
            List<TempFileDescriptor>? tfdList = await UploadFile(uploadFiles, contextToken, languageKey);
            if (!(tfdList is null) && tfdList.Count > 0)
            {
                return tfdList[tfdList.Count - 1];
            }
            return null;
        }

        public async Task<TempFileDescriptor?> UploadText(string text, string contextToken, LangKey languageKey)
        {
            string path = ApiSession.CombineUri(Const.MTR_BACKEND, Const.APPLICATION_PATH, Const.TEMP_FILE);
            ParameterCollection pc = new ParameterCollection
            {
                {Const.PARAM_CONTEXT_TOKEN, contextToken  },
                {languageKey.KeyAndValue()  }
            };
            StringContent content = new StringContent(text, Encoding.UTF8, MediaType(ExpectedResponse.Plain));
            HttpResponseMessage response = await Post(path, ExpectedResponse.Json, content, pc);
            EnsureSuccessStatusCode(response);
            return JsonConvert.DeserializeObject<TempFileDescriptor>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);
        }

        private void CheckFiles(List<UploadFile> files)
        {
            foreach (var item in files)
            {
                if (!File.Exists(item.FilePath))
                {
                    throw new FileNotFoundException("File not found" + item.FilePath);
                }
            }
        }

        public async Task<List<TempFileDescriptor>?> UploadFile(List<UploadFile> uploadFiles, string contextToken, LangKey languageKey)
        {
            CheckFiles(uploadFiles);
            string path = ApiSession.CombineUri(TempFileDescriptor.RESTBasePath());
            ParameterCollection pc = new ParameterCollection
            {
                {Const.PARAM_CONTEXT_TOKEN, contextToken  },
                {languageKey.KeyAndValue()  }
            };
            HttpWebResponse response = (HttpWebResponse)(await UploadFiles(path, ExpectedResponse.Json, pc, uploadFiles));
            EnsureSuccessStatusCode(response);
            string stringResponse;
            using (StreamReader reader = new StreamReader(response.GetResponseStream()))
            {
                stringResponse = reader.ReadToEnd();
            };
            return JsonConvert.DeserializeObject<List<TempFileDescriptor>>(stringResponse, DefaultSerializerOptions);
        }

        public async Task RemoveTempfiles(string contextToken)
        {
            string path = ApiSession.CombineUri(Const.MTR_BACKEND, Const.APPLICATION_PATH, Const.TEMP_FILE, contextToken);
            HttpResponseMessage response = await Delete(path, ExpectedResponse.Nothing);
            EnsureSuccessStatusCode(response);
        }

        public async Task RemoveTempfile(TempFileType tempFileType, string contextToken, string fileToken, LangKey languageKey)
        {
            string path = ApiSession.CombineUri(Const.MTR_BACKEND, Const.APPLICATION_PATH, Const.TEMP_FILE, tempFileType.PathParameterValue(), contextToken, fileToken);
            ParameterCollection pc = new ParameterCollection
            {
                {languageKey.KeyAndValue()  }
            };
            HttpResponseMessage response = await Delete(path, ExpectedResponse.Nothing, pc);
            EnsureSuccessStatusCode(response);
        }

        public async Task RemoveTempfile(TempFileDescriptor tempFileDescriptor)
        {
            CheckTempDescriptorValid(tempFileDescriptor);
            if (!Enum.TryParse(tempFileDescriptor.FileType, out TempFileType t))
            {
                return;
            }
            TempFileType fType = t;
            if (Enum.TryParse<LangKey>(tempFileDescriptor.FileType, out LangKey l))
            {
                await RemoveTempfile(fType, tempFileDescriptor.ContextToken ?? "", tempFileDescriptor.FileToken ?? "", l);
            }
        }
    }
}