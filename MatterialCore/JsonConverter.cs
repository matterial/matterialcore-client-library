﻿using System;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace MatterialCore
{
    public class BoolNullHandlingConverter : JsonConverter<bool>
    {
        public override bool Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
        {
            if (reader.TokenType == JsonTokenType.Null)
            {
                return default;
            }
            return reader.GetBoolean();
        }

        public override void Write(Utf8JsonWriter writer, bool boolValue, JsonSerializerOptions options)
        {
            writer.WriteBooleanValue(boolValue);
        }
    }
}