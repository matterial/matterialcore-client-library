﻿/*! \mainpage
 * 
 * \section Intro Matterial Core
 * 
 *
 * The Matterial Core library is a wrapper of the Matterial REST API (https://www.matterial.com/documentation/api/).
 * It can be used to perform all operations on the Matterial server application.
 *
 * The main entry of the library is the class Matterial which exposes static functions to create an API session. Once a session is aquired, it is used to poerform actions on entities, like documents, users, permissions etc.
 * The Quick Start guide can be found here: https://www.matterial.com/documentation/api/644
 */
