﻿using MatterialCore.Enums;

namespace MatterialCore.Params.Person
{
    public abstract class PersonBaseParams : PersonAutocompleteParams
    {
        internal override ParameterCollection UrlParameter()
        {
            pc = new ParameterCollection(base.UrlParameter())
            {
                { Const.PARAM_OFFSET, Offset.ToString() },
                { Const.PARAM_LIMIT, Limit.ToString() }
            };
            AddQueryParam(OrderDir.KeyAndValue().Value, OrderDir.KeyAndValue().Key, OrderDirection.asc.KeyAndValue().Value);

            return pc;
        }

        public OrderDirection OrderDir { get; set; }
        public int Offset { get; set; }
        public int Limit { get; set; }
    }
}