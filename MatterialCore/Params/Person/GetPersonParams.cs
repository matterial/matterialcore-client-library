﻿namespace MatterialCore.Params.Person
{
    public sealed class GetPersonParams : PersonExportParams
    {
        public GetPersonParams() : base()
        {
            LoadOptions = new GetPersonByIdParams();
        }

        internal override ParameterCollection UrlParameter()
        {
            pc = new ParameterCollection(base.UrlParameter());

            AddQueryParam(Count, Const.PARAM_COUNT);
            LoadOptions.DebugUrlParameter = DebugUrlParameter;
            pc.Copy(LoadOptions.UrlParameter());

            return pc;
        }

        public GetPersonByIdParams LoadOptions { get; set; }
        public bool Count { get; set; }
    }
}