﻿using MatterialCore.Enums;

namespace MatterialCore.Params.Person
{
    public class PersonAutocompleteParams : ParamBase
    {
        public PersonAutocompleteParams()
        {
        }

        internal virtual ParameterCollection UrlParameter()
        {
            pc = new ParameterCollection();
            AddQueryParam(RoleId, Const.PARAM_ROLE_ID);
            AddQueryParam(InstanceOwner, "InstanceOwner");
            AddQueryParam(DemoUser, "Demo");
            AddQueryParam(LimitedUser, "Limited");
            AddQueryParam(Active, "Active");
            return pc;
        }

        public long RoleId { get; set; }
        public ParamHideValue InstanceOwner { get; set; }
        public ParamHideValue DemoUser { get; set; }
        public ParamHideValue LimitedUser { get; set; }
        public ParamHideValue Active { get; set; }
    }
}