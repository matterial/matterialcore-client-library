﻿using System;

namespace MatterialCore.Params.Person
{
    public class UpdatePersonParams : ParamBase
    {
        public UpdatePersonParams() : base()
        { }

        internal ParameterCollection UrlParameter()
        {
            pc = new ParameterCollection();
            AddQueryParam(IgnoreClients, Const.PARAM_IGNORE_CLIENTS);
            AddQueryParam(IgnoreContactImages, Const.PARAM_IGNORE_CONTACT_IMAGES);
            AddQueryParam(IgnoreAddreses, Const.PARAM_IGNORE_ADDRESSES);
            AddQueryParam(IgnoreCommuncationData, Const.PARAM_IGNORE_COMMUNICATION_DATA);

            return pc;
        }

        /// <summary>
        /// If true, the clients will not be updated.
        /// </summary>
        public bool IgnoreClients { get; set; }

        /// <summary>
        /// If true, the contact images will not be updated.
        /// </summary>
        public bool IgnoreContactImages { get; set; }

        /// <summary>
        /// If true, the addresses will not be updated.
        /// </summary>
        public bool IgnoreAddreses { get; set; }

        /// <summary>
        /// If true, the communication data will not be updated.
        /// </summary>
        public bool IgnoreCommuncationData { get; set; }
    }
}