﻿using MatterialCore.Enums;

namespace MatterialCore.Params.Person
{
    public sealed class SearchPersonParams : PersonBaseParams
    {
        public SearchPersonParams() : base()
        {
            Offset = 0;
            Limit = Matterial.DefaultSearchResultLimit;
        }

        internal override ParameterCollection UrlParameter()
        {
            pc = new ParameterCollection(base.UrlParameter());

            AddQueryParam(OrderBy.KeyAndValue().Value, OrderBy.KeyAndValue().Key, PersonSearchOrderBy.LastName.KeyAndValue().Value);

            return pc;
        }

        public PersonSearchOrderBy OrderBy { get; set; }
    }
}