﻿using MatterialCore.Enums;

namespace MatterialCore.Params.Person
{
    public class PersonExportParams : PersonBaseParams
    {
        public PersonExportParams() : base()
        {
            Offset = 0;
            Limit = Matterial.DefaultSearchResultLimit;
        }

        internal override ParameterCollection UrlParameter()
        {
            pc = new ParameterCollection(base.UrlParameter());
            AddQueryParam(AccountId, Const.PARAM_ACCOUNT_ID);
            AddQueryParam(ContactId, Const.PARAM_CONTACT_ID);
            AddQueryParam(AccountLogin, Const.PARAM_ACCOUNT_LOGIN);
            AddQueryParam(IncludeSystemUser, Const.PARAM_INCLUDE_SYSTEM_USER);
            AddQueryParam(FollowedCategoryId, Const.PARAM_FOLLOWED_CATEGORY_ID);
            AddQueryParam(FollowedAdditionalPropertyId, Const.PARAM_FOLLOWED_ADDITIONAL_PROPERTY_ID);
            AddQueryParam(FollowedDocumentId, Const.PARAM_FOLLOWED_DOCUMENT_ID);
            AddQueryParam(FollowedLanguageId, Const.PARAM_FOLLOWED_LANGUAGE_ID);
            AddQueryParam(MarkedAsHelpfulDocumentId, Const.PARAM_MARKED_AS_HELPFUL_DOCUMENT_ID);
            AddQueryParam(MarkedAsHelpfulDocumentLanguageVersionId, Const.PARAM_MARKED_AS_HELPFUL_DOCUMENT_LANGUAGE_VERSION_ID);
            AddQueryParam(MarkedAsHelpfulLanguageId, Const.PARAM_MARKED_AS_HELPFUL_LANGUAGE_ID);
            AddQueryParam(ResponsibleForDocumentId, Const.PARAM_RESPONSIBLE_FOR_DOCUMENT_ID);
            AddQueryParam(AuthorshipForDocumentId, Const.PARAM_AUTHORSHIP_FOR_DOCUMENT_ID);
            AddQueryParam(AuthorshipForLanguageVersionId, Const.PARAM_AUTHORSHIP_FOR_LANGUAGE_VERSION_ID);
            AddQueryParam(AuthorshipForLanguageId, Const.PARAM_AUTHORSHIP_FOR_LANGUAGE_ID);
            AddQueryParam(OrderBy.KeyAndValue().Value, OrderBy.KeyAndValue().Key, PersonOrderBy.LastName.KeyAndValue().Value);

            return pc;
        }

        public long AccountId { get; set; }
        public long ContactId { get; set; }
        public string? AccountLogin { get; set; }
        public bool IncludeSystemUser { get; set; }
        public long FollowedCategoryId { get; set; }
        public long FollowedAdditionalPropertyId { get; set; }
        public long FollowedDocumentId { get; set; }
        public long FollowedLanguageId { get; set; }
        public long MarkedAsHelpfulDocumentId { get; set; }
        public long MarkedAsHelpfulDocumentLanguageVersionId { get; set; }
        public long MarkedAsHelpfulLanguageId { get; set; }
        public bool ResponsibleForDocumentId { get; set; }
        public bool AuthorshipForDocumentId { get; set; }
        public bool AuthorshipForLanguageVersionId { get; set; }
        public bool AuthorshipForLanguageId { get; set; }
        public PersonOrderBy OrderBy { get; set; }
    }
}