﻿using MatterialCore.Enums;

using System;

namespace MatterialCore.Params.Person
{
    public sealed class GetPersonByIdParams : ParamBase
    {
        public GetPersonByIdParams() : base()
        {
        }

        internal ParameterCollection UrlParameter()
        {
            pc = new ParameterCollection();
            AddQueryParam(Clients, Const.PARAM_LOAD_CLIENTS);
            AddQueryParam(Addresses, Const.PARAM_LOAD_ADDRESSES);
            AddQueryParam(CommunicationData, Const.PARAM_LOAD_COMMUNICATION_DATA);
            AddQueryParam(Roles, Const.PARAM_LOAD_ROLES);
            AddQueryParam(ContactImages, Const.PARAM_LOAD_CONTACT_IMAGES);
            if (!(BioDocumentLanguageKey is null))
            {
                AddQueryParam(BioDocumentLanguageKey.KeyAndValue().Value, Const.PARAM_LOAD_BIO_DOCUMENT_LANGUAGE_KEY, Guid.NewGuid().ToString()); //Always
            }
            else
            {
                if (DebugUrlParameter)
                {
                    pc.Add(Const.PARAM_LOAD_BIO_DOCUMENT_LANGUAGE_KEY, "en");
                }
            }
            return pc;
        }

        public bool Clients { get; set; }
        public bool Addresses { get; set; }
        public bool CommunicationData { get; set; }
        public bool Roles { get; set; }
        public bool ContactImages { get; set; }
        public LangKey? BioDocumentLanguageKey { get; set; }
    }
}