﻿using MatterialCore.Enums;

namespace MatterialCore.Params
{
    public class TrackingSearchParams : ParamBase
    {
        public TrackingSearchParams()
        {
            Offset = 0;
            Limit = Matterial.DefaultSearchResultLimit;
        }

        internal virtual ParameterCollection UrlParameter()
        {
            pc = new ParameterCollection()
            {
                { Const.PARAM_OFFSET, Offset.ToString() },
                { Const.PARAM_LIMIT , Limit.ToString() }
            };

            AddQueryParam(OrderBy.KeyAndValue().Value, OrderBy.KeyAndValue().Key, TrackingOrderBy.Date.KeyAndValue().Value);
            AddQueryParam(OrderDir.KeyAndValue().Value, OrderDir.KeyAndValue().Key, OrderDirection.asc.KeyAndValue().Value);
            return pc;
        }

        public TrackingOrderBy OrderBy { get; set; }
        public OrderDirection OrderDir { get; set; }
        public int Offset { get; set; }
        public int Limit { get; set; }
    }
}