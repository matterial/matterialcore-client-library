﻿using MatterialCore.Enums;

namespace MatterialCore.Params
{
    public class CategoryParams : CategoryByTypeIdParams
    {
        public CategoryParams() : base()
        {
        }

        internal override ParameterCollection UrlParameter()
        {
            pc = new ParameterCollection(base.UrlParameter());

            AddQueryParam(Personal.KeyAndValue(Const.PARAM_PERSONAL).Value, Personal.KeyAndValue(Const.PARAM_PERSONAL).Key, ParamHideBoolValue.Include.KeyAndValue(Const.PARAM_PERSONAL).Value);
            AddQueryParam(CategoryId, Const.PARAM_CATEGORY_ID);
            AddQueryParam(CategoryTypeId, Const.PARAM_CATEGORY_TYPE_ID);

            return pc;
        }

        /// <summary>
        /// Only load current user's personal categories.
        /// </summary>
        public ParamHideBoolValue Personal { get; set; }

        public long? CategoryId { get; set; }
        public long? CategoryTypeId { get; set; }
    }
}