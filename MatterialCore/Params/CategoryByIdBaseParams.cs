﻿using MatterialCore.Enums;

namespace MatterialCore.Params
{
    public class CategoryByIdBaseParams : LoadLanguageParam
    {
        public CategoryByIdBaseParams()
        {
            LanguageKey = Matterial.DefaultLanguage;
        }

        public bool LoadUsage { get; set; }
        public LangKey? LoadFollowingLanguageKey { get; set; }

        internal override ParameterCollection UrlParameter()
        {
            pc = new ParameterCollection(base.UrlParameter());

            AddQueryParam(LoadUsage, Const.PARAM_LOAD_USAGE);

            if (!(LoadFollowingLanguageKey is null))
            {
                pc.Add(LoadFollowingLanguageKey.KeyAndValue(Const.PARAM_LOAD_FOLLOWING_LANGUAGE_KEY));
            }
            else
            {
                if (DebugUrlParameter)
                {
                    pc.Add(Const.PARAM_LOAD_FOLLOWING_LANGUAGE_KEY, Matterial.DefaultLanguage.KeyAndValue().Value);
                }
            }

            return pc;
        }
    }
}