﻿using MatterialCore.Enums;

namespace MatterialCore.Params
{
    public class CategoryByTypeIdParams : CategoryByIdBaseParams
    {
        public CategoryByTypeIdParams() : base()
        {
            LanguageKey = Matterial.DefaultLanguage;
            Offset = 0;
            Limit = Matterial.DefaultSearchResultLimit;
        }

        internal override ParameterCollection UrlParameter()
        {
            pc = new ParameterCollection(base.UrlParameter())
            {
                { Const.PARAM_OFFSET, Offset.ToString() },
                { Const.PARAM_LIMIT, Limit.ToString() }
            };

            AddQueryParam(Quick.KeyAndValue(Const.PARAM_QUICK).Value, Quick.KeyAndValue(Const.PARAM_QUICK).Key, ParamHideBoolValue.Include.KeyAndValue(Const.PARAM_QUICK).Value);
            AddQueryParam(OrderBy.KeyAndValue().Value, OrderBy.KeyAndValue().Key, CategoryOrderBy.Name.KeyAndValue().Value);
            AddQueryParam(OrderDir.KeyAndValue().Value, OrderDir.KeyAndValue().Key, OrderDirection.asc.KeyAndValue().Value);

            return pc;
        }

        /// <summary>
        /// Used for paging of search result. The number of document from which on the results
        /// should be returned.
        /// Default: 1
        /// </summary>
        public int Offset { get; set; }

        /// <summary>
        /// Number of documents to return in the search result.
        /// Default: 20
        /// </summary>
        public int Limit { get; set; }

        /// <summary>
        /// Wehret or not to show quick categories (tags).
        /// </summary>
        public ParamHideBoolValue Quick { get; set; }

        /// <summary>
        /// Order by. Default is name
        /// </summary>
        public CategoryOrderBy OrderBy { get; set; }

        /// <summary>
        /// Order direction. Default is ascending.
        /// </summary>
        public OrderDirection OrderDir { get; set; }
    }
}