﻿using MatterialCore.Enums;

namespace MatterialCore.Params
{
    public class ClientParams : ParamBase
    {
        public ClientParams()
        {
            Limit = Matterial.DefaultSearchResultLimit;
            Offset = 0;
        }

        internal virtual ParameterCollection UrlParameter()
        {
            pc = new ParameterCollection
            {
                { Const.PARAM_OFFSET, Offset.ToString() },
                { Const.PARAM_LIMIT, Limit.ToString() }
            };

            AddQueryParam(AccountId, Const.PARAM_ACCOUNT_ID);
            AddQueryParam(LoadClientPreferences, Const.PARAM_LOAD_CLIENT_PREFERENCES);
            AddQueryParam(OrderBy.KeyAndValue().Value, OrderBy.KeyAndValue().Key, ClientOrderBy.Name.KeyAndValue().Value);
            AddQueryParam(OrderDir.KeyAndValue().Value, OrderDir.KeyAndValue().Key, OrderDirection.asc.KeyAndValue().Value);
            return pc;
        }

        public long AccountId { get; set; }
        public bool LoadClientPreferences { get; set; }
        public ClientOrderBy OrderBy { get; set; }
        public OrderDirection OrderDir { get; set; }
        public int Limit { get; set; }
        public int Offset { get; set; }
    }
}