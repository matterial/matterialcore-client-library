﻿using MatterialCore.Enums;

using System.Collections.Generic;

namespace MatterialCore.Params
{
    public class TaskParams : ParamBase
    {
        public TaskParams()
        {
            StatusIds = new List<long>();
            StatusIdExcludes = new List<long>();
            Offset = 0;
            Limit = Matterial.DefaultSearchResultLimit;
        }

        internal virtual ParameterCollection UrlParameter()
        {
            pc = new ParameterCollection
            {
                { Const.PARAM_OFFSET, Offset.ToString() },
                { Const.PARAM_LIMIT, Limit.ToString() }
            };
            AddQueryParam(DocumentId, Const.PARAM_DOCUMENT_ID);
            AddQueryParam(DocumentLanguageVersionId, Const.PARAM_DOCUMENT_LANGUAGE_VERSION_ID);
            if (!(LanguageKey is null))
            {
                AddQueryParam(LanguageKey.KeyAndValue().Value, LanguageKey.KeyAndValue().Key, Matterial.DefaultLanguage.KeyAndValue().Value);
            }
            AddQueryParam(AuthorAccountId, Const.PARAM_AUTHOR_ACCOUNT_ID);
            AddQueryParam(AccountId, Const.PARAM_ACCOUNT_ID);
            AddQueryParam(RoleId, Const.PARAM_ROLE_ID);
            AddQueryParam(Snap, "Snap", ParamHideValue.Include);
            AddQueryParam(HideTasksOfRemovedDocuments, Const.PARAM_HIDE_TASKS_OF_REMOVED_DOCUMENTS);
            pc.AllowedDuplicateKeys.Add(Const.PARAM_STATUS_ID);
            if (StatusIds.Count == 0 && DebugUrlParameter)
            {
                AddQueryParam(1, Const.PARAM_STATUS_ID);
            }
            foreach (var item in StatusIds)
            {
                AddQueryParam(item, Const.PARAM_STATUS_ID);
            }
            if (StatusIdExcludes.Count == 0 && DebugUrlParameter)
            {
                AddQueryParam(1, Const.PARAM_STATUS_ID_EXCLUDE);
            }
            foreach (var item in StatusIdExcludes)
            {
                AddQueryParam(item, Const.PARAM_STATUS_ID_EXCLUDE);
            }
            AddQueryParam(OrderBy.KeyAndValue().Value, OrderBy.KeyAndValue().Key, TaskOrderBy.Date.KeyAndValue().Value);
            AddQueryParam(OrderDir.KeyAndValue().Value, OrderDir.KeyAndValue().Key, OrderDirection.asc.KeyAndValue().Value);
            AddQueryParam(Count, Const.PARAM_COUNT);

            return pc;
        }

        public long DocumentId { get; set; }
        public long DocumentLanguageVersionId { get; set; }
        public LangKey? LanguageKey { get; set; }
        public long AuthorAccountId { get; set; }
        public long AccountId { get; set; }
        public long RoleId { get; set; }
        public bool AssignedOnly { get; set; }
        public ParamHideValue Snap { get; set; }
        public bool HideTasksOfRemovedDocuments { get; set; }
        public List<long> StatusIds { get; set; }
        public List<long> StatusIdExcludes { get; set; }
        public TaskOrderBy OrderBy { get; set; }
        public OrderDirection OrderDir { get; set; }
        public bool Count { get; set; }
        public int Offset { get; set; }
        public int Limit { get; set; }
    }
}