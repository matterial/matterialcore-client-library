﻿using MatterialCore.Enums;

namespace MatterialCore.Params
{
    /// <summary>
    /// Base class of parameters for searching and loading of entities.
    /// </summary>

    public abstract class ParamBase //: IDebugUrlParams
    {
        protected ParameterCollection pc = new ParameterCollection();
        internal bool DebugUrlParameter { get; set; }

        protected void AddQueryParam(ParamShowValue value, string propertyName, ParamShowValue defaultValue = ParamShowValue.Exclude)
        {
            if (DebugUrlParameter || value != defaultValue)
            {
                pc.Add(value.KeyAndValue(propertyName));
            }
        }

        protected void AddQueryParam(CategoryAssignedValue value, CategoryAssignedValue defaultValue = CategoryAssignedValue.Any)
        {
            if (DebugUrlParameter || value != defaultValue)
            {
                pc.Add(value.KeyAndValue());
            }
        }

        protected void AddQueryParam(ParamHideValue value, string propertyName, ParamHideValue defaultValue = ParamHideValue.Include)
        {
            if (DebugUrlParameter || value != defaultValue)
            {
                pc.Add(value.KeyAndValue(propertyName));
            }
        }

        protected void AddQueryParam(ParamHideBoolValue value, string propertyName, ParamHideBoolValue defaultValue = ParamHideBoolValue.Include)
        {
            if (DebugUrlParameter || value != defaultValue)
            {
                pc.Add(value.KeyAndValue(propertyName));
            }
        }

        protected void AddQueryParam(bool value, string propertyName)
        {
            if (DebugUrlParameter || value)
            {
                pc.Add(propertyName, value.ToString().ToLower());
            }
        }

        protected void AddQueryParam(bool? value, string propertyName)
        {
            if (value is null)
            {
                if (DebugUrlParameter)
                {
                    pc.Add(propertyName, "false");
                }
            }
            else
            {
                pc.Add(propertyName, value.ToString().ToLower());
            }
        }

        protected void AddQueryParam(long value, string propertyName, long defaultValue = 0)
        {
            if (DebugUrlParameter || value != defaultValue)
            {
                pc.Add(propertyName, value.ToString());
            }
        }

        protected void AddQueryParam(long? value, string propertyName)
        {
            if (value is null)
            {
                if (DebugUrlParameter)
                {
                    pc.Add(propertyName, "0");
                }
            }
            else
            {
                if (DebugUrlParameter || value != 0)
                {
                    pc.Add(propertyName, value.ToString());
                }
            }
        }

        protected void AddQueryParam(string value, string propertyName, string defaultValue = "")
        {
            if (DebugUrlParameter || value != defaultValue)
            {
                pc.Add(propertyName, value);
            }
        }

        protected void AddQueryParam(string value, string propertyName, bool defaultValue = false)
        {
            if (bool.TryParse(value, out bool b) || DebugUrlParameter)
                {
                if (DebugUrlParameter || b != defaultValue)
                {
                    pc.Add(propertyName, value);
                }
            }
        }
        protected void AddQueryParam(string? value, string propertyName)
        {
            if (value is null)
            {
                if (DebugUrlParameter)
                    pc.Add(propertyName, "");
            }
            else
            {
                pc.Add(propertyName, value);
            }
        }
    }
}