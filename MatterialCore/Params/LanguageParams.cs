﻿using MatterialCore.Enums;

namespace MatterialCore.Params
{
    public class LanguageParams : ParamBase
    {
        public LanguageParams()
        {
        }

        internal virtual ParameterCollection UrlParameter()
        {
            pc = new ParameterCollection();
            AddQueryParam(Active, Const.PARAM_ACTIVE);
            AddQueryParam(OrderBy.KeyAndValue().Value, OrderBy.KeyAndValue().Key, LanguageOrderBy.Prio.KeyAndValue().Value);
            AddQueryParam(OrderDir.KeyAndValue().Value, OrderDir.KeyAndValue().Key, OrderDirection.asc.KeyAndValue().Value);
            return pc;
        }

        public bool? Active { get; set; }
        public LanguageOrderBy OrderBy { get; set; }
        public OrderDirection OrderDir { get; set; }
    }
}