﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;

namespace MatterialCore.Params
{
    public class ParameterCollection : IEnumerable<KeyValuePair<string, string>>
    {
        private readonly List<KeyValuePair<string, string>> pairs = new List<KeyValuePair<string, string>>();

        public List<string> AllowedDuplicateKeys { get; set; }

        public ParameterCollection()
        {
            AllowedDuplicateKeys = new List<string>();
        }

        public ParameterCollection(ParameterCollection source)
        {
            pairs = new List<KeyValuePair<string, string>>(source);
            AllowedDuplicateKeys = new List<string>();
        }

        private bool IsDuplicate(string key)
        {
            foreach (KeyValuePair<string, string> item in pairs)
            {
                if (item.Key == key)
                {
                    if (!AllowedDuplicateKeys.Contains(key))
                    { return true; }
                }
            }
            return false;
        }

        public void Add(string key, string val)
        {
            if (IsDuplicate(key))
            {
                if (Debugger.IsAttached)
                    Debugger.Break();
            }
            if (!String.IsNullOrEmpty(key))
            {
                pairs.Add(new KeyValuePair<string, string>(key, val));
            }
            else
            {
                if (Debugger.IsAttached)
                    Debugger.Break();
            }
        }

        public void Add(KeyValuePair<string, string> keyValuePair)
        {
            if (!String.IsNullOrEmpty(keyValuePair.Key))
            {
                if (IsDuplicate(keyValuePair.Key))
                {
                    if (Debugger.IsAttached)
                        Debugger.Break();
                }
                pairs.Add(new KeyValuePair<string, string>(keyValuePair.Key, keyValuePair.Value));
            }
            else
            {
                if (Debugger.IsAttached)
                    Debugger.Break();
            }
        }

        public void Copy(ParameterCollection source)
        {
            foreach (var item in source)
            {
                if (!String.IsNullOrEmpty(item.Key))
                {
                    if (IsDuplicate(item.Key))
                    {
                        if (Debugger.IsAttached)
                            Debugger.Break();
                    }
                    Add(item.Key, item.Value);
                }
                else
                {
                    if (Debugger.IsAttached)
                        Debugger.Break();
                }
            }
        }

        public IEnumerator<KeyValuePair<string, string>> GetEnumerator()
        {
            return pairs.GetEnumerator();
        }

        public override string ToString()
        {
            string query = "";
            using (FormUrlEncodedContent urlContent = new FormUrlEncodedContent(pairs))
            {
                query = urlContent.ReadAsStringAsync().Result;
            }
            return query;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return pairs.GetEnumerator();
        }
    }
}