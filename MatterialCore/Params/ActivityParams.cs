﻿using MatterialCore.Enums;

namespace MatterialCore.Params
{
    public class ActivityParams : LoadLanguageParam
    {
        public ActivityParams() : base()
        {
            Offset = 0;
            Limit = Matterial.DefaultSearchResultLimit;
            MessageType = ActivityMessageType.Any;
            NotificationStatus = NotificationStatuses.Status_Any;
        }

        internal override ParameterCollection UrlParameter()
        {
            pc = new ParameterCollection(base.UrlParameter())
            {
                { Const.PARAM_OFFSET, Offset.ToString() },
                { Const.PARAM_LIMIT , Limit.ToString() }
            };
            AddQueryParam(MessageType.KeyAndValue().Value, MessageType.KeyAndValue().Key, ActivityMessageType.Any.KeyAndValue().Value);
            AddQueryParam(PersonAccountId, Const.PARAM_PERSON_ACCOUNT_ID);
            AddQueryParam(DocumentId, Const.PARAM_DOCUMENT_ID);
            AddQueryParam(DocumentLanguageVersionId, Const.PARAM_DOCUMENT_LANGUAGE_VERSION_ID);
            AddQueryParam(DocumentLanguageVersionLanguageKey, Const.PARAM_DOCUMENT_LANGUAGE_VERSION_LANGUAGE_KEY);
            AddQueryParam(TaskId, Const.PARAM_TASK_ID);
            AddQueryParam(NotificationStatus.KeyAndValue().Value, NotificationStatus.KeyAndValue().Key, NotificationStatuses.Status_Any.KeyAndValue().Value);
            AddQueryParam(Count, Const.PARAM_COUNT);
            AddQueryParam(OrderBy.KeyAndValue().Value, OrderBy.KeyAndValue().Key, ActivityOrderBy.Date.KeyAndValue().Value);
            AddQueryParam(OrderDir.KeyAndValue().Value, OrderDir.KeyAndValue().Key, OrderDirection.asc.KeyAndValue().Value);
            return pc;
        }

        /// <summary>
        /// Filter by related account
        /// </summary>
        public long PersonAccountId { get; set; }

        /// <summary>
        /// Filter by related document
        /// </summary>
        public long DocumentId { get; set; }

        /// <summary>
        /// Filter by related version
        /// </summary>
        public long DocumentLanguageVersionId { get; set; }

        /// <summary>
        /// Filter by language key of the related document
        /// </summary>
        public string? DocumentLanguageVersionLanguageKey { get; set; }

        /// <summary>
        /// Filter by related task
        /// </summary>
        public long TaskId { get; set; }

        public NotificationStatuses NotificationStatus { get; set; }

        /// <summary>
        /// The message type
        /// </summary>
        public ActivityMessageType MessageType { get; set; }

        /// <summary>
        /// Additionally loading count of activities
        /// </summary>
        public bool Count { get; set; }

        public ActivityOrderBy OrderBy { get; set; }
        public OrderDirection OrderDir { get; set; }
        public int Offset { get; set; }
        public int Limit { get; set; }
    }
}