﻿using MatterialCore.Enums;

namespace MatterialCore.Params
{
    public class CategoryTypeParams : LoadLanguageParam
    {
        public CategoryTypeParams()
        {
            LanguageKey = Matterial.DefaultLanguage;
        }

        public CategoryTypeParams(long categoryTypeId)
        {
            CategoryTypeId = categoryTypeId;
        }

        internal override ParameterCollection UrlParameter()
        {
            pc = new ParameterCollection(base.UrlParameter());
            AddQueryParam(Personal.KeyAndValue(Const.PARAM_PERSONAL).Value, Personal.KeyAndValue(Const.PARAM_PERSONAL).Key, ParamHideBoolValue.Include.KeyAndValue(Const.PARAM_PERSONAL).Value);
            AddQueryParam(CategoryTypeId, Const.PARAM_CATEGORY_TYPE_ID);
            AddQueryParam(Quick.KeyAndValue(Const.PARAM_QUICK).Value, Quick.KeyAndValue(Const.PARAM_QUICK).Key, ParamHideBoolValue.Include.KeyAndValue(Const.PARAM_QUICK).Value);
            AddQueryParam(OrderBy.KeyAndValue().Value, OrderBy.KeyAndValue().Key, CategoryOrderBy.Name.KeyAndValue().Value);
            AddQueryParam(OrderDir.KeyAndValue().Value, OrderDir.KeyAndValue().Key, OrderDirection.asc.KeyAndValue().Value);

            return pc;
        }

        /// <summary>
        /// The type of category to load.
        /// </summary>
        public long? CategoryTypeId { get; set; }

        /// <summary>
        /// Only load current user's personal categories.
        /// </summary>
        public ParamHideBoolValue Personal { get; set; }

        /// <summary>
        /// Wehret or not to show quick categories (tags).
        /// </summary>
        public ParamHideBoolValue Quick { get; set; }

        /// <summary>
        /// Order by. Default is name
        /// </summary>
        public CategoryOrderBy OrderBy { get; set; }

        /// <summary>
        /// Order direction. Default is ascending.
        /// </summary>
        public OrderDirection OrderDir { get; set; }
    }
}