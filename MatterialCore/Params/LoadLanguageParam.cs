﻿using MatterialCore.Enums;

using System;

namespace MatterialCore.Params
{
    public abstract class LoadLanguageParam : ParamBase
    {
        public LangKey? LanguageKey { get; set; }

        internal virtual ParameterCollection UrlParameter()
        {
            pc = new ParameterCollection();
            if (!(LanguageKey is null))
            {
                AddQueryParam(LanguageKey.KeyAndValue().Value, LanguageKey.KeyAndValue().Key, Guid.NewGuid().ToString()); //Always if not null
            }
            else
            {
                if (DebugUrlParameter)
                {
                    pc.Add(Const.PARAM_LANGUAGE_KEY, "en");
                }
            }
            return pc;
        }
    }
}