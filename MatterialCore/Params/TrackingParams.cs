﻿namespace MatterialCore.Params
{
    public sealed class TrackingParams : TrackingSearchParams
    {
        public TrackingParams()
        {
            Offset = 0;
            Limit = Matterial.DefaultSearchResultLimit;
        }

        internal override ParameterCollection UrlParameter()
        {
            pc = new ParameterCollection(base.UrlParameter());
            AddQueryParam(Count, Const.PARAM_COUNT);
            return pc;
        }

        public bool Count { get; set; }
    }
}