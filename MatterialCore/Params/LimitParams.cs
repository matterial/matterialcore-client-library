﻿namespace MatterialCore.Params
{
    public abstract class LimitParams : ParamBase
    {
        public ParameterCollection UrlParameter()
        {
            pc = new ParameterCollection
            {
                { Const.PARAM_OFFSET, Offset.ToString() },
                { Const.PARAM_LIMIT, Limit.ToString() }
            };

            return pc;
        }

        /// <summary>
        /// Used for paging of search result. The number of document from which on the results
        /// should be returned.
        /// Default: 1
        /// </summary>
        public int Offset { get; set; }

        /// <summary>
        /// Number of documents to return in the search result.
        /// Default: 20
        /// </summary>
        public int Limit { get; set; }
    }
}