﻿using MatterialCore.Enums;

namespace MatterialCore.Params.Doc
{
    public sealed class SavedSearchLoadParams : ParamBase
    {
        public SavedSearchLoadParams()
        {
            OnDashboard = ParamHideBoolValue.Include;
            LanguageKey = Matterial.DefaultLanguage;
        }

        public SavedSearchLoadParams(LangKey languageKey)
        {
            LanguageKey = languageKey;
            OnDashboard = ParamHideBoolValue.Include;
        }

        /// <summary>
        /// Document languages that will be included into the search.
        /// Default: Show all languages
        /// </summary>
        public LangKey LanguageKey { get; set; }

        /// <summary>
        /// Show items that are displayed on the dashboard.
        /// </summary>
        public ParamHideBoolValue OnDashboard { get; set; }

        /// <summary>
        /// Show only personal saved searches.
        /// </summary>
        public ParamHideValue Personal { get; set; }

        /// <summary>
        /// Order by.
        /// </summary>
        public SavedSearchesOrderBy OrderBy { get; set; }

        /// <summary>
        /// Ortder direction.
        /// </summary>
        public OrderDirection OrderDir { get; set; }

        public ParameterCollection UrlParameter()
        {
            pc = new ParameterCollection
            {
                LanguageKey.KeyAndValue()
            };

            AddQueryParam(OnDashboard, Const.PARAM_ON_DASHBOARD);
            AddQueryParam(Personal, "Personal");
            AddQueryParam(OrderDir.KeyAndValue().Value, OrderDir.KeyAndValue().Key, OrderDirection.asc.KeyAndValue().Value);
            AddQueryParam(OrderBy.KeyAndValue().Value, OrderBy.KeyAndValue().Key, SavedSearchesOrderBy.Name.KeyAndValue().Value);
            return pc;
        }
    }
}