﻿namespace MatterialCore.Params.Doc
{
    /// <summary>
    /// Aggregations can be used with categories or languages.
    /// </summary>
    public sealed class SearchAggregationParams : ParamBase
    {
        public SearchAggregationParams()
        {
        }

        /// <summary>
        /// Whether to build category aggregations or not
        /// </summary>
        public bool Category { get; set; }

        /// <summary>
        /// Whether to build language aggregations or not
        /// </summary>
        public bool Language { get; set; }

        /// <summary>
        /// Whether to build last change aggregations or not
        /// </summary>
        public bool LastChange { get; set; }

        public ParameterCollection UrlParameter()
        {
            pc = new ParameterCollection();
            if (DebugUrlParameter || Language)
            {
                pc.Add(Const.PARAM_LANGUAGE_AGGREGATIONS, "true");
            }
            if (DebugUrlParameter || Category)
            {
                pc.Add(Const.PARAM_CATEGORY_AGGREGATIONS, "true");
            }
            if (DebugUrlParameter || LastChange)
            {
                pc.Add(Const.PARAM_LAST_CHANGE_AGGREGATIONS, "true");
            }

            return pc;
        }
    }
}