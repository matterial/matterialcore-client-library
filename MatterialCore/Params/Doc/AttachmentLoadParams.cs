﻿using MatterialCore.Enums;

namespace MatterialCore.Params.Doc
{
    public sealed class AttachmentLoadParams : ParamBase
    {
        public AttachmentLoadParams()
        {
            Format = AttachmentFormats.Original;
        }

        public AttachmentFormats Format { get; set; }

        /// <summary>
        /// The size of the attachment to generate. Only used in case thge attaachment is an image format.
        /// </summary>
        public LoadSize Size { get; set; }

        /// <summary>
        /// Force ContentDisposition-Header: attachment
        /// </summary>
        public bool ForceAttachment { get; set; }

        public ParameterCollection UrlParameter()
        {
            pc = new ParameterCollection();
            if (Format != AttachmentFormats.Pdf || DebugUrlParameter)
                AddQueryParam(Size.KeyAndValue().Value, Size.KeyAndValue().Key);
            AddQueryParam(Format.KeyAndValue().Value, Format.KeyAndValue().Key, false);           
            AddQueryParam(ForceAttachment, Const.PARAM_FORCE_ATTACHMENT);
            return pc;
        }
    }
}