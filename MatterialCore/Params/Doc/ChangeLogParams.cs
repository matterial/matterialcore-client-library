﻿using MatterialCore.Enums;

namespace MatterialCore.Params.Doc
{
    public sealed class ChangeLogParams : ChangeLogMapParams
    {
        public ChangeLogParams()
        {
            Offset = 0;
            Limit = Matterial.DefaultSearchResultLimit;
        }

        public ChangeLogParams(long documentId)
        {
            Offset = 0;
            Limit = Matterial.DefaultSearchResultLimit;
            DocumentId = documentId;
        }

        public ChangeLogParams(long documentId, long documentLanguageVersionId)
        {
            Offset = 0;
            Limit = Matterial.DefaultSearchResultLimit;
            DocumentId = documentId;
            DocumentLanguageVersionId = documentLanguageVersionId;
        }

        internal override ParameterCollection UrlParameter()
        {
            pc = new ParameterCollection(base.UrlParameter());
            AddQueryParam(OrderDir.KeyAndValue().Value, OrderDir.KeyAndValue().Key, OrderDirection.asc.KeyAndValue().Value);
            AddQueryParam(OrderBy.KeyAndValue().Value, OrderBy.KeyAndValue().Key, DocChangeLogOrderBy.Date.KeyAndValue().Value);
            return pc;
        }

        /// <summary>
        /// Order direction. Default is ascending.
        /// </summary>
        public OrderDirection OrderDir { get; set; }

        /// <summary>
        /// Order by. Default is date.
        /// </summary>
        public DocChangeLogOrderBy OrderBy { get; set; }
    }
}