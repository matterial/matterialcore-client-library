﻿using MatterialCore.Enums;

namespace MatterialCore.Params.Doc
{
    public class LoadDocTrashParams : DocFilterOptions  //, IPathParameter
    {
        public LoadDocTrashParams()
        {
            ExpiresInDays = -1;
            LoadOptions = new DocInfoLoadParams();
            Offset = 0;
            Limit = Matterial.DefaultSearchResultLimit;
            MimeTypeId = -1;
            OrderBy = DocLoadOrderBy.LanguageVersionTitle;
            LanguageKey = Matterial.DefaultLanguage;
            ReadStatus = ReadStatuses.NeverMind;
            AdditionalPropertyType = AdditionalPropertyLoadTypes.Undefined;
        }

        public LoadDocTrashParams(LangKey languageKey) : base()
        {
            ExpiresInDays = -1;
            LoadOptions = new DocInfoLoadParams();
            Offset = 0;
            Limit = Matterial.DefaultSearchResultLimit;
            MimeTypeId = -1;
            OrderBy = DocLoadOrderBy.LanguageVersionTitle;
            LanguageKey = languageKey;
            ReadStatus = ReadStatuses.NeverMind;
            AdditionalPropertyType = AdditionalPropertyLoadTypes.Undefined;
        }

        public virtual string PathParameter
        {
            get { return Const.TRASH; }
        }

        internal override ParameterCollection UrlParameter()
        {
            pc = new ParameterCollection(base.UrlParameter())
            {
                { Const.PARAM_OFFSET, Offset.ToString() },
                { Const.PARAM_LIMIT, Limit.ToString() }
            };

            AddQueryParam(AllLanguages, Const.PARAM_ALL_LANGUAGES);
            AddQueryParam(LanguagePrefer, Const.PARAM_LANGUAGE_PREFER);
            AddQueryParam(LanguageExclude, Const.PARAM_LANGUAGE_EXCLUDE);
            AddQueryParam(OrderDir.KeyAndValue().Value, OrderDir.KeyAndValue().Key, OrderDirection.asc.KeyAndValue().Value);
            AddQueryParam(OrderBy.KeyAndValue().Value, OrderBy.KeyAndValue().Key, DocLoadOrderBy.LanguageVersionTitle.KeyAndValue().Value);
            AddQueryParam(RelatedDocumentId, Const.PARAM_RELATED_DOCUMENT_ID);

            if (CategoryFollowing == false)
            {
                AddQueryParam(CategoryFollowerAccountId, Const.PARAM_CATEGORY_FOLLOWER_ACCOUNT_ID);
            }

            AddQueryParam(CategoryFollowing, Const.PARAM_CATEGORY_FOLLOWING);

            if (AdditionalPropertyFollowing == false)
            {
                AddQueryParam(AdditionalPropertyFollowerAccountId, Const.PARAM_ADDITIONAL_PROPERTY_FOLLOWER_ACCOUNT_ID);
            }

            AddQueryParam(AdditionalPropertyFollowing, Const.PARAM_ADDITIONAL_PROPERTY_FOLLOWING);
            if (ExpiresOn <= 0)
            {
                AddQueryParam(ExpiresInDays, Const.PARAM_DOCUMENT_EXPIRES_IN_DAYS, -1);
            }
            AddQueryParam(ExpiresOn, Const.PARAM_DOCUMENT_EXPIRES_ON);
            AddQueryParam(Expires, Const.PARAM_DOCUMENT_EXPIRES);
            AddQueryParam(HasArchivedBegin, Const.PARAM_DOCUMENT_HAS_ARCHIVED_BEGIN);
            AddQueryParam(MimeTypeId, Const.PARAM_MIME_TYPE_ID, -1);
            AddQueryParam(MimeType, Const.PARAM_MIME_TYPE);
            AddQueryParam(MentionedAccountIdInComment, Const.PARAM_MENTIONED_ACCOUNT_ID_IN_COMMENT);
            AddQueryParam(MentionedAccountIdInCommentUnread, Const.PARAM_MENTIONED_ACCOUNT_ID_IN_COMMENT_UNREAD);
            //Copy load options
            LoadOptions.DebugUrlParameter = DebugUrlParameter;
            pc.Copy(LoadOptions.UrlParameter());
            AddQueryParam(Count, Const.PARAM_COUNT);

            return pc;
        }

        /// <summary>
        /// Loading count of documents.
        /// </summary>
        public bool Count { get; set; }

        public DocInfoLoadParams LoadOptions { get; set; }

        /// <summary>
        /// Does only work with mentionedAccountIdInComment &gt; 0. If true, only documents having
        /// “unread” comments (which means “unread document” since comment creation, which means
        /// comment.createTime &gt; document.lastReadTime will be returned.
        /// </summary>
        public int MentionedAccountIdInCommentUnread { get; set; }

        /// <summary>
        /// Account-id of a mentioned account within a document-related comment.
        /// </summary>
        public int MentionedAccountIdInComment { get; set; }

        /// <summary>
        /// Only return documents having the given mime type, like text/html or text/markdown.
        /// </summary>
        public string? MimeType { get; set; }

        /// <summary>
        /// Only return documents having the id of a mime type.
        /// </summary>
        public int MimeTypeId { get; set; }

        /// <summary>
        /// Only return documents that have a “archived begin” set.
        /// </summary>
        public bool HasArchivedBegin { get; set; }

        /// <summary>
        /// Only return documents that have a “valid end” set.
        /// </summary>
        public bool Expires { get; set; }

        /// <summary>
        /// Documents valid end unix timestamp in seconds. Only values greater than zero will be considered.
        /// </summary>

        public long ExpiresOn { get; set; }

        /// <summary>
        /// Documents valid end is reached within the next count of days. Values below zero will be ignored.
        /// </summary>
        public int ExpiresInDays { get; set; }

        public bool AdditionalPropertyFollowing { get; set; }

        /// <summary>
        /// Documents, the given account id follows by additional property. This will be OR-related
        /// with direct following and following by category, if more than one are given.
        /// </summary>
        public int AdditionalPropertyFollowerAccountId { get; set; }

        /// <summary>
        /// Documents, the given account id follows by category. This will be OR-related with direct
        /// following and following by additional property, if more than one are given.
        /// </summary>
        public long CategoryFollowerAccountId { get; set; }

        /// <summary>
        /// If true, only documents are returned where currently logged-in account follows categories.
        /// </summary>
        public bool CategoryFollowing { get; set; }

        /// <summary>
        /// Documents related to this document-id.
        /// </summary>
        public int RelatedDocumentId { get; set; }

        /// <summary>
        /// Used for paging of search result. The number of document from which on the results
        /// should be returned.
        /// Default: 1
        /// </summary>
        public int Offset { get; set; }

        /// <summary>
        /// Number of documents to return in the search result.
        /// Default: 20
        /// </summary>
        public int Limit { get; set; }

        /// <summary>
        /// Order direction. Default=asc
        /// </summary>
        public OrderDirection OrderDir { get; set; }

        /// <summary>
        /// Order by.
        /// </summary>
        public DocLoadOrderBy OrderBy { get; set; }

        ///// <summary>
        ///// Documents, the given account id follows by additional property. This will be OR-related with direct following and
        ///// following by category, if more than one are given.
        ///// </summary>
        //public int AdditionalPropertyFollowerAccountId { get; set; }
        ///// <summary>
        ///// If true, only documents are returned where currently logged-in account follows categories.
        ///// </summary>
        //public bool CategoryFollowing { get; set; }
        ///// <summary>
        ///// Documents, the given account id follows by category. This will be OR-related with direct following and
        ///// following by additional property, if more than one are given.
        ///// </summary>
        //public int CategoryFollowerAccountId { get; set; }
        /// <summary>
        /// Exclude given language-key. Does only work with “disable rights”
        /// </summary>
        public bool LanguageExclude { get; set; }

        /// <summary>
        /// return doc in preffered language, otherwise any language. Does only work with “disable rights”.
        /// </summary>
        public bool LanguagePrefer { get; set; }

        /// <summary>
        /// Return all documents in all languages.
        /// </summary>
        public bool AllLanguages { get; set; }
    }
}