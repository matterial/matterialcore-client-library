﻿using MatterialCore.Enums;

namespace MatterialCore.Params.Doc
{
    public sealed class MainFileLoadParams : ParamBase
    {
        public MainFileLoadParams() : base()
        {
            LanguageKey = Matterial.DefaultLanguage;
        }

        public LoadMainFileFormats LoadFormat { get; set; }

        /// <summary>
        /// This parameter is only relevant if the mime type of the main file is text/html. If set
        /// to true, compound links will be resolved.
        /// </summary>
        public bool ResolveCompounds { get; set; }

        public LangKey LanguageKey { get; set; }

        internal ParameterCollection UrlParameter()
        {
            pc = new ParameterCollection();

            pc.Add(LanguageKey.KeyAndValue());

            if (DebugUrlParameter || LoadFormat != LoadMainFileFormats.AsIs)
            {
                pc.Add(LoadFormat.KeyAndValue());
            }

            AddQueryParam(ResolveCompounds, Const.PARAM_RESOLVE_COMPOUNDS);
            return pc;
        }
    }
}