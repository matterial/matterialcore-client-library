﻿using MatterialCore.Enums;

namespace MatterialCore.Params.Doc
{
    public sealed class DocAutocompleteParams : DocFilterOptions
    {
        public DocAutocompleteParams() : base()
        {
            AdditionalPropertyType = AdditionalPropertyLoadTypes.Undefined;
        }

        internal override ParameterCollection UrlParameter()
        {
            pc = new ParameterCollection(base.UrlParameter());

            AddQueryParam(Removed, "Removed");
            AddQueryParam(AllLanguages, Const.PARAM_ALL_LANGUAGES);
            return pc;
        }

        /// <summary>
        /// Deleted documents
        /// </summary>
        public ParamShowValue Removed { get; set; }

        /// <summary>
        /// Overwrites the language key to return all docs in all languages.
        /// </summary>
        public bool AllLanguages { get; set; }
    }
}