﻿using MatterialCore.Enums;

namespace MatterialCore.Params.Doc
{
    public sealed class PropertyLoadParams : PropertyLoadByIdParams
    {
        private void Init()
        {
            Offset = 0;
            Limit = Matterial.DefaultSearchResultLimit;
            PropertyType = AdditionalPropertyLoadTypes.Undefined;
            OrderBy = DocPropertyOrderBy.Id;
            OrderDir = OrderDirection.asc;
        }

        public PropertyLoadParams() : base()
        {
            Init();
            LanguageKey = Matterial.DefaultLanguage;
        }

        public PropertyLoadParams(AdditionalPropertyLoadTypes propertyType) : base()
        {
            Init();
            LanguageKey = Matterial.DefaultLanguage;
            PropertyType = propertyType;
        }

        public PropertyLoadParams(AdditionalPropertyLoadTypes propertyType, LangKey languageKey) : base()
        {
            Init();
            LanguageKey = languageKey;
            PropertyType = propertyType;
        }

        internal override ParameterCollection UrlParameter()
        {
            pc = new ParameterCollection(base.UrlParameter());
            AddQueryParam(OrderDir.KeyAndValue().Value, OrderDir.KeyAndValue().Key, OrderDirection.asc.KeyAndValue().Value);
            AddQueryParam(OrderBy.KeyAndValue().Value, OrderBy.KeyAndValue().Key, DocPropertyOrderBy.Id.KeyAndValue().Value);
            AddQueryParam(PropertyType.KeyAndValue().Value, PropertyType.KeyAndValue().Key, AdditionalPropertyLoadTypes.Undefined.KeyAndValue().Value);

            return pc;
        }

        /// <summary>
        /// The property type.
        /// </summary>
        public AdditionalPropertyLoadTypes PropertyType { get; set; }

        /// <summary>
        /// Order of the search results, Ascending or descending.
        /// </summary>
        public OrderDirection OrderDir { get; set; }

        /// <summary>
        /// Property of the additional property by which the result should be ordered by.
        /// </summary>
        public DocPropertyOrderBy OrderBy { get; set; }
    }
}