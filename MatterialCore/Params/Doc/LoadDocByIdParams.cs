﻿using MatterialCore.Entity;
using MatterialCore.Enums;

using System;

namespace MatterialCore.Params.Doc
{
    public sealed class LoadDocByIdParams : LoadLanguageParam
    {
        public LoadDocByIdParams() : base()
        {
            //Id = documentId;
            LanguageKey = Matterial.DefaultLanguage;
            LoadOptions = new DocInfoLoadParams();
        }

        public LoadDocByIdParams(Document doc) : base()
        {
            //Id = doc.Id;
            LanguageKey = Matterial.DefaultLanguage;
            LoadOptions = new DocInfoLoadParams();
            if (Enum.TryParse<LangKey>(doc.LanguageVersionLanguageKey, out LangKey k))
            {
                LanguageKey = k;
            }
        }

        internal override ParameterCollection UrlParameter()
        {
            pc = new ParameterCollection(base.UrlParameter());
            if (!(UpdateReadTime is null))
            {
                pc.Add(UpdateReadTime.KeyAndValue(Const.PARAM_UPDATE_READ_TIME));
            }
            else
            {
                if (DebugUrlParameter)
                {
                    pc.Add(Const.PARAM_UPDATE_READ_TIME, "en");
                }
            }

            AddQueryParam(LanguagePrefer, Const.PARAM_LANGUAGE_PREFER);
            AddQueryParam(LanguageExclude, Const.PARAM_LANGUAGE_EXCLUDE);
            LoadOptions.DebugUrlParameter = DebugUrlParameter;
            pc.Copy(LoadOptions.UrlParameter());

            return pc;
        }

        public DocInfoLoadParams LoadOptions { get; set; }

        //public long Id { get; set; }
        /// <summary>
        /// Return doc in preffered language, otherwise any language. Does only work with “disable rights”.
        /// </summary>
        public bool LanguagePrefer { get; set; }

        /// <summary>
        /// Exclude given language-key. Does only work with “disable rights”.
        /// </summary>
        public bool LanguageExclude { get; set; }

        /// <summary>
        /// The language key of the document to update the read time.
        /// </summary>
        public LangKey? UpdateReadTime { get; set; }

        //public string PathParameter
        //{
        //    get { return Id.ToString(); }
        //}
    }
}