﻿using MatterialCore.Enums;

namespace MatterialCore.Params.Doc
{
    public sealed class AttachmentsLoadParams : LoadLanguageParam
    {
        public AttachmentsLoadParams()
        {
            Offset = 0;
            Limit = Matterial.DefaultSearchResultLimit;
        }

        public AttachmentsLoadParams(long documentLanguageVersionId)
        {
            DocumentLanguageVersionId = documentLanguageVersionId;
            Offset = 0;
            Limit = Matterial.DefaultSearchResultLimit;
        }

        internal override ParameterCollection UrlParameter()
        {
            pc = new ParameterCollection(base.UrlParameter())
            {
                { Const.PARAM_OFFSET, Offset.ToString() },
                { Const.PARAM_LIMIT, Limit.ToString() }
            };

            if (DebugUrlParameter || OrderBy != AttachmentsOrderBy.Name)
            {
                pc.Add(OrderBy.KeyAndValue());
            }
            AddQueryParam(OrderDir.KeyAndValue().Value, Const.PARAM_ORDER_DIR, OrderDirection.asc.KeyAndValue().Value);

            AddQueryParam(DocumentLanguageVersionId, Const.PARAM_DOCUMENT_LANGUAGE_VERSION_ID);

            return pc;
        }

        /// <summary>
        /// Used for paging of search result. The number of document from which on the results
        /// should be returned.
        /// Default: 1
        /// </summary>
        public int Offset { get; set; }

        /// <summary>
        /// Number of documents to return in the search result.
        /// Default: 20
        /// </summary>
        public int Limit { get; set; }

        /// <summary>
        /// The language version id of the document
        /// </summary>
        public long? DocumentLanguageVersionId { get; set; }

        /// <summary>
        /// Ortder direction.
        /// </summary>
        public OrderDirection OrderDir { get; set; }

        /// <summary>
        /// Order by. Default is Name
        /// </summary>
        public AttachmentsOrderBy OrderBy { get; set; }
    }
}