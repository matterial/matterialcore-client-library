﻿using MatterialCore.Enums;

namespace MatterialCore.Params.Doc
{
    public abstract class MainFileLoadBaseParams : ParamBase
    {
        public LoadMainFileFormats LoadFormat { get; set; }

        /// <summary>
        /// This parameter is only relevant if the mime type of the main file is text/html. If set
        /// to true, compound links will be resolved.
        /// </summary>
        public bool ResolveCompounds { get; set; }

        internal virtual ParameterCollection UrlParameter()
        {
            pc = new ParameterCollection();

            if (DebugUrlParameter || LoadFormat != LoadMainFileFormats.AsIs)
            {
                pc.Add(LoadFormat.KeyAndValue());
            }

            AddQueryParam(ResolveCompounds, Const.PARAM_RESOLVE_COMPOUNDS);
            return pc;
        }
    }
}