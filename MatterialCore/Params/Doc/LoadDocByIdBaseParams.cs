﻿namespace MatterialCore.Params.Doc
{
    public abstract class LoadDocByIdBaseParams : LoadLanguageParam
    {
        internal override ParameterCollection UrlParameter()
        {
            pc = new ParameterCollection(base.UrlParameter());
            AddQueryParam(LoadRoleRights, Const.PARAM_LOAD_ROLE_RIGHTS);
            AddQueryParam(LoadCategoriesPublicOnly, Const.PARAM_LOAD_CATEGORIES_PUBLIC_ONLY);
            AddQueryParam(LoadCategories, Const.PARAM_LOAD_CATEGORIES);
            AddQueryParam(LoadAuthors, Const.PARAM_LOAD_AUTHORS);
            AddQueryParam(LoadLastAuthorOnly, Const.PARAM_LOAD_LAST_AUTHOR_ONLY);
            AddQueryParam(LoadFollowers, Const.PARAM_LOAD_FOLLOWERS);
            AddQueryParam(LoadMarkedAsHelpfulBy, Const.PARAM_LOAD_MARKED_AS_HELPFUL_BY);
            AddQueryParam(LoadAttachments, Const.PARAM_LOAD_ATTACHMENTS);
            AddQueryParam(LoadLanguageAttachments, Const.PARAM_LOAD_LANGUAGE_ATTACHMENTS);
            AddQueryParam(LoadDocumentAttachments, Const.PARAM_LOAD_DOCUMENT_ATTACHMENTS);
            AddQueryParam(LoadRelatedDocumentIds, Const.PARAM_LOAD_RELATED_DOCUMENT_IDS);
            AddQueryParam(LoadAdditionalProperties, Const.PARAM_LOAD_ADDITIONAL_PROPERTIES);
            AddQueryParam(LoadExtensionValues, Const.PARAM_LOAD_EXTENSION_VALUES);
            AddQueryParam(Count, Const.PARAM_COUNT);
            return pc;
        }

        /// <summary>
        /// Loading the count of documents.
        /// </summary>
        public bool Count { get; set; }

        /// <summary>
        /// The language key of the document to update the read time.
        /// </summary>
        public bool LoadRoleRights { get; set; }

        /// <summary>
        /// Load only public categories and ignore all user specific ones, like favourite, etc..
        /// </summary>
        public bool LoadCategoriesPublicOnly { get; set; }

        /// <summary>
        /// Load all categories.
        /// </summary>
        public bool LoadCategories { get; set; }

        public bool LoadAuthors { get; set; }
        public bool LoadLastAuthorOnly { get; set; }

        /// <summary>
        /// Load all folowers of the document.
        /// </summary>
        public bool LoadFollowers { get; set; }

        public bool LoadMarkedAsHelpfulBy { get; set; }

        /// <summary>
        /// Load attachments of the current document version and language
        /// </summary>
        public bool LoadAttachments { get; set; }

        /// <summary>
        /// Load attachments of all document versions of the current language
        /// </summary>
        public bool LoadLanguageAttachments { get; set; }

        /// <summary>
        /// Load attachments of all document versions and all languages.
        /// </summary>
        public bool LoadDocumentAttachments { get; set; }

        public bool LoadRelatedDocumentIds { get; set; }
        public bool LoadAdditionalProperties { get; set; }
        public bool LoadExtensionValues { get; set; }
    }
}