﻿using MatterialCore.Enums;

using System.Collections.Generic;

namespace MatterialCore.Params.Doc
{
    public abstract class DocFilterOptions : DocScopeParams
    {
        #region Public Members

        internal override ParameterCollection UrlParameter()
        {
            pc = new ParameterCollection(base.UrlParameter());
            pc.AllowedDuplicateKeys.Add(Const.PARAM_CATEGORY_IDS_AND);
            pc.AllowedDuplicateKeys.Add(Const.PARAM_CATEGORY_IDS_OR);

            if (!(CategoryAndIds is null))
            {
                foreach (var item in CategoryAndIds)
                {
                    pc.Add(Const.PARAM_CATEGORY_IDS_AND, item.ToString());
                }
            }
            else
            {
                if (DebugUrlParameter)
                {
                    pc.Add(Const.PARAM_CATEGORY_IDS_AND, "0");
                }
            }

            if (!(CategoryOrIds is null))
            {
                foreach (var item in CategoryOrIds)
                {
                    pc.Add(Const.PARAM_CATEGORY_IDS_OR, item.ToString());
                }
            }
            else
            {
                if (DebugUrlParameter)
                {
                    pc.Add(Const.PARAM_CATEGORY_IDS_OR, "0");
                }
            }

            if (!(RoleRightRoleId is null))
            {
                pc.Add(Const.PARAM_ROLE_RIGHT_ROLE_ID, RoleRightRoleId.ToString());
            }
            else
            {
                if (DebugUrlParameter)
                {
                    pc.Add(Const.PARAM_ROLE_RIGHT_ROLE_ID, "0");
                }
            }

            if (DebugUrlParameter || RoleRightType != RoleRightFilterTypes.ANY)
            {
                pc.Add(RoleRightType.KeyAndValue());
            }

            if (DebugUrlParameter || ReadStatus != ReadStatuses.NeverMind)
            {
                pc.Add(ReadStatus.KeyAndValue());
            }

            if (!(MarkedHelpfulByContactId is null))
            {
                pc.Add(Const.PARAM_MARKED_HELPFUL_BY_CONTACT_ID, MarkedHelpfulByContactId.ToString());
            }
            else
            {
                if (DebugUrlParameter)
                {
                    pc.Add(Const.PARAM_MARKED_HELPFUL_BY_CONTACT_ID, "0");
                }
            }

            AddQueryParam(Responsible, Const.PARAM_RESPONSIBLE);

            if (DebugUrlParameter || !Responsible)
            {
                if (!(ResponsibleContactId is null))
                {
                    pc.Add(Const.PARAM_RESPONSIBLE_CONTACT_ID, ResponsibleContactId.ToString());
                }
                else
                {
                    if (DebugUrlParameter)
                    {
                        pc.Add(Const.PARAM_RESPONSIBLE_CONTACT_ID, "0");
                    }
                }
            }

            AddQueryParam(Authorship, Const.PARAM_AUTHORSHIP);

            if (DebugUrlParameter || !Authorship)
            {
                if (!(AuthorshipAccountId is null))
                {
                    pc.Add(Const.PARAM_AUTHORSHIP_ACCOUNT_ID, AuthorshipAccountId.ToString());
                }
                else
                {
                    if (DebugUrlParameter)
                    {
                        pc.Add(Const.PARAM_AUTHORSHIP_ACCOUNT_ID, "0");
                    }
                }
            }

            if (!(AdditionalPropertyId is null))
            {
                pc.Add(Const.PARAM_ADDITIONAL_PROPERTY_ID, AdditionalPropertyId.ToString());
            }
            else
            {
                if (DebugUrlParameter)
                {
                    pc.Add(Const.PARAM_ADDITIONAL_PROPERTY_ID, "0");
                }
            }

            if (DebugUrlParameter || AdditionalPropertyType != AdditionalPropertyLoadTypes.Undefined)
            {
                pc.Add(Const.PARAM_ADDITIONAL_PROPERTY_TYPE, ((int)AdditionalPropertyType).ToString());
            }

            if (DebugUrlParameter || LastChangedSince > 0)
            {
                pc.Add(Const.PARAM_LAST_CHANGED_SINCE, LastChangedSince.ToString());
            }

            if (DebugUrlParameter || LastChangedSince < 1)
            {
                if (!(LastChangedSinceDays is null))
                {
                    pc.Add(Const.PARAM_LAST_CHANGED_SINCE_DAYS, LastChangedSinceDays.ToString());
                }
                else
                {
                    if (DebugUrlParameter)
                    {
                        pc.Add(Const.PARAM_LAST_CHANGED_SINCE_DAYS, "0");
                    }
                }
            }

            AddQueryParam(Following, Const.PARAM_FOLLOWING);

            if (DebugUrlParameter || !Following)
            {
                if (!(FollowerAccountId is null))
                {
                    pc.Add(Const.PARAM_FOLLOWER_ACCOUNT_ID, FollowerAccountId.ToString());
                }
                else
                {
                    if (DebugUrlParameter)
                    {
                        pc.Add(Const.PARAM_FOLLOWER_ACCOUNT_ID, "0");
                    }
                }
            }

            return pc;
        }

        /// <summary>
        /// The account id of the person following the document. This will be OR-related with
        /// categoryFollowerAccountId, if both are given. If the property Following is set to true,
        /// it will take precedence.
        /// </summary>
        public long? FollowerAccountId { get; set; }

        /// <summary>
        /// List of Catecories which the document must have and which will be AND-connected.
        /// </summary>
        public List<long>? CategoryAndIds { get; set; }

        /// <summary>
        /// List of Catecories which the document must have and which will be OR-connected. Does
        /// only work, if categoryIdsAnd is not set.
        /// </summary>
        public List<long>? CategoryOrIds { get; set; }

        /// <summary>
        /// Specifies the workgroup or personal role that the document must have.
        /// </summary>
        public long? RoleRightRoleId { get; set; }

        /// <summary>
        /// Type of a related roleRight (1 - READ, 2 EDIT). Should normally be used with roleRightRoleId.
        /// </summary>
        public RoleRightFilterTypes RoleRightType { get; set; }

        /// <summary>
        /// The read status of the document.
        /// Default: The read status is ignored (Nevermind).
        /// </summary>
        public ReadStatuses ReadStatus { get; set; }

        /// <summary>
        /// Only show documents that the current user follows
        /// Default: false
        /// </summary>
        public bool Following { get; set; }

        /// <summary>
        /// Person that marked the document-language-version as helpful
        /// </summary>
        public long? MarkedHelpfulByContactId { get; set; }

        /// <summary>
        /// The Contact id of the person that is responsible. If the property Responsible is set to
        /// true, it will take precedence.
        /// </summary>
        public long? ResponsibleContactId { get; set; }

        /// <summary>
        /// Only show documents that the current user is responsible for.
        /// Default: false
        /// </summary>
        public bool Responsible { get; set; }

        /// <summary>
        /// Last change withing days.
        /// </summary>
        public int? LastChangedSinceDays { get; set; }

        /// <summary>
        /// Last changed since timestamp in seconds, e.g. 1451904693
        /// </summary>
        public long LastChangedSince { get; set; }

        /// <summary>
        /// The account id of the author of the document. If the property Authorship is set to true,
        /// it will take precedence.
        /// </summary>
        public long? AuthorshipAccountId { get; set; }

        /// <summary>
        /// Only show documents that the current user is author of.
        /// Default: false
        /// </summary>
        public bool Authorship { get; set; }

        /// <summary>
        /// AdditionalProperty of the document.
        /// </summary>
        public long? AdditionalPropertyId { get; set; }

        /// <summary>
        /// The type of additional property. Only applies if AdditionalProperty is defined.
        /// </summary>
        public AdditionalPropertyLoadTypes AdditionalPropertyType { get; set; }

        #endregion Public Members
    }
}