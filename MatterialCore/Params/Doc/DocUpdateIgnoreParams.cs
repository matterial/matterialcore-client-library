﻿using System;

namespace MatterialCore.Params.Doc
{
    public class DocUpdateIgnoreParams : UpdateContextParams
    {
        public DocUpdateIgnoreParams() : base()
        {
        }

        internal override ParameterCollection UrlParameter()
        {
            pc = new ParameterCollection(base.UrlParameter());
            AddQueryParam(IgnoreCategories, Const.PARAM_IGNORE_CATEGORIES);
            AddQueryParam(IgnoreRelatedDocumentIds, Const.PARAM_IGNORE_RELATED_DOCUMENT_IDS);
            AddQueryParam(IgnoreExtensionValues, Const.PARAM_IGNORE_EXTENSION_VALUES);
            AddQueryParam(IgnoreAttachments, Const.PARAM_IGNORE_ATTACHMENTS);
            AddQueryParam(IgnoreRoleRights, Const.PARAM_IGNORE_ROLE_RIGHTS);
            AddQueryParam(IgnoreResponsibles, Const.PARAM_IGNORE_RESPONSIBLES);

            return pc;
        }

        public bool IgnoreCategories { get; set; }
        public bool IgnoreRelatedDocumentIds { get; set; }
        public bool IgnoreExtensionValues { get; set; }
        public bool IgnoreAttachments { get; set; }
        public bool IgnoreRoleRights { get; set; }
        public bool IgnoreResponsibles { get; set; }
    }
}