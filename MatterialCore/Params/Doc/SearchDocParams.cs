﻿using MatterialCore.Entity;
using MatterialCore.Enums;

using System.Collections.Generic;

namespace MatterialCore.Params.Doc
{
    public sealed class SearchDocParams : DocFilterOptions
    {
        public SearchDocParams() : base()
        {
            OrderBy = DocSearchOrderBy.None;
            OrderDir = OrderDirection.asc;
            HighlightFields = new HighlightFields();
            Offset = 0;
            Limit = Matterial.DefaultSearchResultLimit;
            InitIgnoreKeys();
            Aggregation = new SearchAggregationParams();
            Offset = 0;
            Limit = Matterial.DefaultSearchResultLimit;
            ReadStatus = ReadStatuses.NeverMind;
            AdditionalPropertyType = AdditionalPropertyLoadTypes.Undefined;
        }

        public SearchDocParams(LangKey languageKey)
        {
            OrderBy = DocSearchOrderBy.None;
            OrderDir = OrderDirection.asc;
            HighlightFields = new HighlightFields();
            Offset = 0;
            Limit = Matterial.DefaultSearchResultLimit;
            InitIgnoreKeys();
            Aggregation = new SearchAggregationParams();
            Offset = 0;
            Limit = Matterial.DefaultSearchResultLimit;
            LanguageKey = languageKey;
            ReadStatus = ReadStatuses.NeverMind;
            AdditionalPropertyType = AdditionalPropertyLoadTypes.Undefined;
        }

        private List<string> SavedSearchIgnoreKeys = new List<string>();

        private void InitIgnoreKeys()
        {
            SavedSearchIgnoreKeys = new List<string>
            {
                Const.PARAM_LANGUAGE_KEY,
                Const.PARAM_LOAD_ROLE_RIGHTS,
                Const.PARAM_LOAD_CATEGORIES_PUBLIC_ONLY,
                Const.PARAM_LOAD_CATEGORIES,
                Const.PARAM_LOAD_RESPONSIBLES,
                Const.PARAM_LOAD_AUTHORS,
                Const.PARAM_LOAD_LAST_AUTHOR_ONLY,
                Const.PARAM_LOAD_FOLLOWERS,
                Const.PARAM_LOAD_MARKED_AS_HELPFUL_BY,
                Const.PARAM_LOAD_ATTACHMENTS,
                Const.PARAM_LOAD_LANGUAGE_ATTACHMENTS,
                Const.PARAM_LOAD_DOCUMENT_ATTACHMENTS,
                Const.PARAM_LOAD_RELATED_DOCUMENT_IDS,
                Const.PARAM_LOAD_ADDITIONAL_PROPERTIES,
                Const.PARAM_LOAD_EXTENSION_VALUES,
                Const.PARAM_CATEGORY_AGGREGATIONS,
                Const.PARAM_LANGUAGE_AGGREGATIONS,
                Const.PARAM_LAST_CHANGE_AGGREGATIONS,
                Const.PARAM_HIGHLIGHT_FIELD,
                Const.PARAM_COUNT,
                Const.PARAM_OFFSET,
                Const.PARAM_LIMIT
            };
        }

        public void CopyToSavedSearch(SavedSearch savedSearch, string searchTerm)
        {
            savedSearch.Params = new List<SavedSearchParameter>();
            //Add search term
            SavedSearchParameter p = new SavedSearchParameter
            {
                Key = Const.PARAM_QUERY,
                Value = searchTerm
            };
            savedSearch.Params.Add(p);
            //Other parameters
            ParameterCollection para = UrlParameter();
            foreach (var item in para)
            {
                if (!SavedSearchIgnoreKeys.Contains(item.Key))
                {
                    p = new SavedSearchParameter
                    {
                        Key = item.Key,
                        Value = item.Value
                    };
                    savedSearch.Params.Add(p);
                }
            }
        }

        internal override ParameterCollection UrlParameter()
        {
            pc = new ParameterCollection(base.UrlParameter())
            {
                { Const.PARAM_OFFSET, Offset.ToString() },
                { Const.PARAM_LIMIT, Limit.ToString() }
            };
            AddQueryParam(OrderDir.KeyAndValue().Value, OrderDir.KeyAndValue().Key, OrderDirection.asc.KeyAndValue().Value);
            AddQueryParam(OrderBy.KeyAndValue().Value, OrderBy.KeyAndValue().Key, DocSearchOrderBy.None.KeyAndValue().Value);

            pc.AllowedDuplicateKeys.Add(Const.PARAM_HIGHLIGHT_FIELD);
            HighlightFields.DebugUrlParameter = DebugUrlParameter;
            pc.Copy(HighlightFields.UrlParameter());

            AddQueryParam(Removed, "Removed");
            AddQueryParam(AllLanguages, Const.PARAM_ALL_LANGUAGES);
            Aggregation.DebugUrlParameter = DebugUrlParameter;
            pc.Copy(Aggregation.UrlParameter());
            return pc;
        }

        /// <summary>
        /// Used for paging of search result. The number of document from which on the results
        /// should be returned.
        /// Default: 1
        /// </summary>
        public int Offset { get; set; }

        /// <summary>
        /// Number of documents to return in the search result. The default is taken from MatterialCore.DefaultSearchResultLimit
        /// </summary>
        public int Limit { get; set; }

        /// <summary>
        /// Property of the document by which the search result should be ordered.
        /// Default: Auto-sorting of the search engine.
        /// </summary>
        public DocSearchOrderBy OrderBy { get; set; }

        /// <summary>
        /// Order of the search results, Ascending or descending.
        /// Default: asc
        /// </summary>
        public OrderDirection OrderDir { get; set; }

        /// <summary>
        /// Field(s) in which to apply highlighting. This returns html tags so a search result text
        /// can be highlighted in a browser view.
        /// </summary>
        public HighlightFields HighlightFields { get; set; }

        /// <summary>
        /// Applies an aggregation of the result.
        /// </summary>
        public SearchAggregationParams Aggregation { get; internal set; }

        /// <summary>
        /// Deleted documents
        /// </summary>
        public ParamShowValue Removed { get; set; }

        /// <summary>
        /// Overwrites the language key to return all docs in all languages.
        /// </summary>
        public bool AllLanguages { get; set; }
    }
}