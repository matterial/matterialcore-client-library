﻿namespace MatterialCore.Params.Doc
{
    public sealed class DocUpdateParams : UpdateTemplateParams
    {
        public DocUpdateParams()
        {
        }

        public long? ValidBeginInSeconds { get; set; }

        internal override ParameterCollection UrlParameter()
        {
            pc = new ParameterCollection(base.UrlParameter());

            if (!(ValidBeginInSeconds is null))
            {
                pc.Add(Const.PARAM_VALID_BEGIN_IN_SECONDS_REQUEST, ValidBeginInSeconds.ToString());
            }
            else
            {
                if (DebugUrlParameter)
                {
                    pc.Add(Const.PARAM_VALID_BEGIN_IN_SECONDS_REQUEST, "0");
                }
            }

            return pc;
        }
    }
}