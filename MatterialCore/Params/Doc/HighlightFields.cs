﻿using System.Collections.Generic;

namespace MatterialCore.Params.Doc
{
    public sealed class HighlightFields : List<string>
    {
        public HighlightFields() : base()
        {
        }

        public HighlightFields(IEnumerable<string> collection) : base(collection)
        {
        }

        public HighlightFields(int capacity) : base(capacity)
        {
        }

        public bool DebugUrlParameter { get; set; }

        public ParameterCollection UrlParameter()
        {
            ParameterCollection pc = new ParameterCollection();
            pc.AllowedDuplicateKeys.Add("highlightField");

            if (DebugUrlParameter && this.Count == 0)
            {
                pc.Add(Const.PARAM_HIGHLIGHT_FIELD, "field");
            }
            foreach (string item in this)
            {
                if (!(item is null))
                {
                    pc.Add(Const.PARAM_HIGHLIGHT_FIELD, item.ToString());
                }
            }
            return pc;
        }
    }
}