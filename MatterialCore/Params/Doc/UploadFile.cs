﻿using MatterialCore.Enums;

namespace MatterialCore.Params.Doc
{
    public sealed class UploadFile
    {
        public UploadFile()
        {
            FileType = TempFileType.Attachment;
        }

        public UploadFile(string filePath, TempFileType fileType)
        {
            FilePath = filePath;
            FileType = fileType;
        }

        public UploadFile(string filePath, string name, TempFileType fileType)
        {
            FilePath = filePath;
            Name = name;
            FileType = fileType;
        }

        public string? FilePath { get; set; }
        public string? Name { get; set; }
        public TempFileType FileType { get; set; }
    }
}