﻿namespace MatterialCore.Params.Doc
{
    public class UpdateLockParams : DocUpdateIgnoreParams
    {
        public UpdateLockParams() : base()
        {
        }

        internal override ParameterCollection UrlParameter()
        {
            pc = new ParameterCollection(base.UrlParameter());

            if (!(UniqueLockId is null))
            {
                pc.Add(Const.PARAM_UNIQUE_LOCK_ID, UniqueLockId.ToString());
            }
            else
            {
                if (DebugUrlParameter)
                {
                    pc.Add(Const.PARAM_UNIQUE_LOCK_ID, "xxxx");
                }
            }
            return pc;
        }

        /// <summary>
        /// A typical client should create a “unique lock id” for each document editor. This lockId
        /// should be reused each time updating or unlocking the document. If the client does not
        /// create such a lockId, it is not possible to further edit the document.
        /// </summary>
        public string? UniqueLockId { get; set; }
    }
}