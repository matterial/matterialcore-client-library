﻿using MatterialCore.Enums;

namespace MatterialCore.Params.Doc
{
    public sealed class DocInfoLoadParams : ParamBase
    {
        public DocInfoLoadParams()
        {
            AttachmentOrderBy = AttachmentsOrderBy.Name;
        }

        public ParameterCollection UrlParameter()
        {
            pc = new ParameterCollection();

            AddQueryParam(RoleRights, Const.PARAM_LOAD_ROLE_RIGHTS);
            AddQueryParam(CategoriesPublicOnly, Const.PARAM_LOAD_CATEGORIES_PUBLIC_ONLY);
            AddQueryParam(Categories, Const.PARAM_LOAD_CATEGORIES);
            AddQueryParam(Responsibles, Const.PARAM_LOAD_RESPONSIBLES);
            AddQueryParam(Authors, Const.PARAM_LOAD_AUTHORS);
            AddQueryParam(LastAuthorOnly, Const.PARAM_LOAD_LAST_AUTHOR_ONLY);
            AddQueryParam(Followers, Const.PARAM_LOAD_FOLLOWERS);
            AddQueryParam(AmIFollowing, Const.PARAM_LOAD_AM_I_FOLLOWING);
            AddQueryParam(MarkedAsHelpfulBy, Const.PARAM_LOAD_MARKED_AS_HELPFUL_BY);
            AddQueryParam(Attachments, Const.PARAM_LOAD_ATTACHMENTS);
            AddQueryParam(LanguageAttachments, Const.PARAM_LOAD_LANGUAGE_ATTACHMENTS);
            AddQueryParam(DocumentAttachments, Const.PARAM_LOAD_DOCUMENT_ATTACHMENTS);
            AddQueryParam(RelatedDocumentIds, Const.PARAM_LOAD_RELATED_DOCUMENT_IDS);
            AddQueryParam(AdditionalProperties, Const.PARAM_LOAD_ADDITIONAL_PROPERTIES);
            AddQueryParam(ExtensionValues, Const.PARAM_LOAD_EXTENSION_VALUES);
            AddQueryParam(SnapFlag, Const.PARAM_LOAD_SNAP_FLAG);
            AddQueryParam(Comments, Const.PARAM_LOAD_COMMENTS);

            if (DebugUrlParameter || AttachmentOrderBy != AttachmentsOrderBy.Name)
            {
                pc.Add(AttachmentOrderBy.KeyAndValue());
            }
            if (DebugUrlParameter || AttachmentOrderDir != OrderDirection.asc)
            {
                pc.Add(AttachmentOrderDir.KeyAndValue(Const.PARAM_ATTACHMENT_ORDER_DIR));
            }
            return pc;
        }

        /// <summary>
        /// Ortder direction of attachments in case ther are loaded.
        /// Default: asc
        /// </summary>
        public OrderDirection AttachmentOrderDir { get; set; }

        /// <summary>
        /// Order by of attachments in case ther are loaded.
        /// Default: Name
        /// </summary>
        public AttachmentsOrderBy AttachmentOrderBy { get; set; }

        public bool AmIFollowing { get; set; }
        public bool Comments { get; set; }
        public bool SnapFlag { get; set; }
        public bool ExtensionValues { get; set; }
        public bool AdditionalProperties { get; set; }
        public bool RelatedDocumentIds { get; set; }
        /// <summary>
        /// Load all attachments related to the whole document (all versions of a document). 
        /// This means you will also load outdated attachments or attachments of other languages.
        /// </summary>
        public bool DocumentAttachments { get; set; }
        /// <summary>
        /// Load all attachments related to the language (all versions of a document with the same language). 
        /// This means you will also load outdated attachments.
        /// </summary>
        public bool LanguageAttachments { get; set; }
        /// <summary>
        /// Load all attachments related to the current version.
        /// </summary>
        public bool Attachments { get; set; }
        public bool MarkedAsHelpfulBy { get; set; }
        public bool Followers { get; set; }
        public bool LastAuthorOnly { get; set; }
        public bool Authors { get; set; }
        public bool Responsibles { get; set; }
        public bool Categories { get; set; }
        public bool CategoriesPublicOnly { get; set; }
        public bool RoleRights { get; set; }
    }
}