﻿using MatterialCore.Enums;

namespace MatterialCore.Params.Doc
{
    public class PropertyLoadByIdParams : LoadLanguageParam
    {
        public PropertyLoadByIdParams() : base()
        {
            LanguageKey = Matterial.DefaultLanguage;
            Offset = 0;
            Limit = Matterial.DefaultSearchResultLimit;
        }

        public PropertyLoadByIdParams(LangKey languageKey) : base()
        {
            LanguageKey = languageKey;
            Offset = 0;
            Limit = Matterial.DefaultSearchResultLimit;
        }

        internal override ParameterCollection UrlParameter()
        {
            pc = new ParameterCollection(base.UrlParameter())
            {
                { Const.PARAM_OFFSET, Offset.ToString() },
                { Const.PARAM_LIMIT, Limit.ToString() }
            };
            if (!(LoadFollowingLanguageKey is null))
            {
                pc.Add(LoadFollowingLanguageKey.KeyAndValue(Const.PARAM_LOAD_FOLLOWING_LANGUAGE_KEY));
            }
            else
            {
                if (DebugUrlParameter)
                {
                    pc.Add(Const.PARAM_LOAD_FOLLOWING_LANGUAGE_KEY, "en");
                }
            }
            return pc;
        }

        /// <summary>
        /// Used for paging of search result. The number of document from which on the results
        /// should be returned.
        /// Default: 1
        /// </summary>
        public int Offset { get; set; }

        /// <summary>
        /// Number of documents to return in the search result.
        /// Default: 20
        /// </summary>
        public int Limit { get; set; }

        /// <summary>
        /// Requesting a language key will fill AdditionalProperty.following for current account
        /// </summary>
        public LangKey? LoadFollowingLanguageKey { get; set; }
    }
}