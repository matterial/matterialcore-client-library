﻿namespace MatterialCore.Params.Doc
{
    public class UpdateContextParams : ParamBase
    {
        public UpdateContextParams() : base()
        {
        }

        /// <summary>
        /// This token is used to identify all temporarily uploaded files (main-file, attachments).
        /// </summary>
        public string? ContextToken { get; set; }

        internal virtual ParameterCollection UrlParameter()
        {
            pc = new ParameterCollection();
            if (!(ContextToken is null))
            {
                pc.Add(Const.PARAM_CONTEXT_TOKEN, ContextToken);
            }
            else
            {
                if (DebugUrlParameter)
                {
                    pc.Add(Const.PARAM_CONTEXT_TOKEN, "xxxx");
                }
            }
            return pc;
        }
    }
}