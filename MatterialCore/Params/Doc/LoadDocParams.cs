﻿using MatterialCore.Enums;

using System.Collections.Generic;

namespace MatterialCore.Params.Doc
{
    public sealed class LoadDocParams : LoadDocTrashParams  //, IPathParameter
    {
        public LoadDocParams() : base()
        {
            DocumentIds = new List<long>();
            LanguageVersionIds = new List<long>();
            ExpiresInDays = -1;
            Offset = 0;
            Limit = Matterial.DefaultSearchResultLimit;
            MimeTypeId = -1;
            OrderBy = DocLoadOrderBy.LanguageVersionTitle;
        }

        internal override ParameterCollection UrlParameter()
        {
            pc = new ParameterCollection(base.UrlParameter());
            pc.AllowedDuplicateKeys.Add(Const.PARAM_DOCUMENT_ID);
            pc.AllowedDuplicateKeys.Add(Const.PARAM_DOCUMENT_LANGUAGE_VERSION_ID);

            if (DebugUrlParameter)
            {
                if (DocumentIds.Count == 0)
                {
                    pc.Add(Const.PARAM_DOCUMENT_ID, "0");
                }
                if (LanguageVersionIds.Count == 0)
                {
                    pc.Add(Const.PARAM_DOCUMENT_LANGUAGE_VERSION_ID, "0");
                }
            }

            foreach (var item in DocumentIds)
            {
                pc.Add(Const.PARAM_DOCUMENT_ID, item.ToString());
            }
            foreach (var item in LanguageVersionIds)
            {
                pc.Add(Const.PARAM_DOCUMENT_LANGUAGE_VERSION_ID, item.ToString());
            }

            AddQueryParam(AllVersions, Const.PARAM_ALL_VERSIONS);
            AddQueryParam(ClipBoard, Const.PARAM_CLIP_BOARD);
            AddQueryParam(UpdateReadTime, Const.PARAM_UPDATE_READ_TIME);
            AddQueryParam(Removed, "Removed");
            return pc;
        }

        public bool UpdateReadTime { get; set; }

        /// <summary>
        /// True to get documents from session-clipboard.
        /// </summary>
        public bool ClipBoard { get; set; }

        /// <summary>
        /// Does only work with mentionedAccountIdInComment &gt; 0. If true, only documents having
        /// “unread” comments (which means “unread document” since comment creation, which means
        /// comment.createTime &gt; document.lastReadTime will be returned.
        /// </summary>
        public bool AllVersions { get; set; }

        /// <summary>
        /// List of document ids to load.
        /// </summary>
        public List<long> DocumentIds { get; set; }

        /// <summary>
        /// Document-language-version-id(s).
        /// </summary>
        public List<long> LanguageVersionIds { get; set; }

        /// <summary>
        /// Deleted documents
        /// </summary>
        public ParamShowValue Removed { get; set; }

        public override string PathParameter
        {
            get { return string.Empty; }
        }
    }
}