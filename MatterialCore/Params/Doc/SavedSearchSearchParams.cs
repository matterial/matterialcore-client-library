﻿namespace MatterialCore.Params.Doc
{
    public sealed class SavedSearchSearchParams : LoadLanguageParam
    {
        public SavedSearchSearchParams()
        {
            HighlightFields = new HighlightFields();
            Aggregation = new SearchAggregationParams();
            LoadOptions = new DocInfoLoadParams();
            LanguageKey = Matterial.DefaultLanguage;
            Offset = 0;
            Limit = Matterial.DefaultSearchResultLimit; ;
        }

        public SavedSearchSearchParams(int offset, int limit)
        {
            HighlightFields = new HighlightFields();
            Offset = offset;
            Limit = limit;
            Aggregation = new SearchAggregationParams();
            LoadOptions = new DocInfoLoadParams();
            LanguageKey = Matterial.DefaultLanguage;
        }

        internal override ParameterCollection UrlParameter()
        {
            pc = new ParameterCollection(base.UrlParameter())
            {
                { Const.PARAM_OFFSET, Offset.ToString() },
                { Const.PARAM_LIMIT, Limit.ToString() }
            };

            pc.AllowedDuplicateKeys.Add(Const.PARAM_HIGHLIGHT_FIELD);
            LoadOptions.DebugUrlParameter = DebugUrlParameter;
            pc.Copy(LoadOptions.UrlParameter());
            HighlightFields.DebugUrlParameter = DebugUrlParameter;
            pc.Copy(HighlightFields.UrlParameter());
            Aggregation.DebugUrlParameter = DebugUrlParameter;
            pc.Copy(Aggregation.UrlParameter());

            AddQueryParam(Count, Const.PARAM_COUNT);

            return pc;
        }

        /// <summary>
        /// If true, the number of change log entries will be counted.
        /// </summary>
        public bool Count { get; set; }

        /// <summary>
        /// Used for paging of search result. The number of document from which on the results
        /// should be returned.
        /// Default: 1
        /// </summary>
        public int Offset { get; set; }

        /// <summary>
        /// Number of documents to return in the search result.
        /// Default: 20
        /// </summary>
        public int Limit { get; set; }

        public DocInfoLoadParams LoadOptions { get; set; }

        public SearchAggregationParams Aggregation { get; internal set; }

        /// <summary>
        /// Field(s) in which to apply highlighting. This returns html tags so a search result text
        /// can be highlighted in a browser view.
        /// </summary>
        public HighlightFields HighlightFields { get; set; }
    }
}