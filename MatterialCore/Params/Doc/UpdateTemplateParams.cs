﻿using MatterialCore.Enums;

namespace MatterialCore.Params.Doc
{
    public class UpdateTemplateParams : UpdateLockParams
    {
        public UpdateTemplateParams()
        {
        }

        public PublishRequestTypes PublishRequestType { get; set; }

        internal override ParameterCollection UrlParameter()
        {
            pc = new ParameterCollection(base.UrlParameter());

            if (DebugUrlParameter || PublishRequestType != PublishRequestTypes.None)
            {
                pc.Add(PublishRequestType.KeyAndValue());
            }
            return pc;
        }
    }
}