﻿using MatterialCore.Enums;

namespace MatterialCore.Params.Doc
{
    public class DocScopeParams : LoadLanguageParam
    {
        public DocScopeParams()
        {
        }

        internal override ParameterCollection UrlParameter()
        {
            pc = new ParameterCollection(base.UrlParameter());
            AddQueryParam(Bios, "Bios");
            AddQueryParam(Invalid, "Invalid");
            AddQueryParam(Snaps, "Snap");
            AddQueryParam(Templates, "Templates");
            AddQueryParam(Archived, "Archived");
            AddQueryParam(Reviewed, "LanguageVersionReviewed");
            AddQueryParam(Published, "LanguageVersionReady");
            AddQueryParam(InProcessing, "LanguageVersionCurrentlyInProcessing");
            AddQueryParam(ReviewRequested, "LanguageVersionReviewRequested");
            AddQueryParam(Landscape, "Landscape");
            AddQueryParam(HasCategoryAssigned, CategoryAssignedValue.Any);
            
            return pc;
        }

        /// <summary>
        /// Documents in landscape mode.
        /// Default: Show landscape documents
        /// </summary>
        public ParamHideValue Landscape { get; set; }

        /// <summary>
        /// Bio-documents
        /// Default: Do not show Bio-documents
        /// </summary>
        public ParamShowValue Bios { get; set; }

        /// <summary>
        /// Snap-documents (flash)
        /// Default: Show snap-documents.
        /// </summary>
        public ParamHideValue Snaps { get; set; }

        /// <summary>
        /// Templates.
        /// Default: Do not show templates.
        /// </summary>
        public ParamShowValue Templates { get; set; }

        /// <summary>
        /// Archived documents
        /// Default: Do not show archived.
        /// </summary>
        public ParamShowValue Archived { get; set; }

        /// <summary>
        /// Invalid documents.
        /// Default: Do not show invalid.
        /// </summary>
        public ParamShowValue Invalid { get; set; }

        /// <summary>
        /// Reviewed documents.
        /// Default: Show reviewed
        /// </summary>
        public ParamHideValue Reviewed { get; set; }

        /// <summary>
        /// Published documents (drafts are not returned).
        /// Default: Show published.
        /// </summary>
        public ParamHideValue Published { get; set; }

        /// <summary>
        /// Documents that are currently in processing.
        /// Default: Show in processing.
        /// </summary>
        public ParamHideValue InProcessing { get; set; }

        /// <summary>
        /// Documents where the review is requested.
        /// Default: Show review requested documents.
        /// </summary>
        public ParamHideValue ReviewRequested { get; set; }

        /// <summary>
        /// Documents that have categories assigned
        /// </summary>
        public CategoryAssignedValue HasCategoryAssigned { get; set; }

        ///// <summary>
        ///// Deleted documents
        ///// </summary>
        //public ParamShowValue Removed { get; set; }
    }
}