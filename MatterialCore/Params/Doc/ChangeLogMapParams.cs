﻿namespace MatterialCore.Params.Doc
{
    public class ChangeLogMapParams : ParamBase
    {
        public ChangeLogMapParams()
        {
            Offset = 0;
            Limit = Matterial.DefaultSearchResultLimit;
        }

        public ChangeLogMapParams(long documentId)
        {
            Offset = 0;
            Limit = Matterial.DefaultSearchResultLimit;
            DocumentId = documentId;
        }

        public ChangeLogMapParams(long documentId, long documentLanguageVersionId)
        {
            Offset = 0;
            Limit = Matterial.DefaultSearchResultLimit;
            DocumentId = documentId;
            DocumentLanguageVersionId = documentLanguageVersionId;
        }

        internal virtual ParameterCollection UrlParameter()
        {
            pc = new ParameterCollection()
            {
                { Const.PARAM_OFFSET, Offset.ToString() },
                { Const.PARAM_LIMIT, Limit.ToString() }
            };

            AddQueryParam(DocumentId, Const.PARAM_DOCUMENT_ID);
            AddQueryParam(DocumentLanguageVersionId, Const.PARAM_DOCUMENT_LANGUAGE_VERSION_ID);
            AddQueryParam(ContactId, Const.PARAM_CONTACT_ID);
            AddQueryParam(Count, Const.PARAM_COUNT);

            return pc;
        }

        /// <summary>
        /// Used for paging of search result. The number of document from which on the results
        /// should be returned.
        /// Default: 1
        /// </summary>
        public int Offset { get; set; }

        /// <summary>
        /// Number of documents to return in the search result.
        /// Default: 20
        /// </summary>
        public int Limit { get; set; }

        public long DocumentId { get; set; }
        public long DocumentLanguageVersionId { get; set; }
        public long ContactId { get; set; }

        /// <summary>
        /// If true, the number of change log entries will be counted.
        /// </summary>
        public bool Count { get; set; }
    }
}