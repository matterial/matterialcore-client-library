﻿using MatterialCore.Enums;

using System.Collections.Generic;

namespace MatterialCore.Params
{
    public class RoleParams : ParamBase
    {
        public RoleParams()
        {
            EntityTypeIds = new List<RoleEntityType>();
            Limit = Matterial.DefaultSearchResultLimit;
        }

        internal virtual ParameterCollection UrlParameter()
        {
            pc = new ParameterCollection
            {
                { Const.PARAM_OFFSET, Offset.ToString() },
                { Const.PARAM_LIMIT, Limit.ToString() }
            };
            AddQueryParam(AccountId, Const.PARAM_ACCOUNT_ID);

            pc.AllowedDuplicateKeys.Add(Const.PARAM_ENTITY_TYPE_ID);
            if (DebugUrlParameter && EntityTypeIds.Count == 0)
            {
                AddQueryParam(-1, Const.PARAM_ENTITY_TYPE_ID);
            }
            foreach (var item in EntityTypeIds)
            {
                AddQueryParam((long)item, Const.PARAM_ENTITY_TYPE_ID);
            }
            AddQueryParam(Count, Const.PARAM_COUNT);
            AddQueryParam(OrderBy.KeyAndValue().Value, OrderBy.KeyAndValue().Key, RoleOrderBy.Type.KeyAndValue().Value);
            AddQueryParam(OrderDir.KeyAndValue().Value, OrderDir.KeyAndValue().Key, OrderDirection.asc.KeyAndValue().Value);
            return pc;
        }

        public long AccountId { get; set; }

        public List<RoleEntityType> EntityTypeIds { get; set; }

        public bool Count { get; set; }

        public RoleOrderBy OrderBy { get; set; }

        public OrderDirection OrderDir { get; set; }

        public int Limit { get; set; }

        public int Offset { get; set; }
    }
}