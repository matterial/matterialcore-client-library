﻿using MatterialCore.Enums;

namespace MatterialCore.Params
{
    public sealed class CommentParams : LoadLanguageParam
    {
        public CommentParams()
        {
            Offset = 0;
            Limit = Matterial.DefaultSearchResultLimit;
        }

        internal override ParameterCollection UrlParameter()
        {
            pc = new ParameterCollection(base.UrlParameter())
            {
                { Const.PARAM_OFFSET, Offset.ToString() },
                { Const.PARAM_LIMIT , Limit.ToString() },
            };

            AddQueryParam(DocumentId, Const.PARAM_DOCUMENT_ID);
            AddQueryParam(ContactId, Const.PARAM_CONTACT_ID);
            AddQueryParam(MentionedAccountId, Const.PARAM_MENTIONED_ACCOUNT_ID);
            AddQueryParam(DocumentLanguageVersionId, Const.PARAM_DOCUMENT_LANGUAGE_VERSION_ID);
            AddQueryParam(Count, Const.PARAM_COUNT);
            AddQueryParam(OrderDir.KeyAndValue().Value, OrderDir.KeyAndValue().Key, OrderDirection.asc.KeyAndValue().Value);
            AddQueryParam(OrderBy.KeyAndValue().Value, OrderBy.KeyAndValue().Key, CommentOrderBy.Createtime.KeyAndValue().Value);
            return pc;
        }

        /// <summary>
        /// If &gt; 0, only comments of the specified document id will be loaded.
        /// </summary>
        public long DocumentId { get; set; }

        /// <summary>
        /// If &gt; 0, only comments of the specified document language version id will be loaded.
        /// </summary>
        public long DocumentLanguageVersionId { get; set; }

        /// <summary>
        /// If &gt; 0, only comments of the specified contact id will be loaded.
        /// </summary>
        public long ContactId { get; set; }

        /// <summary>
        /// If &gt; 0, only comments will be loaded where the the MentionedAccountId is mentioned.
        /// </summary>
        public long MentionedAccountId { get; set; }

        /// <summary>
        /// Load the number of comments
        /// </summary>
        public bool Count { get; set; }

        public CommentOrderBy OrderBy { get; set; }
        public OrderDirection OrderDir { get; set; }
        public int Offset { get; set; }
        public int Limit { get; set; }
    }
}