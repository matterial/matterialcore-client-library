﻿using MatterialCore.Enums;

namespace MatterialCore.Params
{
    public class InstanceParams : ParamBase
    {
        public InstanceParams()
        {
            Limit = Matterial.DefaultSearchResultLimit;
            Offset = 0;
        }

        internal virtual ParameterCollection UrlParameter()
        {
            pc = new ParameterCollection
            {
                { Const.PARAM_OFFSET, Offset.ToString() },
                { Const.PARAM_LIMIT, Limit.ToString() }
            };
            AddQueryParam(DataSourceId, Const.PARAM_DATA_SOURCE_ID);
            AddQueryParam(Active, Const.PARAM_ACTIVE);
            AddQueryParam(DataSourceName, Const.PARAM_DATA_SOURCE_NAME);
            AddQueryParam(DataSourceReference, Const.PARAM_DATA_SOURCE_REFERENCE);
            AddQueryParam(Count, Const.PARAM_COUNT);
            AddQueryParam(OrderBy.KeyAndValue().Value, OrderBy.KeyAndValue().Key, InstanceOrderBy.Active.KeyAndValue().Value);
            AddQueryParam(OrderDir.KeyAndValue().Value, OrderDir.KeyAndValue().Key, OrderDirection.asc.KeyAndValue().Value);
            return pc;
        }

        public long DataSourceId { get; set; }
        public bool? Active { get; set; }
        public string? AccountLogin { get; set; }
        public string? DataSourceName { get; set; }
        public string? DataSourceReference { get; set; }
        public bool Count { get; set; }
        public InstanceOrderBy OrderBy { get; set; }
        public OrderDirection OrderDir { get; set; }
        public int Limit { get; set; }
        public int Offset { get; set; }
    }
}