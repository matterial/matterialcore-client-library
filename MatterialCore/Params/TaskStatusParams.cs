﻿using MatterialCore.Enums;

namespace MatterialCore.Params
{
    public class TaskStatusParams : LoadLanguageParam
    {
        public TaskStatusParams()
        {
            LanguageKey = Matterial.DefaultLanguage;
        }

        internal override ParameterCollection UrlParameter()
        {
            pc = new ParameterCollection(base.UrlParameter());
            AddQueryParam(OrderBy.KeyAndValue().Value, OrderBy.KeyAndValue().Key, TaskOrderBy.Date.KeyAndValue().Value);
            AddQueryParam(OrderDir.KeyAndValue().Value, OrderDir.KeyAndValue().Key, OrderDirection.asc.KeyAndValue().Value);

            return pc;
        }

        public TaskStatusOrderBy OrderBy { get; set; }
        public OrderDirection OrderDir { get; set; }
    }
}