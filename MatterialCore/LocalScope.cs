﻿using MatterialCore.Entity;

using Newtonsoft.Json;

using System;
using System.IO;
using System.Net.Http;

using static MatterialCore.RestClient;

namespace MatterialCore
{
    /// <summary>
    /// This class contains functions that only work in case of On-Premises matterial installations
    /// (private cloud). The server only accepts these function calls on localhost.
    /// </summary>
    public static class LocalScope
    {
        private static readonly Object uniqueLock = new Object();
        private static readonly Object uniqueLock1 = new Object();
        private static readonly Object uniqueLock2 = new Object();

        private static JsonSerializerSettings DefaultSerializerOptions()
        {
            JsonSerializerSettings opt = new JsonSerializerSettings
            {
                Formatting = Formatting.None,
                NullValueHandling = NullValueHandling.Ignore
            };
            return opt;
        }

        /// <summary>
        /// Special method to get document from instance without any permission checks, which does
        /// only work on localhost. This logs in as ‘system’ and performs the query on the given
        /// instance. The result is a DocumentForPdfConversion-object which contains the Document
        /// and the MainFile as HTML with rewritten attachment-links. <see href="https://www.matterial.com/documentation/api/90#get-document-by-instance-name-and-cas-id-pdf-conversion">API-Reference</see>
        /// </summary>
        /// <param name="basePath"></param>
        /// <param name="doc"></param>
        /// <param name="dataSource"></param>
        /// <returns></returns>
        public static DocumentForPdfConversion? GetByCasId(string basePath, Document doc, Instance dataSource)
        {
            lock (uniqueLock)
            {
                RestClient restClient = new RestClient
                {
                    BasePath = basePath
                };
                string path = Document.RESTBasePath();
                if (String.IsNullOrEmpty(doc.LanguageVersionCasId))
                {
                    throw new ArgumentException("The Language CAS Id must not be null.");
                }
                if (String.IsNullOrEmpty(dataSource.Name))
                {
                    throw new ArgumentException("The data source name must not be null.");
                }
                path = ApiSession.CombineUri(path, Const.PDF_CONVERSION, dataSource.Name, doc.LanguageVersionCasId.ToString());

                //Get the options
                HttpResponseMessage response = restClient.Get(path, ExpectedResponse.Json).Result;
                restClient.EnsureSuccessStatusCode(response);
                return JsonConvert.DeserializeObject<DocumentForPdfConversion>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions());
            }
        }

        /// <summary>
        /// Special method to get the title, abstract and main-file as html from instance without
        /// any permission checks, which does only work on localhost. This logs in as ‘system’ and
        /// performs the query on the given instance. The result is a complete document-view as
        /// html. This method is intended to be used for pdf-conversion! <see href="https://www.matterial.com/documentation/api/90#get-complete-file-by-instance-name-and-cas-id-pdf-conversion">API-Reference</see>
        /// </summary>
        /// <param name="basePath"></param>
        /// <param name="doc"></param>
        /// <param name="dataSource"></param>
        /// <returns></returns>
        public static Stream? GetHtmlFileByCasId(string basePath, Document doc, Instance dataSource)
        {
            lock (uniqueLock1)
            {
                RestClient restClient = new RestClient
                {
                    BasePath = basePath
                };
                string path = Document.RESTBasePath();
                if (String.IsNullOrEmpty(doc.LanguageVersionCasId))
                {
                    throw new ArgumentException("The Language CAS Id must not be null.");
                }
                if (String.IsNullOrEmpty(dataSource.Name))
                {
                    throw new ArgumentException("The data source name must not be null.");
                }
                path = ApiSession.CombineUri(path, Const.PDF_CONVERSION, dataSource.Name, doc.LanguageVersionCasId.ToString(), Const.COMPLETE);
                //Get the stream
                return restClient.GetStream(path).Result;
            }
        }

        /// <summary>
        /// Special method to load files (related images) from instance without any permission
        /// checks, which does only work on localhost. This logs in as ‘system’ and performs the
        /// query on the given instance.The result is the original file content(image). This method
        /// is intended to be used for pdf-conversion <see href="https://www.matterial.com/documentation/api/90#get-related-image-by-instance-name-and-cas-id-pdf-conversion">API-Reference</see>
        /// </summary>
        /// <param name="basePath"></param>
        /// <param name="doc"></param>
        /// <param name="dataSource"></param>
        /// <returns></returns>
        public static Stream? GetImageFileByCasId(string basePath, Document doc, Instance dataSource)
        {
            lock (uniqueLock2)
            {
                RestClient restClient = new RestClient
                {
                    BasePath = basePath
                };
                string path = Document.RESTBasePath();
                if (String.IsNullOrEmpty(doc.LanguageVersionCasId))
                {
                    throw new ArgumentException("The Language CAS Id must not be null.");
                }
                if (String.IsNullOrEmpty(dataSource.Name))
                {
                    throw new ArgumentException("The data source name must not be null.");
                }
                path = ApiSession.CombineUri(path, Const.PDF_CONVERSION, dataSource.Name, doc.LanguageVersionCasId.ToString(), Const.FILE);
                //Get the stream
                return restClient.GetStream(path).Result;
            }
        }
    }
}