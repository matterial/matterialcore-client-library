﻿// This file is used by Code Analysis to maintain SuppressMessage attributes that are applied to
// this project. Project-level suppressions either have no target or are given a specific target and
// scoped to a namespace, type, member, etc.

#if DEBUG

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification = "<Pending>", Scope = "member", Target = "~M:MatterialCore.DocumentController.Remove(MatterialCore.Entity.Document,System.Boolean)~System.Threading.Tasks.Task{MatterialCore.Entity.DocumentLock}")]
[assembly: SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification = "The property IsLoggedIn does not need to catch exception in case of not logged in.", Scope = "member", Target = "~P:MatterialCore.ApiSession.IsLoggedIn")]
[assembly: SuppressMessage("Maintainability", "CA1507:Use nameof to express symbol names", Justification = "Properties of the type ParamShowValues and ParamHideValues are based on a static string creation which must be independent on the name of the property.", Scope = "member", Target = "~M:MatterialCore.Params.Doc.LoadDocParams.UrlParameter~MatterialCore.Params.ParameterCollection")]
[assembly: SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification = "The exception does not need more specific catches because the mtr status codes are not always available.", Scope = "member", Target = "~M:MatterialCore.RestClient.EnsureSuccessStatusCode(System.Net.Http.HttpResponseMessage)")]

#endif