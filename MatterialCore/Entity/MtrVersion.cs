﻿using Newtonsoft.Json;

using System;
using System.Collections.Generic;
using System.Text;

namespace MatterialCore.Entity
{
    public partial class MtrVersion
    {

        public MtrVersion() { }

        [JsonProperty("buildNumber")]
        public string? BuildNumber { get; internal set; }

        [JsonProperty("commit")]
        public string? Commit { get; internal set; }

        [JsonProperty("info")]
        public string? Info { get; internal set; }

        [JsonProperty("major")]
        public int Major { get; internal set; }

        [JsonProperty("minor")]
        public int Minor { get; internal set; }

        [JsonProperty("moduleName")]
        public string? ModuleName { get; internal set; }

        [JsonProperty("patch")]
        public int Patch { get; internal set; }

        [JsonProperty("scmBranch")]
        public string? ScmBranch { get; internal set; }

        [JsonProperty("timestamp")]
        public string? Timestamp { get; internal set; }

        [JsonProperty("timeStampInMillis")]
        public long? TimeStampInMillis { get; internal set; }
    }
}
