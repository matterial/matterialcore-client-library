﻿using MatterialCore.Interfaces;

using Newtonsoft.Json;

//using System.Text.Json;
using Newtonsoft.Json.Serialization;

namespace MatterialCore.Entity
{
    [MtrClass("1.0", "The category class is used to manage document categories. Categories can be assigned to documents and persons can follow categories.")]
    public class Category : EntityBase, IRestEntity
    {
        public Category()
        {
        }

        public Category(long id)
        {
            Id = id;
        }

        public Category(string name)
        {
            Name = name;
        }

        public Category(string name, string description)
        {
            Name = name;
            Description = description;
        }

        [JsonProperty("id")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Number, Description = "The identifier of the object.")]
        public long Id { get; internal set; }

        [JsonProperty("name")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.String, Description = "The name of the category.", Updateable = true, RequiredOnCreate = true, Min = "1", Max = "255")]
        public string? Name { get; set; }

        [JsonProperty("description")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.String, "The description of the category.", Max = "4000", Updateable = true)]
        public string? Description { get; set; }

        [JsonProperty("typeId")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.String, Description = "The id of the category type to which this category belongs to", Updateable = true, RequiredOnCreate = true)]
        public long TypeId { get; set; }

        [JsonProperty("typePersonal")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Boolean, Description = "True, if the category type, to which this category belongs to, is a personal category type.")]
        public bool TypePersonal { get; internal set; }

        [JsonProperty("following")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Boolean, "Indicates, if the current user follows the category in the specified language. Requires the parameter \"loadFollowingLanguageKey\" when loading the category.")]
        public bool? Following { get; internal set; }

        [JsonProperty("usageCount")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Number, "Number, how many times the category is used. Requires the \"loadUsage\" parameter when loading the category.")]
        public long? UsageCount { get; internal set; }

        [JsonProperty("quick")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Boolean, Description = "A quick category is currently only represented by the \"Mark as favourite\" (the little star) in the user interface.")]
        public bool Quick { get; internal set; }

        [JsonProperty("jsonType")]
        public string JsonType
        {
            get
            {
                return this.GetType().Name;
            }
        }

        [JsonProperty("casId")]
        internal string? CasId { get; set; }

        [JsonProperty("casIdThumbnail")]
        internal string? CasIdThumbnail { get; set; }

        [JsonProperty("fileSize")]
        internal long? FileSize { get; set; }

        [JsonProperty("fileSizeThumbnail")]
        internal long? FileSizeThumbnail { get; set; }

        internal static string RESTBasePath()
        {
            return ApiSession.CombineUri(Const.MTR_BACKEND, Const.APPLICATION_PATH, Const.CATEGORY);
        }

        string IRestEntity.RESTBasePath()
        {
            return RESTBasePath();
        }
    }
}