﻿using MatterialCore.Interfaces;

using Newtonsoft.Json;

namespace MatterialCore.Entity
{
    [MtrClass("1.0", "The category type class is used to manage categories types. Category types are used to group categories together.")]
    public class CategoryType : EntityBase, IEntity
    {
        public CategoryType()
        {
        }

        public CategoryType(long id)
        {
            Id = id;
        }

        public CategoryType(string name)
        {
            Name = name;
        }

        public CategoryType(string name, string description)
        {
            Description = description;
            Name = name;
        }

        [JsonProperty("id")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Number, Description = "The identifier of the object.")]
        public long Id { get; internal set; }

        [JsonProperty("name")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.String, Description = "The name of the category type.", Updateable = true, RequiredOnCreate = true, Min = "1", Max = "255")]
        public string Name { get; set; } = string.Empty;

        [JsonProperty("description")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.String, "The description of the category type.", Max = "4000", Updateable = true)]
        public string Description { get; set; } = string.Empty;

        [JsonProperty("quick")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Boolean, Description = "A quick category type is a special category type.", Note = "Currently, the system only supports one quick category type and it is handled by the system and cannot be updated or removed.")]
        public bool? Quick { get; internal set; }

        [JsonProperty("personal")]
        public bool? Personal { get; internal set; }
    }
}