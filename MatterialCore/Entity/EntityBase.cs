﻿using MatterialCore.Interfaces;

//using System.Text.Json
using Newtonsoft.Json;

namespace MatterialCore.Entity
{
    public abstract class EntityBase : IEntity
    {
        public virtual string ToJson(Formatting formatting = Formatting.None, NullValueHandling nullValueHandling = NullValueHandling.Ignore)
        {
            var opt = new JsonSerializerSettings
            {
                Formatting = formatting,
                NullValueHandling = nullValueHandling
            };
            return JsonConvert.SerializeObject(this, this.GetType(), opt);
        }
    }
}