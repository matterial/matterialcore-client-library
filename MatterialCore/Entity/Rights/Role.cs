﻿//using System.Text.Json.Serialization;

using MatterialCore.Enums;
using MatterialCore.Interfaces;

using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace MatterialCore.Entity
{
    [MtrClass("1.0", "The role class is used to manage roles. There are four different types of roles: Functional, WorkGroup, Personal and ReviewGroup.")]
    public partial class Role : EntityBase, IListableEntity, IRestEntity
    {
        public Role()
        {
        }

        public Role(long id)
        {
            Id = id;
        }

        public Role(long id, string name)
        {
            Name = name;
            Id = id;
        }

        public Role(long id, string name, RoleEntityType entityTypeId)
        {
            Name = name;
            Id = id;
            EntityTypeId = entityTypeId;
        }

        public Role(string name)
        {
            Name = name;
        }

        public Role(string name, RoleEntityType entityTypeId)
        {
            Name = name;
            EntityTypeId = entityTypeId;
        }

        internal static string RESTBasePath()
        {
            return ApiSession.CombineUri(Const.MTR_BACKEND, Const.APPLICATION_PATH, Const.ROLE);
        }

        string IRestEntity.RESTBasePath()
        {
            return RESTBasePath();
        }

        [JsonProperty("jsonType")]
        public string JsonType
        {
            get
            {
                return this.GetType().Name;
            }
        }

        [JsonProperty("id")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Number, Description = "The identifier of the object.")]
        public long Id { get; internal set; }

        [JsonProperty("entityTypeId")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Permissions, "The type of role to create.", Note = "The system handles WorkGroups, PersonalRoles, ReviewGroups and FunctionalRoles as roles.", Updateable = true, RequiredOnCreate = true)]
        public RoleEntityType EntityTypeId { get; set; }

        [JsonProperty("name")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.String, Description = "The name of the role.", Updateable = true, RequiredOnCreate = true, Min = "1", Max = "255")]
        public string? Name { get; set; }

        [JsonProperty("description")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.String, "The description of the role.", Max = "4000", Updateable = true)]
        public string? Description { get; set; }

        [JsonProperty("permissions")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Permissions, "The permissions of the role.", Updateable = true)]
        public Permissions? Permissions { get; set; }

        [JsonProperty("initiallyAssignedToAccount")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Boolean, "If true, the role will automatically be assigned to a new account.", Updateable = true)]
        public bool InitiallyAssignedToAccount { get; set; }

        [JsonProperty("initiallyAssignedTypeToDocument")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Boolean, "If true, the role will automatically be assigned to a new document.", Updateable = true)]
        public long InitiallyAssignedTypeToDocument { get; set; }

        [JsonProperty("personalRolePerson")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Person, "If the role is of type PersonalRole, this property is set with the person.", Updateable = true)]
        public Person? PersonalRolePerson { get; internal set; }

        [JsonProperty("bitmask")]
        internal long Bitmask { get; set; }

        [JsonProperty("clientId")]
        public long ClientId { get; internal set; }

        [JsonProperty("notRemovable")]
        public bool NotRemovable { get; internal set; }
    }
}