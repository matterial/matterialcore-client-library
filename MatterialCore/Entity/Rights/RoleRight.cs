﻿using MatterialCore.Enums;
using MatterialCore.Interfaces;

using Newtonsoft.Json;

//using System.Text.Json;
using Newtonsoft.Json.Serialization;

namespace MatterialCore.Entity
{
    public class RoleRight : EntityBase, IListableEntity
    {
        public RoleRight()
        {
        }

        [JsonProperty("role")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Role, "The role this role right belongs to.")]
        public Role? Role { get; set; }

        [JsonProperty("type")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Number, "Type of the role right:  READ=1, EDIT=2")]
        public RoleRightTypes Type { get; set; }

        [JsonProperty("readConfirmationRequested")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Boolean, "`true` if the reading confirmation for this role and the related document is enabled.", Updateable = true, RequiredOnCreate = false, VersionImplemented = "2.5.0")]
        public bool ReadConfirmationRequested { get; internal set; }

        [JsonProperty("jsonType")]
        public string JsonType
        {
            get
            {
                return this.GetType().Name;
            }
        }
    }
}