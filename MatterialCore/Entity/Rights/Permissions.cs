﻿//using System.Text.Json;

using MatterialCore.Interfaces;

using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace MatterialCore.Entity
{
    [MtrClass("1.0", "The permissions class is used for Persons an roles.")]
    public class Permissions : EntityBase, IEntity
    {
        public Permissions()
        {
        }

        [JsonProperty("bitmask")]
        internal long Bitmask { get; set; }

        [JsonProperty("administrateAll")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Boolean, Description = "Administrate all areas of the application.", Updateable = true)]
        public bool AdministrateAll { get; set; }

        [JsonProperty("editPerson")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Boolean, Description = "Edit persons.", Updateable = true)]
        public bool EditPerson { get; set; }

        [JsonProperty("editDocument")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Boolean, Description = "Create and edit documents.", Updateable = true)]
        public bool EditDocument { get; set; }

        [JsonProperty("editDocumentTemplate")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Boolean, Description = "Create and edit document templates.", Updateable = true)]
        public bool EditDocumentTemplate { get; set; }

        [JsonProperty("editRole")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Boolean, Description = "Create and edit roles and groups.", Updateable = true)]
        public bool EditRole { get; set; }

        [JsonProperty("editCategory")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Boolean, Description = "Create and edit categories.", Updateable = true)]
        public bool EditCategory { get; set; }

        [JsonProperty("editTask")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Boolean, Description = "Create and edit tasks.", Updateable = true)]
        public bool EditTask { get; set; }

        [JsonProperty("comment")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Boolean, Description = "Create and edit my comments.", Updateable = true)]
        public bool Comment { get; set; }

        [JsonProperty("viewArchive")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Boolean, Description = "View archived documents", Updateable = true)]
        public bool ViewArchive { get; set; }

        [JsonProperty("viewStatistic")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Boolean, Description = "Not used yet.", Updateable = true)]
        public bool ViewStatistic { get; set; }

        [JsonProperty("showTrash")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Boolean, Description = "Show the Administrator's trash showing trashed documents of all users.", Updateable = true)]
        public bool ShowTrash { get; set; }

        [JsonProperty("editGallery")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Boolean, Description = "Not used yet.", Updateable = true)]
        public bool EditGallery { get; set; }

        [JsonProperty("immediateReview")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Boolean, Description = "Ability to save and review a document in one step.", Updateable = true)]
        public bool ImmediateReview { get; set; }

        [JsonProperty("publishUnreviewed")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Boolean, Description = "Can publish documents immediately without having to request a review.", Updateable = true)]
        public bool PublishUnreviewed { get; set; }

        [JsonProperty("editAdditionalProperty")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Boolean, Description = "Create and edit additional properties.", Updateable = true)]
        public bool EditAdditionalProperty { get; set; }

        [JsonProperty("editGlobalSavedSearch")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Boolean, Description = "Create and edit global saved searches.", Updateable = true)]
        public bool EditGlobalSavedSearch { get; set; }

        [JsonProperty("createSnap")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Boolean, Description = "Create a Knowledge Flash (a Snap).", Updateable = true)]
        public bool CreateSnap { get; set; }
    }
}