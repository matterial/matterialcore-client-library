﻿using MatterialCore.Interfaces;

using Newtonsoft.Json;

//using System.Text.Json;
using Newtonsoft.Json.Serialization;

using System.Collections.Generic;

namespace MatterialCore.Entity
{
    public class PreferenceMapContainer : EntityBase, IEntity
    {
        public PreferenceMapContainer()
        {
            PreferenceDescriptions = new Dictionary<string, object>();
            Preferences = new Dictionary<string, object>();
        }

        [JsonProperty("preferenceDescriptions")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.DictionaryStringObject, "A key-value list containing the preference descriptions. The key is the same as in Preferences")]
        public Dictionary<string, object>? PreferenceDescriptions { get; internal set; }

        [JsonProperty("preferences")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.DictionaryStringObject, "A key-value list containing the preferences. The key is the same as in PreferenceDescriptions.")]
        public Dictionary<string, object>? Preferences { get; internal set; }
    }
}