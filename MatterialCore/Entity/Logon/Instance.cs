﻿using MatterialCore.Interfaces;

using Newtonsoft.Json;

//using System.Text.Json.Serialization;
using Newtonsoft.Json.Serialization;

namespace MatterialCore.Entity
{
    [MtrClass("The Instance class represents the a set of data of a matterial environment related to an instance owner. " +
        "The term used in the matterial SaaS environment is Instance whereas in some areas of the API the term DataSource is used. " +
        "A user can be linked to multiple Instances.")]
    public class Instance : EntityBase, IListableEntity, IRestEntity
    {
        public Instance()
        {
        }

        public Instance(long id)
        {
            Id = id;
        }

        [JsonProperty("id")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Number, Description = "The identifier of the object.")]
        public long Id { get; internal set; }

        [JsonProperty("name")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.String, Description = "The name of the data source.", Updateable = true, RequiredOnCreate = true, Min = "1", Max = "255")]
        public string? Name { get; /*internal*/ set; }

        [JsonProperty("displayName")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.String, "The display name of the data source.", Max = "255", Updateable = true)]
        public string? DisplayName { get; /*internal*/ set; }

        [JsonProperty("reference")]
        public string? Reference { get; internal set; }

        [JsonProperty("active")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.String, "Indicates if the data source is active", Updateable = true)]
        public bool Active { get; /*internal*/ set; }

        [JsonProperty("jsonType")]
        public string JsonType
        {
            get
            {
                return "DataSource"; // this.GetType().Name;
            }
        }

        internal static string RESTBasePath()
        {
            return ApiSession.CombineUri(Const.MTR_BACKEND, Const.APPLICATION_PATH, Const.INSTANCE_CONTROL);
        }

        string IRestEntity.RESTBasePath()
        {
            return RESTBasePath();
        }
    }
}