﻿using MatterialCore.Interfaces;

using Newtonsoft.Json;

//using System.Text.Json.Serialization;
using Newtonsoft.Json.Serialization;

using System.Collections.Generic;

namespace MatterialCore.Entity
{
    public class LoginData : EntityBase, IEntity
    {
        public LoginData()
        {
        }

        // *** replaces `pwdMustChange`. => pwdSet: "Set" the password without giving the old one. Instead of "change" password, which requires the old one.
        [JsonProperty("pwdSetEnabled")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Boolean, "If `true` you are allowed to change your password without knowing the old one (For example after login with MagicLink).", Updateable = false, RequiredOnCreate = false, VersionImplemented = "2.5.0")]
        public bool PwdSetEnabled { get; internal set; }

        [JsonProperty("sessionId")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.String, Description = "The current session id.")]
        public string? SessionId { get; internal set; }

        [JsonProperty("externallyAuthenticated")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Boolean, Description = "Is true, if the user was authenticated with en external service (OAuth, LDAP, etc).")]
        public bool ExternallyAuthenticated { get; internal set; }

        [JsonProperty("currentDataSource")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.String, Description = "The current instance.")]
        public Instance? CurrentInstance { get; internal set; }

        [JsonProperty("favouriteDataSource")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.String, Description = "The favourite instance.")]
        public Instance? FavouriteInstance { get; internal set; }

        [JsonProperty("permissions")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.String, Description = "The current user's permissions.")]
        public Permissions? Permissions { get; internal set; }

        [JsonProperty("person")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Person, Description = "This property represents the current logged in person.")]
        public Person? Person { get; internal set; }

        [JsonProperty("client")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.String, Description = "The current user's client.")]
        public Client? Client { get; internal set; }

        [JsonProperty("availableLanguages")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.ListOfLanguage)]
        public List<Language>? AvailableLanguages { get; internal set; }

        [JsonProperty("availableDataSources")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.ListOfInstance, "All instances the current logged in user has access to.")]
        public List<Instance>? AvailableInstances { get; internal set; }

        [JsonProperty("accountSettings")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.DictionaryStringObject, "A key-value list with all account settings.")]
        public Dictionary<string, object>? AccountSettings { get; internal set; }

        [JsonProperty("clientPreferences")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.DictionaryStringObject, "A key-value list with all client preferences.")]

  

        public Dictionary<string, object>? ClientPreferences { get; internal set; }
    }
}