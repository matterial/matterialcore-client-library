﻿//using System.Text.Json.Serialization;

using MatterialCore.Interfaces;

using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace MatterialCore.Entity
{
    public class Language : EntityBase, IRestEntity
    {
        public Language()
        {
        }

        [JsonProperty("active")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.String, Description = "True, if the language is active.", Updateable = true)]
        public bool Active { get; set; }

        [JsonProperty("id")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Number, Description = "The identifier of the object.")]
        public int Id { get; internal set; }

        [JsonProperty("name")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.String, Description = "The name of the language.")]
        public string? Name { get; internal set; }

        [JsonProperty("i18nKey")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.String, Description = "The i18n key of the language.")]
        public string? I18nKey { get; internal set; }

        [JsonProperty("prio")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Number, Description = "Priority of the language.")]
        public int Prio { get; internal set; }

        internal static string RESTBasePath()
        {
            return ApiSession.CombineUri(Const.MTR_BACKEND, Const.APPLICATION_PATH, Const.LANGUAGE);
        }

        string IRestEntity.RESTBasePath()
        {
            return RESTBasePath();
        }
    }
}