﻿using MatterialCore.Interfaces;

using Newtonsoft.Json;

using System.Collections.Generic;

namespace MatterialCore.Entity
{
    public class Credential : EntityBase, IRestEntity
    {
        public Credential()
        {
        }

        public Credential(long id)
        {
            Id = id;
        }

        public Credential(long id, string login)
        {
            Id = id;
            Login = login;
        }

        [JsonProperty("id")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Number, Description = "The identifier of the object.")]
        public long Id { get; internal set; }

        /// <summary>
        /// The user's login. If login is not set, subscriptionEmail will be used instead.
        /// </summary>
        [JsonProperty("login")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.String, Description = "The user's login.", Note = "If login is not set, subscriptionEmail will be used instead.", Min = "1", Max = "255", Updateable = true)]
        public string? Login { get; set; }

        /// <summary>
        /// The user's password. The password is RequiredOnCreate if updatePassword==true.
        /// </summary>
        [JsonProperty("password")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.String, Description = "The user's password.", Note = "The password is RequiredOnCreate if updatePassword==true.", Min = "1", Max = "255", Updateable = true)]
        public string? Password { get; set; }

        /// <summary>
        /// The user's email address used for the sign-up. This is required when storing new credentials.
        /// </summary>
        [JsonProperty("subscriptionEmail")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.String, Description = "The user's email address.", Min = "3", Max = "255", Updateable = true, RequiredOnCreate = true)]
        public string? SubscriptionEmail { get; set; }

        /// <summary>
        /// Update the user's password. If true, no invitation email will be sent when storing the credential.
        /// </summary>
        [JsonProperty("updatePassword")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Boolean, Description = "Update the user's password.", Note = "If true, no invitation email will be sent.", Min = "3", Max = "255", Updateable = true)]
        public bool UpdatePassword { get; set; }

        [JsonProperty("dataSources")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.ListOfInstance, Description = "The list of data sources this credential has access to.")]
        public List<Instance>? Instances { get; internal set; }

        internal static string RESTBasePath()
        {
            return ApiSession.CombineUri(Const.MTR_BACKEND, Const.APPLICATION_PATH, Const.CREDENTIAL);
        }

        string IRestEntity.RESTBasePath()
        {
            return RESTBasePath();
        }
    }
}