﻿//using System.Text.Json;
using MatterialCore.Interfaces;

using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace MatterialCore.Entity
{
    /// <summary>
    /// This class is used for the update password function.
    /// </summary>
    public class PasswordContainer : EntityBase, IEntity
    {
        public PasswordContainer()
        {
        }

        public PasswordContainer(string passwordCurrent, string passwordNew, string passwordConfirm)
        {
            PasswordCurrent = passwordCurrent;
            PasswordNew = passwordNew;
            PasswordNew2 = passwordConfirm;
        }

        /// <summary>
        /// The current passwored
        /// </summary>
        [JsonProperty("passwordCurrent")]
        public string? PasswordCurrent { get; set; }

        /// <summary>
        /// The new password.
        /// </summary>
        [JsonProperty("passwordNew")]
        public string? PasswordNew { get; set; }

        /// <summary>
        /// Confirmation of the new password.
        /// </summary>
        [JsonProperty("passwordNew2")]
        public string? PasswordNew2 { get; set; }
    }
}