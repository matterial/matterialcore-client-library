﻿using MatterialCore.Interfaces;

using Newtonsoft.Json;

//using System.Text.Json.Serialization;
using Newtonsoft.Json.Serialization;

namespace MatterialCore.Entity
{
    public class Client : EntityBase, IRestEntity
    {
        public Client()
        {
        }

        public Client(long id, string name)
        {
            Id = id;
            Name = name;
        }

        [JsonProperty("id")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Number, Description = "The identifier of the object.")]
        public long Id { get; internal set; }

        [JsonProperty("name")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.String, Description = "The name of the Client.", Updateable = true, RequiredOnCreate = true, Min = "1", Max = "255")]
        public string? Name { get; set; }

        [JsonProperty("clientPreferences")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.PreferenceMapContainer, Description = "A PreferenceMapContainer containing all the preferences of a client.")]
        public PreferenceMapContainer? ClientPreferences { get; internal set; }

        [JsonProperty("skinColourButton")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.String, Description = "This property defines the button color.", Updateable = true)]
        public string? SkinColourButton { get; set; }

        [JsonProperty("skinColourLabel")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.String, Description = "This property defines the label color.", Updateable = true)]
        public string? SkinColourLabel { get; set; }

        [JsonProperty("skinColourNotification")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.String, Description = "This property defines the Notification color.", Updateable = true)]
        public string? SkinColourNotification { get; set; }

        [JsonProperty("jsonType")]
        public string JsonType
        {
            get
            {
                return this.GetType().Name;
            }
        }

        [JsonProperty("casId")]
        public string? CasId { get; internal set; }

        [JsonProperty("casIdImgLarge")]
        public string? CasIdImgLarge { get; internal set; }

        [JsonProperty("casIdImgSmall")]
        public string? CasIdImgSmall { get; internal set; }

        [JsonProperty("casIdThumbnail")]
        public string? CasIdThumbnail { get; internal set; }

        [JsonProperty("fileSize")]
        public long? FileSize { get; internal set; }

        [JsonProperty("fileSizeImgLarge")]
        public long? FileSizeImgLarge { get; internal set; }

        [JsonProperty("fileSizeImgSmall")]
        public long? FileSizeImgSmall { get; internal set; }

        [JsonProperty("fileSizeThumbnail")]
        public long? FileSizeThumbnail { get; internal set; }

        internal static string RESTBasePath()
        {
            return ApiSession.CombineUri(Const.MTR_BACKEND, Const.APPLICATION_PATH, Const.CLIENT);
        }

        string IRestEntity.RESTBasePath()
        {
            return RESTBasePath();
        }
    }
}