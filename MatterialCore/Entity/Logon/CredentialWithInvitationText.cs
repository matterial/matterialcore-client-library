﻿using Newtonsoft.Json;

namespace MatterialCore.Entity
{
    public class CredentialWithInvitationText : Credential
    {
        public CredentialWithInvitationText()
        { }

        public CredentialWithInvitationText(long id)
        {
            Id = id;
        }

        public CredentialWithInvitationText(long id, string login)
        {
            Id = id;
            Login = login;
        }

        [JsonProperty("invitationText")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Number, Description = "The invitation text used. This is used when a person is invited.", Updateable = true, Min = "1", Max = "4000")]
        public string? InvitationText { get; set; }
    }
}