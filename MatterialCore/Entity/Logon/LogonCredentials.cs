﻿using MatterialCore.Interfaces;

using System.Net;
using System.Security;

//using System.Text.Json;

namespace MatterialCore.Entity
{
    /// <summary>
    /// This class is used to provide credentials for authentication.
    /// </summary>
    public class LogonCredentials : EntityBase, IEntity
    {
        //To make it a bit more secure we use SecureString
        private readonly SecureString pw = new SecureString();

        public LogonCredentials(string user, SecureString password)
        {
            Login = user;
            pw = password;
        }

        internal LogonCredentials(string user)
        {
            Login = user;
        }

        public LogonCredentials()
        {
        }

        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.String, "The login id - typically the email address.")]
        public string? Login { get; set; }

        /// <summary>
        /// A bit more secure by not allowing to get the plain Json from external.
        /// </summary>
        /// <returns></returns>
        internal string ToJsonSecure()
        {
            return "{\"login\":\"" + Login + "\",\"password\":\"" + new NetworkCredential(string.Empty, pw).Password + "\"}";
        }
    }
}