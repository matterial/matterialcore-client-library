﻿using MatterialCore.Interfaces;

using Newtonsoft.Json;

namespace MatterialCore.Entity
{
    public class License : EntityBase, IRestEntity
    {
        public License()
        {
        }

        [JsonProperty("casSize")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Number, "The maximum storage size which the license allows in mega bytes.", Updateable = true, RequiredOnCreate = true)]
        public long CasSize { get; set; }

        [JsonProperty("hash")]
        public string? Hash { get; internal set; }

        [JsonProperty("packageReview")]
        public bool PackageReview { get; internal set; }

        [JsonProperty("user")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Number, "Number of users.", Updateable = true, RequiredOnCreate = true, Min = "1")]
        public long User { get; set; }

        internal static string RESTBasePath()
        {
            return ApiSession.CombineUri(Const.MTR_BACKEND, Const.APPLICATION_PATH, Const.LICENCE);
        }

        string IRestEntity.RESTBasePath()
        {
            return RESTBasePath();
        }
    }
}