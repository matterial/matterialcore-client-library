﻿using MatterialCore.Interfaces;

using Newtonsoft.Json;

//using System.Text.Json.Serialization;
using Newtonsoft.Json.Serialization;

namespace MatterialCore.Entity
{
    public class Address : EntityBase, IEntity, IListableEntity
    {
        public Address()
        {
        }

        public Address(long id)
        {
            Id = id;
        }

        [JsonProperty("jsonType")]
        public string JsonType
        {
            get
            {
                return this.GetType().Name;
            }
        }

        [JsonProperty("id")]
        public long Id { get; internal set; }

        [JsonProperty("city")]
        [MtrProperty(Min = "0", Max = "255")]
        public string? City { get; set; }

        [JsonProperty("country")]
        [MtrProperty(Min = "0", Max = "255")]
        public string? Country { get; set; }

        [JsonProperty("houseNumber")]
        [MtrProperty(Min = "0", Max = "255")]
        public string? HouseNumber { get; set; }

        [JsonProperty("imported")]
        public bool Imported { get; internal set; }

        [JsonProperty("postalCode")]
        [MtrProperty(Min = "0", Max = "255")]
        public string? PostalCode { get; set; }

        [JsonProperty("street")]
        [MtrProperty(Min = "0", Max = "255")]
        public string? Street { get; set; }
    }
}