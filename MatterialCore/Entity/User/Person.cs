﻿using MatterialCore.Interfaces;

using Newtonsoft.Json;

//using System.Text.Json.Serialization;
using Newtonsoft.Json.Serialization;

using System.Collections.Generic;

namespace MatterialCore.Entity
{
    public class Person : EntityBase, ISearchable
    {
        public Person()
        {
        }

        public Person(long contactId)
        {
            ContactId = contactId;
        }

        public Person(long accountId, long? contactId)
        {
            AccountId = accountId;
            if (!(contactId is null))
                ContactId = contactId ?? 0;
        }

        internal static string RESTBasePath()
        {
            return ApiSession.CombineUri(Const.MTR_BACKEND, Const.APPLICATION_PATH, Const.PERSON);
        }

        string IRestEntity.RESTBasePath()
        {
            return RESTBasePath();
        }

        [JsonProperty("accountId")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Number, "The unique account id.")]
        public long AccountId { get; internal set; }

        [JsonProperty("contactId")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Number, "The unique contact id.")]
        public long ContactId { get; internal set; }

        [JsonProperty("accountLogin")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.String, "The account login (typically the email address).", Updateable = false, RequiredOnCreate = true, Min = "1", Max = "255")]
        public string? AccountLogin { get; set; }

        [JsonProperty("contactImage")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.ContactImage, "The contact image of the person.")]
        public ContactImage? ContactImage { get; set; }

        [JsonProperty("firstName")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.String, "First name", Min = "0", Max = "255", Updateable = true)]
        public string? FirstName { get; set; }

        [JsonProperty("lastName")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.String, "Last name", Min = "0", Max = "255", Updateable = true)]
        public string? LastName { get; set; }

        [JsonProperty("gender")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Number, "Gender.", Updateable = true)]
        public int? Gender { get; set; }

        [JsonProperty("addresses")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.ListOfAddress, "Addresses of the person.", Updateable = true)]
        public List<Address>? Addresses { get; set; }

        [JsonProperty("birthdayInSeconds")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Number, "Unix Time Stamp of the birthday.", Updateable = true)]
        public long? BirthdayInSeconds { get; set; }

        [JsonProperty("position")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.String, "Position", Updateable = true)]
        public string? Position { get; set; }

        [JsonProperty("communicationData")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.ListOfCommunicationData, "List of Communication data like email, telephone, etc.", Updateable = true)]
        public List<CommunicationData>? CommunicationData { get; set; }

        [JsonProperty("contactImages")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.ListOfContactImage, Updateable = true)]
        public List<ContactImage>? ContactImages { get; set; }

        [JsonProperty("accountCreateTimeInSeconds")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Number, "Unix Time Stamp in seconds when the person was created.")]
        public long? AccountCreateTimeInSeconds { get; internal set; }

        [JsonProperty("accountLastLoginInSeconds")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Number, "Unix Time Stamp in seconds of the last login.")]
        public long? AccountLastLoginInSeconds { get; internal set; }

        [JsonProperty("bioDocument")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Document, "The biography document.")]
        public Document? BioDocument { get; internal set; }

        [JsonProperty("clients")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.ListOfClient, "The list of clients this person is related to.")]
        public List<Client>? Clients { get; internal set; }

        [JsonProperty("demo")]
        public bool Demo { get; internal set; }

        [JsonProperty("instanceOwner")]
        public bool InstanceOwner { get; internal set; }

        [JsonProperty("permissions")]
        public Permissions? Permissions { get; internal set; }

        [JsonProperty("rolePersonal")]
        public Role? RolePersonal { get; internal set; }

        [JsonProperty("rolesContent")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.ListOfRole)]
        public List<Role>? RolesContent { get; internal set; }

        [JsonProperty("rolesFunctional")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.ListOfRole)]
        public List<Role>? RolesFunctional { get; internal set; }

        [JsonProperty("rolesReview")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.ListOfRole)]
        public List<Role>? RolesReview { get; internal set; }

        [JsonProperty("systemAccount")]
        public bool SystemAccount { get; internal set; }

        [JsonProperty("jsonType")]
        public string JsonType
        {
            get
            {
                return this.GetType().Name;
            }
        }
    }
}