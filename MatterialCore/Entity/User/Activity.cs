﻿using MatterialCore.Interfaces;

using Newtonsoft.Json;
//using System.Text.Json.Serialization;
using Newtonsoft.Json.Serialization;

using System;

namespace MatterialCore.Entities.User
{
    public class Activity : Entity, IListableEntity
    {
        public Activity()
        {
        }


        [JsonProperty("id")]
        public long Id { get; /* internTest*/ internal set; }

        [JsonProperty("commentText")]
        public string? CommentText { get; internal set; }

        [JsonProperty("contentStyle")]
        public string? ContentStyle { get; internal set; }

        [JsonProperty("createTimeInSeconds")]
        public long? CreateTimeInSeconds { get; internal set; }

        [JsonProperty("dataSourceDisplayName")]
        public string? InstanceDisplayName { get; internal set; }

        [JsonProperty("dataSourceId")]
        public long? InstanceId { get; internal set; }

        [JsonProperty("documentFollowedByAdditionalProperty")]
        public bool DocumentFollowedByAdditionalProperty { get; internal set; }

        [JsonProperty("documentFollowedByCategory")]
        public bool DocumentFollowedByCategory { get; internal set; }

        [JsonProperty("documentFollowedDirectly")]
        public bool DocumentFollowedDirectly { get; internal set; }

        [JsonProperty("documentId")]
        public long? DocumentId { get; /* internTest*/ internal set; }

        [JsonProperty("documentLanguageVersionId")]
        public long? DocumentLanguageVersionId { get; /* internTest*/ internal set; }

        [JsonProperty("documentLanguageVersionLanguageKey")]
        public string? DocumentLanguageVersionLanguageKey { get; /* internTest*/ internal set; }

        [JsonProperty("documentLanguageVersionTitle")]
        public string? DocumentLanguageVersionTitle { get; /* internTest*/ internal set; }

        [JsonProperty("documentLanguageVersionVersion")]
        public long? DocumentLanguageVersionVersion { get; /* internTest*/ internal set; }


        [JsonProperty("languageKey")]
        public string? LanguageKey { get; internal set; }

        [JsonProperty("messageType")]
        public string? MessageType { get; internal set; }

        [JsonProperty("notificationStatus")]
        public long? NotificationStatus { get; internal set; }

        [JsonProperty("personAccountId")]
        public long? PersonAccountId { get; internal set; }

        [JsonProperty("personAccountLogin")]
        public string? PersonAccountLogin { get; internal set; }

        [JsonProperty("personFirstName")]
        public string? PersonFirstName { get; internal set; }

        [JsonProperty("personLastName")]
        public string? PersonLastName { get; internal set; }

        [JsonProperty("receiverAcountId")]
        public long ReceiverAcountId { get; internal set; }

        [JsonProperty("receiverEmail")]
        public string? ReceiverEmail { get; internal set; }

        [JsonProperty("receiverFirstName")]
        public string? ReceiverFirstName { get; internal set; }

        [JsonProperty("receiverLastName")]
        public string? ReceiverLastName { get; internal set; }

        [JsonProperty("receiverLogin")]
        public string? ReceiverLogin { get; internal set; }

        [JsonProperty("taskAssignee")]
        public string? TaskAssignee { get; internal set; }

        [JsonProperty("taskDescription")]
        public string? TaskDescription { get; internal set; }

        [JsonProperty("taskDueDate")]
        public string? TaskDueDate { get; internal set; }

        [JsonProperty("taskId")]
        public long? TaskId { get; internal set; }

        [JsonProperty("taskSnap")]
        public bool TaskSnap { get; internal set; }

        [JsonProperty("taskStatus")]
        public string? TaskStatus { get; internal set; }

        public string RESTBasePath()
        {
            throw new NotImplementedException();
        }
        [JsonProperty("jsonType")]
        public string JsonType
        {
            get
            {
                return this.GetType().Name;
            }
        }
    }
}
