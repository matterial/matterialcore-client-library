﻿using MatterialCore.Enums;
using MatterialCore.Interfaces;

using Newtonsoft.Json;

//using System.Text.Json.Serialization;
using Newtonsoft.Json.Serialization;

namespace MatterialCore.Entity
{
    public class CommunicationData : EntityBase, IListableEntity
    {
        public CommunicationData()
        {
        }

        public CommunicationData(long id)
        {
            Id = id;
        }

        [JsonProperty("jsonType")]
        public string JsonType
        {
            get
            {
                return this.GetType().Name;
            }
        }

        [JsonProperty("id")]
        public long Id { get; internal set; }

        [JsonProperty("description")]
        public string? Description { get; set; }

        [JsonProperty("usedForNotification")]
        public bool UsedForNotification { get; set; }

        [JsonProperty("value")]
        public string? Value { get; set; }

        [JsonProperty("entityTypeId")]
        public CommunicationEntityType EntityTypeId { get; set; }

        [JsonProperty("imported")]
        public bool Imported { get; internal set; }
    }
}