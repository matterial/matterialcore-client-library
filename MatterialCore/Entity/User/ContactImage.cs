﻿using MatterialCore.Interfaces;

using Newtonsoft.Json;

//using System.Text.Json.Serialization;
using Newtonsoft.Json.Serialization;

namespace MatterialCore.Entity
{
    public class ContactImage : EntityBase, IListableEntity
    {
        private object? reference;

        public ContactImage()
        {
        }

        public ContactImage(long id)
        {
            Id = id;
        }

        [JsonProperty("jsonType")]
        public string JsonType
        {
            get
            {
                return this.GetType().Name;
            }
        }

        [JsonProperty("id")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Number, "Internal id of the object.")]
        public long Id { get; internal set; }

        [JsonProperty("name")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.String, "Name of the contact image.", Min = "1", Max = "255")]
        public string? Name { get; internal set; }

        [JsonProperty("casId")]
        public string? CasId { get; internal set; }

        [JsonProperty("casIdThumbnail")]
        public string? CasIdThumbnail { get; internal set; }

        [JsonProperty("fileSize")]
        public long? FileSize { get; internal set; }

        [JsonProperty("fileSizeThumbnail")]
        public long? FileSizeThumbnail { get; internal set; }

        [JsonProperty("createTimeInSeconds")]
        public long CreateTimeInSeconds { get; internal set; }

        [JsonProperty("mimeType")]
        public string? MimeType { get; internal set; }

        [JsonProperty("externalImgUrl")]
        public string? ExternalImgUrl { get; internal set; }

        [JsonProperty("internal")]
        public bool IsInternal { get; internal set; }

        [JsonProperty("active")]
        public bool IsActive { get; internal set; }

        [JsonProperty("external")]
        public bool IsExternal { get; internal set; }

        [JsonProperty("gravatar")]
        public bool IsGravatar { get; internal set; }

        [JsonProperty("google")]
        public bool IsGoogle { get; internal set; }

        [JsonProperty("cobot")]
        public bool IsCobot { get; internal set; }

        [JsonProperty("reference")]
        public object? Reference
        {
            get
            {
                if (this.reference is null && this.IsGravatar)
                {
                    return this.Name;
                }
                return reference;
            }
            internal set
            {
                reference = value;
            }
        }
    }
}