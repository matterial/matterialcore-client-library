﻿using MatterialCore.Interfaces;

using Newtonsoft.Json;

//using System.Text.Json.Serialization;
using Newtonsoft.Json.Serialization;

using System.Collections.Generic;

namespace MatterialCore.Entity
{
    public class Aggregation : EntityBase, IEntity
    {
        public Aggregation()
        {
        }

        [JsonProperty("buckets")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.DictionaryStringLong, "Buckets-map containing the aggregated results. key: value of indexField, value: hits")]
        public Dictionary<string, long>? Buckets { get; internal set; }

        [JsonProperty("indexField")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.String, "The index field that was aggregated.")]
        public string? IndexField { get; internal set; }
    }
}