﻿using MatterialCore.Interfaces;

using Newtonsoft.Json;

//using System.Text.Json.Serialization;
using Newtonsoft.Json.Serialization;

using System.Collections.Generic;

namespace MatterialCore.Entity
{
    public class Highlight : EntityBase, IEntity
    {
        public Highlight()
        {
        }

        [JsonProperty("fragments")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.ListOfString)]
        public List<string>? Fragments { get; internal set; }

        [JsonProperty("indexField")]
        public string? IndexField { get; internal set; }
    }
}