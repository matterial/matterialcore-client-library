﻿using MatterialCore.Interfaces;

//using System.Text.Json;
using Newtonsoft.Json;

//using System.Text.Json.Serialization;
using Newtonsoft.Json.Serialization;

using System.Collections.Generic;

namespace MatterialCore.Entity
{
    public class SearchResultEntry<T> : ListResultEntry<T>
        where T : ISearchable, new()
    {
#pragma warning disable CS8618 // Non-nullable field is uninitialized. Consider declaring as nullable.

        public SearchResultEntry()
        {
        }

#pragma warning restore CS8618 // Non-nullable field is uninitialized. Consider declaring as nullable.

        [JsonProperty("id")]
        public string? Id { get; internal set; }

        [JsonProperty("type")]
        public string? Type { get; internal set; }

        [JsonProperty("highlights")]
        public List<Highlight>? Highlights { get; internal set; }

        [JsonProperty("score")]
        public double? Score { get; internal set; }

        [JsonProperty("source")]
        public T Source { get; internal set; }
    }
}