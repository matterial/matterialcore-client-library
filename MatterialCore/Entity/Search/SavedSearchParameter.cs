﻿//using System.Text.Json.Serialization;
using MatterialCore.Interfaces;

using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace MatterialCore.Entity
{
    public class SavedSearchParameter : EntityBase, IEntity
    {
        public SavedSearchParameter()
        {
        }

        public SavedSearchParameter(long id)
        {
            Id = id;
        }

        [JsonProperty("id")]
        public long Id { get; internal set; }

        [JsonProperty("key")]
        public string? Key { get; set; }

        [JsonProperty("value")]
        public string? Value { get; set; }
    }
}