﻿//using System.Text.Json;
using Newtonsoft.Json;

//using System.Text.Json.Serialization;
using Newtonsoft.Json.Serialization;

using System;
using System.Collections.Generic;

namespace MatterialCore.Entity
{
    public class ListResult<T>
        where T : new()
    {
        public ListResult()
        {
        }

        internal Type ResultType
        {
            get
            {
                return typeof(T);
            }
        }

        /// <summary>
        /// The result list of the searched object.
        /// </summary>
        [JsonProperty("results")]
        public List<T>? Results { get; internal set; }

        [JsonProperty("totalHits")]
        public long TotalHits { get; internal set; }

        public string ToJson()
        {
            JsonSerializerSettings opt = new JsonSerializerSettings
            {
                Formatting = Formatting.None,
                NullValueHandling = NullValueHandling.Ignore
            };
            return JsonConvert.SerializeObject(this, this.GetType(), opt);
        }

        public Type GetMyType()
        {
            return typeof(ListResult<T>);
        }
    }
}