﻿using MatterialCore.Interfaces;

using Newtonsoft.Json;

//using System.Text.Json.Serialization;
using Newtonsoft.Json.Serialization;

using System.Collections.Generic;

namespace MatterialCore.Entity
{
    public class SearchResult<T> : ListResult<SearchResultEntry<T>>
        where T : ISearchable, new()
    {
        public SearchResult()
        {
        }

        [JsonProperty("aggregations")]
        public List<Aggregation>? Aggregations { get; internal set; }
    }
}