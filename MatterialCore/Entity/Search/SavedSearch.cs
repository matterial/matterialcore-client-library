﻿using MatterialCore.Enums;
using MatterialCore.Interfaces;

using Newtonsoft.Json;

//using System.Text.Json.Serialization;
using Newtonsoft.Json.Serialization;

using System.Collections.Generic;

namespace MatterialCore.Entity
{
    public class SavedSearch : EntityBase, IEntity
    {
        public SavedSearch()
        {
        }

        public SavedSearch(long id)
        {
            Id = id;
        }

        [JsonProperty("id")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Number, "Id of the object.")]
        public long Id { get; internal set; }

        [JsonProperty("dashboardPrio")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Number, "If &gt; 0 then the saved search will be displayed on the dashboard. The actual dashboard priority can be set using the prio function", Updateable = true)]
        public long DashboardPrio { get; set; }

        [JsonProperty("dashboardStyle")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.DashboardStyles, "The style of the dashboard: List=0, ListDark=1, ListLight=2, Columns=3, ColumnsDark=4, ColumnsLight=5, Grid=6, GridDark=7, GridLight=8", Updateable = true)]
        public DashboardStyles DashboardStyle { get; set; }

        [JsonProperty("name")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.String, "The name of the saved search.", Updateable = true)]
        public string? Name { get; set; }

        [JsonProperty("params")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.ListOfSavedSearchParameter, "The search parameters. This is a Key-Value list where each item specifies a query parameter key and its value of the search to be executed. See Document Search for details", Updateable = true)]
        public List<SavedSearchParameter>? Params { get; internal set; }

        [JsonProperty("personal")]
        public bool Personal { get; internal set; }

        [JsonProperty("roleIds")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.ListOfLong, "Set the ids of the roles. The permission EDIT_GLOBAL_SAVED_SEARCH is required to set the roleIds", Updateable = true)]
        public List<long>? RoleIds { get; set; }
    }
}