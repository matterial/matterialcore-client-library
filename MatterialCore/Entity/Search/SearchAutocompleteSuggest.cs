﻿//using System.Text.Json;
using MatterialCore.Interfaces;

using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace MatterialCore.Entity
{
    public class SearchAutocompleteSuggest : EntityBase, IEntity
    {
        public SearchAutocompleteSuggest()
        {
        }

        [JsonProperty("label")]
        public string? Label { get; internal set; }

        [JsonProperty("object")]
        public object? Object { get; internal set; }

        [JsonProperty("value")]
        public string? Value { get; internal set; }
    }
}