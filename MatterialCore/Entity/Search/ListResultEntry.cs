﻿using MatterialCore.Interfaces;

//using System.Text.Json
using Newtonsoft.Json;

namespace MatterialCore.Entity
{
    public abstract class ListResultEntry<T>
        where T : IListableEntity, new()
    {
        [JsonProperty("jsonType")]
        public string JsonType
        {
            get
            {
                return this.GetType().Name;
            }
        }

        public string ToJson()
        {
            var opt = new JsonSerializerSettings
            {
                Formatting = Formatting.None,
                NullValueHandling = NullValueHandling.Ignore
            };
            return JsonConvert.SerializeObject(this, this.GetType(), opt);
        }
    }
}