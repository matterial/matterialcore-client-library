﻿using MatterialCore.Enums;
using MatterialCore.Interfaces;

using Newtonsoft.Json;

//using System.Text.Json.Serialization;
using Newtonsoft.Json.Serialization;

namespace MatterialCore.Entity
{
    /// <summary>
    /// The tasks class. <seealso href="https://my.matterial.com/matterial/#/document/652/en?instanceId=1">API-Reference</seealso>
    /// </summary>
    public class MtrTask : EntityBase, IListableEntity, IRestEntity
    {
        public MtrTask()
        {
            TaskStatusId = TaskStatusId.Open;
        }

        public MtrTask(long id)
        {
            TaskStatusId = TaskStatusId.Open;
            Id = id;
        }

        [JsonProperty("id")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Number, Description = "The identifier of the object.")]
        public long Id { get; internal set; }

        [JsonProperty("description")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.String, "The description of the task.", Min = "1", Max = "255", Updateable = true, RequiredOnCreate = true)]
        public string? Description { get; set; }

        [JsonProperty("assignedRole")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Role, "The role assigned to the task.", Note = "This can be either a person or a group role.", Updateable = true, RequiredOnCreate = true)]
        public Role? AssignedRole { get; set; }

        [JsonProperty("documentId")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Number, "The document assigned to the task.", Note = "A task can be created without being assigned to a document but the general approach is to always assign a document to a task. Make sure, the correct documentLanguageVersionId is set.", Updateable = true)]
        public long? DocumentId { get; set; }

        [JsonProperty("documentLanguageVersionId")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Number, "The document language version assigned to the task.", Note = "When a task is assigned to a document, it has to be assigned to a specific language version. The documentLanguageVersionId must belong to the documentId specified.", Updateable = true)]
        public long? DocumentLanguageVersionId { get; set; }

        [JsonProperty("taskStatusId")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Number, "The id of the task status.", Updateable = true, RequiredOnCreate = true)]
        public TaskStatusId TaskStatusId { get; set; }

        [JsonProperty("snap")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Boolean, "True if the task was created by a Snap document (Knowledge Flash).", Updateable = true)]
        public bool Snap { get; set; }

        [JsonProperty("acceptedAccount")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Person, "The person which accepts the task. Accepting a task moves the task to the reponsibility of the person.", Updateable = true)]
        public Person? AcceptedAccount { get; set; }

        [JsonProperty("dueDateInSeconds")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.UnixTimeStamp, "The due date of the task in secoonds (Unix Time Stamp).")]
        public long? DueDateInSeconds { get; set; }

        [JsonProperty("jsonType")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.String)]
        public string JsonType
        {
            get
            {
                return "Task";
            }
        }

        [JsonProperty("authorAccount")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Person)]
        public Person? AuthorAccount { get; internal set; }

        [JsonProperty("creationDateInSeconds")]
        public long CreationDateInSeconds { get; internal set; }

        [JsonProperty("documentLanguageVersionLanguageKey")]
        public string? DocumentLanguageVersionLanguageKey { get; set; }

        [JsonProperty("documentLanguageVersionTitle")]
        public string? DocumentLanguageVersionTitle { get; set; }

        [JsonProperty("documentLanguageVersionVersion")]
        public long? DocumentLanguageVersionVersion { get; set; }

        [JsonProperty("resubmissionTimePeriod")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Number, "The interval for the resubmission task (in days or months).", Updateable = true, RequiredOnCreate = false, VersionImplemented = "2.5.0")]
        public int? ResubmissionTimePeriod { get; set; }

        //ToDo: Check when creating tasks about resubmission values
        [JsonProperty("resubmissionTimeUnit")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Number, "The unit: `0` days, `1` months.", Updateable = true, RequiredOnCreate = false, VersionImplemented = "2.5.0")]
        public ResubmissionTimeUnits? ResubmissionTimeUnit { get; set; }

        [JsonProperty("resubmissionDateInSeconds")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Number, "The next due date for a recurring task. First date could be set manually. All further dates are automatically calculated on action `resubmit`.", Updateable = true, RequiredOnCreate = false, VersionImplemented = "2.5.0")]
        public long? ResubmissionDateInSeconds { get; set; }

        internal static string RESTBasePath()
        {
            return ApiSession.CombineUri(Const.MTR_BACKEND, Const.APPLICATION_PATH, Const.TASK);
        }

        string IRestEntity.RESTBasePath()
        {
            return RESTBasePath();
        }
    }
}