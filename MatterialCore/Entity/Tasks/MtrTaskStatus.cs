﻿using MatterialCore.Enums;
using MatterialCore.Interfaces;

using Newtonsoft.Json;

namespace MatterialCore.Entity
{
    public class MtrTaskStatus : EntityBase, IEntity
    {
        public MtrTaskStatus()
        {
        }

        [JsonProperty("id")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Number, Description = "The identifier of the object.")]
        public TaskStatusId? Id { get; internal set; }

        [JsonProperty("description")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.String, Description = "The description of the object.", Min = "0", Max = "255")]
        public string? Description { get; internal set; }

        [JsonProperty("name")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.String, Description = "The name of the object.", Min = "0", Max = "255")]
        public string? Name { get; internal set; }
    }
}