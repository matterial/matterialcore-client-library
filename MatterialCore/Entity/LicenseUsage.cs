﻿using MatterialCore.Interfaces;

using Newtonsoft.Json;

namespace MatterialCore.Entity
{
    public class LicenseUsage : EntityBase, IEntity
    {
        public LicenseUsage()
        {
        }

        [JsonProperty("casSize")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Number, "The currentdocument storage sizeused.")]
        public long CasSize { get; internal set; }

        [JsonProperty("dataSourceActiveUntilInSeconds")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Number, "Unix Time Stamp in seconds until when the instance is active.", Updateable = true, RequiredOnCreate = true)]
        public long? InstanceActiveUntilInSeconds { get; internal set; }

        [JsonProperty("licence")]
        public License? Licence { get; internal set; }

        [JsonProperty("user")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Number, "Number of already licensed users.")]
        public long User { get; internal set; }
    }
}