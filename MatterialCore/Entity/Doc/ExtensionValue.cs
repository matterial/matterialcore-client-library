﻿using MatterialCore.Interfaces;

using Newtonsoft.Json;

//using System.Text.Json.Serialization;
using Newtonsoft.Json.Serialization;

namespace MatterialCore.Entity
{
    public class ExtensionValue : EntityBase, IListableEntity
    {
        public ExtensionValue()
        {
        }

        [JsonProperty("jsonType")]
        public string JsonType
        {
            get
            {
                return this.GetType().Name;
            }
        }

        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("key")]
        public string? Key { get; set; }

        [JsonProperty("type")]
        public long Type { get; set; }

        [JsonProperty("value")]
        public object? Value { get; set; }
    }
}