﻿using MatterialCore.Interfaces;

using Newtonsoft.Json;

//using System.Text.Json.Serialization;
using Newtonsoft.Json.Serialization;

using System.Collections.Generic;

namespace MatterialCore.Entity
{
    public class DocumentDuplicate : EntityBase, IEntity
    {
        public DocumentDuplicate()
        {
        }

        [JsonProperty("documents")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.ListOfDocument)]
        public List<Document>? Documents { get; internal set; }

        [JsonProperty("originId")]
        public long OriginId { get; internal set; }

        [JsonProperty("targetId")]
        public long TargetId { get; internal set; }

        [JsonProperty("tempFileDescriptors")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.ListOfTempFileDescriptor)]
        public List<TempFileDescriptor>? TempFileDescriptors { get; internal set; }

        [JsonProperty("contextToken")]
        public string? ContextToken { get; internal set; }
    }
}