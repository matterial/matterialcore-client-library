﻿using MatterialCore.Interfaces;

using Newtonsoft.Json;

//using System.Text.Json;
using Newtonsoft.Json.Serialization;

using System.Collections.Generic;

namespace MatterialCore.Entity
{
    public class Comment : EntityBase, IRestEntity
    {
        public Comment()
        {
        }

        public Comment(long id)
        {
            Id = id;
        }

        string IRestEntity.RESTBasePath()
        {
            return RESTBasePath();
        }

        internal static string RESTBasePath()
        {
            return ApiSession.CombineUri(Const.MTR_BACKEND, Const.APPLICATION_PATH, Const.COMMENT);
        }

        [JsonProperty("id")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Number, Description = "The identifier of the object.")]
        public long Id { get; internal set; }

        [JsonProperty("text")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.String, Description = "The text of the comment.", Min = "0", Max = "4000", Updateable = true)]
        public string? Text { get; set; }

        [JsonProperty("documentLanguageVersionId")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Number, Description = "The documentLanguageVersionId of the document to which the comment is linked to.", RequiredOnCreate = true, Updateable = true)]
        public long DocumentLanguageVersionId { get; internal set; }

        [JsonProperty("person")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Person, Description = "The person who created the comment.")]
        public Person? Person { get; internal set; }

        [JsonProperty("documentId")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Number, Description = "The documentId of the document to which the comment is linked to.")]
        public long DocumentId { get; internal set; }

        [JsonProperty("documentLanguageVersionLanguageKey")]
        public string? DocumentLanguageVersionLanguageKey { get; internal set; }

        [JsonProperty("documentLanguageVersionTitle")]
        public string? DocumentLanguageVersionTitle { get; internal set; }

        [JsonProperty("documentLanguageVersionVersion")]
        public long DocumentLanguageVersionVersion { get; internal set; }

        [JsonProperty("createTimeInSeconds")]
        public long CreateTimeInSeconds { get; internal set; }

        [JsonProperty("mentionedAccountIds")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.ListOfLong)]
        public List<long>? MentionedAccountIds { get; internal set; }

        [JsonProperty("textHtml")]
        public string? TextHtml { get; internal set; }

        [JsonProperty("jsonType")]
        public string JsonType
        {
            get
            {
                return this.GetType().Name;
            }
        }
    }
}