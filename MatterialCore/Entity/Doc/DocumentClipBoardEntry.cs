﻿//using System.Text.Json.Serialization;
using MatterialCore.Interfaces;

using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace MatterialCore.Entity
{
    public class DocumentClipBoardEntry : EntityBase, IEntity
    {
        public DocumentClipBoardEntry()
        {
        }

        public DocumentClipBoardEntry(long documentId, long languageVersionLanguageId)
        {
            DocumentId = documentId;
            LanguageVersionLanguageId = languageVersionLanguageId;
        }

        [JsonProperty("id")]
        public long DocumentId { get; internal set; }

        [JsonProperty("languageVersionLanguageId")]
        public long LanguageVersionLanguageId { get; internal set; }
    }
}