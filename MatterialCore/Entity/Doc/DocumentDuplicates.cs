﻿using MatterialCore.Interfaces;

using Newtonsoft.Json;

//using System.Text.Json.Serialization;
using Newtonsoft.Json.Serialization;

using System.Collections.Generic;

namespace MatterialCore.Entity
{
    public class DocumentDuplicates : EntityBase, IEntity
    {
        public DocumentDuplicates()
        {
        }

        [JsonProperty("map")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.DictionaryStringDocumentDuplicate, "A key-value list containing the document duplicates. key: contextToken, value: DocumentDuplicate, which represents a duplicated multi-language-document.")]
        public Dictionary<string, DocumentDuplicate>? Map { get; internal set; }
    }
}