﻿using MatterialCore.Controller;
using MatterialCore.Enums;
using MatterialCore.Interfaces;

using Newtonsoft.Json;

//using System.Text.Json.Serialization;
using Newtonsoft.Json.Serialization;

using System.Collections.Generic;

namespace MatterialCore.Entity
{
    public class Document : DocumentBase, ISearchable
    {
        public Document()
        {
            Valid = true;
            LanguageVersionLanguageKey = Matterial.DefaultLanguage.ToLangString();
            RoleRights = new List<RoleRight>();
        }

        public Document(LangKey language)
        {
            Valid = true;
            LanguageVersionLanguageKey = language.ToLangString();
            RoleRights = new List<RoleRight>();
        }

        public Document(string languageVersionTitle, LangKey? language)
        {
            LanguageVersionTitle = languageVersionTitle;
            if (!(language is null))
            {
                LanguageVersionLanguageKey = language.KeyAndValue().Value;
            }
            Valid = true;
            RoleRights = new List<RoleRight>();
        }

        public Document(long id)
        {
            Valid = true;
            LanguageVersionLanguageKey = Matterial.DefaultLanguage.ToLangString();
            RoleRights = new List<RoleRight>();
            Id = id;
        }

        public Document(long id, long languageVersionId)
        {
            Valid = true;
            LanguageVersionLanguageKey = Matterial.DefaultLanguage.ToLangString();
            RoleRights = new List<RoleRight>();
            Id = id;
            LanguageVersionId = languageVersionId;
        }

        public Document(long id, long languageVersionId, string languageVersionTitle, LangKey? language)
        {
            LanguageVersionTitle = languageVersionTitle;
            LanguageVersionLanguageKey = Matterial.DefaultLanguage.ToLangString();
            if (!(language is null))
            {
                LanguageVersionLanguageKey = language.KeyAndValue().Value;
            }
            Valid = true;
            RoleRights = new List<RoleRight>();
            Id = id;
            LanguageVersionId = languageVersionId;
        }

        internal IDocument? GetController(string sessionId, string basePath)
        {
            return new Documents(sessionId, basePath);
        }

        internal static string RESTBasePath()
        {
            return ApiSession.CombineUri(Const.MTR_BACKEND, Const.APPLICATION_PATH, Const.DOCUMENT);
        }

        string IRestEntity.RESTBasePath()
        {
            return RESTBasePath();
        }

        [JsonProperty("languageVersionId")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Number, "Internal unique id of the language version. This id identifies the document uniquely in the system.")]
        public long LanguageVersionId { get; internal set; }

        [JsonProperty("languageVersionTitle")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.String, "The title of the document language version. This title is the one visible as document title in the Browser UI.", Updateable = true, RequiredOnCreate = true)]
        public string? LanguageVersionTitle { get; set; }

        [JsonProperty("languageVersionAbstract")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.String, "The abstract (description) of the document.", Updateable = true)]
        public string? LanguageVersionAbstract { get; set; }

        [JsonProperty("languageVersionLanguageKey")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.String, "The language key in two-string format (en, de, it, fr, etc.)", Updateable = true)]
        public string? LanguageVersionLanguageKey { get; set; }

        [JsonProperty("validBeginInSeconds")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Number, "Unix Time Stamp in seconds until when the document will start to be valid (if \"valid\" is true).", Updateable = true)]
        public long? ValidBeginInSeconds { get; set; }

        [JsonProperty("validEndInSeconds")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Number, "Unix Time Stamp in seconds until when the document will end to be valid (if \"valid\" is true).", Updateable = true)]
        public long? ValidEndInSeconds { get; set; }

        [JsonProperty("valid")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Boolean, "If true, \"validBeginInSeconds\" and \"validEndInSeconds\" will be checked for validity of the document.", Updateable = true)]
        public bool Valid { get; set; }

        [JsonProperty("landscape")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Boolean, "If true, the document will be displayed in landscape mode.", Updateable = true)]
        public bool Landscape { get; set; }

        [JsonProperty("additionalProperties")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.ListOfAdditionalProperties, Updateable = true)]
        public List<AdditionalProperty>? AdditionalProperties { get; set; }

        [JsonProperty("languageVersionLanguageId")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Number, "Id of the language of the document.")]
        public long LanguageVersionLanguageId { get; internal set; }

        [JsonProperty("archived")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Boolean, "True if the document is archived.", Updateable = true)]
        public bool Archived { get; set; }

        [JsonProperty("archivedBeginInSeconds")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Number, "Unix Time Stamp from when on the document will be archived (if \"archived\" is true).", Updateable = true)]
        public long? ArchivedBeginInSeconds { get; internal set; }

        [JsonProperty("attachments")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.ListOfDocument, Updateable = true)]
        public List<Attachment>? Attachments { get; set; }

        [JsonProperty("categories")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.ListOfCategory, Updateable = true)]
        public List<Category>? Categories { get; set; }

        [JsonProperty("comments")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.ListOfComment)]
        public List<Comment>? Comments { get; internal set; }

        [JsonProperty("createTimeInSeconds")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Number, "Unix Time Stamp when the document was created.")]
        public long? CreateTimeInSeconds { get; internal set; }

        [JsonProperty("documentAttachments")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.ListOfAttachment)]
        public List<Attachment>? DocumentAttachments { get; internal set; }

        [JsonProperty("editorType")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Number, "Type of editor used to create the document.", Updateable = true)]
        public EditorTypes EditorType { get; internal set; }

        [JsonProperty("followers")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.ListOfPerson, "Persons following the document")]
        public List<Person>? Followers { get; internal set; }

        [JsonProperty("languageAttachments")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.ListOfAttachment, "Attachments of the language defined when loading the document.")]
        public List<Attachment>? LanguageAttachments { get; internal set; }

        [JsonProperty("languageVersionCasId")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.String, "Internal id of the CAS (Content Addressable Storage)")]
        public string? LanguageVersionCasId { get; internal set; }

        [JsonProperty("languageVersionCasIdPdf")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.String, "Internal id of the CAS (Content Addressable Storage) of the auto-generated PDF file.")]
        public string? LanguageVersionCasIdPdf { get; internal set; }

        [JsonProperty("languageVersionCasIdThumbnail")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.String, "Internal id of the CAS (Content Addressable Storage) of the auto-generated thumbnail file.")]
        public string? LanguageVersionCasIdThumbnail { get; internal set; }

        [JsonProperty("languageVersionCasMd5")]
        public string? LanguageVersionCasMd5 { get; internal set; }

        [JsonProperty("languageVersionCreateTimeInSeconds")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Number, "Unix Time Stamp when the document language version was created.")]
        public long? LanguageVersionCreateTimeInSeconds { get; internal set; }

        [JsonProperty("languageVersionCurrentlyInProcessing")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Boolean, "If true, this document is the newest language version of the document.")]
        public bool LanguageVersionCurrentlyInProcessing { get; internal set; }

        [JsonProperty("languageVersionFileSize")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Number, "The size in bytes of the documents text (main file)html or markdown).")]
        public long? LanguageVersionFileSize { get; internal set; }

        [JsonProperty("languageVersionFileSizePdf")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Number, "The size in bytes of the auto-generated pdf file.")]
        public long? LanguageVersionFileSizePdf { get; internal set; }

        [JsonProperty("languageVersionFileSizeThumbnail")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Number, "The size in bytes of the auto-generated thumbnail file.")]
        public long? LanguageVersionFileSizeThumbnail { get; internal set; }

        [JsonProperty("languageVersionLastChangeInSeconds")]
        public long? LanguageVersionLastChangeInSeconds { get; internal set; }

        [JsonProperty("languageVersionMimeType")]
        public string? LanguageVersionMimeType { get; internal set; }

        [JsonProperty("languageVersionReady")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Boolean, "If true, this language version cannot be changed anymore.")]
        public bool LanguageVersionReady { get; internal set; }

        [JsonProperty("languageVersionReviewed")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Boolean, "If true, this language version is reviewed.")]
        public bool LanguageVersionReviewed { get; internal set; }

        [JsonProperty("languageVersionStatus")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Number, "Bit mask of the status.")]
        public long LanguageVersionStatus { get; internal set; }

        [JsonProperty("languageVersionVersion")]
        public long LanguageVersionVersion { get; internal set; }

        [JsonProperty("markedAsHelpfulBy")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.ListOfPerson)]
        public List<Person>? MarkedAsHelpfulBy { get; internal set; }

        [JsonProperty("mentionedInCommentUnread")]
        public bool MentionedInCommentUnread { get; internal set; }

        [JsonProperty("relatedDocumentIds")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.ListOfLong, Updateable = true)]
        public List<long>? RelatedDocumentIds { get; set; }

        [JsonProperty("removed")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Boolean, "If true, the document is in the trash.", Updateable = true)]
        public bool Removed { get; set; }

        [JsonProperty("firstReadTimesInSeconds")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.DictionaryLongLong, "A key-value list containing the first read time stamps in seconds (Unix Time Stamp). The key of the map is the account id.")]
        public Dictionary<long, long>? FirstReadTimesInSeconds { get; internal set; }

        [JsonProperty("lastReadTimesInSeconds")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.DictionaryLongLong, "A key-value list containing the last read time stamps in seconds (Unix Time Stamp). The key of the map is the account id.")]
        public Dictionary<long, long>? LastReadTimesInSeconds { get; internal set; }

        [JsonProperty("lastWriteTimesInSeconds")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.DictionaryLongLong, "A key-value list containing the last write time stamps in seconds (Unix Time Stamp). The key of the map is the account id.")]
        public Dictionary<long, long>? LastWriteTimesInSeconds { get; internal set; }

        [JsonProperty("languageVersionValidBeginInSeconds")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Number, "Unix Time Stamp in seconds when the document language version will be valid.")]
        public long? LanguageVersionValidBeginInSeconds { get; internal set; }

        [JsonProperty("languageVersionValidEndInSeconds")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Number, "Unix Time Stamp in seconds when the document language version will be invalid.")]
        public long? LanguageVersionValidEndInSeconds { get; internal set; }

        [JsonProperty("removeTimeInSeconds")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Number, "Unix Time Stamp in seconds when the document was removed.")]
        public long? RemoveTimeInSeconds { get; internal set; }

        [JsonProperty("reviewUntilInSeconds")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Number, "Unix Time Stamp in seconds until when the document should be reviewed (but currently unused).", Updateable = true)]
        public long? ReviewUntilInSeconds { get; set; }

        [JsonProperty("responsibles")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.ListOfPerson, "Person(s) responsible for the document.", Updateable = true)]
        public List<Person>? Responsibles { get; set; }

        [JsonProperty("roleRelationType")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Number, "Can be set to Read=1 oder EDIT=2 depending on the permission of the current user.")]
        public long RoleRelationType { get; internal set; }

        [JsonProperty("roleRights")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.ListOfRoleRight, "The list of role rights to this document. This defines who has READ or EDIT permissions to the document.", Updateable = true)]
        public List<RoleRight> RoleRights { get; set; }

        [JsonProperty("reviewRight")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Boolean, "If true, the document can be reviewed by the current user.")]
        public bool ReviewRight { get; internal set; }

        [JsonProperty("languageVersionReviewRequested")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Boolean, "If true, a review is requested for this language version.")]
        public bool LanguageVersionReviewRequested { get; internal set; }

        [JsonProperty("snap")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Boolean, "If true, the document is a snap (Knowledge Flash).")]
        public bool Snap { get; internal set; }

        [JsonProperty("template")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Boolean, "If true, the document is a template.")]
        public bool Template { get; internal set; }

        [JsonProperty("bio")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Boolean, "If true, the document is a person's biography.")]
        public bool Bio { get; internal set; }

        [JsonProperty("successorId")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Boolean, "The successor id of this document (currently not used).", Updateable = true)]
        public long? SuccessorId { get; set; }

        [JsonProperty("sumRating")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Number, "Rating of the document.")]
        public long? SumRating { get; internal set; }

        [JsonProperty("extensionValues")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.ListOfExtensionValue, "List of extension values.", Note = "Extension values are currently not supported by the standard web UI (v.2.3)")]
        public List<ExtensionValue>? ExtensionValues { get; set; }

        [JsonProperty("languageVersionCustomVersionString")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.String, "The custom definable version number of the document.", Updateable = true, RequiredOnCreate = false, VersionImplemented = "2.5.0")]
        public string? LanguageVersionCustomVersionString { get; internal set; }

        [JsonProperty("readConfirmationTimesInSeconds")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.DictionaryLongLong, "Map with key: `accountId` and value: `the confirmation time in seconds of that account`", Updateable = false, RequiredOnCreate = false, VersionImplemented = "2.5.0")]
        public Dictionary<long, long>? ReadConfirmationTimesInSeconds { get; internal set; }

        [JsonProperty("roleRelationReadConfirmationRequested")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Boolean, "At least one of *my* roles are requested for reading confirmation.`", Updateable = false, RequiredOnCreate = false, VersionImplemented = "2.5.0")]
        public bool RoleRelationReadConfirmationRequested { get; internal set; }

        [JsonProperty("readConfirmationActive")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Boolean, "At least one of the related roles are set with `readConfirmationRequested=true` (overall)", Updateable = false, RequiredOnCreate = false, VersionImplemented = "2.5.0")]
        public bool ReadConfirmationActive { get; internal set; }


        [JsonProperty("jsonType")]
        public string JsonType
        {
            get
            {
                return this.GetType().Name;
            }
        }
    }
}