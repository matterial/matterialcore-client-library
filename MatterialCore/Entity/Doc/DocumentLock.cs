﻿//using System.Text.Json.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace MatterialCore.Entity
{
    public class DocumentLock : DocumentBase
    {
        public DocumentLock()
        {
        }

        [JsonProperty("documentId")]
        public long DocumentId { get; internal set; }

        [JsonProperty("lockedByMyself")]
        public bool LockedByMyself { get; internal set; }

        [JsonProperty("lockedForMe")]
        public bool LockedForMe { get; internal set; }

        [JsonProperty("lockTimestampInSeconds")]
        public long LockTimestampInSeconds { get; internal set; }

        [JsonProperty("person")]
        public Person? Person { get; internal set; }

        [JsonProperty("previousAccountId")]
        public long PreviousAccountId { get; internal set; }

        [JsonProperty("sessionId")]
        public string? SessionId { get; internal set; }
    }
}