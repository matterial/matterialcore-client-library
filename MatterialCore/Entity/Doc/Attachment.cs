﻿//using System.Text.Json;
using MatterialCore.Interfaces;

using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace MatterialCore.Entity
{
    public class Attachment : EntityBase, IEntity
    {
        public Attachment()
        {
        }

        public Attachment(long id)
        {
            Id = id;
        }

        [JsonProperty("id")]
        public long Id { get; internal set; }

        [JsonProperty("name")]
        public string? Name { get; set; }

        [JsonProperty("status")]
        public long Status { get; set; }

        [JsonProperty("mimeType")]
        public string? MimeType { get; set; }

        [JsonProperty("casId")]
        public string? CasId { get; internal set; }

        [JsonProperty("casIdImgLarge")]
        public string? CasIdImgLarge { get; internal set; }

        [JsonProperty("casIdImgSmall")]
        public string? CasIdImgSmall { get; internal set; }

        [JsonProperty("casIdPdf")]
        public string? CasIdPdf { get; internal set; }

        [JsonProperty("casIdThumbnail")]
        public string? CasIdThumbnail { get; internal set; }

        [JsonProperty("createTimeInSeconds")]
        public long CreateTimeInSeconds { get; internal set; }

        [JsonProperty("displayName")]
        public string? DisplayName { get; set; }

        [JsonProperty("fileSize")]
        public long? FileSize { get; internal set; }

        [JsonProperty("fileSizeImgLarge")]
        public long? FileSizeImgLarge { get; internal set; }

        [JsonProperty("fileSizeImgSmall")]
        public long? FileSizeImgSmall { get; internal set; }

        [JsonProperty("fileSizePdf")]
        public long? FileSizePdf { get; internal set; }

        [JsonProperty("fileSizeThumbnail")]
        public long? FileSizeThumbnail { get; internal set; }
    }
}