﻿using MatterialCore.Interfaces;

using Newtonsoft.Json;

//using System.Text.Json.Serialization;
using Newtonsoft.Json.Serialization;

using System.Collections.Generic;

namespace MatterialCore.Entity
{
    public class DocumentChangeLogs : EntityBase, IEntity
    {
        public DocumentChangeLogs()
        {
        }

        [JsonProperty("documentChangeLog")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.ListOfDocumentChangeLog)]
        public List<DocumentChangeLog>? DocumentChangeLog { get; internal set; }
    }
}