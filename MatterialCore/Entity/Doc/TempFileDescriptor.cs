﻿//using System.Text.Json.Serialization;
using MatterialCore.Interfaces;

using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace MatterialCore.Entity
{
    public class TempFileDescriptor : EntityBase, IRestEntity
    {
        public TempFileDescriptor()
        {
        }

        [JsonProperty("contextToken")]
        public string? ContextToken { get; internal set; }

        [JsonProperty("displayName")]
        public string? DisplayName { get; internal set; }

        [JsonProperty("fileName")]
        public string? FileName { get; internal set; }

        [JsonProperty("fileSize")]
        public long FileSize { get; internal set; }

        [JsonProperty("fileToken")]
        public string? FileToken { get; internal set; }

        [JsonProperty("fileType")]
        public string? FileType { get; internal set; }

        [JsonProperty("languageKey")]
        public string? LanguageKey { get; internal set; }

        [JsonProperty("mimeType")]
        public string? MimeType { get; internal set; }

        [JsonProperty("tempFileName")]
        public string? TempFileName { get; internal set; }

        string IRestEntity.RESTBasePath()
        {
            return RESTBasePath();
        }

        internal static string RESTBasePath()
        {
            return ApiSession.CombineUri(Const.MTR_BACKEND, Const.APPLICATION_PATH, Const.TEMP_FILE);
        }
    }
}