﻿//using System.Text.Json;

using MatterialCore.Enums;
using MatterialCore.Interfaces;

using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace MatterialCore.Entity
{
    public class AdditionalProperty : EntityBase, IListableEntity
    {
        public AdditionalProperty()
        {
        }

        public AdditionalProperty(long id)
        {
            Id = id;
        }

        public AdditionalProperty(string name)
        {
            Name = name;
        }

        public AdditionalProperty(string name, string description)
        {
            Name = name;
            Description = description;
        }

        [JsonProperty("id")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Number, Description = "The identifier of the object.")]
        public long Id { get; internal set; }

        [JsonProperty("name")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.String, "", Updateable = true, RequiredOnCreate = true, Min = "1", Max = "255")]
        public string? Name { get; set; }

        [JsonProperty("description")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.String, "", Updateable = true, RequiredOnCreate = true, Min = "0", Max = "4000")]
        public string? Description { get; set; }

        [JsonProperty("following")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Boolean, "Indicates, if the current user follows the additional property in the specified language. Requires the parameter \"loadFollowingLanguageKey\" when loading the additional property.")]
        public bool? Following { get; internal set; }

        [JsonProperty("propertyType")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Number, "The type of the additional property.")]
        public AdditionalPropertyTypes PropertyType { get; internal set; }

        [JsonProperty("validBeginInSeconds")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Number, "Unix time stamp in seconds when the additional property will be valid.")]
        public long? ValidBeginInSeconds { get; internal set; }

        [JsonProperty("validEndInSeconds")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Number, "Unix time stamp in seconds when the additional property will not be valid anymore.")]
        public long? ValidEndInSeconds { get; internal set; }

        public string RESTBasePath()
        {
            return ApiSession.CombineUri(Const.MTR_BACKEND, Const.APPLICATION_PATH, Const.DOCUMENT, Const.ADDITIONAL_PROPERTY);
        }

        [JsonProperty("jsonType")]
        public string JsonType
        {
            get
            {
                return this.GetType().Name;
            }
        }
    }
}