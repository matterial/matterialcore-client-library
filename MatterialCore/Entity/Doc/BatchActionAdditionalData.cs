﻿using Newtonsoft.Json;

using System.Collections.Generic;

namespace MatterialCore.Entity
{
    [MtrClass("The BatchActionAdditionalData is the class that is used to execute batch actions on documents. " +
        "Each property of the class can contain elements that the batch process can update.")]
    public class BatchActionAdditionalData
    {
        public BatchActionAdditionalData()
        { }

        [JsonProperty("additionalProperties")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.ListOfAdditionalProperties, "The list of additional properties a document should get.")]
        public List<AdditionalProperty>? AdditionalProperties { get; set; }

        [JsonProperty("archivedBeginInSecondsRequest")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Number, "The start time stamp (Unix Time Stamp) when the document will be archived.")]
        public long? ArchivedBeginInSecondsRequest { get; set; }

        [JsonProperty("categoryIds")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.ListOfLong, "The list of category ids the document should be assigned to.")]
        public List<long>? CategoryIds { get; set; }
    }
}