﻿using MatterialCore.Interfaces;

using Newtonsoft.Json;

//using System.Text.Json.Serialization;
using Newtonsoft.Json.Serialization;

using System.Collections.Generic;

namespace MatterialCore.Entity
{
    public class DocumentChangeLogMap : EntityBase, IEntity
    {
        public DocumentChangeLogMap()
        {
        }

        [JsonProperty("map")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.DictionaryLongDocumentChangeLogs, "The document change log map. The key of the map is the start of each day in seconds (Unix Time Stamp).")]
        public Dictionary<long, DocumentChangeLogs>? Map { get; internal set; }

        [JsonProperty("totalHits")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Number, "The number of hits.")]
        public long TotalHits { get; internal set; }
    }
}