﻿//using System.Text.Json.Serialization;
using MatterialCore.Interfaces;

using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace MatterialCore.Entity
{
    public class DocumentForPdfConversion : EntityBase, IEntity
    {
        public DocumentForPdfConversion()
        {
        }

        [JsonProperty("document")]
        public Document? Document { get; internal set; }

        [JsonProperty("mainFile")]
        public string? MainFile { get; internal set; }

        [JsonProperty("lastAuthor")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.String, "The last author of the document (version).", Updateable = false, RequiredOnCreate = false, VersionImplemented = "2.5.0")]
        public string? LastAuthor { get; internal set; }

        [JsonProperty("publishingDate")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.String, "The publishing date (formatted string) of the document (version).", Updateable = false, RequiredOnCreate = false, VersionImplemented = "2.5.0")]
        public string? PublishingDate { get; internal set; }

        [JsonProperty("reviewer")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.String, "The reviewer of the document (version).", Updateable = false, RequiredOnCreate = false, VersionImplemented = "2.5.0")]
        public string? Reviewer { get; internal set; }

        [JsonProperty("showLastAuthor")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.String, "Hint for the pdf generation to show the last author.", Updateable = false, RequiredOnCreate = false, VersionImplemented = "2.5.0")]
        public bool ShowLastAuthor { get; internal set; }

        [JsonProperty("showVersion")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.String, "Hint for the pdf generation to show the version.", Updateable = false, RequiredOnCreate = false, VersionImplemented = "2.5.0")]
        public bool ShowVersion { get; internal set; }

        [JsonProperty("showPublishingDate")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.String, "Hint for the pdf generation to show the publishing date.", Updateable = false, RequiredOnCreate = false, VersionImplemented = "2.5.0")]
        public bool ShowPublishingDate { get; internal set; }

        [JsonProperty("showReviewer")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.String, "Hint for the pdf generation to show the reviewer.", Updateable = false, RequiredOnCreate = false, VersionImplemented = "2.5.0")]
        public bool ShowReviewer { get; internal set; }

    }
}