﻿using MatterialCore.Interfaces;

using Newtonsoft.Json;

//using System.Text.Json.Serialization;
using Newtonsoft.Json.Serialization;

namespace MatterialCore.Entity
{
    public class DocumentChangeLog : EntityBase, IListableEntity
    {
        public DocumentChangeLog()
        {
        }
        [JsonProperty("jsonType")]
        public string JsonType
        {
            get
            {
                return this.GetType().Name;
            }
        }

        [JsonProperty("id")]
        public long Id { get; internal set; }

        [JsonProperty("action")]
        public long Action { get; internal set; }

        [JsonProperty("changeDateInSeconds")]
        public long? ChangeDateInSeconds { get; internal set; }

        [JsonProperty("dlvAbstract")]
        public bool DlvAbstract { get; internal set; }

        [JsonProperty("dlvAttachment")]
        public bool DlvAttachment { get; internal set; }

        [JsonProperty("dlvExtensionValue")]
        public bool DlvExtensionValue { get; internal set; }

        [JsonProperty("dlvMainFile")]
        public bool DlvMainFile { get; internal set; }

        [JsonProperty("dlvPublished")]
        public bool DlvPublished { get; internal set; }

        [JsonProperty("dlvReviewRequestDeclined")]
        public bool DlvReviewRequestDeclined { get; internal set; }

        [JsonProperty("dlvReviewRequested")]
        public bool DlvReviewRequested { get; internal set; }

        [JsonProperty("dlvReviewRequestGranted")]
        public bool DlvReviewRequestGranted { get; internal set; }

        [JsonProperty("dlvReviewUnrequested")]
        public bool DlvReviewUnrequested { get; internal set; }

        [JsonProperty("dlvTitle")]
        public bool DlvTitle { get; internal set; }

        [JsonProperty("dlvValidBegin")]
        public bool DlvValidBegin { get; internal set; }

        [JsonProperty("dlvValidEnd")]
        public bool DlvValidEnd { get; internal set; }

        [JsonProperty("docAdditionalProperty")]
        public bool DocAdditionalProperty { get; internal set; }

        [JsonProperty("docArchivedBegin")]
        public bool DocArchivedBegin { get; internal set; }

        [JsonProperty("docArchivedFalse")]
        public bool DocArchivedFalse { get; internal set; }

        [JsonProperty("docArchivedTrue")]
        public bool DocArchivedTrue { get; internal set; }

        [JsonProperty("docCategory")]
        public bool DocCategory { get; internal set; }

        [JsonProperty("docLandscapeFalse")]
        public bool DocLandscapeFalse { get; internal set; }

        [JsonProperty("docLandscapeTrue")]
        public bool DocLandscapeTrue { get; internal set; }

        [JsonProperty("docLanguageRemoved")]
        public bool DocLanguageRemoved { get; internal set; }

        [JsonProperty("docPermission")]
        public bool DocPermission { get; internal set; }

        [JsonProperty("docResponsible")]
        public bool DocResponsible { get; internal set; }

        [JsonProperty("docReviewUntil")]
        public bool DocReviewUntil { get; internal set; }

        [JsonProperty("docTrashedFalse")]
        public bool DocTrashedFalse { get; internal set; }

        [JsonProperty("docTrashedTrue")]
        public bool DocTrashedTrue { get; internal set; }

        [JsonProperty("documentId")]
        public long DocumentId { get; internal set; }

        [JsonProperty("docValidBegin")]
        public bool DocValidBegin { get; internal set; }

        [JsonProperty("docValidEnd")]
        public bool DocValidEnd { get; internal set; }

        [JsonProperty("docValidFalse")]
        public bool DocValidFalse { get; internal set; }

        [JsonProperty("docValidTrue")]
        public bool DocValidTrue { get; internal set; }

        [JsonProperty("docVersionRemoved")]
        public bool DocVersionRemoved { get; internal set; }

        [JsonProperty("languageVersionId")]
        public long? LanguageVersionId { get; internal set; }

        [JsonProperty("languageVersionLanguageId")]
        public long? LanguageVersionLanguageId { get; internal set; }

        [JsonProperty("languageVersionLanguageKey")]
        public string? LanguageVersionLanguageKey { get; internal set; }

        [JsonProperty("languageVersionVersion")]
        public int? LanguageVersionVersion { get; internal set; }

        [JsonProperty("person")]
        public Person? Person { get; internal set; }

        [JsonProperty("languageVersionCustomVersionString")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.String, "The version comment of the related document (language-version).", Updateable = false, RequiredOnCreate = false, VersionImplemented = "2.5.0")]
        public string? LanguageVersionCustomVersionString { get; internal set; }

        [JsonProperty("languageVersionVersionComment")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.String, "The custom version number of the related document (language-version).", Updateable = false, RequiredOnCreate = false, VersionImplemented = "2.5.0")]
        public string? LanguageVersionVersionComment { get; internal set; }

        [JsonProperty("dlvVersionComment")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Boolean, "`true`, if version comment was changed.", Updateable = false, RequiredOnCreate = false, VersionImplemented = "2.5.0")]
        public bool DlvVersionComment { get; internal set; }

        [JsonProperty("dlvCustomVersionString")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Boolean, "`true`, if custom version string was changed.", Updateable = false, RequiredOnCreate = false, VersionImplemented = "2.5.0")]
        public bool DlvCustomVersionString { get; internal set; }


    }
}