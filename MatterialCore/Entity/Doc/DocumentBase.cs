﻿using MatterialCore.Interfaces;

using Newtonsoft.Json;

namespace MatterialCore.Entity
{
    public class DocumentBase : EntityBase, IEntity
    {
        [JsonProperty("id")]
        [MtrProperty(MtrPropertyAttribute.MtrDataTypes.Number, "Internal id of the object.")]
        public long Id { get; internal set; }
    }
}