﻿using MatterialCore.Interfaces;

using Newtonsoft.Json;

namespace MatterialCore.Entity
{
    public class TrackingItem : EntityBase, ISearchable
    {
        public TrackingItem()
        {
        }

        [JsonProperty("id")]
        public long? Id { get; internal set; }

        [JsonProperty("actionId")]
        public string? ActionId { get; internal set; }

        [JsonProperty("actionType")]
        public string? ActionType { get; internal set; }

        [JsonProperty("appContext")]
        public string? AppContext { get; internal set; }

        [JsonProperty("appInstance")]
        public string? AppInstance { get; internal set; }

        [JsonProperty("appType")]
        public string? AppType { get; internal set; }

        [JsonProperty("changeLog")]
        public string? ChangeLog { get; internal set; }

        [JsonProperty("creationDateInSeconds")]
        public long? CreationDateInSeconds { get; internal set; }

        [JsonProperty("objectId")]
        public long? ObjectId { get; internal set; }

        [JsonProperty("objectRef")]
        public string? ObjectRef { get; internal set; }

        [JsonProperty("sessionId")]
        public string? SessionId { get; internal set; }

        [JsonProperty("userId")]
        public long? UserId { get; internal set; }

        internal static string RESTBasePath()
        {
            return ApiSession.CombineUri(Const.MTR_BACKEND, Const.APPLICATION_PATH, Const.TRACKING);
        }

        string IRestEntity.RESTBasePath()
        {
            return RESTBasePath();
        }

        [JsonProperty("jsonType")]
        public string JsonType
        {
            get
            {
                return this.GetType().Name;
            }
        }
    }
}