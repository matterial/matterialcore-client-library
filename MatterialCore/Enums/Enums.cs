﻿namespace MatterialCore.Enums
{
    public enum DocumentContentMediaType : int
    {
        Markdown = 0,
        Html = 1
    }

    public enum EditorTypes : int
    {
        MARKDOWN = 0,
        WYSIWYG = 1
    }

    public enum DocLoadOrderBy : int
    {
        /// <summary>
        /// Title
        /// </summary>
        LanguageVersionTitle = 0,

        /// <summary>
        /// Document-id
        /// </summary>
        Id = 1,

        /// <summary>
        /// Document-valid
        /// </summary>
        Valid = 2,

        /// <summary>
        /// Document-archived
        /// </summary>
        Archived = 3,

        /// <summary>
        /// Archived begin
        /// </summary>
        ArchivedBeginInSeconds = 4,

        /// <summary>
        /// Document removed
        /// </summary>
        Removed = 5,

        /// <summary>
        /// Number of document clicks
        /// </summary>
        ClickCount = 6,

        /// <summary>
        /// End of valid
        /// </summary>
        ValidEndInSeconds = 7,

        /// <summary>
        /// Creation time stamp
        /// </summary>
        CreateTimeInSeconds = 8,

        /// <summary>
        /// Valid begin or created time stamp
        /// </summary>
        ValidBeginOrCreateTime = 9,

        /// <summary>
        /// First read time
        /// </summary>
        FirstReadTimeAndValidBegin = 10,

        /// <summary>
        /// Document language priority
        /// </summary>
        LanguageVersionLanguagePrio = 11,

        /// <summary>
        /// Document language Version
        /// </summary>
        LanguageVersionVersion = 12,

        /// <summary>
        /// Last read time stamp
        /// </summary>
        LastReadTime = 13,

        /// <summary>
        /// Last change time stamp
        /// </summary>
        LanguageVersionLastChangeInSeconds = 14,

        /// <summary>
        /// Creation time stamp
        /// </summary>
        LanguageVersionCreateTimeInSeconds = 15,

        /// <summary>
        /// Version status
        /// </summary>
        LanguageVersionStatus = 16,

        /// <summary>
        /// Rating
        /// </summary>
        SumRating = 17,

        /// <summary>
        /// First read time stamp and last change
        /// </summary>
        FirstReadTimeAndLastChange = 18,

        /// <summary>
        /// Documents with unread comment first/last. Only works with filter mentionedAccountIdInComment
        /// </summary>
        MentionedInComment = 19
    }

    public enum AttachmentsOrderBy : int
    {
        Name = 0,
        Id = 1,
        CreateTime = 2
    }

    public enum CategoryOrderBy : int
    {
        Name = 0,
        Id = 1,
        CategoryTypeId = 2,
        Usage = 3
    }

    public enum SavedSearchesOrderBy : int
    {
        Name = 0,
        Id = 1,
        Prio = 2
    }

    public enum DocSearchOrderBy : int
    {
        None = 0,
        CreateTimeInSeconds = 1,
        Title = 2,
        LastChangeInSeconds = 3,
        SumRating = 4,
        Id = 5,
        ValidBeginOrCreateTime = 6,
        FirstReadTimeAndValidBegin = 7,
        Removed = 8,
        LastReadTime = 9
    }

    public enum DocPropertyOrderBy : int
    {
        None = 0,
        Id = 1,
        Name = 2,
        Description = 3
    }

    public enum PersonOrderBy : int
    {
        LastName = 0,
        Id = 1,
        Login = 2,
        CreateTime = 3,
        LastLogin = 4,
        FirstName = 5,

        /// <summary>
        /// Used to get last author(s) of an document, does only work with one of these filter set:
        /// AuthorshipForDocumentId, AuthorshipForLanguageVersionId or AuthorshipForLanguageId
        /// </summary>
        LastWrite = 6
    }

    public enum PersonSearchOrderBy : int
    {
        LastName = 0,
        Id = 1,
        Active = 2,
        Login = 3,
        CreateTime = 4,
        LastLogin = 5,
        FirstName = 6
    }

    public enum RoleOrderBy : int
    {
        Type = 0,
        Id = 1,
        Name = 2
    }

    public enum TaskOrderBy : int
    {
        Date = 0,
        Id = 1,
        DueDate = 2,
        Status = 3,
        DocumentId = 4,
        DocumentName = 5
    }

    public enum TaskStatusOrderBy : int
    {
        Type = 0,
        Id = 1,
        Name = 2
    }

    public enum DocChangeLogOrderBy : int
    {
        Date = 0,
        Id = 1
    }

    public enum ClientOrderBy : int
    {
        Name = 0,
        Id = 1
    }

    public enum InstanceOrderBy : int
    {
        Active = 0,
        Id = 1,
        Name = 2,
        DisplayName = 3,
        Reference = 4
    }

    public enum LanguageOrderBy : int
    {
        Prio = 0,
        Id = 1,
        Name = 2
    }

    public enum TrackingOrderBy : int
    {
        Date = 0,
        Id = 1
    }

    public enum ActivityOrderBy : int
    {
        Date,
        Id,
        DocumentId,
        DocumentTitle
    }

    public enum CommentOrderBy : int
    {
        Createtime = 0
    }

    public enum OrderDirection : int
    {
        asc = 0,
        desc = 1
    }

    public enum TempFileType : int
    {
        Attachment = 0,
        Document = 1,
        ContactImage = 2,
        Logo = 3
    }

    public enum LangKey : int
    {
        en = 0,
        de = 1,
        fr = 2,
        es = 3,
        it = 4
    }

    public enum LoadSize : int
    {
        Small,
        Large,
        Thumbnail
    }

    public enum AttachmentFormats : int
    {
        Original = -1,
        Pdf = 0
    }

    public enum ContactImageFormats : int
    {
        Pdf = 0,
        Thumbnail = 1
    }

    public enum ReadStatuses : int
    {
        NeverMind = 0,
        Unread = 1,
        Read = 2,
        UnreadSinceAccountCreation = 3,
        UnreadSinceLastLogin = 4
    }

    public enum RoleRightTypes : int
    {
        READ = 1,
        EDIT = 2
    }

    public enum RoleRightFilterTypes : int
    {
        ANY = 0,
        READ = 1,
        EDIT = 2
    }

    public enum ParamShowValue : int
    {
        Exclude = 0,
        Include = 1,
        Exclusive = 2
    }

    public enum ParamHideValue : int
    {
        Include = 0,
        Exclude = 1,
        Exclusive = 2
    }

    public enum CategoryAssignedValue : int
    {
        Any = 0,
        Assigned = 1,
        NoneAssigned = 2
    }

    public enum ParamHideBoolValue : int
    {
        Include = 0,
        Exclude = 1,
        Exclusive = 2
    }

    public enum LoadMainFileFormats : int
    {
        AsIs = 0,
        Pdf = 1,
        Thumbnail = 2,
        Html = 3
    }

    public enum PublishRequestTypes : int
    {
        /// <summary>
        /// Do not publish. The document will be saved as a draft.
        /// </summary>
        None = 0,

        /// <summary>
        /// Publish unreviewed. This requires the permission "publishUnreviewed".
        /// </summary>
        Unreviewed = 1,

        /// <summary>
        /// Publish a document as reviewed. This requires the permission "immediateReview".
        /// </summary>
        Reviewed = 2,

        /// <summary>
        /// Save a document and request review.
        /// </summary>
        ReviewRequested = 3
    }

    public enum CommunicationEntityType : int
    {
        Email = 11,
        Mobile = 12,
        Web = 13,
        Phone = 14,
        Fax = 15,
        Skype = 53,
        Other = 105
    }

    /// <summary>
    /// Type of roles.
    /// </summary>
    public enum RoleEntityType : int
    {
        /// <summary>
        /// A functional role is a role that is used to manage permissions.
        /// </summary>
        Functional = 21,

        /// <summary>
        /// A work group is a role containing users.
        /// </summary>
        WorkGroup = 22,

        /// <summary>
        /// A personal role is
        /// </summary>
        Personal = 24,

        /// <summary>
        /// a review group is a special group that allows users of the group to review documents.
        /// </summary>
        ReviewGroup = 28
    }

    public enum BatchActions : int
    {
        TRASH = 1,
        UNTRASH = 2,
        ARCHIVE = 3,
        UNARCHIVE = 4,
        FINALLY_REMOVE = 5,
        SET_CATEGORIES = 6,
        ADD_CATEGORIES = 7,
        REMOVE_CATEGORIES = 8,
        SET_ADDITIONAL_PROPERTIES = 9,
        ADD_ADDITIONAL_PROPERTIES = 10,
        REMOVE_ADDITIONAL_PROPERTIES = 11,
        CREATE_BOOKLET = 12
    }

    public enum DuplicateMode : int
    {
        Default = 0,
        TemplateToDocument = 1,
        DocumentToTemplate = 2
    }

    public enum DashboardStyles : int
    {
        List = 0,
        ListDark = 1,
        ListLight = 2,
        Columns = 3,
        ColumnsDark = 4,
        ColumnsLight = 5,
        Grid = 6,
        GridDark = 7,
        GridLight = 8
    }

    public enum AdditionalPropertyLoadTypes : int
    {
        Undefined = -1,
        News = 1,
        Urgent = 2,
        HelpSectionDashboard = 3,
        HelpSectionDocumentEditor = 4,
        InfoCenter = 5
    }

    public enum AdditionalPropertyTypes : int
    {
        Custom = 0,
        News = 1,
        Urgent = 2,
        HelpSectionDashboard = 3,
        HelpSectionDocumentEditor = 4,
        InfoCenter = 5
    }

    public enum ConversionAction : int
    {
        MarkdownToHtml = 0,
        HtmlToMarkdown = 1
    }

    public enum TaskStatusId : int
    {
        Open = 1,
        Accepted = 2,
        Closed = 3,
        Rejected = 4
    }

    public enum ResubmissionTimeUnits : int
    {
        Days = 0,
        Months=1
    }

    public enum ActivityMessageType : int
    {
        Any,
        Document_Read,
        Document_Stored,
        Document_Changed,
        Document_Published,
        Document_Deletion,
        Document_Review_Request,
        Document_Reviewed,
        Document_Review_Declined,
        Document_Comment,
        Document_Comment_Account_Mentioned,
        Document_Archive,
        Document_Unarchive,
        Document_Restore,
        Document_Delete_Language,
        Document_Delete_Language_Version,
        Document_Helpful,
        Document_Lock_Overwritten,
        Task_Created,
        Task_Changed,
        Task_Removed,
        Invitation,
        Password_Reset_Link,
        Signup
    }

    public enum NotificationStatuses : int
    {
        Status_Any = -1,
        Status_False = 0,
        Status_Pending = 1,
        Status_Sent = 2,
        Status_Opened = 3,
    }

    public enum UnrestrictedPreferences : int
    {
        NTML_Supported = 0,  //auth.ntlm.jespaSupported
        OAuth_Google_Enabled = 1,  //auth.oauth2.google.supported
        OAuth_Microsoft_Enabled = 2,  //auth.oauth2.microsoft.supported
        OAuth_Cobot_Enabled = 3, //auth.oauth2.cobot.supported
        Demo_Host_Name = 4,  //general.demo.hostname
        Inhouse_Enabled = 5  //general.inhouse.enabled
    }
}