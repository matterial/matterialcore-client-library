﻿using System;
using System.Collections.Generic;

namespace MatterialCore.Enums
{
    public static class Extensions
    {
        public static string ToLangString(this LangKey me)
        {
            return me.KeyAndValue().Value; ;
        }

        public static string ToLangString(this LangKey? me)
        {
            if (!(me is null))
            {
                return me.KeyAndValue().Value;
            }
            return string.Empty;
        }

        public static string ToLowerFirstChar(this string input)
        {
            string newString = input;
            if (!String.IsNullOrEmpty(newString) && Char.IsUpper(newString[0]))
                newString = Char.ToLower(newString[0]) + newString.Substring(1);
            return newString;
        }
        public static string SubStringTry(this string me, int startIndex, int length)
        {
            if ((String.IsNullOrEmpty(me)) || (me.Length == 0) || (startIndex + 1 > me.Length) || (startIndex + length > me.Length))
            {
                return string.Empty;
            }
            return me.Substring(startIndex, length);
        }

        public static LangKey ToLanguageKey(this string me, bool ignoreError = false)
        {
            if (Enum.TryParse<LangKey>(me, out LangKey lk))
            {
                return lk;
            }
            if (ignoreError)
            {
                return Matterial.DefaultLanguage;
            }
            throw new ArgumentException("Converting string \"" + me + "\" to LangKey failed.");
        }

        public static long ToLong(this string me, bool suppressException = false)
        {
            if (long.TryParse(me, out long l))
                return l;
            if (suppressException)
                return 0;
            throw new ArgumentException("Converting string \"" + me + "\" to long failed.");
        }

        public static bool ToBool(this string me, bool suppressException = false)
        {
            if (bool.TryParse(me, out bool b))
                return b;
            if (suppressException)
                return false;
            throw new ArgumentException("Converting string \"" + me + "\" to bool failed.");
        }

        public static int ToInt(this string me, bool suppressException = false)
        {
            if (int.TryParse(me, out int i))
                return i;
            if (suppressException)
                return 0;
            throw new ArgumentException("Converting string \"" + me + "\" to int failed.");
        }

        public static KeyValuePair<string, string> KeyAndValue(this AdditionalPropertyLoadTypes me)
        {
            string key = Const.PARAM_PROPERTY_TYPE;
            string value = me switch
            {
                AdditionalPropertyLoadTypes.News => "1",
                AdditionalPropertyLoadTypes.Urgent => "2",
                AdditionalPropertyLoadTypes.HelpSectionDashboard => "3",
                AdditionalPropertyLoadTypes.HelpSectionDocumentEditor => "4",
                AdditionalPropertyLoadTypes.InfoCenter => "5",
                _ => "0",
            };
            return new KeyValuePair<string, string>(key, value);
        }

        public static KeyValuePair<string, string> KeyAndValue(this LangKey? me, string? keyName = null)
        {
            string key;
            if (!String.IsNullOrEmpty(keyName))
            {
                key = keyName;
            }
            else
            {
                key = Const.PARAM_LANGUAGE_KEY;
            }

            if (me is null)
            {
                me = Matterial.DefaultLanguage;
            }

            string value = me switch
            {
                LangKey.de => "de",
                LangKey.en => "en",
                LangKey.es => "es",
                LangKey.fr => "fr",
                LangKey.it => "it",
                _ => "en"
            };
            return new KeyValuePair<string, string>(key, value);
        }

        public static KeyValuePair<string, string> KeyAndValue(this LangKey me, string? keyName = null)
        {
            string key;
            if (!String.IsNullOrEmpty(keyName))
            {
                key = keyName;
            }
            else
            {
                key = Const.PARAM_LANGUAGE_KEY;
            }

            string value = me switch
            {
                LangKey.de => "de",
                LangKey.en => "en",
                LangKey.es => "es",
                LangKey.fr => "fr",
                LangKey.it => "it",
                _ => "en"
            };
            return new KeyValuePair<string, string>(key, value);
        }

        public static string PathParameterValue(this TempFileType me)
        {
            string value = me switch
            {
                TempFileType.Attachment => Const.ATTACHMENT,
                TempFileType.ContactImage => Const.CONTACT_IMAGE,
                TempFileType.Document => Const.DOCUMENT,
                TempFileType.Logo => Const.LOGO,
                _ => Const.ATTACHMENT
            };
            return value;
        }

        public static KeyValuePair<string, string> KeyAndValue(this ReadStatuses me)
        {
            string key = Const.PARAM_READ;
            string value = me switch
            {
                ReadStatuses.NeverMind => string.Empty,
                ReadStatuses.Unread => Const.VALUE_UNREAD,
                ReadStatuses.Read => Const.VALUE_READ,
                ReadStatuses.UnreadSinceAccountCreation => Const.VALUE_UNREAD_SINCE_ACCOUNT_CREATION,
                ReadStatuses.UnreadSinceLastLogin => Const.VALUE_UNREAD_SINCE_LAST_LOGIN,
                _ => String.Empty,
            };
            return new KeyValuePair<string, string>(key, value);
        }

        public static KeyValuePair<string, string> KeyAndValue(this PublishRequestTypes me)
        {
            string key = Const.PARAM_PUBLISH_REQUEST;
            string value = me switch
            {
                PublishRequestTypes.Unreviewed => Const.VALUE_PUBLISH_REQUEST_UNREVIEWED,
                PublishRequestTypes.Reviewed => Const.VALUE_PUBLISH_REQUEST_REVIEWED,
                PublishRequestTypes.ReviewRequested => Const.VALUE_PUBLISH_REQUEST_REVIEW_REQUESTED,
                PublishRequestTypes.None => "None",
                _ => String.Empty,
            };
            return new KeyValuePair<string, string>(key, value);
        }

        public static KeyValuePair<string, string> KeyAndValue(this DocSearchOrderBy me)
        {
            string key = Const.PARAM_ORDER_BY;
            string value = me switch
            {
                DocSearchOrderBy.None => String.Empty,
                DocSearchOrderBy.CreateTimeInSeconds => Const.ORDER_BY_CREATE_TIME_IN_SECONDS,
                DocSearchOrderBy.Title => Const.ORDER_BY_LANGUAGE_VERSION_TITLE,
                DocSearchOrderBy.LastChangeInSeconds => Const.ORDER_BY_LANGUAGE_VERSION_LAST_CHANGE_IN_SECONDS,
                DocSearchOrderBy.SumRating => Const.ORDER_BY_SUM_RATING,
                DocSearchOrderBy.Id => Const.ORDER_BY_ID,
                DocSearchOrderBy.ValidBeginOrCreateTime => Const.ORDER_BY_VALID_BEGIN_OR_CREATE_TIME,
                DocSearchOrderBy.FirstReadTimeAndValidBegin => Const.ORDER_BY_FIRST_READ_TIME_AND_VALID_BEGIN,
                DocSearchOrderBy.Removed => Const.ORDER_BY_REMOVED,
                DocSearchOrderBy.LastReadTime => Const.ORDER_BY_LAST_READ_TIME,
                _ => String.Empty,
            };
            return new KeyValuePair<string, string>(key, value);
        }

        public static KeyValuePair<string, string> KeyAndValue(this DocPropertyOrderBy me)
        {
            string key = Const.PARAM_ORDER_BY;
            string value = me switch
            {
                DocPropertyOrderBy.None => String.Empty,
                DocPropertyOrderBy.Id => Const.ORDER_BY_ID,
                DocPropertyOrderBy.Name => Const.ORDER_BY_NAME,
                DocPropertyOrderBy.Description => Const.ORDER_BY_DESCRIPTION,
                _ => String.Empty,
            };
            return new KeyValuePair<string, string>(key, value);
        }

        public static KeyValuePair<string, string> KeyAndValue(this OrderDirection me, string? paramkey = null)
        {
            string key = Const.PARAM_ORDER_DIR;
            if (!String.IsNullOrEmpty(paramkey))
            {
                key = paramkey;
            }

            string value = me switch
            {
                OrderDirection.asc => Const.ORDER_BY_DIR_ASC,
                OrderDirection.desc => Const.ORDER_BY_DIR_DESC,
                _ => Const.ORDER_BY_DIR_ASC
            };
            return new KeyValuePair<string, string>(key, value);
        }

        public static KeyValuePair<string, string> KeyAndValue(this DocChangeLogOrderBy me)
        {
            string key = Const.PARAM_ORDER_BY;
            string value = me switch
            {
                DocChangeLogOrderBy.Date => Const.ORDER_BY_DATE,
                DocChangeLogOrderBy.Id => Const.ORDER_BY_ID,
                _ => String.Empty,
            };
            return new KeyValuePair<string, string>(key, value);
        }

        public static KeyValuePair<string, string> KeyAndValue(this SavedSearchesOrderBy me)
        {
            string key = Const.PARAM_ORDER_BY;
            string value = me switch
            {
                SavedSearchesOrderBy.Prio => Const.PRIO,
                SavedSearchesOrderBy.Id => Const.ORDER_BY_ID,
                SavedSearchesOrderBy.Name => Const.ORDER_BY_NAME,
                _ => String.Empty,
            };
            return new KeyValuePair<string, string>(key, value);
        }

        public static KeyValuePair<string, string> KeyAndValue(this LanguageOrderBy me)
        {
            string key = Const.PARAM_ORDER_BY;
            string value = me switch
            {
                LanguageOrderBy.Prio => Const.PRIO,
                LanguageOrderBy.Id => Const.ORDER_BY_ID,
                LanguageOrderBy.Name => Const.ORDER_BY_NAME,
                _ => Const.PRIO,
            };
            return new KeyValuePair<string, string>(key, value);
        }
        
        public static KeyValuePair<string, string> KeyAndValue(this CategoryAssignedValue me)
        {
            //hideWithCategories=false&showWithCategoriesOnly=true
            string key = me switch
            {
                CategoryAssignedValue.Assigned => "showWithCategoriesOnly",
                CategoryAssignedValue.NoneAssigned => "hideWithCategories",
                _ => String.Empty,
            };            
            return new KeyValuePair<string, string>(key, "true");
        }

        public static KeyValuePair<string, string> KeyAndValue(this ParamShowValue me, string propertyName)
        {
            string key = me switch
            {
                ParamShowValue.Exclude => "show" + propertyName,
                ParamShowValue.Include => "show" + propertyName,
                ParamShowValue.Exclusive => "show" + propertyName + "Only",
                _ => String.Empty,
            };
            string value = me switch
            {
                ParamShowValue.Exclude => "false",
                ParamShowValue.Exclusive => "true",
                ParamShowValue.Include => "true",
                _ => String.Empty,
            };
            return new KeyValuePair<string, string>(key, value);
        }

        public static KeyValuePair<string, string> KeyAndValue(this ParamHideValue me, string propertyName)
        {
            string key = me switch
            {
                ParamHideValue.Exclude => "hide" + propertyName,
                ParamHideValue.Exclusive => "show" + propertyName + "Only",
                ParamHideValue.Include => "hide" + propertyName, //Is only is for testing parameters.
                _ => String.Empty,
            };
            string value = me switch
            {
                ParamHideValue.Exclude => "true",
                ParamHideValue.Exclusive => "true",
                ParamHideValue.Include => "false",
                _ => String.Empty,
            };
            return new KeyValuePair<string, string>(key, value);
        }

        public static KeyValuePair<string, string> KeyAndValue(this ParamHideBoolValue me, string propertyName)
        {
            string key = propertyName;

            string value = me switch
            {
                ParamHideBoolValue.Exclude => "false",
                ParamHideBoolValue.Exclusive => "true",
                ParamHideBoolValue.Include => "debug",
                _ => String.Empty,
            };
            return new KeyValuePair<string, string>(key, value);
        }

        public static KeyValuePair<string, string> KeyAndValue(this AttachmentsOrderBy me)
        {
            string key = Const.PARAM_ATTACHMENT_ORDER_BY;
            string value = me switch
            {
                AttachmentsOrderBy.Name => Const.ORDER_BY_NAME,
                AttachmentsOrderBy.Id => Const.ORDER_BY_ID,
                AttachmentsOrderBy.CreateTime => Const.ORDER_BY_ATTACHMENT_CREATE_TIME,
                _ => String.Empty,
            };
            return new KeyValuePair<string, string>(key, value);
        }

        public static KeyValuePair<string, string> KeyAndValue(this DocLoadOrderBy me)
        {
            string key = Const.PARAM_ORDER_BY;
            string value = me switch
            {
                DocLoadOrderBy.LanguageVersionTitle => Const.ORDER_BY_LANGUAGE_VERSION_TITLE,
                DocLoadOrderBy.Id => Const.ORDER_BY_ID,
                DocLoadOrderBy.Valid => Const.ORDER_BY_VALID,
                DocLoadOrderBy.Archived => Const.ORDER_BY_ARCHIVED,
                DocLoadOrderBy.ArchivedBeginInSeconds => Const.ORDER_BY_ARCHIVED_BEGIN_IN_SECONDS,
                DocLoadOrderBy.Removed => Const.ORDER_BY_REMOVED,
                DocLoadOrderBy.ClickCount => Const.ORDER_BY_CLICK_COUNT,
                DocLoadOrderBy.ValidEndInSeconds => Const.ORDER_BY_VALID_END_IN_SECONDS,
                DocLoadOrderBy.CreateTimeInSeconds => Const.ORDER_BY_CREATE_TIME_IN_SECONDS,
                DocLoadOrderBy.ValidBeginOrCreateTime => Const.ORDER_BY_VALID_BEGIN_OR_CREATE_TIME,
                DocLoadOrderBy.FirstReadTimeAndValidBegin => Const.ORDER_BY_FIRST_READ_TIME_AND_VALID_BEGIN,
                DocLoadOrderBy.LanguageVersionLanguagePrio => Const.ORDER_BY_LANGUAGE_VERSION_LANGUAGE_PRIO,
                DocLoadOrderBy.LanguageVersionVersion => Const.ORDER_BY_LANGUAGE_VERSION_VERSION,
                DocLoadOrderBy.LastReadTime => Const.ORDER_BY_LAST_READ_TIME,
                DocLoadOrderBy.LanguageVersionLastChangeInSeconds => Const.ORDER_BY_LANGUAGE_VERSION_LAST_CHANGE_IN_SECONDS,
                DocLoadOrderBy.LanguageVersionCreateTimeInSeconds => Const.ORDER_BY_LANGUAGE_VERSION_CREATE_TIME_IN_SECONDS,
                DocLoadOrderBy.LanguageVersionStatus => Const.ORDER_BY_LANGUAGE_VERSION_STATUS,
                DocLoadOrderBy.SumRating => Const.ORDER_BY_SUM_RATING,
                DocLoadOrderBy.FirstReadTimeAndLastChange => Const.ORDER_BY_FIRST_READ_TIME_AND_LAST_CHANGE,
                DocLoadOrderBy.MentionedInComment => Const.ORDER_BY_MENTIONED_IN_COMMENT,
                _ => Const.ORDER_BY_LANGUAGE_VERSION_TITLE,
            };

            return new KeyValuePair<string, string>(key, value);
        }

        public static KeyValuePair<string, string> KeyAndValue(this CommentOrderBy me)
        {
            string key = Const.PARAM_ORDER_BY;
            string value = me switch
            {
                CommentOrderBy.Createtime => Const.ORDER_BY_CREATE_TIME_COMMENT,
                _ => Const.ORDER_BY_CREATE_TIME_COMMENT,
            };

            return new KeyValuePair<string, string>(key, value);
        }

        public static KeyValuePair<string, string> KeyAndValue(this DuplicateMode me)
        {
            string key = me switch
            {
                DuplicateMode.Default => string.Empty,
                DuplicateMode.DocumentToTemplate => Const.PARAM_DOCUMENT_TO_TEMPLATE,
                DuplicateMode.TemplateToDocument => Const.PARAM_TEMPLATE_TO_DOCUMENT,
                _ => String.Empty,
            };
            string value = me switch
            {
                DuplicateMode.Default => string.Empty,
                DuplicateMode.DocumentToTemplate => "true",
                DuplicateMode.TemplateToDocument => "true",
                _ => String.Empty,
            };
            return new KeyValuePair<string, string>(key, value);
        }

        public static KeyValuePair<string, string> KeyAndValue(this LoadMainFileFormats me)
        {
            string key = me switch
            {
                LoadMainFileFormats.Html => Const.PARAM_LOAD_HTML,
                LoadMainFileFormats.Pdf => Const.PARAM_LOAD_PDF,
                LoadMainFileFormats.Thumbnail => Const.PARAM_LOAD_THUMBNAIL,
                LoadMainFileFormats.AsIs => "asis",
                _ => String.Empty,
            };
            string value = "true";
            return new KeyValuePair<string, string>(key, value);
        }

        public static KeyValuePair<string, string> KeyAndValue(this AttachmentFormats me)
        {
            string value = me switch
            {
                AttachmentFormats.Pdf => "true",
                _ => "false"
            };
            string key = Const.PARAM_LOAD_PDF;
            return new KeyValuePair<string, string>(key, value);
        }

        public static KeyValuePair<string, string> KeyAndValue(this LoadSize me)
        {
            string key = "size";
            string value = me switch
            {
                LoadSize.Small => "s",
                LoadSize.Large => "l",
                LoadSize.Thumbnail => "t",
                _ => String.Empty,
            };
            return new KeyValuePair<string, string>(key, value);
        }

        public static KeyValuePair<string, string> KeyAndValue(this RoleRightFilterTypes me)
        {
            string key = Const.PARAM_ROLE_RIGHT_TYPE;
            string value = ((int)me).ToString();
            return new KeyValuePair<string, string>(key, value);
        }

        public static KeyValuePair<string, string> KeyAndValue(this CategoryOrderBy me)
        {
            string key = Const.PARAM_ORDER_BY;
            string value = me switch
            {
                CategoryOrderBy.Name => Const.ORDER_BY_NAME,
                CategoryOrderBy.Id => Const.ORDER_BY_ID,
                CategoryOrderBy.CategoryTypeId => Const.ORDER_BY_CATEGORY_TYPE_ID,
                CategoryOrderBy.Usage => Const.ORDER_BY_USAGE,
                _ => Const.ORDER_BY_NAME
            };
            return new KeyValuePair<string, string>(key, value);
        }

        public static KeyValuePair<string, string> KeyAndValue(this PersonOrderBy me)
        {
            string key = Const.PARAM_ORDER_BY;
            string value = me switch
            {
                PersonOrderBy.CreateTime => Const.ORDER_BY_CREATE_TIME,
                PersonOrderBy.Id => Const.ORDER_BY_ID,
                PersonOrderBy.FirstName => Const.ORDER_BY_FIRST_NAME,
                PersonOrderBy.LastName => Const.ORDER_BY_LAST_NAME,
                PersonOrderBy.LastLogin => Const.ORDER_BY_LAST_LOGIN,
                PersonOrderBy.LastWrite => Const.ORDER_BY_LAST_WRITE,
                PersonOrderBy.Login => Const.ORDER_BY_LOGIN,
                _ => Const.ORDER_BY_LAST_NAME
            };
            return new KeyValuePair<string, string>(key, value);
        }

        public static KeyValuePair<string, string> KeyAndValue(this PersonSearchOrderBy me)
        {
            string key = Const.PARAM_ORDER_BY;
            string value = me switch
            {
                PersonSearchOrderBy.CreateTime => Const.ORDER_BY_CREATE_TIME,
                PersonSearchOrderBy.Id => Const.ORDER_BY_ID,
                PersonSearchOrderBy.FirstName => Const.ORDER_BY_FIRST_NAME,
                PersonSearchOrderBy.LastName => Const.ORDER_BY_LAST_NAME,
                PersonSearchOrderBy.LastLogin => Const.ORDER_BY_LAST_LOGIN,
                PersonSearchOrderBy.Active => Const.ORDER_BY_ACTIVE,
                PersonSearchOrderBy.Login => Const.ORDER_BY_LOGIN,
                _ => Const.ORDER_BY_LAST_NAME
            };
            return new KeyValuePair<string, string>(key, value);
        }

        public static KeyValuePair<string, string> KeyAndValue(this RoleOrderBy me)
        {
            string key = Const.PARAM_ORDER_BY;
            string value = me switch
            {
                RoleOrderBy.Name => Const.ORDER_BY_NAME,
                RoleOrderBy.Id => Const.ORDER_BY_ID,
                RoleOrderBy.Type => Const.ORDER_BY_TYPE,
                _ => Const.ORDER_BY_TYPE
            };
            return new KeyValuePair<string, string>(key, value);
        }

        public static KeyValuePair<string, string> KeyAndValue(this TaskOrderBy me)
        {
            string key = Const.PARAM_ORDER_BY;
            string value = me switch
            {
                TaskOrderBy.Date => Const.ORDER_BY_DATE,
                TaskOrderBy.Id => Const.ORDER_BY_ID,
                TaskOrderBy.DocumentName => Const.ORDER_BY_DOCUMENT_NAME,
                TaskOrderBy.DocumentId => Const.ORDER_BY_DOCUMENT_ID,
                TaskOrderBy.DueDate => Const.ORDER_BY_DUE_DATE,
                TaskOrderBy.Status => Const.ORDER_BY_STATUS,
                _ => Const.ORDER_BY_DATE
            };
            return new KeyValuePair<string, string>(key, value);
        }

        public static KeyValuePair<string, string> KeyAndValue(this TaskStatusOrderBy me)
        {
            string key = Const.PARAM_ORDER_BY;
            string value = me switch
            {
                TaskStatusOrderBy.Name => Const.ORDER_BY_NAME,
                TaskStatusOrderBy.Id => Const.ORDER_BY_ID,
                TaskStatusOrderBy.Type => Const.ORDER_BY_TYPE,
                _ => Const.ORDER_BY_TYPE
            };
            return new KeyValuePair<string, string>(key, value);
        }

        public static KeyValuePair<string, string> KeyAndValue(this ClientOrderBy me)
        {
            string key = Const.PARAM_ORDER_BY;
            string value = me switch
            {
                ClientOrderBy.Name => Const.ORDER_BY_NAME,
                ClientOrderBy.Id => Const.ORDER_BY_ID,
                _ => Const.ORDER_BY_NAME
            };
            return new KeyValuePair<string, string>(key, value);
        }

        public static KeyValuePair<string, string> KeyAndValue(this InstanceOrderBy me)
        {
            string key = Const.PARAM_ORDER_BY;
            string value = me switch
            {
                InstanceOrderBy.Name => Const.ORDER_BY_NAME,
                InstanceOrderBy.Id => Const.ORDER_BY_ID,
                InstanceOrderBy.Active => Const.ORDER_BY_ACTIVE,
                InstanceOrderBy.DisplayName => Const.ORDER_BY_DISPLAY_NAME,
                InstanceOrderBy.Reference => Const.ORDER_BY_REFERENCE,
                _ => Const.ORDER_BY_ACTIVE
            };
            return new KeyValuePair<string, string>(key, value);
        }

        public static KeyValuePair<string, string> KeyAndValue(this TrackingOrderBy me)
        {
            string key = Const.PARAM_ORDER_BY;
            string value = me switch
            {
                TrackingOrderBy.Date => Const.ORDER_BY_DATE,
                TrackingOrderBy.Id => Const.ORDER_BY_ID,
                _ => Const.ORDER_BY_DATE
            };
            return new KeyValuePair<string, string>(key, value);
        }

        public static KeyValuePair<string, string> KeyAndValue(this ActivityOrderBy me)
        {
            string key = Const.PARAM_ORDER_BY;
            string value = me switch
            {
                ActivityOrderBy.Date => Const.ORDER_BY_DATE,
                ActivityOrderBy.Id => Const.ORDER_BY_ID,
                ActivityOrderBy.DocumentId => Const.ORDER_BY_DOCUMENT_ID,
                ActivityOrderBy.DocumentTitle => Const.ORDER_BY_DOCUMENT_TITLE,
                _ => Const.ORDER_BY_DATE
            };
            return new KeyValuePair<string, string>(key, value);
        }

        public static KeyValuePair<string, string> KeyAndValue(this ActivityMessageType me)
        {
            string key = Const.PARAM_MESSAGE_TYPE;
            string value = me switch
            {
                ActivityMessageType.Document_Archive => Const.MESSAGE_TYPE_DOCUMENT_ARCHIVE,
                ActivityMessageType.Document_Changed => Const.MESSAGE_TYPE_DOCUMENT_CHANGED,
                ActivityMessageType.Document_Comment => Const.MESSAGE_TYPE_DOCUMENT_COMMENT,
                ActivityMessageType.Document_Comment_Account_Mentioned => Const.MESSAGE_TYPE_DOCUMENT_COMMENT_ACCOUNT_MENTIONED,
                ActivityMessageType.Document_Delete_Language => Const.MESSAGE_TYPE_DOCUMENT_DELETE_LANGUAGE,
                ActivityMessageType.Document_Delete_Language_Version => Const.MESSAGE_TYPE_DOCUMENT_DELETE_LANGUAGE_VERSION,
                ActivityMessageType.Document_Deletion => Const.MESSAGE_TYPE_DOCUMENT_DELETION,
                ActivityMessageType.Document_Helpful => Const.MESSAGE_TYPE_DOCUMENT_HELPFUL,
                ActivityMessageType.Document_Lock_Overwritten => Const.MESSAGE_TYPE_DOCUMENT_LOCK_OVERWRITTEN,
                ActivityMessageType.Document_Published => Const.MESSAGE_TYPE_DOCUMENT_PUBLISHED,
                ActivityMessageType.Document_Read => Const.MESSAGE_TYPE_DOCUMENT_READ,
                ActivityMessageType.Document_Restore => Const.MESSAGE_TYPE_DOCUMENT_RESTORE,
                ActivityMessageType.Document_Reviewed => Const.MESSAGE_TYPE_DOCUMENT_REVIEWED,
                ActivityMessageType.Document_Review_Declined => Const.MESSAGE_TYPE_DOCUMENT_REVIEW_DECLINED,
                ActivityMessageType.Document_Review_Request => Const.MESSAGE_TYPE_DOCUMENT_REVIEW_REQUEST,
                ActivityMessageType.Document_Stored => Const.MESSAGE_TYPE_DOCUMENT_STORED,
                ActivityMessageType.Document_Unarchive => Const.MESSAGE_TYPE_DOCUMENT_UNARCHIVE,
                ActivityMessageType.Invitation => Const.MESSAGE_TYPE_INVITATION,
                ActivityMessageType.Password_Reset_Link => Const.MESSAGE_TYPE_PASSWORD_RESET_LINK,
                ActivityMessageType.Signup => Const.MESSAGE_TYPE_SIGNUP,
                ActivityMessageType.Task_Changed => Const.MESSAGE_TYPE_TASK_CHANGED,
                ActivityMessageType.Task_Created => Const.MESSAGE_TYPE_TASK_CREATED,
                ActivityMessageType.Task_Removed => Const.MESSAGE_TYPE_TASK_REMOVED,
                ActivityMessageType.Any => string.Empty,
                _ => string.Empty
            };
            return new KeyValuePair<string, string>(key, value);
        }

        public static KeyValuePair<string, string> KeyAndValue(this NotificationStatuses me)
        {
            string key = Const.PARAM_NOTIFICATION_STATUS;
            string value = ((int)me).ToString();
            return new KeyValuePair<string, string>(key, value);
        }

        public static string ToKeyString(this UnrestrictedPreferences me)
        {
            string value = me switch
            {
                UnrestrictedPreferences.NTML_Supported => Const.UNCONSTRAINED_PREFERENCES_AUTH_NTLM_SUPPORTED,
                UnrestrictedPreferences.OAuth_Google_Enabled => Const.UNCONSTRAINED_PREFERENCES_AUTH_OAUTH_GOOGLE_SUPPORTED,
                UnrestrictedPreferences.OAuth_Microsoft_Enabled => Const.UNCONSTRAINED_PREFERENCES_AUTH_OAUTH_MICROSOFT_SUPPORTED,
                UnrestrictedPreferences.OAuth_Cobot_Enabled => Const.UNCONSTRAINED_PREFERENCES_AUTH_OAUTH_COBOT_SUPPORTED,
                UnrestrictedPreferences.Demo_Host_Name => Const.UNCONSTRAINED_PREFERENCES_DEMO_HOST_NAME,
                UnrestrictedPreferences.Inhouse_Enabled => Const.UNCONSTRAINED_PREFERENCES_INHOUSE_ENABLED,
                _ => string.Empty
            };
            return value;
        }
    }
}