﻿using System;

namespace MatterialCore
{
    /// <summary>
    /// This class is used to automatically generate documentation in Markdown. Attributes of
    /// properties are used to document them.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    internal sealed class MtrPropertyAttribute : Attribute
    {
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member

        internal enum MtrDataTypes : int
        {
            String,
            Boolean,
            Number,
            UnixTimeStamp,
            Permissions,
            Role,
            Person,
            PreferenceMapContainer,
            ListOfLanguage,
            ListOfInstance,
            ListOfLong,
            ListOfAdditionalProperties,
            ListOfDocument,
            ListOfDocumentChangeLog,
            ListOfAddress,
            ListOfClient,
            ListOfCommunicationData,
            ListOfContactImage,
            ListOfRole,
            ListOfCategory,
            ListOfComment,
            ListOfAttachment,
            ListOfPerson,
            ListOfRoleRight,
            ListOfTempFileDescriptor,
            ListOfString,
            ListOfSavedSearchParameter,
            ListOfExtensionValue,
            DictionaryStringObject,
            DictionaryStringString,
            DictionaryStringLong,
            DictionaryLongLong,
            DictionaryStringDocumentDuplicate,
            DictionaryLongDocumentChangeLogs,
            ListOfCredentials,
            ContactImage,
            Document,
            DashboardStyles
        }

        public MtrPropertyAttribute()
        {
        }

        public MtrPropertyAttribute(MtrDataTypes mtrDataType, string description)
        {
            Description = description;
            MtrDataType = mtrDataType;
        }

        public MtrPropertyAttribute(MtrDataTypes mtrDataType)
        {
            MtrDataType = mtrDataType;
        }

        public MtrPropertyAttribute(MtrDataTypes mtrDataType, string description, bool updateable, bool requiredOnCreate)
        {
            Updateable = updateable;
            Description = description;
            RequiredOnCreate = requiredOnCreate;
            MtrDataType = mtrDataType;
        }

        public MtrPropertyAttribute(MtrDataTypes mtrDataType, string description, bool updateable, bool requiredOnCreate, string min)
        {
            Updateable = updateable;
            Description = description;
            RequiredOnCreate = requiredOnCreate;
            Min = min;
            MtrDataType = mtrDataType;
        }

        public MtrPropertyAttribute(MtrDataTypes mtrDataType, string description, bool updateable, bool requiredOnCreate, string min, string max)
        {
            Updateable = updateable;
            Description = description;
            RequiredOnCreate = requiredOnCreate;
            Min = min;
            Max = max;
            MtrDataType = mtrDataType;
        }

        public MtrDataTypes MtrDataType { get; set; }
        public bool Updateable { get; set; }
        public bool RequiredOnCreate { get; set; }
        public string? Min { get; set; }
        public string? Max { get; set; }
        public string? Description { get; set; }
        public string? Note { get; set; }
        public string? VersionImplemented { get; set; }

#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member
    }
}