﻿using System;

namespace MatterialCore
{
    /// <summary>
    /// This class is used to automatically generate documentation in Markdown. Attributes are used
    /// for classes to documehtthem.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    internal sealed class MtrClassAttribute : Attribute
    {
        /// <summary>
        /// Attribute for markdown documentation creation.
        /// </summary>
        /// <param name="description"></param>
        public MtrClassAttribute(string description)
        {
            Description = description;
        }

        /// <summary>
        /// Attribute for markdown documentation creation.
        /// </summary>
        /// <param name="versionImplemented"></param>
        /// <param name="description"></param>
        public MtrClassAttribute(string versionImplemented, string description)
        {
            Description = description;
            VersionImplemented = versionImplemented;
        }

        /// <summary>
        /// Description
        /// </summary>
        public string? Description { get; set; }

        /// <summary>
        /// Version number since this class was implemented.
        /// </summary>
        public string? VersionImplemented { get; set; }
    }
}