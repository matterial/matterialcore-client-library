﻿using MatterialCore.Entity;
using MatterialCore.Enums;
using MatterialCore.Interfaces;
using MatterialCore.Params;
using MatterialCore.Params.Doc;

using Newtonsoft.Json;

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

//using System.Text.Json;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace MatterialCore
{
    internal class RestClient
    {
        private readonly string SESSION_COOKIE_NAME = "JSESSIONID";
        public const string USER_AGENT_MATTERICL_CORE = "MatterialCoreClientLib";
        public const string USER_AGENT_MATTERICL_CORE_VERSION = "1.0";
        public const string SEND_LOGIN_LINK_ACCEPTED = "accepted";

        internal RestClient()
        {
            basePath = Matterial.BasePath;
            SessionId = String.Empty;
        }

        internal RestClient(string sessionId, string basePath)
        {
            this.basePath = basePath;
            SessionId = sessionId;
        }

        private HttpClient? httpClient;

        private string basePath;

        public void EnsureSuccessStatusCode(HttpWebResponse responseMessage)
        {
            if (!((int)responseMessage.StatusCode >= 200 && (int)responseMessage.StatusCode < 300))
            {
                throw new HttpRequestException("Server returned " + responseMessage.StatusCode.ToString());
            }
        }

        public void EnsureSuccessStatusCode(HttpResponseMessage response)
        {
            try
            {
                response.EnsureSuccessStatusCode();
            }
            catch (Exception ex)
            {
                string exMsg = "";
                string s = "";
                if (!(response.Content is null))
                {
                    s = response.Content.ReadAsStringAsync().Result;
                }
                if (!String.IsNullOrEmpty(s))
                {
                    exMsg += s;
                }
                try
                {
                    s = response.Headers.GetValues(Const.HEADER_STATUS_CODE).FirstOrDefault();
                    if (!String.IsNullOrEmpty(s))
                    {
                        exMsg += " " + Const.HEADER_STATUS_CODE.ToString() + " :" + s;
                    }
                    s = response.Headers.GetValues(Const.HEADER_STATUS_MSG).FirstOrDefault();
                    if (!String.IsNullOrEmpty(s))
                    {
                        exMsg += " " + Const.HEADER_STATUS_MSG.ToString() + " :" + s;
                    }
                }
                catch (Exception)
                {
                }

                if (!String.IsNullOrEmpty(exMsg))
                {
                    Exception myEx = new Exception(exMsg, ex);
                    throw myEx;
                }
                throw;
            }
        }

        internal enum ExpectedResponse
        {
            Nothing = 0,
            Json = 2,
            Xml = 1,
            Plain = 3,
            Html = 4,
            XHtml = 5
        }

        public string BasePath
        {
            get { return basePath; }
            internal set
            {
                if (!String.IsNullOrEmpty(value))
                {
                    Uri u = new Uri(value);
                    basePath = u.AbsoluteUri;
                }
                else
                {
                    basePath = value;
                }
            }
        }

        protected string SessionId { get; private set; }

        private string ToJson(IEntity entity)
        {
            return entity is LogonCredentials credentials
                ? credentials.ToJsonSecure()
                : JsonConvert.SerializeObject(entity, entity.GetType(), DefaultSerializerOptions);
        }

        private string ToJson(List<long> idList)
        {
            return JsonConvert.SerializeObject(idList, idList.GetType(), DefaultSerializerOptions);
        }

        private string ToJson(Dictionary<string, object> map)
        {
            return JsonConvert.SerializeObject(map, map.GetType(), DefaultSerializerOptions);
        }

        private string ToJson(List<IEntity> entities)
        {
            return JsonConvert.SerializeObject(entities, entities.GetType(), DefaultSerializerOptions);
        }

        internal JsonSerializerSettings DefaultSerializerOptions
        {
            get
            {
                JsonSerializerSettings opt = new JsonSerializerSettings
                {
                    Formatting = Formatting.None,
                    NullValueHandling = NullValueHandling.Ignore,
                };
                return opt;
            }
        }

        protected void UpdateSession(string sessionId)
        {
            SessionId = sessionId;
        }

        protected void ClearSession()
        {
            SessionId = String.Empty;
        }

        protected void UpdateSession(HttpResponseMessage? response)
        {
            SessionId = String.Empty; ;
            if (response is null)
                return;
            try
            {
                List<string> cookies = new List<string>(response.Headers.SingleOrDefault(header => header.Key == "Set-Cookie").Value);
                if (cookies != null)
                {
                    string CookieStr = cookies[0];
                    CookieContainer cookiecontainer = new CookieContainer();
                    string[] cookiesStr = CookieStr.Split(';');
                    Uri baseUri = new Uri(BasePath);
                    foreach (string cookie in cookiesStr)
                        cookiecontainer.SetCookies(baseUri, cookie);
                    List<Cookie> cList = cookiecontainer.GetCookies(baseUri).Cast<Cookie>().ToList();
                    Cookie c = cList.Find(item => item.Name == SESSION_COOKIE_NAME);
                    if (c != null)
                        SessionId = c.Value;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        protected string? MediaType(ExpectedResponse responseMediaType)
        {
            return responseMediaType switch
            {
                ExpectedResponse.Nothing => null,
                ExpectedResponse.Json => ("application/json"),
                ExpectedResponse.Xml => ("application/xml"),
                ExpectedResponse.Plain => ("text/plain"),
                ExpectedResponse.Html => ("text/html"),
                ExpectedResponse.XHtml => ("application/xhtml+xml"),
                _ => null,
            };
        }

        protected MediaTypeWithQualityHeaderValue? AcceptHeader(ExpectedResponse responseMediaType)
        {
            switch (responseMediaType)
            {
                case ExpectedResponse.Nothing:
                    return null;

                case ExpectedResponse.Json:
                case ExpectedResponse.Xml:
                case ExpectedResponse.Plain:
                case ExpectedResponse.Html:
                case ExpectedResponse.XHtml:
                    return new MediaTypeWithQualityHeaderValue(MediaType(responseMediaType));

                default:
                    return null;
            }
        }

        protected HttpClient CreateClient(ExpectedResponse expectedResponse)
        {
            if (httpClient is null)
            {
                var handler = new HttpClientHandler()
                {
                    UseCookies = false
                };
                httpClient = new HttpClient(handler);
            }
            httpClient.DefaultRequestHeaders.Accept.Clear();
            httpClient.DefaultRequestHeaders.Clear();
            if (expectedResponse != ExpectedResponse.Nothing)
            {
                httpClient.DefaultRequestHeaders.Accept.Add(AcceptHeader(expectedResponse));
            }
            //Set session ID cookie
            if (!String.IsNullOrEmpty(SessionId))
            {
                httpClient.DefaultRequestHeaders.Add(HttpRequestHeader.Cookie.ToString(), SESSION_COOKIE_NAME + "=" + SessionId);
            }
            httpClient.DefaultRequestHeaders.UserAgent.Add(new ProductInfoHeaderValue(USER_AGENT_MATTERICL_CORE, USER_AGENT_MATTERICL_CORE_VERSION));
            
            return httpClient;
        }

        #region Post

        protected Task<HttpResponseMessage> Post(String path, ExpectedResponse expectedResponse, IEntity entity)
        {
            return Post(path, expectedResponse, entity, new ParameterCollection());
        }

        protected Task<HttpResponseMessage> Post(String path, ExpectedResponse expectedResponse, ParameterCollection? urlParams)
        {
            return Post(path, expectedResponse, new StringContent(""), urlParams);
        }

        protected Task<HttpResponseMessage> Post(String path, ExpectedResponse expectedResponse, IEntity entity, ParameterCollection? urlParams)
        {
            StringContent content = new StringContent("");
            if (entity != null)
            {
                content = new StringContent(ToJson(entity), Encoding.UTF8, MediaType(ExpectedResponse.Json));
            }
            return Post(path, expectedResponse, content, urlParams);
        }

        protected Task<HttpResponseMessage> Post(String path, ExpectedResponse expectedResponse)
        {
            return Post(path, expectedResponse, new StringContent(""));
        }

        protected Task<HttpResponseMessage> Post(String path, ExpectedResponse expectedResponse, string? content, string mediaType)
        {
            StringContent strContent = new StringContent(content, Encoding.UTF8, mediaType);
            return Post(path, expectedResponse, strContent, null);
        }

        protected Task<HttpResponseMessage> Post(String path, ExpectedResponse expectedResponse, StringContent content)
        {
            return Post(path, expectedResponse, content, null);
        }

        protected Task<HttpResponseMessage> Post(String path, ExpectedResponse expectedResponse, StringContent content, ParameterCollection? urlParams)
        {
            string urlP = string.Empty;
            if (!(urlParams is null) && urlParams.Count() > 0)
            {
                urlP += "?" + urlParams.ToString();
            }
            return CreateClient(expectedResponse).PostAsync(ApiSession.CombineUri(BasePath, path) + urlP, content);
        }

        #endregion Post

        #region Put

        internal Task<HttpResponseMessage> Put(String path, ExpectedResponse expectedResponse, IEntity entity, ParameterCollection? urlParams)
        {
            StringContent content = new StringContent("");
            if (!(entity is null))
            {
                //string s = ToJson(entity);
                content = new StringContent(ToJson(entity), Encoding.UTF8, MediaType(ExpectedResponse.Json));
            }
            return Put(path, expectedResponse, content, urlParams);
        }

        protected Task<HttpResponseMessage> Put(String path, ExpectedResponse expectedResponse, List<IEntity> entities)
        {
            StringContent content = new StringContent("");
            if (!(entities is null))
            {
                //string s = ToJson(entities);
                content = new StringContent(ToJson(entities), Encoding.UTF8, MediaType(ExpectedResponse.Json));
            }
            return Put(path, expectedResponse, content);
        }

        protected Task<HttpResponseMessage> Put(String path, ExpectedResponse expectedResponse, List<IEntity> entities, ParameterCollection? urlParams)
        {
            StringContent content = new StringContent("");
            if (!(entities is null))
            {
                //string s = ToJson(entities);
                content = new StringContent(ToJson(entities), Encoding.UTF8, MediaType(ExpectedResponse.Json));
            }
            return Put(path, expectedResponse, content, urlParams);
        }

        protected Task<HttpResponseMessage> Put(String path, ExpectedResponse expectedResponse, List<long> idList)
        {
            StringContent content = new StringContent("");
            if (!(idList is null))
            {
                //string s = ToJson(entities);
                content = new StringContent(ToJson(idList), Encoding.UTF8, MediaType(ExpectedResponse.Json));
            }
            return Put(path, expectedResponse, content);
        }

        protected Task<HttpResponseMessage> Put(String path, ExpectedResponse expectedResponse, Dictionary<string, object> map)
        {
            StringContent content = new StringContent("");
            if (!(map is null))
            {
                content = new StringContent(ToJson(map), Encoding.UTF8, MediaType(ExpectedResponse.Json));
            }
            return Put(path, expectedResponse, content);
        }

        protected Task<HttpResponseMessage> Put(String path, ExpectedResponse expectedResponse, string? stringContent)
        {
            StringContent content = new StringContent(stringContent, Encoding.UTF8, MediaType(ExpectedResponse.Plain));
            return Put(path, expectedResponse, content);
        }

        protected Task<HttpResponseMessage> Put(String path, ExpectedResponse expectedResponse, List<AdditionalProperty> properties)
        {
            List<IEntity> entityList = new List<IEntity>(properties);
            StringContent content = new StringContent("");
            if (!(properties is null))
            {
                content = new StringContent(ToJson(entityList), Encoding.UTF8, MediaType(ExpectedResponse.Json));
            }
            return Put(path, expectedResponse, content);
        }

        protected Task<HttpResponseMessage> Put(String path, ExpectedResponse expectedResponse, IEntity entity)
        {
            StringContent content = new StringContent("");
            if (!(entity is null))
            {
                content = new StringContent(ToJson(entity), Encoding.UTF8, MediaType(ExpectedResponse.Json));
            }
            return Put(path, expectedResponse, content);
        }

        protected Task<HttpResponseMessage> Put(String path, ExpectedResponse expectedResponse)
        {
            return Put(path, expectedResponse, new StringContent(""));
        }

        protected Task<HttpResponseMessage> Put(String path, ExpectedResponse expectedResponse, StringContent content)
        {
            return Put(path, expectedResponse, content, null);
        }

        protected Task<HttpResponseMessage> Put(String path, ExpectedResponse expectedResponse, ParameterCollection? urlParams)
        {
            return Put(path, expectedResponse, new StringContent(""), urlParams);
        }

        protected Task<HttpResponseMessage> Put(String path, ExpectedResponse expectedResponse, StringContent content, ParameterCollection? urlParams)
        {
            string urlP = string.Empty;
            if (!(urlParams is null) && urlParams.Count() > 0)
            {
                urlP += "?" + urlParams.ToString();
            }
            return CreateClient(expectedResponse).PutAsync(ApiSession.CombineUri(BasePath, path) + urlP, content);
        }

        #endregion Put

        #region Get

        internal Task<HttpResponseMessage> Get(String path, ExpectedResponse expectedResponse)
        {
            var uri = ApiSession.CombineUri(BasePath, path);
            return CreateClient(expectedResponse).GetAsync(uri);
        }

        internal Task<Stream> GetStream(String path)
        {
            return CreateClient(ExpectedResponse.Nothing).GetStreamAsync(ApiSession.CombineUri(BasePath, path));
        }

        internal Task<Stream> GetStream(String path, ParameterCollection? urlParams)
        {
            string urlP = string.Empty;
            if (!(urlParams is null) && urlParams.Count() > 0)
            {
                urlP += "?" + urlParams.ToString();
            }
            return CreateClient(ExpectedResponse.Nothing).GetStreamAsync(ApiSession.CombineUri(BasePath, path) + urlP);
        }

        protected Task<HttpResponseMessage> Get(String path, ExpectedResponse expectedResponse, ParameterCollection? urlParams)
        {
            string urlP = string.Empty;
            if (!(urlParams is null) && urlParams.Count() > 0)
            {
                urlP += "?" + urlParams.ToString();
            }
            return CreateClient(expectedResponse).GetAsync(ApiSession.CombineUri(BasePath, path) + urlP);
        }

        #endregion Get

        #region Delete

        protected Task<HttpResponseMessage> Delete(String path, ExpectedResponse expectedResponse)
        {
            return Delete(path, expectedResponse, null);
        }

        protected Task<HttpResponseMessage> Delete(String path, ExpectedResponse expectedResponse, ParameterCollection? urlParams)
        {
            string urlP = string.Empty;
            if (!(urlParams is null) && urlParams.Count() > 0)
            {
                urlP += "?" + urlParams.ToString();
            }
            return CreateClient(expectedResponse).DeleteAsync(ApiSession.CombineUri(BasePath, path) + urlP);
        }

        #endregion Delete

        protected Task<WebResponse> UploadFiles(string path, ExpectedResponse expectedResponse, ParameterCollection pc, List<UploadFile> uploadFiles)
        {
            if (uploadFiles.Count == 0)
            {
                throw new ArgumentException("The list of files to upload must not be empty!");
            }
            if (String.IsNullOrEmpty(SessionId))
            {
                throw new ArgumentException("The session id is null or empty.");
            }

            string boundary = "----------------------------" + Guid.NewGuid().ToString();
            // The first boundary
            byte[] boundaryBytes = System.Text.Encoding.UTF8.GetBytes("\r\n--" + boundary + "\r\n");
            // The last boundary
            byte[] trailer = System.Text.Encoding.UTF8.GetBytes("\r\n--" + boundary + "--\r\n");
            // The first time it itereates, we need to make sure it doesn't put too many new paragraphs down or it completely messes up poor webbrick
            //byte[] boundaryBytesF = System.Text.Encoding.ASCII.GetBytes("--" + boundary + "\r\n");

            path = ApiSession.CombineUri(BasePath, path) + "?" + pc.ToString();

            // Create the request and set parameters
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(path);
            request.ContentType = "multipart/form-data; boundary=" + boundary;
            request.Method = "POST";
            request.KeepAlive = true;
            request.Credentials = CredentialCache.DefaultCredentials;
            if (expectedResponse != ExpectedResponse.Nothing)
            {
                string? acceptHeader = AcceptHeader(expectedResponse)?.ToString();
                if (!String.IsNullOrEmpty(acceptHeader))
                {
                    request.Accept = acceptHeader;
                }
            }
            ProductInfoHeaderValue userAgent = new ProductInfoHeaderValue(USER_AGENT_MATTERICL_CORE, USER_AGENT_MATTERICL_CORE_VERSION);
            request.Headers.Add(HttpRequestHeader.UserAgent, userAgent.ToString());
            request.Headers.Add(HttpRequestHeader.Cookie, SESSION_COOKIE_NAME + "=" + SessionId);
            request.SendChunked = true;
            // Get request stream
            using (Stream requestStream = request.GetRequestStream())
            {
                if (uploadFiles != null)
                {
                    foreach (var uploadFile in uploadFiles)
                    {
                        if (File.Exists(uploadFile.FilePath))
                        {
                            string contentType = Matterial.GetMimeType(Path.GetExtension(uploadFile.FilePath));
                            if (String.IsNullOrEmpty(uploadFile.Name))
                            {
                                uploadFile.Name = Path.GetFileName(uploadFile.FilePath);
                            }
                            int bytesRead = 0;
                            byte[] buffer = new byte[2048];
                            byte[] formItemBytes = System.Text.Encoding.UTF8.GetBytes(string.Format("Content-Disposition: form-data; name=\"{0}\"; filename=\"{1}\"\r\nContent-Type: {2}\r\n\r\n", uploadFile.FileType.PathParameterValue(), uploadFile.Name, contentType));
                            //byte[] formItemBytes = System.Text.Encoding.UTF8.GetBytes(string.Format("Content-Disposition: form-data;  filename=\"{1}\"\r\nContent-Type: {2}\r\n\r\n", uploadFile.FileType.ToString().ToLowerFirstChar(), uploadFile.Name, contentType));
                            requestStream.Write(boundaryBytes, 0, boundaryBytes.Length);
                            requestStream.Write(formItemBytes, 0, formItemBytes.Length);
                            using FileStream fileStream = new FileStream(uploadFile.FilePath, FileMode.Open, FileAccess.Read);
                            while ((bytesRead = fileStream.Read(buffer, 0, buffer.Length)) != 0)
                            {
                                // Write file content to stream, byte by byte
                                requestStream.Write(buffer, 0, bytesRead);
                            }
                            fileStream.Close();
                        }
                    }
                }
                // Write trailer and close stream
                requestStream.Write(trailer, 0, trailer.Length);
                requestStream.Close();
            }
            return request.GetResponseAsync();
        }
    }
}