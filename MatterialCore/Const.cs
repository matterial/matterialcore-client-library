﻿namespace MatterialCore
{
    /// <summary>
    /// REST interface protocol constants
    /// </summary>
    internal class Const
    {
        #region Client Constants

        public const int DefaultTimeOut = 15000;

        public const string MTR_BACKEND = "mtr-backend";

        //Language settings keys
        public const string ACCOUNT_SETTING_KEY_LANGUAGE_CONTENT = "accountSetting.language.content";

        public const string ACCOUNT_SETTING_KEY_LANGUAGE_UI = "accountSetting.language.ui";

        //Unconstraint preference keys
        public static string UNCONSTRAINED_PREFERENCES_AUTH_NTLM_SUPPORTED = "auth.ntlm.jespaSupported";

        public static string UNCONSTRAINED_PREFERENCES_AUTH_OAUTH_GOOGLE_SUPPORTED = "auth.oauth2.google.supported";
        public static string UNCONSTRAINED_PREFERENCES_AUTH_OAUTH_MICROSOFT_SUPPORTED = "auth.oauth2.microsoft.supported";
        public static string UNCONSTRAINED_PREFERENCES_AUTH_OAUTH_COBOT_SUPPORTED = "auth.oauth2.cobot.supported";
        public static string UNCONSTRAINED_PREFERENCES_DEMO_HOST_NAME = "general.demo.hostname";
        public static string UNCONSTRAINED_PREFERENCES_INHOUSE_ENABLED = "general.inhouse.enabled";

        //Document order by constants
        public static string ORDER_BY_ID = "id";

        public static string ORDER_BY_VALID = "valid";
        public static string ORDER_BY_ARCHIVED = "archived";
        public static string ORDER_BY_ARCHIVED_BEGIN_IN_SECONDS = "archivedBeginInSeconds";
        public static string ORDER_BY_REMOVED = "removed";
        public static string ORDER_BY_CLICK_COUNT = "clickCount";
        public static string ORDER_BY_VALID_END_IN_SECONDS = "validEndInSeconds";
        public static string ORDER_BY_CREATE_TIME_IN_SECONDS = "createTimeInSeconds";
        public static string ORDER_BY_VALID_BEGIN_OR_CREATE_TIME = "validBeginOrCreateTime";
        public static string ORDER_BY_FIRST_READ_TIME_AND_VALID_BEGIN = "firstReadTimeAndValidBegin";
        public static string ORDER_BY_LANGUAGE_VERSION_TITLE = "languageVersionTitle";
        public static string ORDER_BY_LANGUAGE_VERSION_LANGUAGE_PRIO = "languageVersionLanguagePrio";
        public static string ORDER_BY_LANGUAGE_VERSION_VERSION = "languageVersionVersion";
        public static string ORDER_BY_LAST_READ_TIME = "lastReadTime";
        public static string ORDER_BY_LANGUAGE_VERSION_LAST_CHANGE_IN_SECONDS = "languageVersionLastChangeInSeconds";
        public static string ORDER_BY_LANGUAGE_VERSION_CREATE_TIME_IN_SECONDS = "languageVersionCreateTimeInSeconds";
        public static string ORDER_BY_LANGUAGE_VERSION_STATUS = "languageVersionStatus";
        public static string ORDER_BY_SUM_RATING = "sumRating";
        public static string ORDER_BY_FIRST_READ_TIME_AND_LAST_CHANGE = "firstReadTimeAndLastChange";
        public static string ORDER_BY_MENTIONED_IN_COMMENT = "mentionedInComment";

        public static string ORDER_BY_NAME = "name";
        public static string ORDER_BY_DISPLAY_NAME = "displayName";
        public static string ORDER_BY_CREATE_TIME = "createTime";
        public static string ORDER_BY_ATTACHMENT_CREATE_TIME = "createtime";
        public static string ORDER_BY_DESCRIPTION = "description";
        public static string ORDER_BY_DATE = "date";

        public static string ORDER_BY_CATEGORY_TYPE_ID = "categorytypeid";
        public static string ORDER_BY_USAGE = "usage";

        public static string ORDER_BY_FIRST_NAME = "firstName";
        public static string ORDER_BY_LAST_NAME = "lastName";
        public static string ORDER_BY_LAST_LOGIN = "lastLogin";
        public static string ORDER_BY_LAST_WRITE = "lastWrite";
        public static string ORDER_BY_LOGIN = "login";
        public static string ORDER_BY_ACTIVE = "active";
        public static string ORDER_BY_TYPE = "type";

        public static string ORDER_BY_DOCUMENT_NAME = "documentName";
        public static string ORDER_BY_DOCUMENT_TITLE = "documentTitle";
        public static string ORDER_BY_DOCUMENT_ID = "documentId";
        public static string ORDER_BY_DUE_DATE = "dueDate";
        public static string ORDER_BY_STATUS = "status";

        public static string ORDER_BY_REFERENCE = "reference";

        public static string ORDER_BY_CREATE_TIME_COMMENT = "createtime";

        //Order directions
        public static string ORDER_BY_DIR_ASC = "asc";

        public static string ORDER_BY_DIR_DESC = "desc";

        //Activity message types
        public static string MESSAGE_TYPE_DOCUMENT_READ = "DOCUMENT_READ";

        public static string MESSAGE_TYPE_DOCUMENT_STORED = "DOCUMENT_STORED";
        public static string MESSAGE_TYPE_DOCUMENT_CHANGED = "DOCUMENT_CHANGED";
        public static string MESSAGE_TYPE_DOCUMENT_PUBLISHED = "DOCUMENT_PUBLISHED";
        public static string MESSAGE_TYPE_DOCUMENT_DELETION = "DOCUMENT_DELETION";
        public static string MESSAGE_TYPE_DOCUMENT_REVIEW_REQUEST = "DOCUMENT_REVIEW_REQUEST";
        public static string MESSAGE_TYPE_DOCUMENT_REVIEWED = "DOCUMENT_REVIEWED";
        public static string MESSAGE_TYPE_DOCUMENT_REVIEW_DECLINED = "DOCUMENT_REVIEW_DECLINED";
        public static string MESSAGE_TYPE_DOCUMENT_COMMENT = "DOCUMENT_COMMENT";
        public static string MESSAGE_TYPE_DOCUMENT_COMMENT_ACCOUNT_MENTIONED = "DOCUMENT_COMMENT_ACCOUNT_MENTIONED";
        public static string MESSAGE_TYPE_DOCUMENT_ARCHIVE = "DOCUMENT_ARCHIVE";
        public static string MESSAGE_TYPE_DOCUMENT_UNARCHIVE = "DOCUMENT_UNARCHIVE";
        public static string MESSAGE_TYPE_DOCUMENT_RESTORE = "DOCUMENT_RESTORE";
        public static string MESSAGE_TYPE_DOCUMENT_DELETE_LANGUAGE = "DOCUMENT_DELETE_LANGUAGE";
        public static string MESSAGE_TYPE_DOCUMENT_DELETE_LANGUAGE_VERSION = "DOCUMENT_DELETE_LANGUAGE_VERSION";
        public static string MESSAGE_TYPE_DOCUMENT_HELPFUL = "DOCUMENT_HELPFUL";
        public static string MESSAGE_TYPE_DOCUMENT_LOCK_OVERWRITTEN = "DOCUMENT_LOCK_OVERWRITTEN";
        public static string MESSAGE_TYPE_TASK_CREATED = "TASK_CREATED";
        public static string MESSAGE_TYPE_TASK_CHANGED = "TASK_CHANGED";
        public static string MESSAGE_TYPE_TASK_REMOVED = "TASK_REMOVED";
        public static string MESSAGE_TYPE_INVITATION = "INVITATION";
        public static string MESSAGE_TYPE_PASSWORD_RESET_LINK = "PASSWORD_RESET_LINK";
        public static string MESSAGE_TYPE_SIGNUP = "SIGNUP";

        public static string PARAM_IGNORE_CLIENTS = "ignoreClients";
        public static string PARAM_IGNORE_CONTACT_IMAGES = "ignoreContactImages";
        public static string PARAM_IGNORE_ADDRESSES = "ignoreAddreses";
        public static string PARAM_IGNORE_COMMUNICATION_DATA = "ignoreCommuncationData";

        public static string PARAM_IGNORE_CATEGORIES = "ignoreCategories";
        public static string PARAM_IGNORE_RELATED_DOCUMENT_IDS = "ignoreRelatedDocumentIds";
        public static string PARAM_IGNORE_EXTENSION_VALUES = "ignoreExtensionValues";
        public static string PARAM_IGNORE_ATTACHMENTS = "ignoreAttachments";
        public static string PARAM_IGNORE_ROLE_RIGHTS = "ignoreRoleRights";
        public static string PARAM_IGNORE_RESPONSIBLES = "ignoreResponsibles";

        #endregion Client Constants

        #region Protocol Constants

        public const string APPLICATION_PATH = "api";

        // *** MODULE;
        public const string ACTIVITY = "activity";

        public const string CATEGORY = "category";
        public const string TYPE = "type";
        public const string CLIENT = "client";
        public const string LOGO = "logo";
        public const string PREFERENCE = "preference";
        public const string COMMENT = "comment";
        public const string CONVERSION = "conversion";
        public const string DOCUMENT = "document";
        public const string ADDITIONAL_PROPERTY = "additionalproperty";
        public const string ATTACHMENT = "attachment";
        public const string CHANGE_LOG = "changelog";
        public const string EXTENSION_VALUE = "extensionvalue";
        public const string LOCK = "lock";
        public const string RATING = "rating";
        public const string SAVED_SEARCH = "savedsearch";
        public const string SKIN = "skin";
        public const string TEMP_FILE = "tempfile";
        public const string APP_INITIALIZER = "appinitializer";
        public const string INSTANCE_CONTROL = "instancecontrol";
        public const string MTR_MESSAGE = "mtrmessage";
        public const string LANGUAGE = "language";
        public const string LICENCE = "licence";
        public const string LOGON = "logon";
        public const string AUTO = "auto";
        public const string LTI = "lti";
        public const string NTLM = "ntlm";
        public const string OAUTH2 = "oauth2";
        public const string COBOT = "cobot";
        public const string GOOGLE = "google";
        public const string MICROSOFT = "microsoft";
        public const string MAGIC_LINK = "magiclink";
        public const string SAML = "saml";
        public const string MIGRATE = "migrate";
        public const string NOTIFICATION = "notification";
        public const string PERSON = "person";
        public const string ACCOUNT_SETTING = "accountsetting";
        public const string CONTACT_IMAGE = "contactimage";
        public const string ROLE = "role";
        public const string TASK = "task";
        public const string STATUS = "status";
        public const string TRACKING = "tracking";
        public const string TEST = "test";
        public const string VERSION = "version";

        // *** PATH;
        // *** general;
        public const string CSV = "csv";

        public const string VCARD = "vcard";
        public const string ACTIVATE = "activate";
        public const string DEACTIVATE = "deactivate";
        public const string RESET = "reset";
        public const string MAP = "map";
        public const string AVAILABLE = "available";
        public const string FOLLOW = "follow";
        public const string UNFOLLOW = "unfollow";
        public const string QUEUE = "queue";
        public const string EXECUTE = "execute";
        public const string CHECK = "check";
        public const string CALLBACK = "callback";
        public const string UPDATE = "update";
        public const string CONVERT = "convert";
        public const string ASSIGN = "assign";
        public const string ADD = "add";
        public const string REMOVE = "remove";
        public const string BATCH_ACTION = "batchaction";
        public const string PRIO = "prio";
        public const string INITIAL = "initial";

        // *** category;
        public const string BY_TYPE = "bytype";

        public const string UNASSIGN = "unassign";
        public const string QUICK = "quick";

        // *** conversion;
        public const string MARKDOWN_2_HTML = "markdown2html";

        public const string HTML_2_MARKDOWN = "html2markdown";
        public const string MIME_TYPE = "mimetype";
        public const string THUMBNAIL = "thumbnail";

        // *** document;
        public const string FILE = "file";

        public const string TRASH = "trash";
        public const string UNTRASH = "untrash";
        public const string ARCHIVE = "archive";
        public const string UNARCHIVE = "unarchive";
        public const string REVIEW = "review";
        public const string REVIEW_DECLINE = "reviewdecline";
        public const string REMOVAL = "removal";
        public const string PDF_CONVERSION = "pdfconversion";
        public const string SNAP = "snap";
        public const string COMPARE = "compare";
        public const string DISABLE_RIGHTS_CHECK = "disablerightscheck";
        public const string DUPLICATE = "duplicate";
        public const string TEMPLATE = "template";
        public const string BIO = "bio";
        public const string COMPLETE = "complete";
        public const string CLIP_BOARD = "clipboard";
        public const string RELEASE = "release";

        // *** skin;
        public const string CUSTOMER_CSS = "customer.css";

        // *** app-initializer;
        public const string RESET_DEMO_INSTANCE = "resetdemoinstance";

        // *** instance-control;
        public const string BY_ID = "byid";

        public const string INSTANCE = "instance";
        public const string CREDENTIAL = "credential";
        public const string INVITEE = "invitee";
        public const string WITH_DATA_SOURCES = "withdatasources";
        public const string FAV_DATA_SOURCE = "favdatasource";
        public const string DISPLAY_NAME = "displayname";
        public const string SIGNUP = "signup";
        public const string DAYS = "days";
        public const string UNLIMITED = "unlimited";
        public const string LIST = "list";
        public const string TWO_FACTOR_AUTH = "twofactorauth";
        public const string QR_CODE = "qrcode";

        // *** preference;
        public const string CACHE = "cache";

        public const string REINIT = "reinit";
        public const string BY_PREFERENCE_KEY = "bypreferencekey";
        public const string BY_UNRESTRICTED_KEY = "byunrestrictedkey";
        public const string UNRESTRICTED = "unrestricted";

        // *** licence;
        public const string USAGE = "usage";

        public const string GENERATE = "generate";

        // *** logon;
        public const string LOGIN_DATA = "logindata";

        public const string LOGOUT = "logout";
        public const string IMPERSONATE = "impersonate";
        public const string PRE_LOGIN = "prelogin";
        public const string PRE_CHANGE_INSTANCE = "prechangeinstance";
        public const string LOGIN = "login";

        // *** logon/saml;
        public const string META_DATA = "metadata";

        public const string LOGOUT_REQUEST = "logoutrequest";

        // *** notification;
        public const string SESSION_COUNT = "sessioncount";

        // *** person;
        public const string BY_ACCOUNT = "byaccount";

        public const string BY_CONTACT = "bycontact";
        public const string PERSONAL_DATA = "personaldata";
        public const string INSTANCE_OWNER = "instanceowner";
        public const string DEMO = "demo";

        // *** person/account-setting;
        public const string ALL = "all";

        // *** search;
        public const string SEARCH = "search";

        public const string REINDEX_ALL = "reindexall";
        public const string INDEX = "index";
        public const string AUTOCOMPLETE = "autocomplete";

        // *** PARAM;
        // *** general;
        public const string PARAM_COUNT = "count";

        public const string PARAM_OFFSET = "offset";
        public const string PARAM_LIMIT = "limit";
        public const string PARAM_ORDER_BY = "orderBy";
        public const string PARAM_ORDER_DIR = "orderDir";
        public const string PARAM_LANGUAGE_KEY = "languageKey";
        public const string PARAM_ACTIVE = "active";
        public const string PARAM_ACTIVE_ONLY = "activeOnly";
        public const string PARAM_HIDE_ACTIVE = "hideActive";
        public const string PARAM_REGENERATE = "regenerate";
        public const string PARAM_ENTITY_TYPE_ID = "entityTypeId";
        public const string PARAM_FORCE_ATTACHMENT = "forceAttachment";
        public const string PARAM_ACTION_CODE = "actionCode";

        // *** activity;
        public const string PARAM_ACTIVITY_ID = "activityId";

        public const string PARAM_MESSAGE_TYPE = "messageType";
        public const string PARAM_PERSON_ACCOUNT_ID = "personAccountId";
        public const string PARAM_DOCUMENT_LANGUAGE_VERSION_LANGUAGE_KEY = "documentLanguageVersionLanguageKey";
        public const string PARAM_NOTIFICATION_STATUS = "notificationStatus";

        // *** category;
        public const string PARAM_CATEGORY_ID = "categoryId";

        public const string PARAM_PERSONAL = "personal";
        public const string PARAM_QUICK = "quick";
        public const string PARAM_LOAD_USAGE = "loadUsage";
        public const string PARAM_LOAD_FOLLOWING_LANGUAGE_KEY = "loadFollowingLanguageKey";

        // *** category-type;
        public const string PARAM_CATEGORY_TYPE_ID = "categoryTypeId";

        // *** client;
        public const string PARAM_CLIENT_ID = "clientId";

        public const string PARAM_LOAD_CLIENT_PREFERENCES = "loadClientPreferences";

        // *** comment;
        public const string PARAM_COMMENT_ID = "commentId";

        public const string PARAM_MENTIONED_ACCOUNT_ID = "mentionedAccountId";

        // *** document;
        public const string PARAM_DOCUMENT_ID = "documentId";

        public const string PARAM_DOCUMENT_LANGUAGE_VERSION_ID = "documentLanguageVersionId";
        public const string PARAM_DOCUMENT_LANGUAGE_VERSION_CAS_ID = "documentLanguageVersionCasId";
        public const string PARAM_ALL_LANGUAGES = "allLanguages";
        public const string PARAM_ALL_VERSIONS = "allVersions";
        public const string PARAM_LANGUAGE_PREFER = "languagePrefer";
        public const string PARAM_LANGUAGE_EXCLUDE = "languageExclude";
        public const string PARAM_CATEGORY_IDS_AND = "categoryIdsAnd";
        public const string PARAM_CATEGORY_IDS_OR = "categoryIdsOr";
        public const string PARAM_ROLE_RIGHT_ROLE_ID = "roleRightRoleId";
        public const string PARAM_ROLE_RIGHT_TYPE = "roleRightType";
        public const string PARAM_LANGUAGE_AGGREGATIONS = "languageAggregations";
        public const string PARAM_LAST_CHANGE_AGGREGATIONS = "lastChangeAggregations";
        public const string PARAM_READ = "read";
        public const string PARAM_UPDATE_READ_TIME = "updateReadTime";
        public const string PARAM_RELATED_DOCUMENT_ID = "relatedDocumentId";
        public const string PARAM_SHOW_INVALID = "showInvalid";
        public const string PARAM_SHOW_INVALID_ONLY = "showInvalidOnly";
        public const string PARAM_SHOW_BIOS = "showBios";
        public const string PARAM_SHOW_TEMPLATES = "showTemplates";
        public const string PARAM_SHOW_ARCHIVED = "showArchived";
        public const string PARAM_SHOW_REMOVED = "showRemoved";
        public const string PARAM_SHOW_BIOS_ONLY = "showBiosOnly";
        public const string PARAM_SHOW_TEMPLATES_ONLY = "showTemplatesOnly";
        public const string PARAM_SHOW_ARCHIVED_ONLY = "showArchivedOnly";
        public const string PARAM_SHOW_REMOVED_ONLY = "showRemovedOnly";
        public const string PARAM_SHOW_LANDSCAPE_ONLY = "showLandscapeOnly";
        public const string PARAM_HIDE_LANDSCAPE = "hideLandscape";
        public const string PARAM_FOLLOWER_ACCOUNT_ID = "followerAccountId";
        public const string PARAM_FOLLOWING = "following";
        public const string PARAM_CATEGORY_FOLLOWER_ACCOUNT_ID = "categoryFollowerAccountId";
        public const string PARAM_CATEGORY_FOLLOWING = "categoryFollowing";
        public const string PARAM_ADDITIONAL_PROPERTY_FOLLOWER_ACCOUNT_ID = "additionalPropertyFollowerAccountId";
        public const string PARAM_ADDITIONAL_PROPERTY_FOLLOWING = "additionalPropertyFollowing";
        public const string PARAM_MARKED_HELPFUL_BY_CONTACT_ID = "markedHelpfulByContactId";
        public const string PARAM_LAST_CHANGED_SINCE_DAYS = "lastChangedSinceDays";
        public const string PARAM_LAST_CHANGED_SINCE = "lastChangedSince";
        public const string PARAM_RESPONSIBLE_CONTACT_ID = "responsibleContactId";
        public const string PARAM_RESPONSIBLE = "responsible";
        public const string PARAM_AUTHORSHIP_ACCOUNT_ID = "authorshipAccountId";
        public const string PARAM_AUTHORSHIP = "authorship";
        public const string PARAM_DOCUMENT_EXPIRES_IN_DAYS = "documentExpiresInDays";
        public const string PARAM_DOCUMENT_EXPIRES_ON = "documentExpiresOn";
        public const string PARAM_DOCUMENT_EXPIRES = "documentExpires";
        public const string PARAM_DOCUMENT_HAS_ARCHIVED_BEGIN = "documentHasArchivedBegin";
        public const string PARAM_SHOW_LANGUAGE_VERSION_REVIEWED_ONLY = "showLanguageVersionReviewedOnly";
        public const string PARAM_HIDE_LANGUAGE_VERSION_REVIEWED = "hideLanguageVersionReviewed";
        public const string PARAM_SHOW_LANGUAGE_VERSION_READY_ONLY = "showLanguageVersionReadyOnly";
        public const string PARAM_HIDE_LANGUAGE_VERSION_READY = "hideLanguageVersionReady";
        public const string PARAM_SHOW_LANGUAGE_VERSION_CURRENTLY_IN_PROCESSING_ONLY = "showLanguageVersionCurrentlyInProcessingOnly";
        public const string PARAM_HIDE_LANGUAGE_VERSION_CURRENTLY_IN_PROCESSING = "hideLanguageVersionCurrentlyInProcessing";
        public const string PARAM_SHOW_LANGUAGE_VERSION_REVIEW_REQUESTED_ONLY = "showLanguageVersionReviewRequestedOnly";
        public const string PARAM_HIDE_LANGUAGE_VERSION_REVIEW_REQUESTED = "hideLanguageVersionReviewRequested";
        public const string PARAM_SHOW_SNAP_ONLY = "showSnapOnly";
        public const string PARAM_HIDE_SNAP = "hideSnap";
        public const string PARAM_MIME_TYPE_ID = "mimeTypeId";
        public const string PARAM_MIME_TYPE = "mimeType";
        public const string PARAM_MENTIONED_ACCOUNT_ID_IN_COMMENT = "mentionedAccountIdInComment";
        public const string PARAM_MENTIONED_ACCOUNT_ID_IN_COMMENT_UNREAD = "mentionedAccountIdInCommentUnread";
        public const string PARAM_CLIP_BOARD = "clipBoard";
        public const string PARAM_LOAD_RESPONSIBLES = "loadResponsibles";
        public const string PARAM_LOAD_AUTHORS = "loadAuthors";
        public const string PARAM_LOAD_LAST_AUTHOR_ONLY = "loadLastAuthorOnly";
        public const string PARAM_LOAD_FOLLOWERS = "loadFollowers";
        public const string PARAM_LOAD_AM_I_FOLLOWING = "loadAmIFollowing";
        public const string PARAM_LOAD_MARKED_AS_HELPFUL_BY = "loadMarkedAsHelpfulBy";
        public const string PARAM_OLD_DOCUMENT_LANGUAGE_VERSION_ID = "oldDocumentLanguageVersionId";
        public const string PARAM_NEW_DOCUMENT_LANGUAGE_VERSION_ID = "newDocumentLanguageVersionId";
        public const string PARAM_TEMPLATE_TO_DOCUMENT = "templateToDocument";
        public const string PARAM_DOCUMENT_TO_TEMPLATE = "documentToTemplate";
        public const string PARAM_ATTACHMENT_ORDER_BY = "attachmentOrderBy";
        public const string PARAM_ATTACHMENT_ORDER_DIR = "attachmentOrderDir";
        public const string PARAM_UNIQUE_LOCK_ID = "uniqueLockId";
        /** load attachments related to current version */
        public const string PARAM_LOAD_ATTACHMENTS = "loadAttachments";
        /** load attachments related to current language */
        public const string PARAM_LOAD_LANGUAGE_ATTACHMENTS = "loadLanguageAttachments";
        /** load attachments related to whole document */
        public const string PARAM_LOAD_DOCUMENT_ATTACHMENTS = "loadDocumentAttachments";
        public const string PARAM_LOAD_RELATED_DOCUMENT_IDS = "loadRelatedDocumentIds";
        public const string PARAM_LOAD_ADDITIONAL_PROPERTIES = "loadAdditionalProperties";
        public const string PARAM_LOAD_EXTENSION_VALUES = "loadExtensionValues";
        public const string PARAM_LOAD_SNAP_FLAG = "loadSnapFlag";
        public const string PARAM_LOAD_COMMENTS = "loadComments";
        public const string PARAM_LOAD_ROLE_RIGHTS = "loadRoleRights";
        public const string PARAM_IGNORE_REMOVE_TIME = "ignoreRemoveTime";
        public const string PARAM_PUBLISH_REQUEST = "publishRequest";
        public const string PARAM_VALID_BEGIN_IN_SECONDS_REQUEST = "validBeginInSecondsRequest";
        public const string PARAM_ARCHIVED_BEGIN_IN_SECONDS_REQUEST = "archivedBeginInSecondsRequest";
        public const string PARAM_LOAD_HTML = "loadHtml";
        public const string PARAM_TRANSLATE_TO = "translateTo";
        public const string PARAM_LOAD_PDF = "loadPdf";
        public const string PARAM_SIZE = "size";
        public const string PARAM_LOAD_THUMBNAIL = "loadThumbnail";
        public const string PARAM_LOAD_CATEGORIES_PUBLIC_ONLY = "loadCategoriesPublicOnly";
        public const string PARAM_LOAD_CATEGORIES = "loadCategories";
        public const string PARAM_RESOLVE_COMPOUNDS = "resolveCompounds";
        public const string PARAM_FORCE_RESOLVE_COMPOUNDS = "forceResolveCompounds";

        // *** additional-property;
        public const string PARAM_ADDITIONAL_PROPERTY_ID = "additionalPropertyId";

        public const string PARAM_ADDITIONAL_PROPERTY_TYPE = "additionalPropertyType";
        public const string PARAM_PROPERTY_TYPE = "propertyType";

        // *** attachment;
        public const string PARAM_ATTACHMENT_ID = "attachmentId";

        // *** saved-search;
        public const string PARAM_SAVED_SEARCH_ID = "savedSearchId";

        public const string PARAM_ON_DASHBOARD = "onDashboard";
        public const string PARAM_SHOW_PERSONAL_ONLY = "showPersonalOnly";
        public const string PARAM_HIDE_PERSONAL = "hidePersonal";

        // *** temp-file;
        public const string PARAM_CONTEXT_TOKEN = "contextToken";

        public const string PARAM_FILE_TOKEN = "fileToken";
        public const string PARAM_FILE_TYPE = "fileType";

        // *** instance-control;
        public const string PARAM_INSTANCE_ID = "instanceId";

        public const string PARAM_INSTANCE = "instance";
        public const string PARAM_CREDENTIAL_ID = "credentialId";
        public const string PARAM_DATA_SOURCE_ID = "dataSourceId";
        public const string PARAM_DATA_SOURCE_NAME = "dataSourceName";
        public const string PARAM_DATA_SOURCE_REFERENCE = "dataSourceReference";
        public const string PARAM_DAYS = "days";
        public const string PARAM_DISPLAY_NAME = "displayName";

        // *** preference;
        public const string PARAM_PREFERENCE_ID = "preferenceId";

        public const string PARAM_PREFERENCE_KEY = "preferenceKey";

        // *** language;
        public const string PARAM_LANGUAGE_ID = "languageId";

        // *** logon;
        public const string PARAM_REMEMBER_ME = "rememberMe";

        public const string PARAM_OVERWRITE_SERVER_URL = "overwriteServerUrl";

        // *** logon/auto;
        public const string PARAM_LOGIN_TOKEN_SERIES_ID_BASE64 = "loginTokenSeriesIdBase64";

        public const string PARAM_LOGIN_TOKEN_BASE64 = "loginTokenBase64";

        // *** person;
        public const string PARAM_ACCOUNT_ID = "accountId";

        public const string PARAM_CONTACT_ID = "contactId";
        public const string PARAM_INCLUDE_SYSTEM_USER = "includeSystemUser";
        public const string PARAM_RESPONSIBLE_FOR_DOCUMENT_ID = "responsibleForDocumentId";
        public const string PARAM_AUTHORSHIP_FOR_DOCUMENT_ID = "authorshipForDocumentId";
        public const string PARAM_AUTHORSHIP_FOR_LANGUAGE_VERSION_ID = "authorshipForLanguageVersionId";
        public const string PARAM_AUTHORSHIP_FOR_LANGUAGE_ID = "authorshipForLanguageId";
        public const string PARAM_ACCOUNT_LOGIN = "accountLogin";
        public const string PARAM_INSTANCE_OWNER_ONLY = "instanceOwnerOnly";
        public const string PARAM_HIDE_INSTANCE_OWNER = "hideInstanceOwner";
        public const string PARAM_FOLLOWED_CATEGORY_ID = "followedCategoryId";
        public const string PARAM_FOLLOWED_ADDITIONAL_PROPERTY_ID = "followedAdditionalPropertyId";
        public const string PARAM_FOLLOWED_DOCUMENT_ID = "followedDocumentId";
        public const string PARAM_FOLLOWED_LANGUAGE_ID = "followedLanguageId";
        public const string PARAM_MARKED_AS_HELPFUL_DOCUMENT_ID = "markedAsHelpfulDocumentId";
        public const string PARAM_MARKED_AS_HELPFUL_DOCUMENT_LANGUAGE_VERSION_ID = "markedAsHelpfulDocumentLanguageVersionId";
        public const string PARAM_MARKED_AS_HELPFUL_LANGUAGE_ID = "markedAsHelpfulLanguageId";
        public const string PARAM_LOAD_CLIENTS = "loadClients";
        public const string PARAM_LOAD_ADDRESSES = "loadAddresses";
        public const string PARAM_LOAD_COMMUNICATION_DATA = "loadCommunicationData";
        public const string PARAM_LOAD_ROLES = "loadRoles";
        public const string PARAM_LOAD_CONTACT_IMAGES = "loadContactImages";
        public const string PARAM_LOAD_BIO_DOCUMENT_LANGUAGE_KEY = "loadBioDocumentLanguageKey";

        // *** person/contact-image;
        public const string PARAM_CONTACT_IMAGE_ID = "contactImageId";

        // *** role;
        public const string PARAM_ROLE_ID = "roleId";

        public const string PARAM_NOT_REMOVABLE = "notRemovable";
        public const string PARAM_INITIALLY_ASSIGNED_TO_ACCOUNT = "initiallyAssignedToAccount";
        public const string PARAM_INITIALLY_ASSIGNED_TYPE_TO_DOCUMENT = "initiallyAssignedTypeToDocument";

        // *** search;
        public const string PARAM_QUERY = "query";

        public const string PARAM_CATEGORY_AGGREGATIONS = "categoryAggregations";
        public const string PARAM_HIGHLIGHT_FIELD = "highlightField";

        // *** task;
        public const string PARAM_TASK_ID = "taskId";

        public const string PARAM_AUTHOR_ACCOUNT_ID = "authorAccountId";
        public const string PARAM_ASSIGNED_ONLY = "assignedOnly";
        public const string PARAM_HIDE_TASKS_OF_REMOVED_DOCUMENTS = "hideTasksOfRemovedDocuments";

        // *** task/status;
        public const string PARAM_STATUS_ID = "statusId";

        public const string PARAM_STATUS_ID_EXCLUDE = "statusIdExclude";

        // *** VALUE;
        // *** document;
        public const string VALUE_READ = "true";

        public const string VALUE_UNREAD = "false";
        public const string VALUE_UNREAD_SINCE_ACCOUNT_CREATION = "unreadSinceAccountCreation";
        public const string VALUE_UNREAD_SINCE_LAST_LOGIN = "unreadSinceLastLogin";
        public const string VALUE_PUBLISH_REQUEST_UNREVIEWED = "unreviewed";
        public const string VALUE_PUBLISH_REQUEST_REVIEWED = "reviewed";
        public const string VALUE_PUBLISH_REQUEST_REVIEW_REQUESTED = "reviewRequested";
        public const string VALUE_UPDATE_READ_TIME_ALL = "all";

        #endregion Protocol Constants

        #region Status Codes

        public const string HEADER_STATUS_CODE = "mtrStatusCode";
        public const string HEADER_STATUS_MSG = "mtrStatusMsg";

        // *** general status starting from 0;
        public const int GENERAL_OK = 0;

        public const int GENERAL_UNKNOWN = 1;
        public const int GENERAL_INVALID_IDS = 2;
        public const int ADDRESS_INVALID = 3;
        public const int COMMUNICATION_DATA_TYPE_INVALID = 4;
        public const int COMMUNICATION_DATA_INVALID = 5;
        public const int LOGON_NOT_LOGGED_IN = 6;
        public const int LOGON_CREDENTIALS_INVALID = 7;
        public const int I18N_INVALID = 8;
        public const int PASSWORD_INVALID = 9;
        public const int DATASOURCE_INVALID = 10;
        public const int SYSTEM_ACCOUNT_REQUIRED = 11;
        public const int CREDENTIAL_INVALID = 12;
        public const int PREFERENCE_INVALID = 13;
        public const int GENERAL_INVALID_API_OBJECT = 14;
        public const int BAD_REQUEST = 15;
        public const int GENERAL_CONFLICT = 16;
        public const int GENERAL_NOT_EXIST = 17;
        public const int FORBIDDEN = 18;
        public const int LICENCE_INVALID = 19;
        public const int LICENCE_USER_LIMIT = 20;
        public const int LICENCE_CAS_SIZE_LIMIT = 21;
        public const int INSTANCE_OWNER_REQUIRED = 22;
        public const int LICENCE_REVIEW_GROUP_REQUIRED = 25;
        public const int LOGON_NO_VALID_DATA_SOURCE = 27;
        public const int REMOTE_ADDR_NOT_ALLOWED = 28;
        public const int TABLE_LOCK_INVALID = 29;
        public const int LOGON_INVALID_SERVER = 30;
        public const int OAUTH2_NO_ACCESS_TOKEN = 31;
        public const int OAUTH2_NOT_SUPPORTED = 32;
        public const int OAUTH2_NO_USER_INFO = 33;
        public const int AUTH_2FA_NOT_SUPPORTED = 34;
        public const int AUTH_2FA_LOGIN_INVALID = 35;
        public const int AUTH_2FA_KEY_INVALID = 36;
        public const int AUTH_2FA_VERFICATION_CODE_INVALID = 37;
        public const int AUTH_2FA_REQUIRED = 38;
        public const int LOGIN_TOKEN_INVALID = 39;
        public const int CREDENTIAL_NOT_INVITABLE_SYSTEM = 40;
        public const int CREDENTIAL_NOT_INVITABLE_CURRENT = 41;
        public const int CREDENTIAL_NOT_INVITABLE_EXISTING = 42;

        // *** document status starting from 100;
        public const int DOCUMENT_INVALID = 101;

        public const int DOCUMENT_LANGUAGE_VERSION_INVALID = 102;
        public const int ATTACHMENT_INVALID = 103;
        public const int DOCUMENT_FORBIDDEN = 104;
        public const int ADDITIONAL_PROPERTY_NOT_FOUND = 105;
        public const int SAVED_SEARCH_INVALID = 106;
        public const int ADDITIONAL_PROPERTY_INVALID = 107;
        public const int EXTENSION_VALUE_INVALID = 108;
        public const int DOCUMENT_LOCK_INVALID = 109;
        public const int DOCUMENT_STATUS = 110;
        public const int DOCUMENT_LANGUAGE_COUNT = 111;
        public const int DOCUMENT_TITLE_EXCEEDS_LENGTH = 112;
        public const int DOCUMENT_ABSTRACT_EXCEEDS_LENGTH = 113;
        public const int DOCUMENT_VERSION_COMMENT_EXCEEDS_LENGTH = 114;
        public const int DOCUMENT_CLIP_BOARD_EXCEEDS_LIMIT = 115;
        public const int DOCUMENT_NOT_IN_TRASH = 116;
        public const int DOCUMENT_CLIP_BOARD_NOTHING_UPDATED = 117;
        public const int DOCUMENT_TRANSLATION_DEEPL_BAD_REQUEST = 118;
        public const int DOCUMENT_TRANSLATION_DEEPL_AUTHORIZATION_FAILED = 119;
        public const int DOCUMENT_TRANSLATION_DEEPL_NOT_FOUND = 120;
        public const int DOCUMENT_TRANSLATION_DEEPL_REQUEST_SIZE_EXCEEDS_LIMIT = 121;
        public const int DOCUMENT_TRANSLATION_DEEPL_TOO_MANY_REQUESTS = 122;
        public const int DOCUMENT_TRANSLATION_DEEPL_QUOTA_EXCEEDED = 123;
        public const int DOCUMENT_TRANSLATION_DEEPL_RESOURCE_CURRENTLY_UNAVAILABLE = 124;
        public const int DOCUMENT_TRANSLATION_DEEPL_INTERNAL_ERROR = 125;

        // ### category/categoryType status codes
        public const int CATEGORY_TYPE_IN_USE = 200;

        public const int CATEGORY_TYPE_NOT_FOUND = 201;
        public const int CATEGORY_NOT_FOUND = 202;
        public const int CATEGORY_TYPE_INVALID = 203;
        public const int CATEGORY_INVALID = 204;

        // *** role status starting from 300;
        public const int ROLE_INVALID = 301;

        // *** cas status starting from 400;
        public const int CAS_INVALID = 401;

        // ### task status codes
        public const int TASK_NOT_FOUND = 500;

        public const int TASK_STATUS_NOT_FOUND = 501;
        public const int TASK_INVALID = 502;
        public const int TASK_STATUS_INVALID = 503;
        public const int TASK_INVALID_DESCRIPTION = 504;
        public const int TASK_INVALID_ASSIGNED_ROLE = 505;
        public const int TASK_INVALID_STATUS = 506;

        // ### person status codes
        public const int PERSON_NOT_FOUND = 600;

        public const int PERSON_ACCOUNT_LOGIN_ALREADY_EXISTS = 601;
        public const int PERSON_INVALID = 602;
        public const int PERSON_DUPLICATE_NAME = 603;
        public const int ACCOUNT_SETTING_INVALID = 604;
        public const int CONTACT_IMAGE_INVALID = 605;
        public const int CONTACT_FIRST_NAME_EXCEEDS_LENGTH = 606;
        public const int CONTACT_LAST_NAME_EXCEEDS_LENGTH = 607;
        public const int CONTACT_POSITION_EXCEEDS_LENGTH = 608;
        public const int ACCOUNT_LOGIN_EXCEEDS_LENGTH = 609;
        public const int ADDRESS_POSTAL_CODE_EXCEEDS_LENGTH = 610;
        public const int ADDRESS_CITY_EXCEEDS_LENGTH = 611;
        public const int ADDRESS_COUNTRY_EXCEEDS_LENGTH = 612;
        public const int ADDRESS_STREET_EXCEEDS_LENGTH = 613;
        public const int ADDRESS_HOUSE_NUMBER_EXCEEDS_LENGTH = 614;
        public const int COMMUNICATION_DATA_VALUE_EXCEEDS_LENGTH = 615;
        public const int COMMUNICATION_DATA_DESCRIPTION_EXCEEDS_LENGTH = 616;

        // *** client status starting from 700;
        public const int CLIENT_INVALID = 701;

        public const int CLIENT_PREFERENCE_INVALID = 702;

        // *** language status starting from 800;
        public const int LANGUAGE_INVALID = 801;

        // ### file related status codes
        public const int FILE_INVALID = 900;

        public const int FILE_SIZE_LARGE = 901;

        // *** notification related status codes;
        public const int NOTIFICATION_ERROR = 1001;

        public const int NOTIFICATION_NOT_SUPPORTED = 1002;
        public const int NOTIFICATION_NOT_ENABLED = 1003;
        public const int NOTIFICATION_MESSAGING_SERVICE_ERROR = 1004;
        public const int NOTIFICATION_REMOTE_UPDATE_ERROR = 1005;
        public const int NOTIFICATION_DELAYED = 1006;

        // *** activity related status codes;
        public const int ACTIVITY_INVALID = 1101;

        // *** comment status codes
        public const int COMMENT_INVALID = 1201;

        public const int COMMENT_INVALID_TEXT = 1202;
        public const int COMMENT_INVALID_DLV = 1203;

        // *** tracking status starting from 10000;
        public const int TRACKING_ITEM_INVALID = 10001;

        #endregion Status Codes
    }
}