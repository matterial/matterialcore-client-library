﻿using MatterialCore.Controller;
using MatterialCore.Entity;
using MatterialCore.Enums;
using MatterialCore.Interfaces;
using MatterialCore.Params;

//using System.Text.Json;
using Newtonsoft.Json;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security;
using System.Threading.Tasks;

using static MatterialCore.RestClient;

namespace MatterialCore
{
    ///<inheritdoc cref="IApiSession"/>

    internal sealed class ApiSession : RestClient, IApiSession
    {
        private static readonly Object uniqueLock = new Object();
        private static readonly Object uniqueLock2 = new Object();

        private LoginData? _CurrentLoginData;
        private bool _disposed = false;

        private Activities? _Activity;
        private Categories? _Category;
        private Clients? _Client;
        private Comments? _Comment;
        private Documents? _Document;
        private Instances? _Instance;
        private Languages? _Language;
        private Licenses? _License;
        private MtrTasks? _Task;
        private Persons? _Person;
        private Propertys? _Property;
        private Roles? _Role;
        private TempFiles? _TempFile;
        private TrackingItems? _TrackingItem;

        internal ApiSession()
        {
            TimeOut = Const.DefaultTimeOut;
        }

        internal List<Instance>? Instances { get; set; }

        public bool LogoffOnDispose { get; set; } = true;

        /// <summary>
        /// The default content language setting for content
        /// </summary>
        public LangKey DefaultContentLanguage
        {
            get
            {
                //Set default language based on default content and UI language setting.
                if (!(_CurrentLoginData is null) && !(_CurrentLoginData.AccountSettings is null))
                {
                    //Content language
                    if ((_CurrentLoginData.AccountSettings.ContainsKey(Const.ACCOUNT_SETTING_KEY_LANGUAGE_CONTENT)))
                    {
                        if (Enum.TryParse<LangKey>(_CurrentLoginData.AccountSettings[Const.ACCOUNT_SETTING_KEY_LANGUAGE_CONTENT].ToString(), out LangKey l))
                        {
                            return l;
                        }
                    }
                }
                return Matterial.DefaultLanguage;
            }

            set
            {
                if (!(_CurrentLoginData is null) && !(_CurrentLoginData.AccountSettings is null))
                {
                    if ((_CurrentLoginData.AccountSettings.ContainsKey(Const.ACCOUNT_SETTING_KEY_LANGUAGE_CONTENT)))
                    {
                        _CurrentLoginData.AccountSettings[Const.ACCOUNT_SETTING_KEY_LANGUAGE_CONTENT] = value.ToLangString();
                        //Save the account settings
                        UpdateAccountSettings(_CurrentLoginData.AccountSettings).Wait(TimeOut);
                    }
                }
            }
        }

        /// <summary>
        /// The default UI language
        /// </summary>
        public LangKey DefaultUiLanguage
        {
            get
            {
                //Set default language based on default content and UI language setting.
                if (!(_CurrentLoginData is null) && !(_CurrentLoginData.AccountSettings is null))
                {
                    //UI language
                    if ((_CurrentLoginData.AccountSettings.ContainsKey(Const.ACCOUNT_SETTING_KEY_LANGUAGE_UI)))
                    {
                        if (Enum.TryParse<LangKey>(_CurrentLoginData.AccountSettings[Const.ACCOUNT_SETTING_KEY_LANGUAGE_UI].ToString(), out LangKey l))
                        {
                            return l;
                        }
                    }
                }
                return Matterial.DefaultLanguage;
            }

            set
            {
                if (!(_CurrentLoginData is null) && !(_CurrentLoginData.AccountSettings is null))
                {
                    if ((_CurrentLoginData.AccountSettings.ContainsKey(Const.ACCOUNT_SETTING_KEY_LANGUAGE_UI)))
                    {
                        _CurrentLoginData.AccountSettings[Const.ACCOUNT_SETTING_KEY_LANGUAGE_UI] = value.ToLangString();
                        //Save the account settings
                        UpdateAccountSettings(_CurrentLoginData.AccountSettings).Wait(TimeOut);
                    }
                }
            }
        }

        public void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
            {
                if (LogoffOnDispose)
                {
                    Disconnect().Wait(TimeOut);
                }
            }
            _disposed = true;
        }

        internal static string CombineUri(params string[] uriParts)
        {
            lock (uniqueLock)
            {
                string uri = string.Empty;
                if (uriParts != null && uriParts.Length > 0)
                {
                    char[] trims = new char[] { '\\', '/' };
                    uri = (uriParts[0] ?? string.Empty).TrimEnd(trims);
                    for (int i = 1; i < uriParts.Length; i++)
                    {
                        if (!String.IsNullOrEmpty(uriParts[i]))
                        {
                            uri = string.Format("{0}/{1}", uri.TrimEnd(trims), (uriParts[i] ?? string.Empty).TrimStart(trims));
                        }
                    }
                }
                return uri;
            }
        }

        internal static string CombineUrlParams(params string[] urlParams)
        {
            //Combine URL parameters and make sure there is no double & char
            lock (uniqueLock2)
            {
                string urlParam = string.Empty;
                if (urlParams != null && urlParams.Length > 0)
                {
                    char[] trims = new char[] { '&', ' ' };
                    urlParam = (urlParams[0] ?? string.Empty).TrimEnd(trims);
                    for (int i = 1; i < urlParams.Length; i++)
                    {
                        urlParam = string.Format("{0}&{1}", urlParam.TrimEnd(trims), (urlParams[i] ?? string.Empty).TrimStart(trims));
                    }
                }
                return urlParam;
            }
        }

        internal static async Task<bool> SendMagicLink(string loginId, LangKey language)
        {
            //CReate logon object
            LogonCredentials lc = new LogonCredentials(loginId);
            RestClient rc = new RestClient();
            //Create query parameter
            ParameterCollection pc = new ParameterCollection { language.KeyAndValue() };
            string path = CombineUri(Const.MTR_BACKEND, Const.APPLICATION_PATH, Const.LOGON, Const.MAGIC_LINK);
            HttpResponseMessage response = await rc.Put(path, ExpectedResponse.Plain, lc, pc);
            rc.EnsureSuccessStatusCode(response);
            //string str = await response.Content.ReadAsStringAsync();
            string? s = response.Content.ReadAsStringAsync().Result;
            if (!String.IsNullOrEmpty(s))
            {
                //Return is “accepted” or “notSend”
                return (String.Compare(s, SEND_LOGIN_LINK_ACCEPTED, false) == 0);
            }
            return false;
        }

        internal async Task InitNTLM(string basePath)
        {
            BasePath = basePath;
            await PreLoginNTLM();

            //Set the login data of the current session
            _CurrentLoginData = null;
            _ = await LoginData();

            //Get Instances
            if ((_CurrentLoginData is null) || (_CurrentLoginData.AvailableInstances is null) || _CurrentLoginData.AvailableInstances.Count == 0)
            {
                throw new Exception("The current user does not own an instance.");
            }
            //Set available instances
            Instances = _CurrentLoginData.AvailableInstances;
        }

        /// <summary>
        /// Create a session with an existing session id.
        /// </summary>
        /// <param name="sessionId">The session id of an existing session.</param>
        /// <param name="basePath">The base path</param>
        /// <param name="instanceId">The id of the instance to connect to</param>
        /// <returns></returns>
        internal async Task Init(string sessionId, string? basePath, long instanceId)
        {
            if (!String.IsNullOrEmpty(basePath))
                BasePath = basePath;

            //Set the sessionID from parameter
            UpdateSession(sessionId);
            //Get the instances the user has access to.
            long instId = await VerifyInstance(instanceId);
            //Set the actual server URL to use
            await RedirectToInstance(instId);
            //Set the login data of the current session
            _ = await LoginData(true);
        }

        /// <summary>
        /// Initialize the Api, connect to the server and load.
        /// </summary>
        /// <param name="user">The user's login, mostly the email address.</param>
        /// <param name="password">Secure password string</param>
        /// <param name="basePath">The URL to the logon server. The default logon server is https://my.matterial.com</param>
        internal async Task Init(string user, SecureString password, string basePath)
        {
            BasePath = basePath;
            //Log in and load data sources
            LogonCredentials credentials = new LogonCredentials(user, password);
            Instances = await PreLogin(credentials);
            //Get the instances the user has access to.
            long instanceId = VerifyInstance(Instances);
            //Set the actual server URL to use
            await RedirectToInstance(instanceId);
            //Set the login data of the current session
            _ = await LoginData(true);
        }

        internal async Task InitMSOAuth()
        {
           
            await PreLoginMSOAuth();

            //Set the login data of the current session
            _CurrentLoginData = null;
            _ = await LoginData();

            //Get Instances
            if ((_CurrentLoginData is null) || (_CurrentLoginData.AvailableInstances is null) || _CurrentLoginData.AvailableInstances.Count == 0)
            {
                throw new Exception("The current user does not own an instance.");
            }
            //Set available instances
            Instances = _CurrentLoginData.AvailableInstances;
        }


        private long VerifyInstance(List<Instance> instances)
        {
            return VerifyInstance(instances, 0);
        }

        private long VerifyInstance(List<Instance> instances, long instanceId)
        {
            long retval = 0;
            //Check if we have instances

            //Check the parameter
            if (instanceId > 0)
            {
                if (instances.First(item => item.Id == instanceId) is null)
                {
                    throw new Exception("The current user does not own an instance with the id " + instanceId);
                }
                retval = instanceId;
            }
            else
            {
                //Take the first one
                retval = instances[0].Id;
            }

            return retval;
        }

        private async Task<long> VerifyInstance(long instanceId = 0)
        {
            //Get and check the instances the user has access to.
            Instances = await PreChangeInstance();
            return VerifyInstance(Instances, instanceId);
        }

        internal async Task<List<Instance>> PreChangeInstance()
        {
            string path = CombineUri(Const.MTR_BACKEND, Const.APPLICATION_PATH, Const.LOGON, Const.PRE_CHANGE_INSTANCE);
            //Get response
            ParameterCollection pc = new ParameterCollection();
            HttpResponseMessage response = await Put(path, ExpectedResponse.Json, pc);
            EnsureSuccessStatusCode(response);
            //Set the sessionID from cookie
            UpdateSession(response);
            Instances = JsonConvert.DeserializeObject<List<Instance>>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);
            //Check if we have instances
            if ((Instances is null) || Instances.Count == 0)
            {
                throw new Exception("The current user does not own an instance.");
            }
            return Instances;
        }

        /// <summary>
        /// Checks if user was successfully authenticated by ntlm-filter and redirects to web ui.
        /// </summary>
        /// <returns></returns>
        private async Task PreLoginNTLM()
        {
            //Clear session
            UpdateSession(String.Empty);
            string path = CombineUri(Const.MTR_BACKEND, Const.APPLICATION_PATH, Const.LOGON, Const.LOGIN, Const.NTLM);
            HttpResponseMessage response = await Get(path, ExpectedResponse.Nothing);
            EnsureSuccessStatusCode(response);
            //Set the sessionID from cookie
            UpdateSession(response);
            //Set the new base path which was returned by the server
            BasePath = response.RequestMessage.RequestUri.Scheme + "://" + response.RequestMessage.RequestUri.Host + "/";
        }

        private async Task<List<Instance>> PreLogin(LogonCredentials credentials)
        {
            //Clear session
            UpdateSession(String.Empty);
            string path = CombineUri(Const.MTR_BACKEND, Const.APPLICATION_PATH, Const.LOGON, Const.PRE_LOGIN);
            //Get response
            HttpResponseMessage response = await Post(path, ExpectedResponse.Json, credentials);
            EnsureSuccessStatusCode(response);
            //Set the sessionID from cookie
            UpdateSession(response);
            //Return list of instances (DataSources)
            List<Instance>? retVal = JsonConvert.DeserializeObject<List<Instance>>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);
            //Check if we have instances
            if ((retVal is null) || retVal.Count == 0)
            {
                throw new Exception("The current user does not own an instance.");
            }
            return retVal;
        }

        private async Task<List<Instance>> PreLoginMSOAuth()
        {
            //Clear session
            UpdateSession(String.Empty);
            string path = CombineUri(Const.MTR_BACKEND, Const.APPLICATION_PATH, Const.LOGON, Const.OAUTH2, Const.MICROSOFT);
            //Get response
            var httpClient = CreateClient(ExpectedResponse.Nothing);

            HttpResponseMessage response = await Get(path, ExpectedResponse.XHtml);
            EnsureSuccessStatusCode(response);
            var MSOAuthPath = response.RequestMessage.RequestUri.AbsoluteUri;
            //Now we have the Microsoft OAuth URI

            response = await httpClient.GetAsync(MSOAuthPath);
            //Set the sessionID from cookie
            //UpdateSession(response);
            //Return list of instances (DataSources)
            List<Instance>? retVal = JsonConvert.DeserializeObject<List<Instance>>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);
            //Check if we have instances
            if ((retVal is null) || retVal.Count == 0)
            {
                throw new Exception("The current user does not own an instance.");
            }
            return retVal;

        }
        private async Task RedirectToInstance(long instanceId)
        {
            string path = CombineUri(Const.MTR_BACKEND, Const.APPLICATION_PATH, Const.LOGON, Const.LOGIN, instanceId.ToString());
            HttpResponseMessage response = await Get(path, ExpectedResponse.Nothing);
            EnsureSuccessStatusCode(response);
            //Set the new base path which was returned by the server
            BasePath = response.RequestMessage.RequestUri.Scheme + "://" + response.RequestMessage.RequestUri.Host + "/";
        }

        #region PublicMembers

        public async Task ChangeInstance(long instanceId)
        {
            //Check if this instance is available
            long instId = await VerifyInstance(instanceId);
            //Set the actual server URL to use
            await RedirectToInstance(instId);
            //Set the login data of the current session
            _ = await LoginData(true);
        }

        public bool IsLoggedIn
        {
            get
            {
                try
                {
                    string path = CombineUri(Const.MTR_BACKEND, Const.APPLICATION_PATH, Const.LOGON);
                    HttpResponseMessage response = Get(path, ExpectedResponse.Plain).Result;
                    EnsureSuccessStatusCode(response);
                    bool retVal = false;
                    if (bool.TryParse(response.Content.ReadAsStringAsync().Result, out bool b))
                    {
                        retVal = b;
                    }
                    return retVal;
                }
                catch
                {
                }
                return false;
            }
        }

        public int TimeOut { get; set; }

        internal async Task<MtrVersion?> Version()
        {
            string path = CombineUri(Const.MTR_BACKEND, Const.APPLICATION_PATH, Const.VERSION);
            HttpResponseMessage response = await Get(path, ExpectedResponse.Json);
            EnsureSuccessStatusCode(response);
            return JsonConvert.DeserializeObject<MtrVersion>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);
        }

        string IApiSession.SessionId
        {
            get { return SessionId; }
        }

        public ICategory Category
        {
            get
            {
                if (_Category is null)
                    _Category = new Categories(SessionId, BasePath);
                return _Category;
            }
        }

        public IActivity Activity
        {
            get
            {
                if (_Activity is null)
                    _Activity = new Activities(SessionId, BasePath);
                return _Activity;
            }
        }

        public IClient Client
        {
            get
            {
                if (_Client is null)
                    _Client = new Clients(SessionId, BasePath);
                return _Client;
            }
        }

        public IComment Comment
        {
            get
            {
                if (_Comment is null)
                    _Comment = new Comments(SessionId, BasePath);
                return _Comment;
            }
        }

        public IDocument Document
        {
            get
            {
                if (_Document is null)
                    _Document = new Documents(SessionId, BasePath);
                return _Document;
            }
        }

        public IInstance Instance
        {
            get
            {
                if (_Instance is null)
                    _Instance = new Instances(SessionId, BasePath);
                return _Instance;
            }
        }

        public ILanguage Language
        {
            get
            {
                if (_Language is null)
                    _Language = new Languages(SessionId, BasePath);
                return _Language;
            }
        }

        public ILicense License
        {
            get
            {
                if (_License is null)
                    _License = new Licenses(SessionId, BasePath);
                return _License;
            }
        }

        public IMtrTask Task
        {
            get
            {
                if (_Task is null)
                    _Task = new MtrTasks(SessionId, BasePath);
                return _Task;
            }
        }

        public IPerson Person
        {
            get
            {
                if (_Person is null)
                    _Person = new Persons(SessionId, BasePath);
                return _Person;
            }
        }

        public IProperty Property
        {
            get
            {
                if (_Property is null)
                    _Property = new Propertys(SessionId, BasePath);
                return _Property;
            }
        }

        public IRole Role
        {
            get
            {
                if (_Role is null)
                    _Role = new Roles(SessionId, BasePath);
                return _Role;
            }
        }

        public ITempFile TempFile
        {
            get
            {
                if (_TempFile is null)
                    _TempFile = new TempFiles(SessionId, BasePath);
                return _TempFile;
            }
        }

        public ITrackingItem TrackingItem
        {
            get
            {
                if (_TrackingItem is null)
                    _TrackingItem = new TrackingItems(SessionId, BasePath);
                return _TrackingItem;
            }
        }

        public async Task<string?> Convert(ConversionAction action, string input)
        {
            string path = CombineUri(Const.MTR_BACKEND, Const.APPLICATION_PATH, Const.CONVERSION);
            ExpectedResponse expRespones = ExpectedResponse.Html;
            ExpectedResponse mediaType = ExpectedResponse.Plain;
            switch (action)
            {
                case ConversionAction.MarkdownToHtml:
                    path = ApiSession.CombineUri(path, Const.MARKDOWN_2_HTML);
                    break;

                case ConversionAction.HtmlToMarkdown:
                    path = ApiSession.CombineUri(path, Const.HTML_2_MARKDOWN);
                    mediaType = ExpectedResponse.Html;
                    expRespones = ExpectedResponse.Plain;
                    break;
            }
            HttpResponseMessage response = await Post(path, expRespones, input, MediaType(mediaType) ?? "");
            EnsureSuccessStatusCode(response);
            return await response.Content.ReadAsStringAsync();
        }

        public async Task Disconnect()
        {
            string path = CombineUri(Const.MTR_BACKEND, Const.APPLICATION_PATH, Const.LOGON);
            _CurrentLoginData = null;
            await Delete(path, ExpectedResponse.Plain);
        }

        public void Dispose()
        {
            Dispose(true);
        }

        public async Task<LoginData?> LoginData(bool forceReload = false)
        {
            if (_CurrentLoginData is null || forceReload)
            {
                string path = CombineUri(Const.MTR_BACKEND, Const.APPLICATION_PATH, Const.LOGON, Const.LOGIN_DATA);
                HttpResponseMessage response = await Get(path, ExpectedResponse.Json);
                EnsureSuccessStatusCode(response);
                //string str = await response.Content.ReadAsStringAsync();
                _CurrentLoginData = JsonConvert.DeserializeObject<LoginData>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);
            }
            return _CurrentLoginData;
        }

        public async Task<long> QueueSize()
        {
            string path = CombineUri(Const.MTR_BACKEND, Const.APPLICATION_PATH, Const.CONVERSION, Const.QUEUE);
            HttpResponseMessage response = await Get(path, ExpectedResponse.Plain);
            EnsureSuccessStatusCode(response);
            string str = await response.Content.ReadAsStringAsync();
            return str.ToLong(true);
        }

        public Person? LoggedOnPerson(bool forceReload = false)
        {
            if (_CurrentLoginData is null || forceReload)
            {
                _ = LoginData(forceReload).Result;
            }
            return _CurrentLoginData?.Person;
        }

        public async Task<LoginData?> UpdatePersonalData()
        {
            LoginData? myReturn = await UpdatePersonalData("", null);
            _CurrentLoginData = null;
            return myReturn;
        }

        public async Task<LoginData?> UpdatePersonalData(string contextToken)
        {
            return await UpdatePersonalData(contextToken, null);
        }

        public async Task<LoginData?> UpdatePersonalData(string contextToken, LangKey? languageKey)
        {
            if (_CurrentLoginData is null)
            {
                throw new Exception("The current user's login data was not loaded and modified.");
            }
            Person? person = _CurrentLoginData.Person;
            if (person is null)
            {
                throw new Exception("The current user's person information is null.");
            }

            string path = ApiSession.CombineUri(Entity.Person.RESTBasePath(), Const.PERSONAL_DATA);
            ParameterCollection pc = new ParameterCollection();
            if (!String.IsNullOrEmpty(contextToken))
            {
                pc.Add(Const.PARAM_CONTEXT_TOKEN, contextToken);
            }
            if (!(languageKey is null))
            {
                pc.Add(languageKey.KeyAndValue());
            }
            HttpResponseMessage response = await Put(path, ExpectedResponse.Json, person, pc);
            EnsureSuccessStatusCode(response);
            return JsonConvert.DeserializeObject<LoginData>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);
        }

        public async Task UpdateAccountSettings(Dictionary<string, object> AccountSettings)
        {
            string path = ApiSession.CombineUri(Entity.Person.RESTBasePath(), Const.ACCOUNT_SETTING, Const.ALL);
            HttpResponseMessage response = await Put(path, ExpectedResponse.Json, AccountSettings);
            EnsureSuccessStatusCode(response);
        }

        public async Task<bool> UploadBioDocument(string markdownContent, LangKey languageKey)
        {
            //Set the context token
            string contextToken = Guid.NewGuid().ToString();
            //Create temp file 
            ITempFile c = new TempFiles(SessionId, BasePath);
            TempFileDescriptor? tfd = await c.UploadText(markdownContent, contextToken, languageKey);
            if (!(tfd is null))
            {
                //Reload login data so we have current values
                _ = await LoginData(true);
                LoginData? ld = await UpdatePersonalData(contextToken, languageKey);
                if (!(ld is null))
                {
                    return true;
                }
            }
            return false;
        }

        public async Task<bool> UploadContactImage(string filePath, bool setActive = true)
        {
            //Reload login data so we have current values
            _ = await LoginData(true);
            Person? person = LoggedOnPerson();
            if (person is null)
            {
                throw new Exception("The current user's person information is null.");
            }
            //Use the Person function
            Persons pc = new Persons(SessionId, BasePath);
            return await pc.UploadContactImage(person, filePath, setActive);
        }

        public async Task<LoginData?> ChangeClient(long clientId)
        {
            string path = CombineUri(Const.MTR_BACKEND, Const.APPLICATION_PATH, Const.LOGON, Const.CLIENT, clientId.ToString());
            HttpResponseMessage response = await Put(path, ExpectedResponse.Json);
            EnsureSuccessStatusCode(response);
            return JsonConvert.DeserializeObject<LoginData>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);
        }

        public async Task<LoginData?> ChangePassword(PasswordContainer passwordContainer)
        {
            string path = CombineUri(Const.MTR_BACKEND, Const.APPLICATION_PATH, Const.LOGON);
            HttpResponseMessage response = await Put(path, ExpectedResponse.Json, passwordContainer);
            EnsureSuccessStatusCode(response);
            return JsonConvert.DeserializeObject<LoginData>(response.Content.ReadAsStringAsync().Result, DefaultSerializerOptions);
        }

        private enum RightsCheckAction : int
        {
            Disable,
            Enable,
            Reset
        }

        private async Task<bool> RightsCheck(RightsCheckAction disable)
        {
            string s = disable switch
            {
                RightsCheckAction.Disable => Const.ACTIVATE,
                RightsCheckAction.Enable => Const.DEACTIVATE,
                RightsCheckAction.Reset => Const.RESET,
                _ => Const.RESET
            };
            string path = CombineUri(Const.MTR_BACKEND, Const.APPLICATION_PATH, Const.LOGON, Const.DISABLE_RIGHTS_CHECK, s);
            HttpResponseMessage response = await Put(path, ExpectedResponse.Plain);
            EnsureSuccessStatusCode(response);
            string retval = await response.Content.ReadAsStringAsync();
            if (int.TryParse(retval, out int i))
            {
                return (i == 1);
            }
            return false;
        }

        public async Task<bool> DisableRightsCheck()
        {
            return await RightsCheck(RightsCheckAction.Disable);
        }

        public async Task<bool> EnableRightsCheck()
        {
            return await RightsCheck(RightsCheckAction.Enable);
        }

        public async Task<bool> ResetRightsCheck()
        {
            return await RightsCheck(RightsCheckAction.Reset);
        }

        private enum IndexEnableAction : int
        {
            DocumentDisable,
            DocumentEnable,
            PersonDisable,
            PersonEnable
        }

        private async Task<bool> IndexEnable(IndexEnableAction action)
        {
            HttpResponseMessage? response;
            string path = CombineUri(Const.MTR_BACKEND, Const.APPLICATION_PATH, Const.LOGON, Const.INDEX);
            switch (action)
            {
                case IndexEnableAction.DocumentDisable:
                    path = CombineUri(path, Const.DOCUMENT);
                    response = await Delete(path, ExpectedResponse.Plain);
                    break;

                case IndexEnableAction.DocumentEnable:
                    path = CombineUri(path, Const.DOCUMENT);
                    response = await Put(path, ExpectedResponse.Plain);
                    break;

                case IndexEnableAction.PersonDisable:
                    path = CombineUri(path, Const.PERSON);
                    response = await Delete(path, ExpectedResponse.Plain);
                    break;

                case IndexEnableAction.PersonEnable:
                    path = CombineUri(path, Const.PERSON);
                    response = await Put(path, ExpectedResponse.Plain);
                    break;

                default:
                    throw new ArgumentException("Invalid action type.");
            }

            EnsureSuccessStatusCode(response);
            string retval = await response.Content.ReadAsStringAsync();
            if (int.TryParse(retval, out int i))
            {
                return (i == 1);
            }
            return false;
        }

        public async Task<bool> DisableDocumentIndexing()
        {
            return await IndexEnable(IndexEnableAction.DocumentDisable);
        }

        public async Task<bool> EnableDocumentIndexing()
        {
            return await IndexEnable(IndexEnableAction.DocumentEnable);
        }

        public async Task<bool> DisablePersonIndexing()
        {
            return await IndexEnable(IndexEnableAction.PersonDisable);
        }

        public async Task<bool> EnablePersonIndexing()
        {
            return await IndexEnable(IndexEnableAction.PersonEnable);
        }

        #endregion PublicMembers
    }
}