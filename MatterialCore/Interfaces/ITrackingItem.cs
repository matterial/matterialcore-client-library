﻿using MatterialCore.Entity;
using MatterialCore.Params;

using System.Threading.Tasks;

namespace MatterialCore.Interfaces
{
    /// <summary>
    /// <see href="https://www.matterial.com/documentation/api/57">API-Reference</see>
    /// </summary>
    public interface ITrackingItem
    {
        /// <summary>
        /// Load tracking items <see href="https://www.matterial.com/documentation/api/57#get-tracking-items">API-Reference</see>
        /// </summary>
        /// <param name="options">Options defining the load bahaviour.</param>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <returns></returns>
        Task<ListResult<TrackingItem>?> Load(TrackingParams options);

        /// <summary>
        /// Search tracking items by query string. <see href="https://www.matterial.com/documentation/api/57#search-tracking-items-by-query">API-Reference</see>
        /// </summary>
        /// <param name="query">Query string. The asterisk character (*) means 'all'</param>
        /// <param name="options">Options defining the search bahaviour.</param>
        /// <returns></returns>
        Task<SearchResult<TrackingItem>?> Search(string query, TrackingSearchParams options);

        /// <summary>
        /// Reindex all tracking items. <see href="https://www.matterial.com/documentation/api/57#reindex-all-tracking-items">API-Reference</see>
        /// </summary>
        /// <returns></returns>
        Task Reindex();
    }
}