﻿using MatterialCore.Entity;
using MatterialCore.Enums;
using MatterialCore.Params.Person;

using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace MatterialCore.Interfaces
{
    /// <summary>
    /// <see href="https://www.matterial.com/documentation/api/66">API-Reference,</see><seealso href="https://www.matterial.com/documentation/api/68">Search</seealso>
    /// </summary>
    public interface IPerson
    {
        #region Search

        /// <summary>
        /// Search person by query string. <see href="https://www.matterial.com/documentation/api/68#search-persons-by-query">API-Reference</see>
        /// </summary>
        /// <param name="query">A string to search for (* means all)</param>
        /// <param name="options">Options defining the search bahaviour.</param>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <returns></returns>
        Task<SearchResult<Person>?> Search(string query, SearchPersonParams options);

        /// <summary>
        /// Search for auto-complete entries. <see href="https://www.matterial.com/documentation/api/68#search-autocomplete">API-Reference</see>
        /// </summary>
        /// <param name="query">String to search for.</param>
        /// <param name="options">Options defining the search bahaviour.</param>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <returns></returns>
        Task<List<SearchAutocompleteSuggest>?> SearchAutocomplete(string query, PersonAutocompleteParams options);

        #endregion Search

        #region Load

        /// <summary>
        /// Load person by account id. <see href="https://www.matterial.com/documentation/api/66#get-person-by-accountid">API-Reference</see>
        /// </summary>
        /// <param name="accountId">Account id</param>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <returns></returns>
        Task<Person?> LoadByAccountId(long accountId);

        /// <summary>
        /// Load person by account id. <see href="https://www.matterial.com/documentation/api/66#get-person-by-accountid">API-Reference</see>
        /// </summary>
        /// <param name="accountId">Account id</param>
        /// <param name="options">Options defining the load bahaviour.</param>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <returns></returns>
        Task<Person?> LoadByAccountId(long accountId, GetPersonByIdParams options);

        /// <summary>
        /// Load person by contact id. <see href="https://www.matterial.com/documentation/api/66#get-person-by-contactid">API-Reference</see>
        /// </summary>
        /// <param name="contactId">Contact id</param>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <returns></returns>
        Task<Person?> LoadByContactId(long contactId);

        /// <summary>
        /// Load person by contact id. <see href="https://www.matterial.com/documentation/api/66#get-person-by-contactid">API-Reference</see>
        /// </summary>
        /// <param name="contactId">Contact id</param>
        /// <param name="options">Options defining the load bahaviour.</param>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <returns></returns>
        Task<Person?> LoadByContactId(long contactId, GetPersonByIdParams options);

        /// <summary>
        /// Load list of persons. <see href="https://www.matterial.com/documentation/api/66#get-persons">API-Reference</see>
        /// </summary>
        /// <param name="options">Options defining the load bahaviour.</param>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <returns></returns>
        Task<ListResult<Person>?> Load(GetPersonParams options);

        #endregion Load

        #region Update

        /// <summary>
        /// Update a person by specifying the accoint id. <see href="https://www.matterial.com/documentation/api/66#update-person">API-Reference</see>
        /// </summary>
        /// <param name="person">Person object to update.</param>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <returns></returns>
        Task<Person?> Update(Person person);

        /// <summary>
        /// Update a person by specifying the accoint id. Additionally a context token of uploaded
        /// contact images can be specified. <see href="https://www.matterial.com/documentation/api/66#update-person">API-Reference</see>
        /// </summary>
        /// <param name="person">Person object to update.</param>
        /// <param name="contextToken">Context token identifying uploaded temp files.</param>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <returns></returns>
        Task<Person?> Update(Person person, string contextToken);

        /// <summary>
        /// Add or remove the instance flag of user identified by account id. <see href="https://www.matterial.com/documentation/api/66#set-instance-owner-flag">API-Reference</see>
        /// </summary>
        /// <param name="accountId"></param>
        /// <param name="instanceOwner">True to make the user instance owner.</param>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <returns></returns>
        Task<bool> SetInstanceOwner(long accountId, bool instanceOwner);

        /// <summary>
        /// Add or remove the demo flag of user identified by account id. <see href="https://www.matterial.com/documentation/api/66#set-demo-flag">API-Reference</see>
        /// </summary>
        /// <param name="accountId"></param>
        /// <param name="demoUser">True to make the user a demo user.</param>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <returns></returns>
        Task<bool> SetDemoUser(long accountId, bool demoUser);

        #endregion Update

        #region Export

        /// <summary>
        /// Get csv-export of filtered persons. This will be UTF-8 encoded. <see href="https://www.matterial.com/documentation/api/66#get-csv">API-Reference</see>
        /// </summary>
        /// <param name="options">Options defining the export bahaviour.</param>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <returns>String in csv format.</returns>
        Task<string?> GetCsvFile(PersonExportParams options);

        /// <summary>
        /// Export of filtered persons as vcards (v4.0). According to rfc6350 “UTF-8 is now the only
        /// possible character set.” <see href="https://www.matterial.com/documentation/api/66#get-vcards">API-Reference</see>
        /// </summary>
        /// <param name="options">Options defining the export bahaviour.</param>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <returns></returns>
        Task<string?> GetVCards(PersonExportParams options);

        /// <summary>
        /// Export of a person by contactId as vcard (v4.0). According to rfc6350 “UTF-8 is now the
        /// only possible character set.” <see href="https://www.matterial.com/documentation/api/66#get-vcard-by-contact">API-Reference</see>
        /// </summary>
        /// <param name="contactId">Contact id</param>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <returns></returns>
        Task<string?> GetVCard(long contactId);

        #endregion Export

        #region Create

        /// <summary>
        /// Create person and allow to link an uploaded contact image. Note: An account is NOT
        /// created when a person is created! That means using this API, you can create persons that
        /// will not be able to log in. Accounts are always invited and have to accept their
        /// invitation before an account is created. <see href="https://www.matterial.com/documentation/api/66#create-person">API-Reference</see>
        /// </summary>
        /// <param name="person">Person to create</param>
        /// <param name="contextToken">Context token identifying uploaded temp files.</param>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <returns></returns>
        Task<Person?> Create(Person person, string contextToken);

        /// <summary>
        /// Create person. Note: An account is NOT created when a person is created! That means
        /// using this API, you can create persons that will not be able to log in. Accounts are
        /// always invited and have to accept their invitation before an account is created. <see href="https://www.matterial.com/documentation/api/66#create-person">API-Reference</see>
        /// </summary>
        /// <param name="person">Person to create</param>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <returns></returns>
        Task<Person?> Create(Person person);

        #endregion Create

        #region Remove

        /// <summary>
        /// Remove a person (account) from current client. Special behaviour for multiple instance
        /// installation (SaaS): Only instance-admins are allowed to remove persons. This removes a
        /// person (account) also from current client (because there should only be one per
        /// instance) and removes the person (credential) from current instance. <see href="https://www.matterial.com/documentation/api/66#remove-person">API-Reference</see>
        /// </summary>
        /// <param name="accountId">Id of the person to remove.</param>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <returns></returns>
        Task<bool> Remove(long accountId);

        #endregion Remove

        #region Misc

        /// <summary>
        /// Reconverts the contact-images of a person. This regenerates all thumbnails even if they
        /// already exists. <see href="https://www.matterial.com/documentation/api/66#reconvert-contact-images-of-a-person">API-Reference</see>
        /// </summary>
        /// <param name="contactId">Id of the contact.</param>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <returns></returns>
        Task<bool> ReConvertContactImage(long contactId);

        /// <summary>
        /// (Re)converts the contact-images of all persons. <see href="https://www.matterial.com/documentation/api/66#convert-contact-images-of-all-persons">API-Reference</see>
        /// </summary>
        /// <param name="regenerate">
        /// If true this regenerates all thumbnails even if they already exist.
        /// </param>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <returns></returns>
        Task<long> ConvertAllPersonsContactImage(bool regenerate);

        /// <summary>
        /// Get a stream of the contact image of a contact. <see href="https://www.matterial.com/documentation/api/66#get-contact-image-by-contact-id-and-contact-image-id">API-Reference</see>
        /// </summary>
        /// <param name="contactId"></param>
        /// <param name="contactImageId"></param>
        /// <param name="format"></param>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <returns></returns>
        Task<Stream> GetContactImage(long contactId, long contactImageId, ContactImageFormats format);

        /// <summary>
        /// Re-index all persons. <see href="https://www.matterial.com/documentation/api/68#reindex-all-persons">API-Reference</see>
        /// </summary>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <returns></returns>
        Task ReindexPersons();

        /// <summary>
        /// Upload a contct image of the specified person.
        /// </summary>
        /// <param name="setActive">If true, the contact image will be set as active.</param>
        /// <param name="contactId">Id of the contact up store the contact image.</param>
        /// <param name="filePath">File path to the image</param>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <returns></returns>
        Task<bool> UploadContactImage(long contactId, string filePath, bool setActive = true);

        #endregion Misc
    }
}