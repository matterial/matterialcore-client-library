﻿using MatterialCore.Entity;
using MatterialCore.Enums;
using MatterialCore.Params;

using System.Collections.Generic;
using System.Threading.Tasks;

namespace MatterialCore.Interfaces
{
    /// <summary>
    /// <see href="https://www.matterial.com/documentation/api/209">API-Reference</see>
    /// </summary>
    public interface ICategory
    {
        #region Load

        /// <summary>
        /// Load categories. <see href="https://www.matterial.com/documentation/api/209#get-categories">API-Reference</see>
        /// </summary>
        /// <param name="options">Parameter defining the load bahaviour.</param>
        /// <returns></returns>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        Task<List<Category>?> Load(CategoryParams options);

        /// <summary>
        /// Load a category by id. <see href="https://www.matterial.com/documentation/api/209#get-category-by-id">API-Reference</see>
        /// </summary>
        /// <param name="categoryId">Id of the category to load.</param>
        /// <returns></returns>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        Task<Category?> Load(long categoryId);

        /// <summary>
        /// Load a category by id. <see href="https://www.matterial.com/documentation/api/209#get-category-by-id">API-Reference</see>
        /// </summary>
        /// <param name="categoryId">Id of the category to load.</param>
        /// <param name="options">Parameter defining the load bahaviour.</param>
        /// <returns></returns>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        Task<Category?> Load(long categoryId, CategoryByIdBaseParams options);

        /// <summary>
        /// Load categories by category type id. <see href="https://www.matterial.com/documentation/api/209#get-categories-by-categorytypeid">API-Reference</see>
        /// </summary>
        /// <param name="typeId">Category type id</param>
        /// <returns></returns>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        Task<List<Category>?> LoadByType(long typeId);

        /// <summary>
        /// Load categories by category type id. <see href="https://www.matterial.com/documentation/api/209#get-categories-by-categorytypeid">API-Reference</see>
        /// </summary>
        /// <param name="typeId">Category type id</param>
        /// <param name="options">Parameter defining the load bahaviour.</param>
        /// <returns></returns>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        Task<List<Category>?> LoadByType(long typeId, CategoryByTypeIdParams options);

        /// <summary>
        /// Load category types by category type id and language key. <see href="https://www.matterial.com/documentation/api/207#get-categorytype-by-id">API-Reference</see>
        /// </summary>
        /// <param name="categoryTypeId">Id of the category type</param>
        /// <param name="languageKey">Language id.</param>
        /// <returns></returns>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        Task<CategoryType?> LoadType(long categoryTypeId, LangKey languageKey);

        /// <summary>
        /// Load category types. <see href="https://www.matterial.com/documentation/api/207#get-categorytypes">API-Reference</see>
        /// </summary>
        /// <param name="options">Parameter defining the load bahaviour.</param>
        /// <returns></returns>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        Task<List<CategoryType>?> LoadTypes(CategoryTypeParams options);

        #endregion Load

        #region Create

        /// <summary>
        /// Create a new category type. <see href="https://www.matterial.com/documentation/api/207#create-categorytype">API-Reference</see>
        /// </summary>
        /// <param name="categoryType">The new category type to create.</param>
        /// <param name="languageKey">Language key</param>
        /// <returns>Id of the new category type.</returns>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        Task<long> CreateType(CategoryType categoryType, LangKey languageKey);

        /// <summary>
        /// Create a new category. A category requires a related category type. <see href="https://www.matterial.com/documentation/api/209#create-category">API-Reference</see>
        /// </summary>
        /// <param name="category">The new category to create. The property TypeId must be set.</param>
        /// <param name="languageKey">Language key</param>
        /// <returns>Id of the new category.</returns>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        Task<long> Create(Category category, LangKey languageKey);

        /// <summary>
        /// Update an existing category. <see href="https://www.matterial.com/documentation/api/209#update-category">API-Reference</see>
        /// </summary>
        /// <param name="category">Category containing the update values.</param>
        /// <param name="languageKey">Language key.</param>
        /// <returns>Id of the updated category.</returns>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        Task<long> Update(Category category, LangKey languageKey);

        /// <summary>
        /// Update category type. <see href="https://www.matterial.com/documentation/api/207#update-categorytype">API-Reference</see>
        /// </summary>
        /// <param name="categoryType">Category type containing the update values.</param>
        /// <param name="languageKey">Language key</param>
        /// <returns>Id of the updated category type.</returns>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        Task<long> UpdateType(CategoryType categoryType, LangKey languageKey);

        #endregion Create

        #region Remove

        /// <summary>
        /// Remove a category by its id. <see href="https://www.matterial.com/documentation/api/209#remove-category">API-Reference</see>
        /// </summary>
        /// <param name="id">Id of the category.</param>
        /// <returns></returns>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        Task Remove(long id);

        /// <summary>
        /// Remove category type by its id. Note: only category types not assigned to categories can
        /// be deleted. <see href="https://www.matterial.com/documentation/api/207#remove-categorytype">API-Reference</see>
        /// </summary>
        /// <param name="typeId">Id of the category type.</param>
        /// <returns></returns>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        Task RemoveType(long typeId);

        /// <summary>
        /// Remove all categories of a specified type. <see href="https://www.matterial.com/documentation/api/209#remove-categories-by-categorytypeid">API-Reference</see>
        /// </summary>
        /// <param name="typeId">Category type id.</param>
        /// <returns>Number of categories removed.</returns>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        Task<long> RemoveByTypeId(long typeId);

        #endregion Remove

        #region Follow

        /// <summary>
        /// Determine, if the current logged on user follows a category in a specified language.
        /// <see href="https://www.matterial.com/documentation/api/209#is-following-logged-in-account-follows-a-category">API-Reference</see>
        /// </summary>
        /// <param name="id">Id of the category.</param>
        /// <param name="languageKey">Language key.</param>
        /// <returns>True if successful</returns>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        Task<bool> IsFollowing(long id, LangKey languageKey);

        /// <summary>
        /// Let the current logged on user follow a category. <see href="https://www.matterial.com/documentation/api/209#follow-category-logged-in-account">API-Reference</see>
        /// </summary>
        /// <param name="id">Id of the category to follow.</param>
        /// <param name="languageKey">Language key</param>
        /// <returns>True if successful</returns>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        Task<bool> Follow(long id, LangKey languageKey);

        /// <summary>
        /// Let the current logged on user not follow a category anymore. <see href="https://www.matterial.com/documentation/api/209#unfollow-category-logged-in-account">API-Reference</see>
        /// </summary>
        /// <param name="id">Id of the category to un-follow.</param>
        /// <param name="languageKey">Language key</param>
        /// <returns>True if successful</returns>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        Task<bool> UnFollow(long id, LangKey languageKey);

        /// <summary>
        /// Let a user defined by account id follow a category. <see href="https://www.matterial.com/documentation/api/209#follow-category-other-account">API-Reference</see>
        /// </summary>
        /// <param name="id">Id of the category to follow.</param>
        /// <param name="accountId">Account id of the user.</param>
        /// <param name="languageKey">Language key</param>
        /// <returns></returns>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        Task<bool> Follow(long id, long accountId, LangKey languageKey);

        /// <summary>
        /// Let a user defined by account id un-follow a category. <see href="https://www.matterial.com/documentation/api/209#unfollow-category-other-account">API-Reference</see>
        /// </summary>
        /// <param name="id">Id of the category to un-follow.</param>
        /// <param name="accountId">Account id of the user.</param>
        /// <param name="languageKey">Language key</param>
        /// <returns></returns>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        Task<bool> UnFollow(long id, long accountId, LangKey languageKey);

        #endregion Follow

        #region Assign

        /// <summary>
        /// Assign a category to a list of documents defined by their id. <see href="https://www.matterial.com/documentation/api/209#add-category-to-documents">API-Reference</see>
        /// </summary>
        /// <param name="id">Id of the category.</param>
        /// <param name="documentIds">List of document ids</param>
        /// <returns>True if successful.</returns>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        Task<bool> Assign(long id, List<long> documentIds);

        /// <summary>
        /// Assign a category to a document defined by its id. <see href="https://www.matterial.com/documentation/api/209#add-category-to-documents">API-Reference</see>
        /// </summary>
        /// <param name="id">Id of the category.</param>
        /// <param name="documentId">Document id</param>
        /// <returns>True if successful.</returns>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        Task<bool> Assign(long id, long documentId);

        /// <summary>
        /// Assign a quick category to a list of documents defined by their id. <see href="https://www.matterial.com/documentation/api/209#add-quick-category-to-documents">API-Reference</see>
        /// </summary>
        /// <param name="documentIds">List of document ids</param>
        /// <returns>True if successful.</returns>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        Task<bool> AssignQuickCategory(List<long> documentIds);

        /// <summary>
        /// Assign a quick category to a document defined by its id. <see href="https://www.matterial.com/documentation/api/209#add-quick-category-to-documents">API-Reference</see>
        /// </summary>
        /// <param name="documentId">Document id</param>
        /// <returns>True if successful.</returns>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        Task<bool> AssignQuickCategory(long documentId);

        /// <summary>
        /// Un-assign a category from a list of documents defined by their id. <see href="https://www.matterial.com/documentation/api/209#remove-category-from-documents">API-Reference</see>
        /// </summary>
        /// <param name="id">Id of the category.</param>
        /// <param name="documentIds">List of document ids</param>
        /// <returns>True if successful.</returns>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        Task<bool> UnAssign(long id, List<long> documentIds);

        /// <summary>
        /// Un-assign a category from a document defined by its id. <see href="https://www.matterial.com/documentation/api/209#remove-category-from-documents">API-Reference</see>
        /// </summary>
        /// <param name="id">Id of the category.</param>
        /// <param name="documentId">List of document ids</param>
        /// <returns>True if successful.</returns>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        Task<bool> UnAssign(long id, long documentId);

        /// <summary>
        /// Un-assign a quick category from a list of documents defined by their id. <see href="https://www.matterial.com/documentation/api/209#remove-my-quick-category-from-documents">API-Reference</see>
        /// </summary>
        /// <param name="documentIds">List of document ids</param>
        /// <returns>True if successful.</returns>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        Task<bool> UnAssignQuickCategory(List<long> documentIds);

        /// <summary>
        /// Un-assign a quick category from a document defined by its id. <see href="https://www.matterial.com/documentation/api/209#remove-my-quick-category-from-documents">API-Reference</see>
        /// </summary>
        /// <param name="documentId">Document id</param>
        /// <returns>True if successful.</returns>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        Task<bool> UnAssignQuickCategory(long documentId);

        #endregion Assign
    }
}