﻿namespace MatterialCore.Interfaces
{
    /// <summary>
    /// Marker interface for all entities that are listable.
    /// </summary>
    public interface IListableEntity : IEntity
    {
        public string JsonType { get; }
    }
}