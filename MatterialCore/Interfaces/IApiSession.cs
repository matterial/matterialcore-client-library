﻿using MatterialCore.Entity;
using MatterialCore.Enums;

using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MatterialCore.Interfaces
{
    /// <summary>
    /// The IApiSession interface is the primary interface to use matterial functions in a context
    /// of a user session.
    /// </summary>
    public interface IApiSession : IDisposable, IConversion
    {
        /// <summary>
        /// If true, the session will be logged off on dispose. If false, the session will not be
        /// closed and the session ID will stay valid.
        /// Default: true
        /// </summary>
        public bool LogoffOnDispose { get; set; }

        /// <summary>
        /// Gets the session id.
        /// </summary>
        public string SessionId { get; }

        /// <summary>
        /// The default content language.
        /// </summary>
        public LangKey DefaultContentLanguage { get; set; }

        /// <summary>
        /// The default content language.
        /// </summary>
        public LangKey DefaultUiLanguage { get; set; }

        /// <summary>
        /// Determines if the current session is valid.
        /// </summary>
        bool IsLoggedIn { get; }

        /// <summary>
        /// Get the login information of the currenbtly logged in user
        /// </summary>
        /// <param name="forceReload">If true, reload the information about the person from server.</param>
        /// <returns></returns>
        Task<LoginData?> LoginData(bool forceReload = false);

        /// <summary>
        /// Get the current logged on person
        /// </summary>
        /// <param name="forceReload">If true, reload the information about the person from server.</param>
        /// <returns></returns>
        public Person? LoggedOnPerson(bool forceReload = false);

        /// <summary>
        /// Disconnect from server and close the session.
        /// </summary>
        /// <returns></returns>
        Task Disconnect();

        /// <summary>
        /// Change the current client. <see href="https://www.matterial.com/documentation/api/224#change-client">API-Reference</see>
        /// </summary>
        /// <param name="clientId">The id of the client to change to.</param>
        /// <exception cref="System.Net.Http.HttpRequestException"></exception>
        /// <returns></returns>
        Task<LoginData?> ChangeClient(long clientId);

        /// <summary>
        /// Change the current user's password <see href="https://www.matterial.com/documentation/api/224#change-password">API-Reference</see>
        /// </summary>
        /// <param name="passwordContainer">The object containing the new password.</param>
        /// <exception cref="System.Net.Http.HttpRequestException"></exception>
        /// <returns></returns>
        Task<LoginData?> ChangePassword(PasswordContainer passwordContainer);

        /// <summary>
        /// This function disables the rights check. That means, for the current user, rights are
        /// not checked when accessing data. <see href="https://www.matterial.com/documentation/api/224#activate-disablerightscheck">API-Reference</see>
        /// </summary>
        /// <exception cref="System.Net.Http.HttpRequestException"></exception>
        /// <returns></returns>
        Task<bool> DisableRightsCheck();

        /// <summary>
        /// This function enables the rights check. That means, for the current user, rights are
        /// checked when accessing data. <see href="https://www.matterial.com/documentation/api/224#deactivate-disablerightscheck">API-Reference</see>
        /// </summary>
        /// <exception cref="System.Net.Http.HttpRequestException"></exception>
        /// <returns></returns>
        Task<bool> EnableRightsCheck();

        /// <summary>
        /// Reset disable rights check for this session and reset to account-setting-value. <see href="https://www.matterial.com/documentation/api/224#reset-disablerightscheck">API-Reference</see>
        /// </summary>
        /// <exception cref="System.Net.Http.HttpRequestException"></exception>
        /// <returns></returns>
        Task<bool> ResetRightsCheck();

        /// <summary>
        /// Disable indexing of documents for the rest of this session. <see href="https://www.matterial.com/documentation/api/224#disable-indexing-of-documents">API-Reference</see>
        /// </summary>
        /// <exception cref="System.Net.Http.HttpRequestException"></exception>
        /// <returns></returns>
        Task<bool> DisableDocumentIndexing();

        /// <summary>
        /// (Re)enable indexing of documents for the rest of this session. <see href="https://www.matterial.com/documentation/api/224#reenable-indexing-of-documents">API-Reference</see>
        /// </summary>
        /// <exception cref="System.Net.Http.HttpRequestException"></exception>
        /// <returns></returns>
        Task<bool> EnableDocumentIndexing();

        /// <summary>
        /// Disable indexing of persons for the rest of this session. <see href="https://www.matterial.com/documentation/api/224#disable-indexing-of-persons">API-Reference</see>
        /// </summary>
        /// <exception cref="System.Net.Http.HttpRequestException"></exception>
        /// <returns></returns>
        Task<bool> DisablePersonIndexing();

        /// <summary>
        /// (Re)enable indexing of persons for the rest of this session. <see href="https://www.matterial.com/documentation/api/224#reenable-indexing-of-persons">API-Reference</see>
        /// </summary>
        /// <exception cref="System.Net.Http.HttpRequestException"></exception>
        /// <returns></returns>
        Task<bool> EnablePersonIndexing();

        /// <summary>
        /// Change the instance.
        /// </summary>
        /// <param name="instanceId">The ID of the instance to change to.</param>
        /// <exception cref="System.Net.Http.HttpRequestException"></exception>
        /// <returns></returns>
        Task ChangeInstance(long instanceId);

        #region Update Personal Data

        /// <summary>
        /// Updates data of a person for current logged-in account. Personal-data includes Addresses
        /// and CommunicationData. <see href="https://www.matterial.com/documentation/api/66#update-personal-data">API-Reference</see>
        /// </summary>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <returns></returns>
        Task<LoginData?> UpdatePersonalData();

        /// <summary>
        /// Updates data of a person for current logged-in account. Personal-data includes
        /// Addresses, CommunicationData, ContactImages and bio document. <see href="https://www.matterial.com/documentation/api/66#update-personal-data">API-Reference</see>
        /// </summary>
        /// <param name="contextToken">Context token identifying uploaded temp files</param>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <returns></returns>
        Task<LoginData?> UpdatePersonalData(string contextToken);

        /// <summary>
        /// Updates data of a person for current logged-in account. Personal-data includes
        /// Addresses, CommunicationData, ContactImages and bio document. <see href="https://www.matterial.com/documentation/api/66#update-personal-data">API-Reference</see>
        /// </summary>
        /// <param name="contextToken">Context token identifying uploaded temp files</param>
        /// <param name="languageKey">The language key to use for the bio-document</param>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <returns></returns>
        Task<LoginData?> UpdatePersonalData(string contextToken, LangKey? languageKey);

        /// <summary>
        /// Upload the markdown text of the user's bio document (Curriculum Vitae).
        /// </summary>
        /// <param name="markdownContent">The contant in markdown.</param>
        /// <param name="languageKey">Language of the markdown content</param>
        /// <returns></returns>
        Task<bool> UploadBioDocument(string markdownContent, LangKey languageKey);

        /// <summary>
        /// Upload a contact image for the curtrent logged on user.
        /// </summary>
        /// <param name="filePath">File path to the image.</param>
        /// <param name="setActive">If true, the contact image will become the active image.</param>
        /// <returns></returns>
        Task<bool> UploadContactImage(string filePath, bool setActive = true);

        /// <summary>
        /// Update the account settings. <see href="https://www.matterial.com/documentation/api/71#update-account-settings">API-Reference</see>
        /// </summary>
        /// <param name="AccountSettings"></param>
        /// <exception cref="System.Net.Http.HttpRequestException"></exception>
        /// <returns></returns>
        Task UpdateAccountSettings(Dictionary<string, object> AccountSettings);

        #endregion Update Personal Data

        #region Other Interfaces

        ICategory Category { get; }
        IActivity Activity { get; }
        IClient Client { get; }
        IComment Comment { get; }
        IDocument Document { get; }
        IInstance Instance { get; }
        ILanguage Language { get; }
        ILicense License { get; }
        IMtrTask Task { get; }
        IPerson Person { get; }
        IProperty Property { get; }
        IRole Role { get; }
        ITempFile TempFile { get; }
        ITrackingItem TrackingItem { get; }

        #endregion Other Interfaces
    }
}