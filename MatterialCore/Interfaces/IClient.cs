﻿using MatterialCore.Entity;
using MatterialCore.Enums;
using MatterialCore.Params;

using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace MatterialCore.Interfaces
{
    /// <summary>
    /// <see href="https://www.matterial.com/documentation/api/331">API-Reference</see>
    /// </summary>
    public interface IClient
    {
        /// <summary>
        /// Load a client by its id. <see href="https://www.matterial.com/documentation/api/331#get-client-by-id">API-Reference</see>
        /// </summary>
        /// <param name="id"></param>
        /// <param name="loadClientPreferences">If true, the client preferences will be loaded.</param>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <returns></returns>
        Task<Client?> Load(long id, bool loadClientPreferences = false);

        /// <summary>
        /// Load clients. <see href="https://www.matterial.com/documentation/api/331#get-clients">API-Reference</see>
        /// </summary>
        /// <param name="options">Options defining the load behaviour.</param>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <returns></returns>
        Task<List<Client>?> Load(ClientParams options);

        /// <summary>
        /// Create a new client. <see href="https://www.matterial.com/documentation/api/331#create-client">API-Reference</see>
        /// </summary>
        /// <param name="item"></param>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <returns></returns>
        Task<long> Create(Client item);

        /// <summary>
        /// Update a client by its id. <see href="https://www.matterial.com/documentation/api/331#update-client">API-Reference</see>
        /// </summary>
        /// <param name="item"></param>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <returns></returns>
        Task<long> Update(Client item);

        /// <summary>
        /// Remove a client by its id. <see href="https://www.matterial.com/documentation/api/331#remove-client">API-Reference</see>
        /// </summary>
        /// <param name="id"></param>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <returns></returns>
        Task<bool> Remove(long id);

        /// <summary>
        /// Get the logo of the client as a stream. <see href="https://www.matterial.com/documentation/api/524#get-file">API-Reference</see>
        /// </summary>
        /// <param name="size">Specifies the size of the logo file.</param>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <returns></returns>
        Task<Stream?> GetLogo(LoadSize size);

        /// <summary>
        /// Update the logo file of the current client. <see href="https://www.matterial.com/documentation/api/524#update-logo-and-skin">API-Reference</see>
        /// </summary>
        /// <param name="contextToken">Contect token that referes to the uploaded logo file.</param>
        /// <param name="item">Client to update</param>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <returns></returns>
        Task<Client?> UpdateLogo(string contextToken, Client item);

        /// <summary>
        /// Remove the logo of the current client. <see href="https://www.matterial.com/documentation/api/524#remove-logo">API-Reference</see>
        /// </summary>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <returns></returns>
        Task<bool> RemoveLogo();

        /// <summary>
        /// Get a client preference by its key. <see href="https://www.matterial.com/documentation/api/333#get-clientpreference-by-key">API-Reference</see>
        /// </summary>
        /// <param name="key"></param>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <returns></returns>
        Task<string?> GetPreference(string key);

        /// <summary>
        /// Get all client-preferences of the current client as map. <see href="https://www.matterial.com/documentation/api/333#get-clientpreferences-map">API-Reference</see>
        /// </summary>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <returns></returns>
        Task<PreferenceMapContainer?> GetPreferences();

        /// <summary>
        /// Update a preference by its key. <see href="https://www.matterial.com/documentation/api/333#store-clientpreference">API-Reference</see>
        /// </summary>
        /// <param name="key">Key of the preference.</param>
        /// <param name="preference">The preference containing the data to update.</param>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <returns></returns>
        Task<bool> StorePreference(string key, string preference);

        /// <summary>
        /// Remove a preference. <see href="https://www.matterial.com/documentation/api/333#remove-clientpreference">API-Reference</see>
        /// </summary>
        /// <param name="key"></param>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <returns></returns>
        Task<bool> RemovePreference(string key);
    }
}