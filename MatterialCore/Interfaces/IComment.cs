﻿using MatterialCore.Entity;
using MatterialCore.Params;

using System.Threading.Tasks;

namespace MatterialCore.Interfaces
{
    /// <summary>
    /// <see href="https://www.matterial.com/documentation/api/267">API-Reference</see>
    /// </summary>
    public interface IComment
    {
        /// <summary>
        /// Load a comment by its id. <see href="https://www.matterial.com/documentation/api/267#get-comment-by-id">API-Reference</see>
        /// </summary>
        /// <param name="id">Id of the comment</param>
        /// <returns></returns>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        Task<Comment?> Load(long id);

        /// <summary>
        /// Load all comments of a document. <see href="https://www.matterial.com/documentation/api/267#get-comments">API-Reference</see>
        /// </summary>
        /// <param name="options">Options that determine the load behaviour.</param>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <returns></returns>
        Task<ListResult<Comment>?> Load(CommentParams options);

        /// <summary>
        /// Create a new comment. <see href="https://www.matterial.com/documentation/api/267#create-comment">API-Reference</see>
        /// </summary>
        /// <param name="documentLanguageVersionId">
        /// The documentLanguageVersionId of the document the comment is linked to.
        /// </param>
        /// <param name="newItem">The new comment to create.</param>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <returns></returns>
        Task<long?> Create(long documentLanguageVersionId, Comment newItem);

        /// <summary>
        /// Update an existing comment. <see href="https://www.matterial.com/documentation/api/267#update-comment">API-Reference</see>
        /// </summary>
        /// <param name="item">
        /// The comment object containing the data to update. The dpcumentLanguageVersionId property
        /// is used to identity the comment to update.
        /// </param>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <returns></returns>
        Task<long?> Update(Comment item);

        /// <summary>
        /// Remove a comment. This action cannot be undone. <see href="https://www.matterial.com/documentation/api/267#remove-comment">API-Reference</see>
        /// </summary>
        /// <param name="id">The id of the comment to remove.</param>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <returns></returns>
        Task<bool> Remove(long id);
    }
}