﻿using MatterialCore.Entity;
using MatterialCore.Enums;
using MatterialCore.Params.Doc;

using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace MatterialCore.Interfaces
{
    /// <summary>
    /// <see href="https://www.matterial.com/documentation/api/90">API-Reference,</see><seealso
    /// href="https://www.matterial.com/documentation/api/194">Search,</seealso><seealso
    /// href="https://www.matterial.com/documentation/api/330">Rating,</seealso><seealso
    /// href="https://www.matterial.com/documentation/api/257">Saved Search,</seealso><seealso href="https://www.matterial.com/documentation/api/198">ChangeLog</seealso>
    /// </summary>
    public interface IDocument : ITempFile
    {
        #region Create

        /// <summary>
        /// Creates a new document with given parameters. After creation, the document is locked and
        /// can be updated or unlocked. <see href="https://www.matterial.com/documentation/api/90#create-document">API-Reference</see>
        /// </summary>
        /// <param name="newDocument">The new document to create.</param>
        /// <param name="options">The document creation parameters.</param>
        /// <returns>The new created document.</returns>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        Task<Document?> Create(Document newDocument, DocUpdateParams options);

        /// <summary>
        /// Creates a new document with given parameters. After creation, the document is locked and
        /// can be updated or unlocked.
        /// </summary>
        /// <param name="newDocument">The new document to create.</param>
        /// <param name="documentContent">The text content which will be used as document content.</param>
        /// <param name="mediaType">The media type of the document content.</param>
        /// <param name="options">The document creation parameters.</param>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <returns></returns>
        Task<Document?> Create(Document newDocument, DocUpdateParams options, string documentContent, DocumentContentMediaType mediaType = DocumentContentMediaType.Markdown);

        /// <summary>
        /// This creates a normal document, but:
        /// - Always sets the template-flag
        /// - Checks for EDIT_DOCUMENT_TEMPLATE-permission(instead of EDIT_DOCUMENT)
        /// - Does not check further document-rights
        /// - Does not add the current user into roleRights <see href="https://www.matterial.com/documentation/api/90#create-template">API-Reference</see>
        /// </summary>
        /// <param name="newDocument">The document to create as a new template.</param>
        /// <param name="options"></param>
        /// <returns>The new created document.</returns>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        Task<Document?> CreateTemplate(Document newDocument, UpdateTemplateParams options);

        /// <summary>
        /// This creates a normal document, but:
        /// - Always sets the template-flag
        /// - Checks for EDIT_DOCUMENT_TEMPLATE-permission(instead of EDIT_DOCUMENT)
        /// - Does not check further document-rights
        /// - Does not add the current user into roleRights <see href="https://www.matterial.com/documentation/api/90#create-template">API-Reference</see>
        /// </summary>
        /// <param name="newDocument">The document to create as a new template.</param>
        /// <param name="documentContent">The text content of the document template</param>
        /// <param name="mediaType">The media type of the document content.</param>
        /// <param name="options"></param>
        /// <returns>The new created document.</returns>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        Task<Document?> CreateTemplate(Document newDocument, UpdateTemplateParams options, string documentContent, DocumentContentMediaType mediaType = DocumentContentMediaType.Markdown);

        /// <summary>
        /// Creating a snap (called Knowledge Flash in the UI) creates a new document with the
        /// following properties:
        /// - checks permission CREATE_SNAP instead of EDIT_DOCUMENT
        /// - always adds all groups marked as initiallyAssigned
        /// - always adds add-prop snap
        /// - always adds a task, with
        /// - task.description = doc.title
        /// - task.assignee = doc.lastManuallyAssignedEditContentRole
        /// - try to publish the snap <see href="https://www.matterial.com/documentation/api/90#create-snap">API-Reference</see>
        /// </summary>
        /// <param name="newDocument">The document to create as a new snap.</param>
        /// <param name="options">
        /// The document options containing the optional contextToken.This token is used to identify
        /// all temporarily uploaded files (main-file, attachments).
        /// </param>
        /// <returns>The new created document.</returns>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        Task<Document?> CreateSnap(Document newDocument, UpdateContextParams options);

        /// <summary>
        /// Creating a snap (called Knowledge Flash in the UI) creates a new document with the
        /// following properties:
        /// - checks permission CREATE_SNAP instead of EDIT_DOCUMENT
        /// - always adds all groups marked as initiallyAssigned
        /// - always adds add-prop snap
        /// - always adds a task, with
        /// - task.description = doc.title
        /// - task.assignee = doc.lastManuallyAssignedEditContentRole
        /// - try to publish the snap <see href="https://www.matterial.com/documentation/api/90#create-snap">API-Reference</see>
        /// </summary>
        /// <param name="newDocument">The document to create as a new snap.</param>
        /// <param name="documentContent">The text content of the snap.</param>
        /// <param name="mediaType">
        /// The media type of the text content (if documentComntent is provided).
        /// </param>
        /// <param name="options">
        /// The document options containing the optional contextToken.This token is used to identify
        /// all temporarily uploaded files (main-file, attachments).
        /// </param>
        /// <returns>The new created document.</returns>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        Task<Document?> CreateSnap(Document newDocument, UpdateContextParams options, string documentContent, DocumentContentMediaType mediaType = DocumentContentMediaType.Markdown);

        #endregion Create

        #region Updating

        /// <summary>
        /// Update a document. <see href="https://www.matterial.com/documentation/api/90#update-document">API-Reference</see>
        /// </summary>
        /// <param name="document">The document to update. It will be identified by its id.</param>
        /// <param name="updateParams">The parameters.</param>
        /// <returns>The updated document.</returns>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        Task<Document?> Update(Document document, DocUpdateParams updateParams);

        /// <summary>
        /// This updates a template like a regular document, but:
        /// - Always sets the template-flag
        /// - Checks for EDIT_DOCUMENT_TEMPLATE-permission(instead of EDIT_DOCUMENT)
        /// - Does not check further document-rights
        /// - Does not add the current user into roleRights <see href="https://www.matterial.com/documentation/api/90#update-template">API-Reference</see>
        /// </summary>
        /// <param name="document">The document template to update.</param>
        /// ///
        /// <param name="updateParams">UpdateTemplateParams containing the options for updating.</param>
        /// <returns>The updated template.</returns>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        Task<Document?> UpdateTemplate(Document document, UpdateTemplateParams updateParams);

        /// <summary>
        /// This updates a bio (a person's biography) like a regular document, but:
        /// - Always sets the bio-flag
        /// - Checks for EDIT_DOCUMENT-permission
        /// - Does not check further document-rights
        /// - Does only set the current user into roleRights and do not allow to update <see href="https://www.matterial.com/documentation/api/90#update-bio">API-Reference</see>
        /// </summary>
        /// <param name="document">The bio document template to update.</param>
        /// <param name="updateParams">UpdateLockParams containing the options for updating.</param>
        /// <returns>The updated template.</returns>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        Task<Document?> UpdateBio(Document document, UpdateLockParams updateParams);

        #endregion Updating

        #region Snap

        /// <summary>
        /// Unsnap a document means closing related snap-task. <see href="https://www.matterial.com/documentation/api/90#unsnap">API-Reference</see>
        /// </summary>
        /// <param name="document">The document snap to close</param>
        /// <returns>True in case of success.</returns>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        Task<bool> Unsnap(Document document);

        #endregion Snap

        #region Review

        /// <summary>
        /// Mark a document of a specific language as reviewed. <see href="https://www.matterial.com/documentation/api/90#mark-document-language-version-as-reviewed">API-Reference</see>
        /// </summary>
        /// <param name="id">The document id.w of</param>
        /// <param name="languageVersionId">The document language version id.</param>
        /// <returns>True in case of success.</returns>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        Task<bool> MarkAsReviewed(long id, long languageVersionId);

        /// <summary>
        /// Decline a review of a specific document language <see href="https://www.matterial.com/documentation/api/90#decline-review-of-language-version">API-Reference</see>
        /// </summary>
        /// <param name="id">The document id.w of</param>
        /// <param name="languageVersionId">The document language version id.</param>
        /// <returns>True in case of success.</returns>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        Task<bool> DeclineReview(long id, long languageVersionId);

        /// <summary>
        /// Mark a document of a specific language as reviewed and generate a task the document. The
        /// Task object must at least contain the assignedRoleId (the assignee of the task) and a
        /// description. <see href="https://www.matterial.com/documentation/api/90#decline-review-of-language-version-with-task">API-Reference</see>
        /// </summary>
        /// <param name="id">The document id.w of</param>
        /// <param name="languageVersionId">The document language version id.</param>
        /// <param name="task">The task to create when the review is declined.</param>
        /// <returns>True in case of success.</returns>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        Task<bool> DeclineReview(long id, long languageVersionId, MtrTask task);

        #endregion Review

        #region Additional Properties

        /// <summary>
        /// Set additional properties of a document, Document properties are name/value keypairs.
        /// <see href="https://www.matterial.com/documentation/api/90#set-additional-properties">API-Reference</see>
        /// </summary>
        /// <param name="documentId">The document id.</param>
        /// <param name="properties">The list of additional properties to set.</param>
        /// <returns>The number of properties set.</returns>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <exception cref="System.ArgumentException">Thrown if documentId is less than 1.</exception>
        Task<Document?> SetAdditionalProperties(long documentId, List<AdditionalProperty> properties);

        #endregion Additional Properties

        #region Search

        /// <summary>
        /// Search documents by a given query string and return a search result. <see href="https://www.matterial.com/documentation/api/194#search-documents-by-query">API-Reference</see>
        /// </summary>
        /// <param name="query">
        /// The string to search for. If a asterisk (*) is provided, all records will be searched.
        /// </param>
        /// <returns>SearchResult containig all results.</returns>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        Task<SearchResult<Document>?> Search(string query);

        /// <summary>
        /// Search documents by a given query string and and search parameters and return a search
        /// result. <see href="https://www.matterial.com/documentation/api/194#search-documents-by-query">API-Reference</see>
        /// </summary>
        /// <param name="query">
        /// The string to search for. If a asterisk (*) is provided, all records will be searched.
        /// </param>
        /// <param name="searchParameter">Search parameter.</param>
        /// <returns>SearchResult containig all results.</returns>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        Task<SearchResult<Document>?> Search(string query, SearchDocParams searchParameter);

        /// <summary>
        /// Search for autocomplete items. <see href="https://www.matterial.com/documentation/api/194#search-autocomplete">API-Reference</see>
        /// </summary>
        /// <param name="query">The query string.</param>
        /// <param name="searchParameter">Search parameter.</param>
        /// <returns>A list of autocomplete suggests.</returns>
        Task<List<SearchAutocompleteSuggest>?> SearchAutocomplete(string query, DocAutocompleteParams searchParameter);

        #endregion Search

        #region SavedSearches

        /// <summary>
        /// Execute a saved search by its id. <see href="https://www.matterial.com/documentation/api/257#get-documents-by-saved-search">API-Reference</see>
        /// </summary>
        /// <param name="savedSearchId">The id of the saved search to execute.</param>
        /// <param name="searchParams">
        /// The additional search parameters. These parameter overwrite eventually existing
        /// parameters in the saved search.
        /// </param>
        /// <returns>
        /// If the saved search contains a query string, the return type will be SearchResult, else
        /// the return type is ListResult.
        /// </returns>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <exception cref="System.ArgumentException">
        /// Thrown if savedSearchId is less than 1.
        /// </exception>
        Task<ListResult<SearchResultEntry<Document>>?> Search(long savedSearchId, SavedSearchSearchParams searchParams);

        /// <summary>
        /// Get a list of all saved searches. <see href="https://www.matterial.com/documentation/api/257#get-saved-searches">API-Reference</see>
        /// </summary>
        /// <param name="searchParameter">Parameter of how to load saved searches</param>
        /// <returns>A list of SavedSearches.</returns>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        Task<List<SavedSearch>?> SavedSearches(SavedSearchLoadParams? searchParameter);

        /// <summary>
        /// Load a saved search by its id. <see href="https://www.matterial.com/documentation/api/257#get-saved-search-by-id">API-Reference</see>
        /// </summary>
        /// <param name="savedSearchid">The id of the saved search.</param>
        /// <param name="languageKey">The language key.</param>
        /// <returns>Saved search</returns>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <exception cref="System.ArgumentException">
        /// Thrown if savedSearchid is less than 1.
        /// </exception>
        Task<SavedSearch?> SavedSearchById(long savedSearchid, LangKey? languageKey);

        /// <summary>
        /// Create a new saved search. <see href="https://www.matterial.com/documentation/api/257#create-saved-search">API-Reference</see>
        /// </summary>
        /// <param name="savedSearch">
        /// A SavedSearch object specifying name, dashboard prio, type etc.
        /// </param>
        /// <param name="searchTerm">The search term.</param>
        /// <param name="searchParams">The search parameter to use.</param>
        /// <returns>The id of the created saved search.</returns>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        Task<long> CreateSavedSearch(SavedSearch savedSearch, string searchTerm, SearchDocParams searchParams);

        /// <summary>
        /// Remove a saved search by its id. <see href="https://www.matterial.com/documentation/api/257#remove-saved-search">API-Reference</see>
        /// </summary>
        /// <param name="savedSearchid">The id of the saved search to remove.</param>
        /// <returns>True in case of success.</returns>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <exception cref="System.ArgumentException">
        /// Thrown if savedSearchid is less than 1.
        /// </exception>
        Task<bool> RemoveSavedSearch(long savedSearchid);

        /// <summary>
        /// Update an existing saved search <see href="https://www.matterial.com/documentation/api/257#update-saved-search">API-Reference</see>
        /// </summary>
        /// <param name="savedSearch">The saved search continging the new values.</param>
        /// <returns>The id of the updated saved search.</returns>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        Task<long> UpdateSavedSearch(SavedSearch savedSearch);

        /// <summary>
        /// Set the priority (the order) of saved searches shown on the dashboard (first gets
        /// highest prio - the highest prio has the highest value in dashboardPrio). <see href="https://www.matterial.com/documentation/api/257#update-dashboard-prios">API-Reference</see>
        /// </summary>
        /// <param name="savedSearchIdList">
        /// List of saved search ids in which the saved searches will be ordered (first gets highest priority).
        /// </param>
        /// <returns>The number of ordered saved searches.</returns>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        Task<long> SetDashboardPrios(List<long> savedSearchIdList);

        /// <summary>
        /// Restore the default saved searches. Only the instance owner has the permission to do so.
        /// Already modifyed existing default saved searches will not be overwritten <see href="https://www.matterial.com/documentation/api/257#recreate-initial-saved-searches">API-Reference</see>
        /// </summary>
        /// <returns>True in case of success.</returns>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        Task<bool> RestoreDefaultSavedSearches();

        /// <summary>
        /// Gets the query string of the saved search - if defined.
        /// </summary>
        /// <param name="savedSearch">The saved search to get the query string from.</param>
        /// <returns>In case a query string exists, returns the string, else null.</returns>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        string? GetQueryString(SavedSearch savedSearch);

        #endregion SavedSearches

        #region Load

        /// <summary>
        /// Load the document of a given document id. <see href="https://www.matterial.com/documentation/api/90#get-document-by-id">API-Reference</see>
        /// </summary>
        /// <returns>The entity with updated information is returned.</returns>
        /// <param name="id">The document id.</param>
        /// <param name="languageKey"></param>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <exception cref="System.ArgumentException">Thrown if document Id is less than 1.</exception>
        Task<Document?> Load(long id, string languageKey);

        /// <summary>
        /// Load information of a given document and passing load options that define the load
        /// bahaviour. <see href="https://www.matterial.com/documentation/api/90#get-document-by-id">API-Reference</see>
        /// </summary>
        /// <param name="id">The document id.</param>
        /// <param name="options">The parameters defining the load behaviour.</param>
        /// <returns>The entity with updated information is returned.</returns>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        Task<Document?> Load(long id, LoadDocByIdParams options);

        /// <summary>
        /// Load a list of documents. <see href="https://www.matterial.com/documentation/api/90#get-documents">API-Reference</see>
        /// </summary>
        /// <param name="options">The parameters defining the load behaviour.</param>
        /// <returns>A ListResult containing all documents found.</returns>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        Task<ListResult<Document>?> Load(LoadDocParams? options);

        /// <summary>
        /// Load a list of documents from the trash. <see href="https://www.matterial.com/documentation/api/90#get-documents-in-trash-admin-trash">API-Reference</see>
        /// </summary>
        /// <param name="options">The parameters defining the load behaviour.</param>
        /// <returns>A ListResult containing all documents found.</returns>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        Task<ListResult<Document>?> LoadTrash(LoadDocTrashParams? options);

        #endregion Load

        #region MainFile

        /// <summary>
        /// Get a stream of the main file. <see href="https://www.matterial.com/documentation/api/90#get-main-file-by-document-id">API-Reference</see>
        /// </summary>
        /// <param name="documentId">The id of the document.</param>
        /// <param name="loadParams">The parameters defining the load behaviour.</param>
        /// <returns>A stream to the main file in the specified format.</returns>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <exception cref="System.ArgumentException">Thrown if documentId is less than 1.</exception>
        Task<Stream?> GetMainFile(long documentId, MainFileLoadParams loadParams);

        /// <summary>
        /// Get a stream of the main file. <see href="https://www.matterial.com/documentation/api/90#get-main-file-by-document-id-and-document-language-version-id">API-Reference</see>
        /// </summary>
        /// <param name="documentId">The id of the document.</param>
        /// <param name="documentLanguageVersionId">The language version id of the document.</param>
        /// <param name="loadParams">The parameters defining the load behaviour.</param>
        /// <returns>A stream to the main file in the specified format.</returns>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <exception cref="System.ArgumentException">Thrown if documentId is less than 1.</exception>
        Task<Stream?> GetMainFile(long documentId, long documentLanguageVersionId, MainFileLoadParams loadParams);

        /// <summary>
        /// Compare two version of html-main-files by languageVersionId and results in a html-file
        /// which are marked with special classes (added/removed). <see href="https://www.matterial.com/documentation/api/90#compare-main-files-by-document-language-version-ids">API-Reference</see>
        /// </summary>
        /// <param name="oldDocumentLanguageVersionId">The old document-language-version-id</param>
        /// <param name="newDocumentLanguageVersionId">The new document-language-version-id</param>
        /// <returns></returns>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <exception cref="System.ArgumentException">
        /// Thrown if oldDocumentLanguageVersionId or newDocumentLanguageVersionId is less than 1.
        /// </exception>
        Task<Stream?> CompareMainFile(long oldDocumentLanguageVersionId, long newDocumentLanguageVersionId);

        #endregion MainFile

        #region Attachments

        /// <summary>
        /// Get all attachments of a given document id. <see href="https://www.matterial.com/documentation/api/90#get-attachments">API-Reference</see>
        /// </summary>
        /// <param name="documentId">The id of the document.</param>
        /// <param name="loadParams">The parameters defining the load behaviour.</param>
        /// <returns>List of attachments</returns>
        /// ///
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <exception cref="System.ArgumentException">
        /// Thrown if documentId or documentLanguageVersionId is less than 1.
        /// </exception>
        Task<List<Attachment>?> GetAttachments(long documentId, AttachmentsLoadParams loadParams);

        /// <summary>
        /// Get a stream of the attachments of a given document id, language version id and
        /// attachment id. <see href="https://www.matterial.com/documentation/api/90#get-attachment-by-document-id-document-language-version-id-attachment-id">API-Reference</see>
        /// </summary>
        /// <param name="documentId">The id of the document.</param>
        /// <param name="documentLanguageVersionId">The language version id of the document.</param>
        /// ///
        /// <param name="attachmentId">The id of the attachment.</param>
        /// <param name="loadParams">The parameters defining the load behaviour.</param>
        /// <returns>List of attachments</returns>
        /// ///
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <exception cref="System.ArgumentException">
        /// Thrown if documentId, documentLanguageVersionId or attachmentId is less than 1.
        /// </exception>
        Task<Stream?> GetAttachment(long documentId, long documentLanguageVersionId, long attachmentId, AttachmentLoadParams loadParams);

        /// <summary>
        /// Get a stream of the attachments of a given document id and attachment id. <see href="https://www.matterial.com/documentation/api/90#get-attachment-by-document-id-attachment-id">API-Reference</see>
        /// </summary>
        /// <param name="documentId">The id of the document.</param>
        /// ///
        /// <param name="attachmentId">The id of the attachment.</param>
        /// <param name="loadParams">The parameters defining the load behaviour.</param>
        /// <returns>List of attachments</returns>
        /// ///
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <exception cref="System.ArgumentException">
        /// Thrown if documentId or attachmentId is less than 1.
        /// </exception>
        Task<Stream?> GetAttachment(long documentId, long attachmentId, AttachmentLoadParams loadParams);

        #endregion Attachments

        #region Remove/Trash/Archive

        /// <summary>
        /// Remove a document with the given document id. <see href="https://www.matterial.com/documentation/api/90#trash-document">API-Reference</see>
        /// </summary>
        /// <param name="documentId">The document id.</param>
        /// <param name="finallyRemove">
        /// If true, the document will be finally removed and cannot be recycled.
        /// </param>
        /// <returns>
        /// In case of success, a document is returned. If the document is locked, a DocumentLock
        /// will be returned.
        /// </returns>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <exception cref="System.ArgumentException">Thrown if documentId or less than 1.</exception>
        Task<DocumentBase?> Remove(long documentId, bool finallyRemove);

        /// <summary>
        /// Removes the language-version. It is only possible to remove versions, that are not
        /// currentlyInProcessing. This cannot be undone! <see href="https://www.matterial.com/documentation/api/90#remove-a-specific-version">API-Reference</see>
        /// </summary>
        /// <param name="documentId">The id of the document to remove.</param>
        /// <param name="documentLanguageVersionId">
        /// The documentLanguageVersionId of the document to remove.
        /// </param>
        /// <returns>
        /// In case of success, a document is returned. If the document is locked, a DocumentLock
        /// will be returned.
        /// </returns>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <exception cref="System.ArgumentException">Thrown if documentId is less than 1.</exception>
        Task<DocumentBase?> Remove(long documentId, long documentLanguageVersionId);

        /// <summary>
        /// Removes all versions of a document by language-key. It is not possible to remove the
        /// last language. This cannot be undone! <see href="https://www.matterial.com/documentation/api/90#remove-a-specific-language">API-Reference</see>
        /// </summary>
        /// <param name="documentId">The id of the document to remove.</param>
        /// <param name="languageKey">The language key.</param>
        /// <returns></returns>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <exception cref="System.ArgumentException">Thrown if documentId is less than 1.</exception>
        Task<DocumentBase?> Remove(long documentId, LangKey languageKey);

        /// <summary>
        /// Restore a document from trash. <see href="https://www.matterial.com/documentation/api/90#untrash-document">API-Reference</see>
        /// </summary>
        /// <param name="documentId">The id of the document to remove.</param>
        /// <returns>
        /// In case of success, a document is returned. If the document is locked, a DocumentLock
        /// will be returned.
        /// </returns>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <exception cref="System.ArgumentException">Thrown if documentId is less than 1.</exception>
        Task<DocumentBase?> Restore(long documentId);

        /// <summary>
        /// Clears the trash and finally removes all documents older than the keep-in-trash time
        /// period. <see href="https://www.matterial.com/documentation/api/90#cleanup-trash">API-Reference</see>
        /// </summary>
        /// <param name="ignoreRemoveTime">If true, removes all documents in trash</param>
        /// <returns>Count of documents placed into removal-queue</returns>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        Task<int> ClearTrash(bool ignoreRemoveTime);

        /// <summary>
        /// Archive a document by its given id. <see href="https://www.matterial.com/documentation/api/90#archive-document">API-Reference</see>
        /// </summary>
        /// <param name="documentId">The id of the document to archive.</param>
        /// <param name="archivedBeginInSeconds">
        /// Start time stamp in seconds (UNIX time stamp) from when on the document will be archived.
        /// </param>
        /// <returns>
        /// In case of success, a document is returned. If the document is locked, a DocumentLock
        /// will be returned.
        /// </returns>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <exception cref="System.ArgumentException">Thrown if documentId is less than 1.</exception>
        Task<DocumentBase?> Archive(long documentId, long archivedBeginInSeconds);

        /// <summary>
        /// Archive a document. <see href="https://www.matterial.com/documentation/api/90#archive-document">API-Reference</see>
        /// </summary>
        /// <param name="documentId">The document id.</param>
        /// <returns>
        /// In case of success, a document is returned. If the document is locked, a DocumentLock
        /// will be returned.
        /// </returns>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <exception cref="System.ArgumentException">Thrown if documentId is less than 1.</exception>
        Task<DocumentBase?> Archive(long documentId);

        /// <summary>
        /// Un-archive a document. <see href="https://www.matterial.com/documentation/api/90#unarchive-document">API-Reference</see>
        /// </summary>
        /// <param name="documentId">The id of the document to un-archive.</param>
        /// <returns>
        /// In case of success, a document is returned. If the document is locked, a DocumentLock
        /// will be returned.
        /// </returns>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <exception cref="System.ArgumentException">Thrown if documentId is less than 1.</exception>
        Task<DocumentBase?> UnArchive(long documentId);

        /// <summary>
        /// Get the current size of the removal-queue. <see href="https://www.matterial.com/documentation/api/90#get-removal-queue-size">API-Reference</see>
        /// </summary>
        /// <returns>Queue size.</returns>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        Task<int> GetRemovalQueueSize();

        #endregion Remove/Trash/Archive

        #region Convert

        /// <summary>
        /// Reconverts the main-file of a document-language-version and all of its attachments. This
        /// regenerates all pdfs and thumbnails even if they already exist. <see href="https://www.matterial.com/documentation/api/90#reconvert-document-language-version">API-Reference</see>
        /// </summary>
        /// <param name="document">The document to re-convert.</param>
        /// <returns>The number of converted items.</returns>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        Task<int> ReconvertDocument(Document document);

        /// <summary>
        /// (Re)converts the main-file of all document-language-version and all of their
        /// attachments. <see href="https://www.matterial.com/documentation/api/90#convert-all-document-language-versions">API-Reference</see>
        /// </summary>
        /// <param name="regenerate">
        /// If true, all pdfs and thumbnails will be regenerated even if they already exist.
        /// </param>
        /// <returns>The number of converted items.</returns>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        Task<int> ReconvertAll(bool regenerate);

        #endregion Convert

        #region Locking

        /// <summary>
        /// Unlock a previously locked document. <see href="https://www.matterial.com/documentation/api/300#release-my-document-lock">API-Reference</see>
        /// </summary>
        /// <param name="uniqueLockId">The unique lock id provided with the Lock function call.</param>
        /// <returns>True in case of success.</returns>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        Task<bool> Unlock(string uniqueLockId);

        /// <summary>
        /// The Lock function creates a document lock. In order to update the document, a lock is
        /// required. <see href="https://www.matterial.com/documentation/api/300#acquire-or-update-document-lock">API-Reference</see>
        /// </summary>
        /// <param name="documentId">The document id.</param>
        /// <param name="uniqueLockId">
        /// A unique string (possibly a GUID) which will be used to reference the lock. The unique
        /// lock id is required to update the lock or unlock the document.
        /// </param>
        /// <returns>The DocumentLock containing information about the lock.</returns>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <exception cref="System.ArgumentException">Thrown if documentId is less than 1.</exception>
        Task<DocumentLock?> Lock(long documentId, string uniqueLockId);

        /// <summary>
        /// Update a document lock. Locks are only valid for a certain time period. In order to keep
        /// the docuemnt locked, the UpdateLock function needs to be called. <see href="https://www.matterial.com/documentation/api/300#acquire-or-update-document-lock">API-Reference</see>
        /// </summary>
        /// <param name="documentId">The document id.</param>
        /// <param name="uniqueLockId">The unique lock id provided with the Lock function call.</param>
        /// <returns>The DocumentLock containing information about the lock.</returns>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <exception cref="System.ArgumentException">Thrown if documentId is less than 1.</exception>
        Task<DocumentLock?> UpdateLock(long documentId, string uniqueLockId);

        /// <summary>
        /// Unlock a document by its document id. This function should only be used in case the
        /// unique lock ID is unknown. <see href="https://www.matterial.com/documentation/api/300#release-all-document-locks-by-document-id">API-Reference</see>
        /// </summary>
        /// <param name="documentId">The document id.</param>
        /// <returns>True in case of success.</returns>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <exception cref="System.ArgumentException">Thrown if documentId is less than 1.</exception>
        Task<bool> UnlockByDocId(long documentId);

        #endregion Locking

        #region Duplicates

        /// <summary>
        /// Create a duplicate of a document. The duplicated document are not yet saved - you have
        /// to use SaveDuplicate to save it. All duplicates are only valid for the current session.
        /// Ending a session will remove all duplicates. <see href="https://www.matterial.com/documentation/api/90#get-duplicate-of-document-by-id">API-Reference</see>
        /// </summary>
        /// <param name="documentId">The document id.</param>
        /// <param name="mode">The duplicate mode.</param>
        /// <returns>The document duplicate.</returns>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <exception cref="System.ArgumentException">Thrown if documentId is less than 1.</exception>
        Task<DocumentDuplicate?> Duplicate(long documentId, DuplicateMode mode);

        /// <summary>
        /// Save a given document duplicate. <see href="https://www.matterial.com/documentation/api/90#get-duplicate-of-document-by-id">API-Reference</see>
        /// </summary>
        /// <param name="documentDuplicate">The duplicate to save.</param>
        /// <returns>The saved document.</returns>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        Task<List<Document>> SaveDuplicate(DocumentDuplicate documentDuplicate);

        /// <summary>
        /// Save a list of duplicates. <see href="https://www.matterial.com/documentation/api/90#get-duplicate-of-document-by-id">API-Reference</see>
        /// </summary>
        /// <param name="documentDuplicates">The duplicates to save.</param>
        /// <returns>The list of saved documents.</returns>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        Task<List<Document>> SaveDuplicates(DocumentDuplicates documentDuplicates);

        /// <summary>
        /// Get the list of all duplicates of the current session. <see href="https://www.matterial.com/documentation/api/90#get-duplicates">API-Reference</see>
        /// </summary>
        /// <returns>The document duplicates.</returns>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        Task<DocumentDuplicates?> GetDuplicates();

        /// <summary>
        /// Remove duplicate from session including all temp-files. <see href="https://www.matterial.com/documentation/api/90#remove-duplicate-from-session-by-context-token">API-Reference</see>
        /// </summary>
        /// <param name="contextToken">The context token of the duplicate.</param>
        /// <returns>The number of removed duplicates.</returns>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        Task<int> RemoveDuplicate(string contextToken);

        /// <summary>
        /// Remove a list of given duplicates from the current session. <see href="https://www.matterial.com/documentation/api/90#remove-duplicate-from-session-by-context-token">API-Reference</see>
        /// </summary>
        /// <param name="duplicates">The duplicates to remove.</param>
        /// <returns>Number of removed documents.</returns>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        Task<int> RemoveDuplicates(DocumentDuplicates duplicates);

        #endregion Duplicates

        #region Clipboard

        /// <summary>
        /// Add a document to the clipboard. The clipboard is used to batch-process documents. <see href="https://www.matterial.com/documentation/api/90#clipboard---add">API-Reference</see>
        /// </summary>
        /// <param name="documentClipBoardEntries">Document clipboard entries to add to the clipboard</param>
        /// <returns>Number of documents added to the clipboard.</returns>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        Task<int> AddToClipboard(List<DocumentClipBoardEntry> documentClipBoardEntries);

        /// <summary>
        /// Remove a document from the clipboard. <see href="https://www.matterial.com/documentation/api/90#clipboard---remove">API-Reference</see>
        /// </summary>
        /// <param name="documentClipBoardEntries">
        /// Document clipboard entries to remove from the clipboard.
        /// </param>
        /// <returns>Number of documents removed from the clipboard.</returns>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        Task<int> RemoveFromClipboard(List<DocumentClipBoardEntry> documentClipBoardEntries);

        /// <summary>
        /// Clear the clipboard. <see href="https://www.matterial.com/documentation/api/90#clipboard---clear">API-Reference</see>
        /// </summary>
        /// <returns>Number of documents removed from the clipboard.</returns>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        Task<int> ClearClipboard();

        /// <summary>
        /// Execute a batch action on the current clipboard. <see href="https://www.matterial.com/documentation/api/90#execute-batch-action-on-clipboard">API-Reference</see>
        /// </summary>
        /// <param name="actionCode">Batch action to start</param>
        /// <param name="options">Options defining the behaviour.</param>
        /// <returns></returns>
        Task ExecuteBatchAction(BatchActions actionCode, BatchActionAdditionalData options);

        #endregion Clipboard

        #region Rating

        /// <summary>
        /// Rate a document as helpful. <see href="https://www.matterial.com/documentation/api/330#mark-document-as-helpful-for-current-account">API-Reference</see>
        /// </summary>
        /// <param name="id">The document id.</param>
        /// <param name="languageVersionId">The document language version id.</param>
        /// <returns>True in case of success.</returns>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        Task<bool> RateDocument(long id, long languageVersionId);

        /// <summary>
        /// Un-rate a documehnt as helpful. <see href="https://www.matterial.com/documentation/api/330#unmark-document-as-helpful-for-current-account">API-Reference</see>
        /// </summary>
        /// <param name="id">The document id.</param>
        /// <param name="languageVersionId">The document language version id.</param>
        /// <returns>True in case of success.</returns>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        Task<bool> UnRateDocument(long id, long languageVersionId);

        /// <summary>
        /// Determine if a document is rated as helpful. <see href="https://www.matterial.com/documentation/api/330#is-document-marked-as-helpful-by-current-account">API-Reference</see>
        /// </summary>
        /// <param name="id">The document id.</param>
        /// <param name="languageVersionId">The document language version id.</param>
        /// <returns>True in case of success.</returns>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        Task<bool> IsRatedDocument(long id, long languageVersionId);

        #endregion Rating

        #region ChangeLog

        /// <summary>
        /// Get the application change log. <see href="https://www.matterial.com/documentation/api/198#get-change-log">API-Reference</see>
        /// </summary>
        /// <param name="loadParams">Parameter to determine the load behaviour.</param>
        /// <returns>List of DocumentChangeLog containing all the changes.</returns>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        Task<ListResult<DocumentChangeLog>?> GetChangeLog(ChangeLogParams loadParams);

        /// <summary>
        /// Get the change log map of the application changes. <see href="https://www.matterial.com/documentation/api/198#get-change-log-map-grouped-by-date">API-Reference</see>
        /// </summary>
        /// <param name="loadParams">Parameter to determine the load behaviour.</param>
        /// <returns>The ChangeLogMap containing all changes.</returns>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        Task<DocumentChangeLogMap?> GetChangeLogMap(ChangeLogMapParams loadParams);

        #endregion ChangeLog

        #region ExtensionValues

        /// <summary>
        /// Get all extension values. <see href="https://www.matterial.com/documentation/api/298#get-available-extension-values-preferences">API-Reference</see>
        /// </summary>
        /// <returns>List of extension values.</returns>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        Task<List<ExtensionValue>?> GetExtensionValues();

        #endregion ExtensionValues

        #region Index

        /// <summary>
        /// Clears the document-part of the index and reindexes all documents for the current
        /// instance. <see href="https://www.matterial.com/documentation/api/194#reindex-all-documents">API-Reference</see>
        /// </summary>
        /// <returns></returns>
        Task ReindexAll();

        /// <summary>
        /// Reindexes one document by id. <see href="https://www.matterial.com/documentation/api/194#index-document">API-Reference</see>
        /// </summary>
        /// <param name="id">The id of the document to re-index.</param>
        /// <returns></returns>
        Task IndexDocument(long id);

        /// <summary>
        /// Reindexes one document by language-version-id. <see href="https://www.matterial.com/documentation/api/194#index-document-language-version">API-Reference</see>
        /// </summary>
        /// <param name="id">Id of the document.</param>
        /// <param name="languageVersionId">The language version Id.</param>
        /// <returns></returns>
        Task IndexDocument(long id, long languageVersionId);

        #endregion Index
    }
}