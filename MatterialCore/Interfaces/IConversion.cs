﻿using MatterialCore.Enums;

using System.Threading.Tasks;

namespace MatterialCore.Interfaces
{
    /// <summary>
    /// <see href="https://www.matterial.com/documentation/api/60">API-Reference</see>
    /// </summary>
    public interface IConversion
    {
        /// <summary>
        /// Get the current size of the conversion-queue. <see href="https://www.matterial.com/documentation/api/60#get-queue-size">API-Reference</see>
        /// </summary>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <returns>Number of queued conversions</returns>
        Task<long> QueueSize();

        /// <summary>
        /// Convert a Markdown string to Html or vice versa. <see href="https://www.matterial.com/documentation/api/60">API-Reference</see>
        /// </summary>
        /// <param name="action">Type of conversion</param>
        /// <param name="input">String to conbvert</param>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <returns>Converted string</returns>
        Task<string?> Convert(ConversionAction action, string input);
    }
}