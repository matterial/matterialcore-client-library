﻿using MatterialCore.Entity;

using System.Threading.Tasks;

namespace MatterialCore.Interfaces
{
    /// <summary>
    /// <see href="https://www.matterial.com/documentation/api/10">API-Reference</see>
    /// </summary>
    public interface ILicense
    {
        /// <summary>
        /// Get the usage of licenses of the current instance. <see href="https://www.matterial.com/documentation/api/10#get-licence-usage-by-instance">API-Reference</see>
        /// </summary>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <returns></returns>
        Task<LicenseUsage?> GetUsage();

        /// <summary>
        /// Get the license of the current instance. <see href="https://www.matterial.com/documentation/api/10#get-licence-by-instance">API-Reference</see>
        /// </summary>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <returns></returns>
        Task<License?> GetLicense();

        /// <summary>
        /// This updates the licence of the current instance. Be sure to add correct “hash”, that
        /// corresponds to matterial-server (HashUtil). <see href="https://www.matterial.com/documentation/api/10#update-licence-by-instance">API-Reference</see>
        /// </summary>
        /// <param name="item"></param>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <returns></returns>
        Task<License?> UpdateLicense(License item);

        /// <summary>
        /// This checks a given licence if it could be updated. Be sure to add correct “hash”, that
        /// corresponds to matterial-server (HashUtil). <see href="https://www.matterial.com/documentation/api/10#check-licence-by-instance">API-Reference</see>
        /// </summary>
        /// <param name="item"></param>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <returns></returns>
        Task<License?> CheckLicense(License item);

        /// <summary>
        /// This adds a valid licence-key to given licence. <see href="https://www.matterial.com/documentation/api/10#generate-licence">API-Reference</see>
        /// </summary>
        /// <param name="item"></param>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <returns></returns>
        Task<License?> GenerateLicense(License item);
    }
}