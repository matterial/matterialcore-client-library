﻿namespace MatterialCore.Interfaces
{
    public interface IRestEntity : IEntity
    {
        string RESTBasePath();
    }
}