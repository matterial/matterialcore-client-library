﻿using MatterialCore.Entity;
using MatterialCore.Enums;
using MatterialCore.Params.Doc;

using System.Collections.Generic;
using System.Threading.Tasks;

namespace MatterialCore.Interfaces
{
    /// <summary>
    /// <see href="https://www.matterial.com/documentation/api/297">API-Reference</see>
    /// </summary>
    public interface IProperty
    {
        /// <summary>
        /// Load additional prioperty by its id. <see href="https://www.matterial.com/documentation/api/297#get-additional-property-by-id">API-Reference</see>
        /// </summary>
        /// <param name="id"></param>
        /// <param name="loadParams">Options that determine the load behaviour.</param>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <returns></returns>
        Task<AdditionalProperty?> Load(long id, PropertyLoadByIdParams loadParams);

        /// <summary>
        /// Load additional properties. <see href="https://www.matterial.com/documentation/api/297#get-additional-properties">API-Reference</see>
        /// </summary>
        /// <param name="loadParams">Options that determine the load behaviour.</param>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <returns></returns>
        Task<List<AdditionalProperty>?> Load(PropertyLoadParams loadParams);

        /// <summary>
        /// Create a new additional property. <see href="https://www.matterial.com/documentation/api/297#create-additional-property">API-Reference</see>
        /// </summary>
        /// <param name="newItem">New object to create</param>
        /// <param name="langParams">Language key that specifies the object's language.</param>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <returns></returns>
        Task<long> Create(AdditionalProperty newItem, LangKey langParams);

        /// <summary>
        /// Update additional property. <see href="https://www.matterial.com/documentation/api/297#update-additional-property">API-Reference</see>
        /// </summary>
        /// <param name="id">Id of the object to update.</param>
        /// <param name="item">Object containing the data to update.</param>
        /// <param name="langParams">Language key that specifies the object's language.</param>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <returns></returns>
        Task<long> Update(long id, AdditionalProperty item, LangKey langParams);

        /// <summary>
        /// Remove an additional property. <see href="https://www.matterial.com/documentation/api/297#remove-additional-property">API-Reference</see>
        /// </summary>
        /// <param name="id">Id of the object to remove.</param>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <returns></returns>
        Task<bool> Remove(long id);

        /// <summary>
        /// Check if the current user follows an additional property of a specified language. <see href="https://www.matterial.com/documentation/api/297#is-following-logged-in-account-follows-an-additional-property">API-Reference</see>
        /// </summary>
        /// <param name="id">Id of the additional property.</param>
        /// <param name="langParams">Language key that specifies the object's language.</param>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <returns></returns>
        Task<bool> IsFollowing(long id, LangKey langParams);

        /// <summary>
        /// Make the current user follow an additional property of a specified language. <see href="https://www.matterial.com/documentation/api/297#follow-additional-property-logged-in-account">API-Reference</see>
        /// </summary>
        /// <param name="id">Id of the additional property.</param>
        /// <param name="langParams">Language key that specifies the object's language.</param>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <returns></returns>
        Task<bool> Follow(long id, LangKey langParams);

        /// <summary>
        /// Stop following an additional property of a specified language for the current user. <see href="https://www.matterial.com/documentation/api/297#unfollow-additional-property-logged-in-account">API-Reference</see>
        /// </summary>
        /// <param name="id">Id of the additional property.</param>
        /// <param name="langParams">Language key that specifies the object's language.</param>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <returns></returns>
        Task<bool> UnFollow(long id, LangKey langParams);

        /// <summary>
        /// Make a specified account follow an additional property of a specified language. <see href="https://www.matterial.com/documentation/api/297#follow-additional-property-other-account">API-Reference</see>
        /// </summary>
        /// <param name="id">Id of the additional property.</param>
        /// <param name="accountId">Áccount id of the user.</param>
        /// <param name="langParams">Language key that specifies the object's language.</param>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <returns></returns>
        Task<bool> Follow(long id, long accountId, LangKey langParams);

        /// <summary>
        /// Stop a specified account from following an additional property of a specified language.
        /// <see href="https://www.matterial.com/documentation/api/297#unfollow-additional-property-other-account">API-Reference</see>
        /// </summary>
        /// <param name="id">Id of the additional property.</param>
        /// <param name="accountId">Áccount id of the user.</param>
        /// <param name="langParams">Language key that specifies the object's language.</param>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <returns></returns>
        Task<bool> UnFollow(long id, long accountId, LangKey langParams);
    }
}