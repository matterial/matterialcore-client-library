﻿using MatterialCore.Entity;
using MatterialCore.Enums;
using MatterialCore.Params;

using System.Collections.Generic;
using System.Threading.Tasks;

namespace MatterialCore.Interfaces
{
    /// <summary>
    /// <see href="https://www.matterial.com/documentation/api/165">API-Reference</see>
    /// </summary>
    public interface IMtrTask
    {
        /// <summary>
        /// Load a task by its id. <see href="https://www.matterial.com/documentation/api/165#get-task-by-id">API-Reference</see>
        /// </summary>
        /// <param name="id">Id of the task to load.</param>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <returns></returns>
        Task<MtrTask?> LoadById(long id);

        /// <summary>
        /// Load tasks. <see href="https://www.matterial.com/documentation/api/165#get-tasks">API-Reference</see>
        /// </summary>
        /// <param name="options">Options defining the load behaviour.</param>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <returns></returns>
        Task<ListResult<MtrTask>?> Load(TaskParams options);

        /// <summary>
        /// Create a new task. <see href="https://www.matterial.com/documentation/api/165#create-task">API-Reference</see>
        /// </summary>
        /// <param name="newTask">the new task object.</param>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <returns></returns>
        Task<MtrTask?> Create(MtrTask newTask);

        /// <summary>
        /// Update a task. <see href="https://www.matterial.com/documentation/api/165#update-task">API-Reference</see>
        /// </summary>
        /// <param name="updateTask">Task containing the update data.</param>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <returns></returns>
        Task<MtrTask?> Update(MtrTask updateTask);

        /// <summary>
        /// Remove a task. <see href="https://www.matterial.com/documentation/api/165#remove-task">API-Reference</see>
        /// </summary>
        /// <param name="id">Id of the task to remove.</param>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <returns></returns>
        Task<bool> Remove(long id);

        /// <summary>
        /// Get the status of a task. <see href="https://www.matterial.com/documentation/api/168">API-Reference</see>
        /// </summary>
        /// <param name="id">Id of the task to update.</param>
        /// <param name="languageKey">Language key.</param>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <returns></returns>
        Task<MtrTaskStatus?> GetStatusById(long id, LangKey languageKey);

        /// <summary>
        /// Get task statuses. <see href="https://www.matterial.com/documentation/api/168">API-Reference</see>
        /// </summary>
        /// <param name="options">Options defining the load behaviour.</param>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <returns></returns>
        Task<List<MtrTaskStatus>?> GetStatus(TaskStatusParams options);
    }
}