﻿using MatterialCore.Entity;
using MatterialCore.Enums;
using MatterialCore.Params;

using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace MatterialCore.Interfaces
{
    /// <summary>
    /// The IInstance interface manages instances. <see href="https://www.matterial.com/documentation/api/42">API-Reference</see>
    /// </summary>
    public interface IInstance
    {
        /// <summary>
        /// Get all invitees, that do not exist in instance yet. <see href="https://www.matterial.com/documentation/api/42#get-invitees-with-no-account">API-Reference</see>
        /// </summary>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <returns></returns>
        Task<List<Credential>?> GetInvitees();

        /// <summary>
        /// This resends the invitation mail to the invitee and renews its LoginToken. <see href="https://www.matterial.com/documentation/api/42#reinvite-credential">API-Reference</see>
        /// </summary>
        /// <param name="credentialId">Id of the credential.</param>
        /// <param name="languageKey">Language key</param>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <returns></returns>
        Task Reinvite(long credentialId, LangKey languageKey);

        /// <summary>
        /// Remove the credential from current instance. Additionally this removes the whole
        /// credential if no more related instances are left. <see href="https://www.matterial.com/documentation/api/42#remove-invitee">API-Reference</see>
        /// </summary>
        /// <param name="credentialId">Id of the credential.</param>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <returns></returns>
        Task<bool> RemoveInvitee(long credentialId);

        /// <summary>
        /// Create a new credential and send an invitation e-mail to the Person. An
        /// email-notification-link will be created to login without password. This is only
        /// available for instance-admins. Instance-Admins can only create credentials within the
        /// current instance. Credential.password and Credential.dataSources will be ignored. The
        /// current instance will be added to the existing. <see href="https://www.matterial.com/documentation/api/42#store-credentials">API-Reference</see>
        /// </summary>
        /// <para>Permission required: instanceAdmin</para>
        /// <param name="item">The credentials to store</param>
        /// <param name="languageKey">Language key</param>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <returns></returns>
        Task<long> StoreCredentials(CredentialWithInvitationText item, LangKey languageKey);

        Task<List<long>?> StoreCredentials(List<CredentialWithInvitationText> items, LangKey languageKey);

        /// <summary>
        /// Make the given instance the favourite for the current user. <see href="https://www.matterial.com/documentation/api/42#update-favourite-data-source">API-Reference</see>
        /// </summary>
        /// <param name="item">The instance to become the favourite instance.</param>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <returns></returns>
        Task<LoginData?> UpdateFavouriteInstance(Instance item);

        /// <summary>
        /// Load an instance by its id. <see href="https://www.matterial.com/documentation/api/42#get-instance-by-id">API-Reference</see>
        /// </summary>
        /// <para>Permission required: systemAccount</para>
        /// <param name="id">Id of the object to load.</param>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <returns></returns>
        Task<Instance?> Load(long id);

        /// <summary>
        /// Load instances. <see href="https://www.matterial.com/documentation/api/42#get-instances">API-Reference</see>
        /// </summary>
        /// <para>Permission required: systemAccount</para>
        /// <param name="options">Options defining the load bahaviour.</param>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <returns></returns>
        Task<ListResult<Instance>?> Load(InstanceParams options);

        /// <summary>
        /// Update displayname of current instance. <see href="https://www.matterial.com/documentation/api/42#update-displayname-of-current-datasource">API-Reference</see>
        /// </summary>
        /// <para>Permission required: instanceOwner</para>
        /// <param name="displayName">The new display name</param>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <returns></returns>
        Task<LoginData?> UpdateDisplayname(string displayName);

        /// <summary>
        /// Activates an instance for unlimited days. <see href="https://www.matterial.com/documentation/api/42#activate-instance-unlimited">API-Reference</see>
        /// </summary>
        /// <para>Permission required: systemAccount</para>
        /// <param name="instanceName">Name of the instance</param>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <returns></returns>
        Task<bool> ActivateInstance(string instanceName);

        /// <summary>
        /// Ativates an instance for number of specified days. <see href="https://www.matterial.com/documentation/api/42#activate-instance">API-Reference</see>
        /// </summary>
        /// <para>Permission required: systemAccount</para>
        /// <param name="instanceName">Name of the instance</param>
        /// <param name="days">Number of days to activate</param>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <returns></returns>
        Task<bool> ActivateInstance(string instanceName, int days);

        /// <summary>
        /// Deactivates an active instance. <see href="https://www.matterial.com/documentation/api/42#deactivate-instance">API-Reference</see>
        /// </summary>
        /// <para>Permission required: systemAccount</para>
        /// <param name="instanceName"></param>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <returns></returns>
        Task<bool> DeactivateInstance(string instanceName);

        /// <summary>
        /// Removes an instance. Instance has to be deactivated before removing it. <see href="https://www.matterial.com/documentation/api/42#remove-instance">API-Reference</see>
        /// </summary>
        /// <para>Permission required: systemAccount</para>
        /// <param name="instanceName"></param>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <returns></returns>
        Task<bool> RemoveInstance(string instanceName);

        /// <summary>
        /// Get the two factor authentication QR code as a stream.
        /// </summary>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <returns></returns>
        Task<Stream?> Get2FAQRCode();

        /// <summary>
        /// Activate the two factor authentication feature for the current user. <see href="https://www.matterial.com/documentation/api/42#fa---activate">API-Reference</see>
        /// </summary>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <returns></returns>
        Task<LoginData?> Activate2FA();

        /// <summary>
        /// Deactivate the two factor authentication feature for the current user. <see href="https://www.matterial.com/documentation/api/42#fa---deactivate">API-Reference</see>
        /// </summary>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <returns></returns>
        Task<LoginData?> Deactivate2FA();

        /// <summary>
        /// Authorize the given Two Factor Auth verification code. <see href="https://www.matterial.com/documentation/api/42#fa---authorize">API-Reference</see>
        /// </summary>
        /// <param name="code">The two factor auth code.</param>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <returns></returns>
        Task<LoginData?> Authorize2FA(string code);

        /// <summary>
        /// Get all global preferences as a map. <see href="https://www.matterial.com/documentation/api/45#get-preferences-map">API-Reference</see>
        /// </summary>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <returns></returns>
        Task<PreferenceMapContainer?> GetPreferences();

        /// <summary>
        /// Get a global preference by key <see href="https://www.matterial.com/documentation/api/45#get-preference-by-key">API-Reference</see>
        /// </summary>
        /// <param name="key">The key of the preference to load.</param>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <returns></returns>
        Task<string?> GetPreference(string key);

        /// <summary>
        /// Get all prteferences that are accessible without being logged in.
        ///<see href="https://www.matterial.com/documentation/api/45#get-permission-free-preferences-map">API-Reference</see>
        /// </summary>
        /// <exception cref="System.Net.Http.HttpRequestException">Thrown if the http request fails.</exception>
        /// <returns></returns>
        Task<PreferenceMapContainer?> GetUnrestrictedPreferences();

        /// <summary>
        /// Get the value of a preference by key accessible without login. <see href="https://www.matterial.com/documentation/api/45#get-permission-free-preference-by-key">API-Reference</see>
        /// </summary>
        /// <param name="key"></param>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <returns></returns>
        Task<string?> GetUnrestrictedPreference(UnrestrictedPreferences key);

        /// <summary>
        /// Re-initialize the preferences cache. <see href="https://www.matterial.com/documentation/api/45#reinit-preference-cache">API-Reference</see>
        /// </summary>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <returns></returns>
        Task ReinitPreferenceCache();

        /// <summary>
        /// Store a new preference. <see href="https://www.matterial.com/documentation/api/45#store-preference">API-Reference</see>
        /// </summary>
        /// <param name="key">The key of the new preference.</param>
        /// <param name="value">The value of the new preference.</param>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <returns></returns>
        Task StorePreference(string key, string value);
    }
}