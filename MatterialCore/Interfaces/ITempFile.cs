﻿using MatterialCore.Entity;
using MatterialCore.Enums;
using MatterialCore.Params.Doc;

using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace MatterialCore.Interfaces
{
    /// <summary>
    /// Interface which offers functions to control temporary files, uploads etc.
    /// </summary>
    public interface ITempFile
    {
        /// <summary>
        /// Loads a temporaty file from the server. <see href="https://www.matterial.com/documentation/api/47#get-temp-file-stream">API-Reference</see>
        /// </summary>
        /// <param name="tempFileType">The type of file.</param>
        /// <param name="contextToken"></param>
        /// <param name="fileToken"></param>
        /// <param name="languageKey"></param>
        /// <param name="forceAttachment">
        /// If true, the ContentDisposition-Header: attachment is forced.
        /// </param>
        /// <returns></returns>
        Task<Stream?> GetTempFile(TempFileType tempFileType, string contextToken, string fileToken, LangKey languageKey, bool forceAttachment);

        /// <summary>
        /// Loads a temporaty file from the server. <see href="https://www.matterial.com/documentation/api/47#get-temp-file-stream">API-Reference</see>
        /// </summary>
        /// <param name="tempFileDescriptor">
        /// A TempFileDescriptor that was retrieved in a pervious call.
        /// </param>
        /// <param name="forceAttachment">
        /// If true, the ContentDisposition-Header: attachment is forced.
        /// </param>
        /// <returns></returns>
        Task<Stream?> GetTempFile(TempFileDescriptor tempFileDescriptor, bool forceAttachment);

        /// <summary>
        /// Upload a file which can then be attached to a Document. <see href="https://www.matterial.com/documentation/api/47#upload-temp-file-multipart">API-Reference</see>
        /// </summary>
        /// <param name="uploadFile">The UploadFile object.</param>
        /// <param name="contextToken">
        /// A unique id that will be needed to reference the uploaded temporary files in later commands.
        /// </param>
        /// <param name="languageKey">The language key.</param>
        /// <returns>A list of TempFileDescriptor objects containing tha information.</returns>
        Task<TempFileDescriptor?> UploadFile(UploadFile uploadFile, string contextToken, LangKey languageKey);

        /// <summary>
        /// Upload a list of files which can then be attached to a Document. <see href="https://www.matterial.com/documentation/api/47#upload-temp-file-multipart">API-Reference</see>
        /// </summary>
        /// <param name="uploadFiles">The UploadFile object.</param>
        /// <param name="contextToken">
        /// A unique id that will be needed to reference the uploaded temporary files in later commands.
        /// </param>
        /// <param name="languageKey">The language key.</param>
        /// <returns>A list of TempFileDescriptor objects containing tha information.</returns>
        Task<List<TempFileDescriptor>?> UploadFile(List<UploadFile> uploadFiles, string contextToken, LangKey languageKey);

        /// <summary>
        /// Upload plain text to a temp file.
        /// </summary>
        /// <param name="text">THe text contant (markdown / html)</param>
        /// <param name="contextToken">
        /// A unique id that will be needed to reference the uploaded temporary files in later commands.
        /// </param>
        /// <param name="languageKey">The language key.</param>
        /// <returns></returns>
        Task<TempFileDescriptor?> UploadText(string text, string contextToken, LangKey languageKey);

        /// <summary>
        /// Remove temp files by contextToken. <see href="https://www.matterial.com/documentation/api/47#remove-temp-files">API-Reference</see>
        /// </summary>
        /// <param name="contextToken">ContextToken used when uploading the temp files.</param>
        /// <returns></returns>
        Task RemoveTempfiles(string contextToken);

        /// <summary>
        /// Remove a tempfile. <see href="https://www.matterial.com/documentation/api/47#remove-temp-file">API-Reference</see>
        /// </summary>
        /// <param name="tempFileType">Type of temp file.</param>
        /// <param name="contextToken">ContextToken used when uploading the temp files.</param>
        /// <param name="fileToken">FileToken returned when uploading the temp files.</param>
        /// <param name="languageKey">Language key.</param>
        /// <returns></returns>
        Task RemoveTempfile(TempFileType tempFileType, string contextToken, string fileToken, LangKey languageKey);

        /// <summary>
        /// Remove a temp file by passing a temp file descriptor. <see href="https://www.matterial.com/documentation/api/47#remove-temp-file-1">API-Reference</see>
        /// </summary>
        /// <param name="tempFileDescriptor">The temp file descriptor.</param>
        /// <returns></returns>
        Task RemoveTempfile(TempFileDescriptor tempFileDescriptor);
    }
}