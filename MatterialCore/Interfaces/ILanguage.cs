﻿using MatterialCore.Entity;
using MatterialCore.Params;

using System.Collections.Generic;
using System.Threading.Tasks;

namespace MatterialCore.Interfaces
{
    /// <summary>
    /// <see href="https://www.matterial.com/documentation/api/59">API-Reference</see>
    /// </summary>
    public interface ILanguage
    {
        /// <summary>
        /// Load a language by its id. <see href="https://www.matterial.com/documentation/api/59#get-language-by-id">API-Reference</see>
        /// </summary>
        /// <param name="id"></param>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <returns></returns>
        Task<Language?> Load(long id);

        /// <summary>
        /// Load languages <see href="https://www.matterial.com/documentation/api/59#get-languages">API-Reference</see>
        /// </summary>
        /// <param name="options">Options defining the load behaviour.</param>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <returns></returns>
        Task<List<Language>?> Load(LanguageParams options);

        /// <summary>
        /// Deactivate a language. <see href="https://www.matterial.com/documentation/api/59#deactivate-language">API-Reference</see>
        /// </summary>
        /// <param name="id">Language id</param>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <returns></returns>
        Task<bool> Deaktivate(long id);

        /// <summary>
        /// Activate a language. <see href="https://www.matterial.com/documentation/api/59#activate-language">API-Reference</see>
        /// </summary>
        /// <param name="id"></param>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <returns></returns>
        Task<bool> Aktivate(long id);
    }
}