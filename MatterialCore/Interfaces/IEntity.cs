﻿using Newtonsoft.Json;

namespace MatterialCore.Interfaces
{
    /// <summary>
    /// The IEntity marker interface for all Matterial entities used in the REST client.
    /// </summary>
    public interface IEntity
    {
        public string ToJson(Formatting formatting = Formatting.None, NullValueHandling nullValueHandling = NullValueHandling.Ignore);
    }
}