﻿namespace MatterialCore.Interfaces
{
    /// <summary>
    /// Marke interface for entities that are searchable.
    /// </summary>
    public interface ISearchable : IListableEntity, IRestEntity
    {
    }
}