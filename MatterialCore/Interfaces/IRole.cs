﻿using MatterialCore.Entity;
using MatterialCore.Params;

using System.Collections.Generic;
using System.Threading.Tasks;

namespace MatterialCore.Interfaces
{
    /// <summary>
    /// This interface defines the functions to control roles.
    /// Note: The system handles ReviewGroups, WorkGroups, PersonalRoles and FunctionalRoles as
    /// roles. <see href="https://www.matterial.com/documentation/api/269">API-Reference</see>
    /// </summary>
    public interface IRole
    {
        /// <summary>
        /// Load a role by its id. <see href="https://www.matterial.com/documentation/api/269#get-role-by-id">API-Reference</see>
        /// </summary>
        /// <param name="id">Id of the role.</param>
        /// <returns></returns>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        Task<Role?> LoadById(long id);

        /// <summary>
        /// Load roles. <see href="https://www.matterial.com/documentation/api/269#get-roles">API-Reference</see>
        /// </summary>
        /// <param name="options">Options defining the load behaviour.</param>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <returns></returns>
        Task<ListResult<Role>?> Load(RoleParams options);

        /// <summary>
        /// Create a new role. This can be a WorkGroup, ReviewGroup, PersonalRole or Role. The type
        /// of role is specified by the property <see href="https://www.matterial.com/documentation/api/269#create-role">API-Reference</see>
        /// </summary>
        /// <param name="newRole"></param>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <returns></returns>
        Task<long> Create(Role newRole);

        /// <summary>
        /// Update a role. <see href="https://www.matterial.com/documentation/api/269#update-role">API-Reference</see>
        /// </summary>
        /// <param name="id">Id of the role.</param>
        /// <param name="updateRole">Object containimng the data to update.</param>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <returns></returns>
        Task<long> Update(long id, Role updateRole);

        /// <summary>
        /// Remove a role. This cannot be undone. <see href="https://www.matterial.com/documentation/api/269#remove-role">API-Reference</see>
        /// </summary>
        /// <param name="id">Id of the role.</param>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <returns></returns>
        Task<bool> Remove(long id);

        /// <summary>
        /// Assign a role to an account. <see href="https://www.matterial.com/documentation/api/269#assign-role-to-person">API-Reference</see>
        /// </summary>
        /// <param name="id">Id of the role.</param>
        /// <param name="accountId">The account id.</param>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <returns></returns>
        Task<List<Person>?> Assign(long id, long accountId);

        /// <summary>
        /// Assigne multiple roles to multiple accounts. <see href="https://www.matterial.com/documentation/api/269#assign-role-to-persons">API-Reference</see>
        /// </summary>
        /// <param name="ids">List of ids of the roles.</param>
        /// <param name="accountIds">List of account ids.</param>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <returns></returns>
        Task<List<Person>?> Assign(List<long> ids, List<long> accountIds);

        /// <summary>
        /// Un-assign a role from an account. <see href="https://www.matterial.com/documentation/api/269#unassign-role-from-person">API-Reference</see>
        /// </summary>
        /// <param name="id">Id of the role.</param>
        /// <param name="accountId">Account id</param>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <returns></returns>
        Task<List<Person>?> UnAssign(long id, long accountId);

        /// <summary>
        /// Un-assign multiple roles from multiple accounts. <see href="https://www.matterial.com/documentation/api/269#unassign-roles-from-persons">API-Reference</see>
        /// </summary>
        /// <param name="ids">List of ids of the roles.</param>
        /// <param name="accountIds">List of account ids.</param>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <returns></returns>
        Task<List<Person>?> UnAssign(List<long> ids, List<long> accountIds);
    }
}