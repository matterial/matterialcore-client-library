﻿using MatterialCore.Entity;
using MatterialCore.Params;

using System.Threading.Tasks;

namespace MatterialCore.Interfaces
{
    /// <summary>
    /// <see href="https://www.matterial.com/documentation/api/653">API-Reference</see>
    /// </summary>
    public interface IActivity
    {
        /// <summary>
        /// Get activity by id <see href="https://www.matterial.com/documentation/api/653#get-activity-by-id">API-Reference</see>
        /// </summary>
        /// <param name="id">Id of the object</param>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <returns></returns>
        Task<Activity?> Load(long id);

        /// <summary>
        /// Get activities by filkter parameter. <see href="https://www.matterial.com/documentation/api/653#get-activities">API-Reference</see>
        /// </summary>
        /// <param name="options"></param>
        /// <exception cref="System.Net.Http.HttpRequestException">
        /// Thrown if the http request fails.
        /// </exception>
        /// <returns></returns>
        Task<ListResult<Activity>?> Load(ActivityParams options);
    }
}