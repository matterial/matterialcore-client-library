﻿using MatterialCore;
using MatterialCore.Entity;
using MatterialCore.Enums;
using MatterialCore.Interfaces;
using MatterialCore.Params;
using MatterialCore.Params.Doc;
using MatterialCore.Params.Person;

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

/// <summary>
/// Matterial Core Library Example Code <see href="https://www.matterial.com/documentation/api/655">API-Reference</see>
/// ToDo: In order for this console app to work, you need to add the required credentials in the
/// file TestCredentials.cs
/// </summary>
namespace MtrExamples
{
    internal class Program
    {
        public static void Main()
        {
            //DisplaySessionInfos();
            //UpdateAccountSettings();
            //ChangeInstance();
            //Connect();
            //ConnectAndReuse();
            SearchDocuments();
            //SearchWordAttachments();
            //DownloadAttachment();
            //CreateDocument();
            //UpdateDocument();
            //CreateCategory();
            //for (int i = 0; i < 5; i++)
            //{
            //    ImportDocuments();
            //}

            //InvitePerson();
            //CreatePerson();
            //FafouriteDocuments();
            //LoadUrgentDocuments();
            //ShowLicenseInfo();
            //UpdatePersonalData();
            //AssignRoleToPerson();
        }

        public static void DisplaySessionInfos()
        {
            //Create a session using the static Connect function of the Matterial class
            using (IApiSession apiSession = Matterial.Connect(TestCrendentials.ApiUserName, Matterial.SecureString(TestCrendentials.ApiUserPassword), TestCrendentials.Url))
            {
                LoginData loginData = apiSession.LoginData().Result;
                Console.WriteLine("------------------------- Account Settings -------------------------------------");
                Console.WriteLine("");
                foreach (var item in loginData.AccountSettings)
                {
                    //Console.WriteLine(item.Key + " = " + item.Value);
                    Console.WriteLine(String.Format("{0,-71} = {1,-20} ", item.Key, item.Value));
                }
                Console.WriteLine("");
                Console.WriteLine("------------------------- Client Preferences -----------------------------------");
                Console.WriteLine("");
                foreach (var item in loginData.ClientPreferences)
                {
                    Console.WriteLine(String.Format("{0,-47} = {1,-20} ", item.Key, item.Value));
                }

                Console.WriteLine("");
                Console.WriteLine("------------------------- Available Languages -----------------------------------");
                Console.WriteLine("");
                foreach (var item in loginData.AvailableLanguages)
                {
                    Console.WriteLine(item.Name);
                }
                Console.WriteLine("");
                Console.WriteLine("Current instance: " + loginData.CurrentInstance.DisplayName);

                Console.WriteLine("");
                Console.WriteLine("Current Person Information ------------------------------");

                Console.WriteLine(String.Format("{0,-23} = {1,-10} ", nameof(loginData.Person.AccountId), loginData.Person.AccountId.ToString()));
                Console.WriteLine(String.Format("{0,-23} = {1,-10} ", nameof(loginData.Person.FirstName), loginData.Person.FirstName));
                Console.WriteLine(String.Format("{0,-23} = {1,-10} ", nameof(loginData.Person.LastName), loginData.Person.LastName));
                Console.WriteLine(String.Format("{0,-23} = {1,-10} ", nameof(loginData.Person.InstanceOwner), loginData.Person.InstanceOwner.ToString()));
                Console.WriteLine(String.Format("{0,-23} = {1,-10} ", nameof(loginData.Person.SystemAccount), loginData.Person.SystemAccount.ToString()));
                Console.WriteLine(String.Format("{0,-23} = {1,-10} ", nameof(loginData.Person.AccountLogin), loginData.Person.AccountLogin));

                Console.WriteLine("");
                Console.WriteLine("------------------------- Permissions -----------------------------------");

                Console.WriteLine(String.Format("{0,-23} = {1,-10} ", (nameof(loginData.Person.Permissions.AdministrateAll)), loginData.Person.Permissions.AdministrateAll.ToString()));
                Console.WriteLine(String.Format("{0,-23} = {1,-10} ", (nameof(loginData.Person.Permissions.Comment)), loginData.Person.Permissions.Comment.ToString()));
                Console.WriteLine(String.Format("{0,-23} = {1,-10} ", (nameof(loginData.Person.Permissions.CreateSnap)), loginData.Person.Permissions.CreateSnap.ToString()));
                Console.WriteLine(String.Format("{0,-23} = {1,-10} ", (nameof(loginData.Person.Permissions.EditAdditionalProperty)), loginData.Person.Permissions.EditAdditionalProperty.ToString()));
                Console.WriteLine(String.Format("{0,-23} = {1,-10} ", (nameof(loginData.Person.Permissions.EditCategory)), loginData.Person.Permissions.EditCategory.ToString()));
                Console.WriteLine(String.Format("{0,-23} = {1,-10} ", (nameof(loginData.Person.Permissions.EditDocument)), loginData.Person.Permissions.EditDocument.ToString()));
                Console.WriteLine(String.Format("{0,-23} = {1,-10} ", (nameof(loginData.Person.Permissions.EditDocumentTemplate)), loginData.Person.Permissions.EditDocumentTemplate.ToString()));
                Console.WriteLine(String.Format("{0,-23} = {1,-10} ", (nameof(loginData.Person.Permissions.EditGallery)), loginData.Person.Permissions.EditGallery.ToString()));
                Console.WriteLine(String.Format("{0,-23} = {1,-10} ", (nameof(loginData.Person.Permissions.EditGlobalSavedSearch)), loginData.Person.Permissions.EditGlobalSavedSearch.ToString()));
                Console.WriteLine(String.Format("{0,-23} = {1,-10} ", (nameof(loginData.Person.Permissions.EditPerson)), loginData.Person.Permissions.EditPerson.ToString()));
                Console.WriteLine(String.Format("{0,-23} = {1,-10} ", (nameof(loginData.Person.Permissions.EditRole)), loginData.Person.Permissions.EditRole.ToString()));
                Console.WriteLine(String.Format("{0,-23} = {1,-10} ", (nameof(loginData.Person.Permissions.EditTask)), loginData.Person.Permissions.EditTask.ToString()));
                Console.WriteLine(String.Format("{0,-23} = {1,-10} ", (nameof(loginData.Person.Permissions.ImmediateReview)), loginData.Person.Permissions.ImmediateReview.ToString()));
                Console.WriteLine(String.Format("{0,-23} = {1,-10} ", (nameof(loginData.Person.Permissions.PublishUnreviewed)), loginData.Person.Permissions.PublishUnreviewed.ToString()));
                Console.WriteLine(String.Format("{0,-23} = {1,-10} ", (nameof(loginData.Person.Permissions.ShowTrash)), loginData.Person.Permissions.ShowTrash.ToString()));
                Console.WriteLine(String.Format("{0,-23} = {1,-10} ", (nameof(loginData.Person.Permissions.ViewArchive)), loginData.Person.Permissions.ViewArchive.ToString()));
                Console.WriteLine(String.Format("{0,-23} = {1,-10} ", (nameof(loginData.Person.Permissions.ViewStatistic)), loginData.Person.Permissions.ViewStatistic.ToString()));
                Console.WriteLine();
                Console.WriteLine("Press any key to exit.");
                Console.ReadKey();
            }
        }

        public static void Connect()
        {
            //Create a session using the static Connect function of the Matterial class
            using (IApiSession apiSession = Matterial.Connect(TestCrendentials.ApiUserName, Matterial.SecureString(TestCrendentials.ApiUserPassword), TestCrendentials.Url))
            {
                //Check if we are connected:
                if (apiSession.IsLoggedIn)
                    Console.WriteLine("Right on!");
                else
                    Console.WriteLine("Something went south...");
            }
        }

        public static void ConnectAndReuse()
        {
            string sessionId;
            //Create a session using the static Connect function of the Matterial class
            using (IApiSession apiSession = Matterial.Connect(TestCrendentials.ApiUserName, Matterial.SecureString(TestCrendentials.ApiUserPassword), TestCrendentials.Url))
            {
                //Check if we are connected:
                if (apiSession.IsLoggedIn)
                    Console.WriteLine("Right on!");
                else
                    Console.WriteLine("Something went south...");

                //Set the property to skip log off so we can re-use the session ID
                apiSession.LogoffOnDispose = false;

                //Save the session ID
                sessionId = apiSession.SessionId;
            }
            //At this point we are disposed but not logged off.

            //Create a session with the session ID
            using (IApiSession apiSession = Matterial.Connect(sessionId, TestCrendentials.Url))
            {
                //Check if we are connected:
                if (apiSession.IsLoggedIn)
                    Console.WriteLine("Right on!");
                else
                    Console.WriteLine("Something went south...");
            }
        }

        public static void ChangeInstance()
        {
            //Create a session using the static Connect function of the Matterial class
            using (IApiSession apiSession = Matterial.Connect(TestCrendentials.ApiUserName, Matterial.SecureString(TestCrendentials.ApiUserPassword), TestCrendentials.Url))
            {
                //Check if we have access to multiple instances.
                LoginData loginData = apiSession.LoginData().Result;
                if (loginData.AvailableInstances.Count > 1)
                {
                    //Switch to second instance
                    apiSession.ChangeInstance(loginData.AvailableInstances[1].Id).Wait();
                }
            }
        }

        private static void SaveStreamToFile(Stream stream, string filePath)
        {
            using FileStream fStream = new(filePath, FileMode.Create);
            stream.Seek(0, SeekOrigin.Begin);
            stream.CopyTo(fStream);
            fStream.Close();
        }

        private static void CreateDocument()
        {
            using (IApiSession apiSession = Matterial.Connect(TestCrendentials.ApiUserName, Matterial.SecureString(TestCrendentials.ApiUserPassword), TestCrendentials.Url))
            {
                //Create a new document object with title, abstract and language key.
                Document newDoc = new()
                {
                    LanguageVersionTitle = "The document's title",
                    LanguageVersionAbstract = "The abstract (description)",
                    LanguageVersionLanguageKey = apiSession.DefaultContentLanguage.ToLangString()
                };

                //In order to unlock it after creation, we need an unlock id.
                DocUpdateParams options = new()
                {
                    UniqueLockId = Guid.NewGuid().ToString()
                };

                Document myNewDoc = apiSession.Document.Create(newDoc, options, "# This is the document content text", DocumentContentMediaType.Markdown).Result;
                //The document is now created and locked. Let's unlock it as we do not want to further update it.
                bool unlocked = apiSession.Document.Unlock(options.UniqueLockId).Result;
            }
        }

        private static void UpdateDocument()
        {
            using (IApiSession apiSession = Matterial.Connect(TestCrendentials.ApiUserName, Matterial.SecureString(TestCrendentials.ApiUserPassword), TestCrendentials.Url))
            {
                //Let's assume we want to update a document with the id 2350
                long docId = 2350;

                //Set the values we want to update
                Document updateValues = new()
                {
                    LanguageVersionTitle = "The document's new title",
                    LanguageVersionAbstract = "The new abstract (description)"
                };

                //In order to unlock it after creation, we need an unlock id.
                //First we have to get a lock. Create a unique lock id which we need to lock and unlock later
                string uniqueLockId = Guid.NewGuid().ToString();
                DocumentLock myLock = apiSession.Document.Lock(docId, uniqueLockId).Result;
                //If it worked, keep going
                if (!(myLock is null) && myLock.LockedForMe)
                {
                    //Set the lock id
                    DocUpdateParams options = new()
                    {
                        UniqueLockId = uniqueLockId,  //our lock id
                        PublishRequestType = PublishRequestTypes.ReviewRequested //We want to request review
                    };

                    //Update it
                    Document updatedDoc = apiSession.Document.Update(updateValues, options).Result;
                    //The document is now updated. Let's unlock it as we do not want to further update it.
                    bool unlocked = apiSession.Document.Unlock(options.UniqueLockId).Result;
                }
            }
        }

        private static void CreateCategory()
        {
            using (IApiSession apiSession = Matterial.Connect(TestCrendentials.ApiUserName, Matterial.SecureString(TestCrendentials.ApiUserPassword), TestCrendentials.Url))
            {
                //A category must be assigned to a category type. In this example, we also create a new category type
                CategoryType newType = new("Comics");
                long catTypeId = apiSession.Category.CreateType(newType, Matterial.DefaultLanguage).Result;

                //If it worked out, we can go ahead and create the category
                if (catTypeId > 0)
                {
                    //create a new category using the TypeId
                    Category newCat = new("Mickey Mouse")
                    {
                        TypeId = catTypeId
                    };
                    long newCatId = apiSession.Category.Create(newCat, Matterial.DefaultLanguage).Result;
                }
            }
        }

        private static void FollowCategory()
        {
            using (IApiSession apiSession = Matterial.Connect(TestCrendentials.ApiUserName, Matterial.SecureString(TestCrendentials.ApiUserPassword), TestCrendentials.Url))
            {
                //We want to follow the catogery with the id 33
                long categoryId = 33;
                bool b = apiSession.Category.Follow(categoryId, Matterial.DefaultLanguage).Result;
            }
        }

        private static void CreateRole()
        {
            using (IApiSession apiSession = Matterial.Connect(TestCrendentials.ApiUserName, Matterial.SecureString(TestCrendentials.ApiUserPassword), TestCrendentials.Url))
            {
                //Create a functional role
                Role newRole = new("New functional role", RoleEntityType.Functional);
                long newRoleId = apiSession.Role.Create(newRole).Result;

                //Create a group
                newRole = new Role("New group", RoleEntityType.WorkGroup);
                long newGroupId = apiSession.Role.Create(newRole).Result;
            }
        }

        private static void SearchDocuments()
        {
            using (IApiSession apiSession = Matterial.Connect(TestCrendentials.ApiUserName,
                Matterial.SecureString(TestCrendentials.ApiUserPassword), TestCrendentials.Url))
            {
                string CsvContent = "DocumentId\tUser\tDocumentTitle\r\n";

                //We need the person controller to load a person by ID
                IPerson PersonController = apiSession.Person;

                //Run a document search using a query string.
                string query = "*";
                //We set options of how the search should behave.
                SearchDocParams options = new();
                options.Limit = 500;
                options.AllLanguages = true;
                options.HasCategoryAssigned = CategoryAssignedValue.NoneAssigned;
                options.Published = ParamHideValue.Exclusive;
                options.Templates = ParamShowValue.Include;

                //Run the search
                SearchResult<Document> results = apiSession.Document.Search(query, options).Result;
                // We need to check if the document was already processed since we might have multiple languages
                // of the same document
                Dictionary<long, long> ProcessedDocs = new();
                //Iterate through the result items
                foreach (var item in results.Results)
                {
                    if (!ProcessedDocs.ContainsKey(item.Source.Id))
                    {
                        ProcessedDocs.Add(item.Source.Id, item.Source.Id);
                        //Load doc including the Author information
                        var args = new LoadDocByIdParams();
                        args.LoadOptions.LastAuthorOnly = false;
                        args.LoadOptions.Authors = true;
                        var doc = apiSession.Document.Load(item.Source.Id, args).Result;
                        //Get the first Author's account id (which is the creator of the document)
                        var accountid = doc.LastWriteTimesInSeconds.Keys.FirstOrDefault();
                        //Load the person to get all information of the person
                        var ThePerson = PersonController.LoadByAccountId(accountid).Result;
                        //create CSV data line
                        CsvContent += $"{doc.Id}\t{ThePerson.FirstName + " " + ThePerson.LastName}\t{item.Source.LanguageVersionTitle.Replace("\n", "")}\r\n";
                    }
                }

                File.WriteAllText("Export.csv", CsvContent);

                Console.WriteLine();
                Console.WriteLine("Press any key to exit.");
                Console.ReadKey();
            }
        }

        private static void FafouriteDocuments()
        {
            using (IApiSession apiSession = Matterial.Connect(TestCrendentials.ApiUserName, Matterial.SecureString(TestCrendentials.ApiUserPassword), TestCrendentials.Url))
            {
                long personalCategoryId = 0;

                //We set the load parameter to Personal Categories = Exclusive.
                //The system automatically creates one personal "Quick" category for a person. This is used for favourites.
                CategoryParams catParams = new()
                {
                    Personal = ParamHideBoolValue.Exclusive
                };

                //Now load the category to get its Id
                List<Category> personalCategories = apiSession.Category.Load(catParams).Result;
                if (!(personalCategories is null) && (personalCategories.Count > 0))
                {
                    personalCategoryId = personalCategories[0].Id;
                }

                //We can set options of how the search should behave. We set it to the category Id
                LoadDocParams docParams = new()
                {
                    CategoryAndIds = new List<long> { personalCategoryId }
                };

                //Load the favourite documents
                ListResult<Document> docs = apiSession.Document.Load(docParams).Result;
            }
        }

        private static void LoadUrgentDocuments()
        {
            using (IApiSession apiSession = Matterial.Connect(TestCrendentials.ApiUserName, Matterial.SecureString(TestCrendentials.ApiUserPassword), TestCrendentials.Url))
            {
                LoadDocParams docParams = new()
                {
                    AdditionalPropertyType = AdditionalPropertyLoadTypes.Urgent
                };
                ListResult<Document> docs = apiSession.Document.Load(docParams).Result;
            }
        }

        private void AssignPermissionToDocument()
        {
            using (IApiSession apiSession = Matterial.Connect(TestCrendentials.ApiUserName, Matterial.SecureString(TestCrendentials.ApiUserPassword), TestCrendentials.Url))
            {
                //Let's assume, we have a document with the id 222. We want to assign the EDIT permission
                //to a group to the english language version of the document.
                long docId = 222;
                string languageKey = Matterial.DefaultLanguage.ToLangString();

                //The role id (id of the work group) is 5
                long roleId = 5;

                //First, load the document so we get the current information
                Document docToUpdate = apiSession.Document.Load(docId, languageKey).Result;

                //Lock the document for update
                DocUpdateParams updateParams = new()
                {
                    UniqueLockId = Guid.NewGuid().ToString()
                };
                DocumentLock docLock = apiSession.Document.Lock(docToUpdate.Id, updateParams.UniqueLockId).Result;

                //Check, if it worked out
                if (docLock.LockedForMe)
                {
                    Role groupRole = apiSession.Role.LoadById(roleId).Result;

                    //Now we assign the role by assigning a RoleRight to the document.
                    RoleRight roleRight = new()
                    {
                        Role = groupRole,
                        Type = RoleRightTypes.EDIT
                    };
                    //Add the role right
                    docToUpdate.RoleRights.Add(roleRight);

                    //Update the document using the document object we prepared and the update parameters
                    //containing the unique lock id
                    Document updatedDoc = apiSession.Document.Update(docToUpdate, updateParams).Result;

                    //Unlock the document.
                    bool b = apiSession.Document.Unlock(updateParams.UniqueLockId).Result;
                }
            }
        }

        private static void AssignRoleToPerson()
        {
            using (IApiSession apiSession = Matterial.Connect(TestCrendentials.ApiUserName, Matterial.SecureString(TestCrendentials.ApiUserPassword), TestCrendentials.Url))
            {
                //Let's load all work groups
                RoleParams roleParams = new()
                {
                    EntityTypeIds = new List<RoleEntityType> { RoleEntityType.WorkGroup }
                };
                ListResult<Role> workGroups = apiSession.Role.Load(roleParams).Result;

                //Assign all groups to the current user.
                Person currentUser = apiSession.LoggedOnPerson();
                foreach (var role in workGroups.Results)
                {
                    _ = apiSession.Role.Assign(role.Id, currentUser.AccountId).Result;
                }
            }
        }

        private void RemoveAndRestoreDocument()
        {
            using (IApiSession apiSession = Matterial.Connect(TestCrendentials.ApiUserName, Matterial.SecureString(TestCrendentials.ApiUserPassword), TestCrendentials.Url))
            {
                //Let's assume, we have a document with the id 222.
                long docId = 222;
                string languageKey = apiSession.DefaultContentLanguage.ToLangString();

                //We call the remove function. On success it returns a Document,
                //in case the document is locked, it returns a DocumentLock
                DocumentBase result = apiSession.Document.Remove(docId, false).Result;
                if (result is Document)
                {
                    //It worked. Let's restore
                    result = apiSession.Document.Restore(docId).Result;
                    if (result is Document)
                    {
                        //Restore OK
                    }
                }
                else
                {
                    DocumentLock docLock = (DocumentLock)result;
                    Console.WriteLine("The document cannot be removed. It is currently locked by " +
                        docLock.Person.FirstName + " " + docLock.Person.LastName);
                }
            }
        }

        private static void ImportDocuments()
        {
            using (IApiSession apiSession = Matterial.Connect(TestCrendentials.ApiUserName, Matterial.SecureString(TestCrendentials.ApiUserPassword), TestCrendentials.Url))
            {
                //Let's assume, we have files in the folder c:\Import including sub folders that we want to import
                //In case a file is a PDF or a Text file, we want to extract the text and use it as document text

                //Import root folder
                string rootFolder = @"C:\Import";

                //Language
                LangKey languageKey = apiSession.DefaultContentLanguage;

                //Tool to extract text from pdf from https://www.xpdfreader.com/pdftotext-man.html
                string pdfTextExtractTool = @"..\..\..\Tools\pdftotext.exe";

                //Tool arguments
                string pdfToTextArgs = "-eol dos -layout -q -enc UTF-8 -nopgbrk";

                //Load all files from import folder
                List<string> fileList = new(Directory.EnumerateFiles(rootFolder, string.Empty, SearchOption.AllDirectories));

                string textContentFile = "";
                int j = 0;
                //Iterate through the files
                foreach (var file in fileList)
                {
                    j++;
                    //Console.WriteLine($"Importing file {file}...");
                    string docTitle = Path.GetFileNameWithoutExtension(file);
                    string abstractText = "File form the folder " + String.Join(Path.DirectorySeparatorChar, Path.GetDirectoryName(file).Split(Path.DirectorySeparatorChar)[2..]);
                    if (Path.GetExtension(file).ToLower() == ".pdf")
                    {
                        //Extract text from pdf using a little tool
                        Process proc = new();
                        proc.StartInfo.FileName = pdfTextExtractTool;
                        proc.StartInfo.Arguments = "\"" + file + "\" " + pdfToTextArgs;
                        proc.StartInfo.WorkingDirectory = Path.GetDirectoryName(file);
                        proc.Start();
                        proc.WaitForExit(30000);
                        textContentFile = Path.ChangeExtension(file, ".txt");
                    }
                    else
                    {
                        //Extract the text from the file
                        if (Path.GetExtension(file).ToLower() == ".txt")
                        {
                            textContentFile = file;
                        }
                    }
                    //Define the contexct token and lock id
                    //The document should be published without reviewing.
                    DocUpdateParams updateParams = new()
                    {
                        ContextToken = Guid.NewGuid().ToString(),
                        PublishRequestType = PublishRequestTypes.Unreviewed,
                        UniqueLockId = Guid.NewGuid().ToString()
                    };

                    //Upload files: One is the main file (the text) and the other is an attachment.
                    List<UploadFile> uploadFiles = new()
                    {
                       new UploadFile(file, TempFileType.Attachment)
                    };
                    if (File.Exists(textContentFile))
                    {
                        uploadFiles.Add(new UploadFile(textContentFile, TempFileType.Document));
                    }
                    List<TempFileDescriptor> tmpFiles = apiSession.Document.UploadFile(uploadFiles, updateParams.ContextToken, languageKey).Result;

                    //Create new document
                    //Define title, abstract and language
                    Document newDoc = new()
                    {
                        LanguageVersionTitle = docTitle,
                        LanguageVersionAbstract = abstractText,
                        LanguageVersionLanguageKey = languageKey.ToLangString()
                    };

                    //Create it
                    Console.WriteLine($"Importing document {j}/{fileList.Count}...");
                    Document createdDoc = apiSession.Document.Create(newDoc, updateParams).Result;

                    //Unlock it
                    _ = apiSession.Document.Unlock(updateParams.UniqueLockId).Result;
                }
            }
        }

        private static void InvitePerson()
        {
            using (IApiSession apiSession = Matterial.Connect(TestCrendentials.ApiUserName, Matterial.SecureString(TestCrendentials.ApiUserPassword), TestCrendentials.Url))
            {
                //The invite language
                LangKey language = apiSession.DefaultContentLanguage;
                //Create invite credential object
                CredentialWithInvitationText invite = new()
                {
                    SubscriptionEmail = "ferdinand.stapenhorst@gmail.com",
                    InvitationText = "Dear user. You have been invited to matterial.com. Please click the link below."
                };

                //Store it which will send an invite to the email address
                long credentialId = apiSession.Instance.StoreCredentials(invite, language).Result;
            }
        }

        private static void CreatePerson()
        {
            using (IApiSession apiSession = Matterial.Connect(TestCrendentials.ApiUserName, Matterial.SecureString(TestCrendentials.ApiUserPassword), TestCrendentials.Url))
            {
                //The person's contact image
                string imagePath = @"..\..\..\ContactImage1.png";

                //Create new person
                Person newPerson = new()
                {
                    FirstName = "John",
                    LastName = "Dilbert",
                    AccountLogin = "john.dilbert@dilbert.com" //This is required when creating a new one.
                };

                //create the person
                Person createdPerson = apiSession.Person.Create(newPerson).Result;

                //Upload contact image
                bool b = apiSession.Person.UploadContactImage(createdPerson.ContactId, imagePath, true).Result;
            }
        }

        private static void CreateComment()
        {
            using (IApiSession apiSession = Matterial.Connect(TestCrendentials.ApiUserName, Matterial.SecureString(TestCrendentials.ApiUserPassword), TestCrendentials.Url))
            {
                //Let's assume, we have a document with the document language version Id 123
                long docLanguageVersionId = 123;

                Comment newComment = new()
                {
                    Text = "This is the actual comment"
                };

                //Create it.
                long? commentId = apiSession.Comment.Create(docLanguageVersionId, newComment).Result;
            }
        }

        private static void GetCommentOfPerson()
        {
            using (IApiSession apiSession = Matterial.Connect(TestCrendentials.ApiUserName, Matterial.SecureString(TestCrendentials.ApiUserPassword), TestCrendentials.Url))
            {
                //Let's assume, we have a person with the account Id 4
                //We want to load all comments where this person was mentioned
                long accountId = 4;

                //Create the load parameters
                CommentParams options = new()
                {
                    MentionedAccountId = accountId
                };

                //Load the comments
                ListResult<Comment> comments = apiSession.Comment.Load(options).Result;
            }
        }

        private static void UpdatePersonalData()
        {
            //Updates data for current logged-in account.
            //Personal-data includes Addresses, CommunicationData, and ContactImages
            using (IApiSession apiSession = Matterial.Connect(TestCrendentials.ApiUserName, Matterial.SecureString(TestCrendentials.ApiUserPassword), TestCrendentials.Url))
            {
                //Upload a new contact image and set it active
                string imagePath = @"..\..\..\ContactImage1.png";
                bool b = apiSession.UploadContactImage(imagePath).Result;

                //Load the person object again to get the current data
                Person reloadedPerson = apiSession.LoginData(true).Result.Person;

                //A an postal address
                reloadedPerson.Addresses.Add(new Address { City = "Dallas", Street = "1st street" });

                //A an email address
                reloadedPerson.CommunicationData.Add(new CommunicationData { Description = "Test email address", Value = "Test@test.de", EntityTypeId = CommunicationEntityType.Email });

                //Add mobile number
                reloadedPerson.CommunicationData.Add(new CommunicationData { Description = "my private mobile", Value = "212-33445566", EntityTypeId = CommunicationEntityType.Phone });

                //Save
                _ = apiSession.UpdatePersonalData().Result;

                //Update the bio document ( in the web ui it is called Curriculum vitae)
                bool b2 = apiSession.UploadBioDocument("This is the text of my CV", Matterial.DefaultLanguage).Result;
            }
        }

        private static void UpdateAccountSettings()
        {
            //Updates account settings for current logged-in account.
            using (IApiSession apiSession = Matterial.Connect(TestCrendentials.ApiUserName, Matterial.SecureString(TestCrendentials.ApiUserPassword), TestCrendentials.Url))
            {
                //Make sure we have the latest version of account settimngs
                LoginData loginData = apiSession.LoginData(true).Result;

                //Now update some of the values in the dictionary
                const string DesktopNotifyKey = "accountSetting.desktopNotificationEnabled";
                const string ShowHelpOnDashboard = "accountSetting.showHelpSectionDashboard";

                loginData.AccountSettings[DesktopNotifyKey] = true;
                loginData.AccountSettings[ShowHelpOnDashboard] = true;

                //Save the settings
                apiSession.UpdateAccountSettings(loginData.AccountSettings).Wait();
            }
        }

        private static void SearchPerson()
        {
            using (IApiSession apiSession = Matterial.Connect(TestCrendentials.ApiUserName, Matterial.SecureString(TestCrendentials.ApiUserPassword), TestCrendentials.Url))
            {
                //Let's use default options
                SearchPersonParams options = new();

                //Run the search
                SearchResult<Person> results = apiSession.Person.Search("*", options).Result;

                //Display results
                foreach (var item in results.Results)
                {
                    Console.WriteLine(item.Source.FirstName + " " + item.Source.LastName);
                }
            }
        }

        private static void CreateTask()
        {
            using (IApiSession apiSession = Matterial.Connect(TestCrendentials.ApiUserName, Matterial.SecureString(TestCrendentials.ApiUserPassword), TestCrendentials.Url))
            {
                //Let's assume we have a document with the id 6 and the document language version id 20
                //We want to create a task that is assigned to this document
                long documentId = 6;
                long documentLanguageVersionId = 20;

                //The following properties are the ones needed for creation
                Person currentPerson = apiSession.LoggedOnPerson();
                MtrTask newTask = new()
                {
                    Description = "The task description", //The description
                    AssignedRole = currentPerson.RolePersonal, //The role the task will be assigned to
                    TaskStatusId = TaskStatusId.Open, //Status of the task
                    DocumentId = documentId,    //Document Id
                    DocumentLanguageVersionId = documentLanguageVersionId //LanguageVersionId
                };

                //Create the task
                MtrTask createdTask = apiSession.Task.Create(newTask).Result;
            }
        }

        private static void ShowLicenseInfo()
        {
            using (IApiSession apiSession = Matterial.Connect(TestCrendentials.ApiUserName, Matterial.SecureString(TestCrendentials.ApiUserPassword), TestCrendentials.Url))
            {
                License myLicense = apiSession.License.GetLicense().Result;
                Console.WriteLine("Storage: " + myLicense.CasSize);
                Console.WriteLine("Users: " + myLicense.User);
                LicenseUsage usage = apiSession.License.GetUsage().Result;

                Console.WriteLine("Used space: " + usage.CasSize);
                Console.WriteLine("Licensed users: " + usage.User);
                if (!(usage.InstanceActiveUntilInSeconds is null) && usage.InstanceActiveUntilInSeconds > 0)
                {
                    Console.WriteLine("Licensed until: " + Matterial.UnixTimeStampToDateTime(usage.InstanceActiveUntilInSeconds));
                }
            }
        }

        private static void SearchWordAttachments()
        {
            //Attachments supported by MS Word
            //see https://docs.microsoft.com/de-de/deployoffice/compat/office-file-format-reference
            List<string> SupportedExtensions = new() { "txt", "doc", "docx", "dot", "dotx" };

            using (IApiSession apiSession = Matterial.Connect(TestCrendentials.ApiUserName, Matterial.SecureString(TestCrendentials.ApiUserPassword), TestCrendentials.Url))
            {
                //Run a search using a query string.
                string query = "My Document";
                //we have to add the file types as special query syntax
                string FileTypeQueryText = "";
                if (SupportedExtensions.Count > 0)
                {
                    FileTypeQueryText = $" AND attachmentName:({String.Join(" OR ", SupportedExtensions.ToArray())}))";
                }
                //We can set options of how the search should behave.
                SearchDocParams options = new()
                {
                    AllLanguages = true, //We do not filter out languages but use Aggregation (see belos)
                    Offset = 0,
                    Limit = 100
                };

                //Aggregate by language so you can show how many documents of each language are in the search result
                options.Aggregation.Language = true;

                //Run the search
                Console.WriteLine($"Searching...");
                SearchResult<Document> results = apiSession.Document.Search(query + FileTypeQueryText, options).Result;

                //Iterate through the result items to show name and description
                if (results.Results.Count > 0)
                {
                    Console.WriteLine($"Found {results.TotalHits} documents...");

                    //Show language aggregates
                    if (results.Aggregations.Count > 0)
                    {
                        Console.WriteLine("Documents in each language:");
                        foreach (var agg in results.Aggregations)
                        {
                            foreach (var bucket in agg.Buckets)
                            {
                                Console.WriteLine($"  {bucket.Key} : {bucket.Value}");
                            }
                        }
                    }

                    foreach (var item in results.Results)
                    {
                        Console.WriteLine($"Doc name:{item.Source.LanguageVersionTitle}, Description: {item.Source.LanguageVersionAbstract}");
                    }
                }
                else
                {
                    Console.WriteLine("No documents found.");
                }

                Console.WriteLine("Press any key to exit.");
                Console.ReadKey();
            }
        }

        private static void DownloadAttachment()
        {
            //Attachments supported by MS Word
            //see https://docs.microsoft.com/de-de/deployoffice/compat/office-file-format-reference
            List<string> SupportedExtensions = new() { "txt", "doc", "docx", "dot", "dotx" };
            string DownloadFolder = Path.GetTempPath();

            using (IApiSession apiSession = Matterial.Connect(TestCrendentials.ApiUserName, Matterial.SecureString(TestCrendentials.ApiUserPassword), TestCrendentials.Url))
            {
                //Run a search using a query string.
                string query = "My Document";

                //we have to add the file types as special query syntax that filters attachmentName
                string FileTypeQueryText = "";
                if (SupportedExtensions.Count > 0)
                {
                    FileTypeQueryText = $" AND attachmentName:({String.Join(" OR ", SupportedExtensions.ToArray())}))";
                }

                //We can set options of how the search should behave.
                SearchDocParams options = new()
                {
                    AllLanguages = true, //We do not filter out languages but use Aggregation (see belos)
                    Offset = 0,
                    Limit = 100
                };

                //Run the search
                Console.WriteLine($"Searching...");
                SearchResult<Document> results = apiSession.Document.Search(query + FileTypeQueryText, options).Result;

                //Iterate through the result items to show name and description
                if (results.Results.Count > 0)
                {
                    Console.WriteLine($"Found {results.TotalHits} documents...");

                    //We simply use the first one here
                    Console.WriteLine($"Downloading first document");
                    Document doc = results.Results[0].Source;
                    Console.WriteLine($"Doc name:{doc.LanguageVersionTitle}, Description: {doc.LanguageVersionAbstract}");

                    //Load document and attachment
                    LoadDocByIdParams loadParams = new(doc);
                    loadParams.LoadOptions.Attachments = true;
                    doc = apiSession.Document.Load(doc.Id, loadParams).Result;
                    //Does it have attachments
                    if (doc.Attachments.Count > 0)
                    {
                        //Load first attachment ans save it to file
                        AttachmentLoadParams lp = new() { ForceAttachment = true, Format = AttachmentFormats.Original };
                        using (var aStream = apiSession.Document.GetAttachment(doc.Id, doc.Attachments[0].Id, lp).Result)
                        {
                            string fPath = Path.Combine(DownloadFolder, doc.Attachments[0].Name);
                            //Save file
                            if (File.Exists(fPath))
                            {
                                File.Delete(fPath);
                            }
                            SaveStreamToFile(aStream, fPath);
                            //Show with Windows Explorer
                            string cmd = $"/select,\"{fPath}\"";
                            Process.Start("Explorer.exe", cmd);
                        }
                    }
                    else
                    {
                        Console.WriteLine($"No attachments:{doc.Id} - {doc.LanguageVersionTitle}");
                    }
                }
                else
                {
                    Console.WriteLine("No documents found.");
                }
                Console.WriteLine("Press any key to exit.");
                Console.ReadKey();
            }
        }
    }
}